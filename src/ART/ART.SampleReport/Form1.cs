﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ART;
using ART.Doc;

namespace ART
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
            //DoStuff();
			TestC2();
		}

		private void DoStuff()
		{
			//int? score;
			//score = null;
			//189 = Fisheries Training report
			//180 = Striped bass test report
			//227 = Santi Test 2
            int reportId = 208;

			//new ARTService().RecalcAllReports();
			new ARTService().RecalcSingleReport(reportId);

			ARTService service = new ARTService();

			Generator x = new Generator();
			//string str = x.CreateStream(reportId);
			x.CreateDoc(@"c:\temp\test.docx", reportId);
			this.Close();

			//service.ReferenceUpdate(189, -1, "Short Name", "", "", "", "", false);

			/*
			service.Criteria2Overview(reportId);

			IList<Criteria31Entity> a = new List<Criteria31Entity>();
			a = service.Criteria31Overview(reportId);

			IList<Criteria32Entity> b = new List<Criteria32Entity>();
			b = service.Criteria32Overview(reportId);

			IList<Criteria3Entity> c = new List<Criteria3Entity>();
			c = service.Criteria3Overview(reportId);


			IList<Criteria1Entity> w = new List<Criteria1Entity>();
			w = service.Criteria1Overview(reportId);

			IList<Criteria2Entity> x = new List<Criteria2Entity>();
			x = service.Criteria2Overview(reportId);

			IList<Criteria21Entity> y = new List<Criteria21Entity>();
			y = service.Criteria21Overview(reportId);

			IList<Criteria24Entity> z = new List<Criteria24Entity>();
			z = service.Criteria24Overview(reportId);
			*/

			//ART.Reporting.OverallRec rpt = new Reporting.OverallRec();
			//rpt.DataSource = new ART.ReportSource().OverallRec(1);

			//ART.Reporting.Criteria1 rpt = new Reporting.Criteria1();
			//rpt.DataSource = new ReportSource().Criteria1(reportId)[2];

			//ART.Reporting.Criteria2 rpt = new Reporting.Criteria2();
			//rpt.DataSource = new ReportSource().Criteria2(reportId)[2];

			//ART.Reporting.Criteria2 rpt = new Reporting.Criteria2();
			//rpt.DataSource = new ReportSource().Criteria2All(reportId);

			//ART.Reporting.Criteria34 rpt = new Reporting.Criteria34();
			//rpt.DataSource = new ReportSource().Criteria31All(reportId);


			//ART.Reporting.Criteria1Grid rpt = new Reporting.Criteria1Grid();
			//rpt.DataSource = service.Criteria1ReportData(1);
			//rpt.Criteria1Synthesis = service.SynthesisGet(1, 1);

			//ART.Reporting.Criteria2Grid rpt = new Reporting.Criteria2Grid();
			//rpt.DataSource = service.Criteria2ReportData(reportId);
			//rpt.Criteria2Synthesis = service.SynthesisGet(reportId, 2);

			//ART.Reporting.Criteria3Grid rpt = new Reporting.Criteria3Grid();
			//rpt.DataSource = service.Criteria3ReportData(reportId);
			//rpt.DataSource31 = service.Criteria31ReportData(reportId);
			//rpt.DataSource32 = service.Criteria32ReportData(reportId);
			//rpt.Criteria3Synthesis = service.SynthesisGet(reportId, 3);

			//ART.Reporting.Criteria4Grid rpt = new Reporting.Criteria4Grid();
			//rpt.DataSource = service.Criteria4ReportData(180);
			//rpt.Criteria4Synthesis = service.SynthesisGet(180, 4);

			//ART.Reporting.AppendixAGrid rpt = new Reporting.AppendixAGrid();
			//rpt.DataSource = service.AppendixAReportData(1);
			
			//ART.Reporting.AppendixN rpt = new Reporting.AppendixN();
			//rpt.DataSource = service.AppendixGetDataTable(1, "B");


			//ART.Reporting.OverallRec rpt = new Reporting.OverallRec();
			//ART.Reporting.FinalRecGrid rpt = new Reporting.FinalRecGrid();
			//rpt.DataSource = new ART.ReportSource().OverallRec(reportId);

			//ART.Reporting.TextBlock rpt = new Reporting.TextBlock();
			//rpt.DataSource = service.TextBlockGetDataTable(reportId, ARTService.TextBlock.Scope);

			//ART.Reporting.References rpt = new Reporting.References();
			//rpt.DataSource = new ART.ReportSource().References(reportId);

			//ART.Reporting.FullReport rpt = new Reporting.FullReport();
			//rpt.DataSources = new ReportSource().FullReport(reportId);


			//rpt.Run();
			//this.viewer1.Document = rpt.Document;
		}

		private void TestC2()
		{
			AqC2Entity e = new AqC2Entity();
			e.RecommendationId = 1;
			e.ProteinContentFeed = 20;
			e.eFCR = 1.5M;
			e.FertilizerNInput = 0;
			e.ProteinContentFish = 18;

			e.BasicDischargeScore = 1;
			e.BasicDischargeAdjustment = -0.1M;

			e.Q22a1 = 0.5M;
			e.Q22a2 = 0.5M;
			e.Q22a3 = 0.5M;
			e.Q22a4 = 0.5M;
			e.Q22a5 = 0.5M;

			e.Q22b1 = 0.5M;
			e.Q22b2 = 0.5M;
			e.Q22b3 = 0.5M;
			e.Q22b4 = 0.5M;
			e.Q22b5 = 0.5M;

			e.Recalculate();
			e.Commit();

			AqC2Entity e2 = AqC2Entity.Load(1);
		}
	}
}
