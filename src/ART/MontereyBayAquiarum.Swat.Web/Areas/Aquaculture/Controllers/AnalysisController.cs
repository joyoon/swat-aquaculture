﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ART;
using MontereyBayAquarium.Swat.Web.Models;

namespace MontereyBayAquarium.Swat.Web.Areas.Aquaculture.Controllers
{
    [Authorize]
    public class AnalysisController : MontereyBayAquarium.Swat.Web.Controllers.ControllerBase
    {
        #region Criteria2
        [HttpGet]
        public ViewResult Criteria2(int id)
        {
            AqC2Entity C2Entity = AqC2Entity.Load(id);
            return View("Criteria2");
        }
        #endregion

        public ActionResult AnalysisHeader(int id, AuthenticatedUser user)
        {
            //if (HttpContext.Cache["Report"] == null) 
            HttpContext.Session["Report"] = Service.LoadReport(id);
            ViewBag.IsSFWStaff = user.UserEntity.IsSFWStaff;
            ReportEntity model = (ReportEntity)HttpContext.Session["Report"];
            return View("_AnalysisHeader", model);
        }
    }
}
