﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MontereyBayAquarium.Swat.Web.Areas.Aquaculture
{
    public class AquacultureAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Aquaculture"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               name: "Aquaculture_default",
               url: "aquaculture/{controller}/{action}/{id}",
               defaults: new { action = "Index", controller = "Analysis", id = UrlParameter.Optional });
        }
    }
}