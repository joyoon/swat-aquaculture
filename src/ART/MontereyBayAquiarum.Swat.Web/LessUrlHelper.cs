﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;


namespace MontereyBayAquarium.Swat.Web
{
    public  static class LessUrlHelper
    {
        public static HtmlString LessCss(this UrlHelper helper, string lessJsPath, params string[] cssFiles) {
            var html = new StringBuilder();

            var useClientside = false;
            var lessLinkTagFormat = @"<link rel=""stylesheet/less"" type=""text/css"" href=""{0}"">";
            var cssLinkTagFormat = @"<link rel=""stylesheet"" type=""text/css"" href=""{0}"">";
#if DEBUG
            useClientside = true;
#endif

            

            foreach (var css in cssFiles)
            {
                if (!css.EndsWith(".less"))
                    html.AppendFormat(cssLinkTagFormat, helper.Content(css));
                else if( useClientside )
                    html.AppendFormat(lessLinkTagFormat, helper.Content(css));
                else
                    html.AppendFormat(cssLinkTagFormat, helper.Content(TranslateToCss(css)));
            }

            if (useClientside)
                html.AppendFormat(@"<script src=""{0}"" type=""text/javascript""></script>", helper.Content(lessJsPath));

            return new HtmlString(html.ToString());
        }

        private static string TranslateToCss(string css)
        {
            if (!css.EndsWith(".less")) return css;

            return css.Replace(".less", ".css");
        }

    }
}