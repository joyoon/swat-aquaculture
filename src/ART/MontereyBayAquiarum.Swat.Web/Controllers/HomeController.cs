﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.Models;
using System.Web.Security;
using System.Net;


namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class HomeController : ControllerBase
    {

        public ViewResult Index()
        {
            return View("Index");
        }

        #region Reports
        public ViewResult Reports(string search, AuthenticatedUser user)
        {
            if (search == null || search == "")
            {
                return View("Reports", null);
            }
            ReportsViewModel model = new ReportsViewModel() 
            { 
                RecSummaries = PublicService.GetRecSummaries(search)
            };
            ViewBag.specie = search;
            return View("Reports", model);
        }
        #endregion

        #region ContactForm
        [HttpGet]
        public ViewResult ContactForm(int id)
        {
            //TODO: Replace with a service call to pull a single report
            RecSummaryEntity model = PublicService.GetRecSummary(id);
            //ReportEntity model = Service.LoadReport(id);
            return View("ContactForm", model);
        }

        [HttpPost]
        public ActionResult ContactForm(int id, string firstname, string lastname, string affiliation, string title, string email, 
            string phone, string comments, HttpPostedFileBase[] files)
        {
            int feedbackId = PublicService.FeedbackSubmit(id, firstname, lastname, affiliation, title, email, phone, comments);
            // Retrieve storage account from connection string.

            foreach(HttpPostedFileBase file in files){
                if (file != null)
                {
                    var finalPath = Helpers.SaveFile(file, Server, "contact/"+id.ToString()+"/");
                    PublicService.SubmitFeedbackAttachment(feedbackId, finalPath);
                }
            }
            
            ReportEntity model = Service.LoadReport(id);
            //TODO: Display a success message
            //return View("ContactForm", model);
            return new RedirectResult("success");
        }

        public ViewResult ContactFormSuccess()
        {
            return View("ContactFormSuccess");
        }

        public ViewResult Help()
        {
            return View("Help");
        }

        #endregion

        #region Login
        [HttpGet]
        public ViewResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(string username, string password, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (ModelState.IsValid)
            {
                UserEntity user = Service.Login(username, password);

                if (user == null)
                {
                    ModelState.AddModelError("Login", "Your username and/or password is invalid");
                    return View("Login");
                }
                else
                {
                    SetAuthenticationCookie(user.Id);
                    return RedirectToAction("Index", "Dashboard");
                }
            } else
                  return View("Login");
        }
        #endregion

        [HttpGet]
        public ActionResult Logout()
        {
            ControllerContext.HttpContext.Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ViewResult ScoringLegend()
        {
            return View("ScoringLegend");
        }

        public ActionResult ExtendTimeout()
        {
            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
        }

        #region Private Methods
        public virtual void SetAuthenticationCookie(int id)
        {
            FormsAuthentication.SetAuthCookie(id.ToString(), false);
        }
        #endregion
    }
}
