﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.ActionResults;



namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class ImageController : ControllerBase
    {

        #region Index

        public ViewResult Index(int id, string keywords)
        {
            string Keywords = "";
            if (keywords != null) { Keywords = keywords; }
            IList<ImageEntity> model = new List<ImageEntity>();
            model = Service.ImageList(id, Keywords);
            return View("Index", model);
        }
        #endregion

        #region Edit
        [HttpGet]
        public ViewResult Edit(int id, int imageId)
        {
            ImageEntity model = Service.ImageGet(id, imageId);
            return View("Edit", model);
        }
        
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, ImageEntity image, HttpPostedFileBase file)
        {
            string[] mediaExtensions = {
                ".PNG", ".JPG", ".JPEG", ".BMP", ".GIF"
            };

            if (image.ImageId > 0)
            {
                if (file == null)
                {
                    Service.ImageUpdate(image.ReportId, image.ImageId, image.Name, image.Description, image.Path);
                }
                else
                {
                    string path = Helpers.SaveFile(file, Server, "images/"+image.ReportId.ToString() + "/");
                    if (mediaExtensions.Contains(Path.GetExtension(path).ToUpperInvariant()))
                    {
                        Service.ImageUpdate(image.ReportId, image.ImageId, image.Name, image.Description, path);
                    }
                    else
                    {
                        TempData["error"] = "Please upload an image file of type PNG, JPG, or GIF";
                        return RedirectToAction("Edit", new { id = id, imageId = image.ImageId});
                    }
                }
                
            }
            else
            {
                string path = Helpers.SaveFile(file, Server, "images/" + image.ReportId.ToString() + "/");
                if (mediaExtensions.Contains(Path.GetExtension(path).ToUpperInvariant()))
                {
                    Service.ImageAdd(id, image.Name, image.Description, path);
                }
                else
                {
                    TempData["error"] = "Please upload an image file of type PNG, JPG, or GIF";
                    return RedirectToAction("Edit", new { id = id, imageId = image.ImageId });
                }
            }
			return RedirectToAction("Index");
        }
        #endregion

        [HttpPost]
        public JsonNetResult Delete(int id, int imageId)
        {
            Service.ImageDelete(id, imageId);
            return new JsonNetResult(new {success = true});
        }
        [HttpPost]
        public ActionResult Search(int id, string filter, string context)
        {
            ViewBag.Context = context;
            IList<ImageEntity> model = new List<ImageEntity>();
            model = Service.ImageList(id, filter ?? String.Empty);
            return View("_Images", model);
        }

        [HttpGet]
        public ViewResult Search(int id)
        {
            ViewBag.Context = "Modal";
            IList<ImageEntity> model = new List<ImageEntity>();
            model = Service.ImageList(id, String.Empty);
            return View("_Images", model);
        }

    }
}
