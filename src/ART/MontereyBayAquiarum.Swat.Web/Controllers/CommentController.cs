﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.Models;
using MontereyBayAquarium.Swat.Web.Models.Shared;
using MontereyBayAquarium.Swat.Web.ActionResults;
using System.Net;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class CommentController : ControllerBase
    {
        [HttpGet]
        public ActionResult Index(int answerId, AuthenticatedUser user)
        {
            //The controller gets all the comments
            //The comments are in order
            //I might want to pass in two variables, comment and subcomments
            //Then we need a new view for subcomments inside the comment
            IList<FactorCommentEntity> comments = Service.FactorCommentGet(answerId);
            CommentsViewModel model = new CommentsViewModel()
            {
                Comments = comments,
                User = user.UserEntity
            };
            return PartialView("_Comments", model);
        }

        [HttpPost, ActionName("Index")]
        public ActionResult Create(int id, int answerId, string description, string parentId, AuthenticatedUser user, HttpPostedFileBase file)
        {
            int pid = Convert.ToInt16(parentId);
            bool isRoot;
            if (parentId == "0")
            {
                isRoot = true;
            }
            else
            {
                isRoot = false;
            }
            string finalPath = Helpers.SaveFile(file, Server, "comments/"+id.ToString()+"/");
            int newCommentId = Service.FactorCommentAdd(id, user.UserEntity, answerId, description, pid, finalPath); 
            FactorCommentEntity comment = new FactorCommentEntity()
            {
                FactorCommentId = newCommentId,
                FactorId = answerId,
                Comment = description,
                DateCreated = System.DateTime.Now,
                UserId = user.UserEntity.Id,
                User = user.UserEntity,
                Path = finalPath
            };
            CommentViewModel model = new CommentViewModel()
            {
                Comment = comment,
                User = user.UserEntity,
                IsRoot = isRoot
            };
            return View("Comment", model);
        }

        [HttpPost]
        public ActionResult Edit(int id, int commentId, string description, HttpPostedFileBase file, string fileModified = "0", string originalPath = "")
        {
            string path;
            if (fileModified != "0") //modified
            {
                path = Helpers.SaveFile(file, Server, "comments/"+id.ToString()+"/"); //returns empty string if file is null/no file uploaded
            }
            else
            {
                path = originalPath;
            }
            Service.FactorCommentUpdate(id, commentId, description, path);
            CommentViewModel model = new CommentViewModel
            {
                NewPath = path
            }; //sharing the same view, passing in same model
            return View("Comment", model);
        }

        public ActionResult Blank()
        {
            return View("Blank");
        }

        [HttpPost]
        public void UpdateStatus(int reportId, int commentId, bool isReviewed)
        {
            Service.FactorCommentUpdateStatus(reportId, commentId, isReviewed);
        }


    }
}
