﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    [Authorize]
    public class ExpertInputController : ControllerBase
    {
        [HttpGet]
        public ViewResult Index(int id)
        {
            IList<FeedbackEntity> model = PublicService.FeedbackGet(id);
            return View("Index", model);
        }

        [HttpGet]
        public ViewResult Details(int id, int expertInputId)
        {
            FeedbackEntity model = PublicService.LoadFeedback(expertInputId);
            model.Documents = PublicService.GetFeedbackAttachments(expertInputId);
            return View("View", model);
        }

    }
}
