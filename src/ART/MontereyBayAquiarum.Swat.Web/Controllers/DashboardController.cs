﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.Models;
using System.Net;
using System.Net.Mail;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    [Authorize]
    public class DashboardController : ControllerBase
    {

        public ViewResult Index(AuthenticatedUser user)
        {
            ReportsViewModel model = new ReportsViewModel()
            {
                Reports = Service.ReportList(user.UserEntity, ""),
                User = user.UserEntity
            };
            return View("Index", model);
        }

        public ActionResult CompleteReview(int id, AuthenticatedUser user)
        {
            
            //Service.UpdateReportStatus(id, 5); //5 = Internal Review 2
            ReportEntity report = Service.LoadReport(id);
            HttpContext.Session["Report"] = report;
            string subject = "Report review complete";
            string body = user.DisplayName + "has completed review of the following report:\n\n" + report.Title + "\n\n This report is now ready for Internal Review 2.";
            Helpers.SendMail(subject, body);
            TempData["Completed"] = true;
            Service.ReviewerReviewed(id, user.Id); 
            return RedirectToAction("Index");
        }

        public ActionResult UncompleteReview(int id, AuthenticatedUser user)
        {
            Service.ReviewerUnReviewed(id, user.Id);
            return RedirectToAction("Index");
        }

    }
}
