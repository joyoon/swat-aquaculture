﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.Models.Answer;
using MontereyBayAquarium.Swat.Web.ActionResults;
using MontereyBayAquarium.Swat.Web.Models.Shared;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class AnswerController : ControllerBase
    {
        #region Answer List
        [HttpGet]
        public ViewResult Index(int id)
        {
            return View("Index");
        }
        #endregion

        #region Answer Edit
        [HttpGet]
        public ViewResult Edit(int id, int answerId)
        {
            return View("Edit");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(int id, int answerId, int factorTypeId, int scoreId, string keyInfo, string rationale, string title = "")
        {
            FactorEntity temp = null;
            if (answerId > 0)
            {
                temp = Service.FactorUpdate(id, answerId, factorTypeId, scoreId, title, keyInfo, rationale);
            }
            else
            {
                temp = Service.FactorAdd(id, factorTypeId, scoreId, title, keyInfo, rationale);
                answerId = temp.Id;
            }
            string label = temp.Label;
            return RedirectToAction("Index", new { id = id });
        }

        #endregion

        [HttpPost]
        #region Answer Delete
        public JsonNetResult Delete(int id, int answerId)
        {
            FactorEntity factor = Service.FactorGet(answerId);
            Service.FactorDelete(id, answerId, factor.FactorTypeId);

            return new JsonNetResult(new { FactorId = answerId, Success = true });
        }
        #endregion

        #region Child Actions & Actions Returning Partial Views
        [HttpGet]
        public PartialViewResult AnswerDetail(int id, int answerId, string callingPage)
        {
            FactorEntity model = Service.FactorGet(answerId);
            ViewBag.CallingPage = callingPage;
            return PartialView("_AnswerDetail", model);
        }

        /// <summary>
        /// Get all answers for a specific factory type, as opposed to the index method, which gets all answers in general
        /// </summary>
        /// <param name="id"></param>
        /// <param name="factorTypeId"></param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult AnswerList(int id, int factorTypeId)
        {
            IList<RowsourceEntity> FactorTypes = Service.FactorType();
            AnswerListViewModel model = new AnswerListViewModel()
            {
                Answers = Service.FactorGet(id, factorTypeId,""), //TODO IMplement filter
                FactorTypes = Service.FactorType(),
                CurrentFactorTypeId = factorTypeId
            };
            //FactorTypeEntity factorType = null;
            //if (factorTypeId > 0) factorType = Service.FactorTypeGet(factorTypeId);
            //model.FactorType = factorType;

            return PartialView("_AnswerList", model);
        }

        [HttpPost]
        public PartialViewResult AnswerList(int id, int factorTypeId, string filter)
        {
            IList<RowsourceEntity> FactorTypes = Service.FactorType();
            AnswerListViewModel model = new AnswerListViewModel()
            {
                Answers = Service.FactorGet(id, factorTypeId, filter),
                FactorTypes = Service.FactorType(),
                CurrentFactorTypeId = factorTypeId
            };
            return PartialView("_AnswerList", model);
        }

        [HttpGet]
        public PartialViewResult EditAnswer(int id, int answerId, int factorTypeId, string context = null) //We need the factortype in the case of adding a new factor--need to know which factor types to display in the drop down list on the form
        {
            if (context == "criteria2")
            {
                TempData["context"] = context;
            }
            EditAnswerViewModel model = new EditAnswerViewModel()
            {
                Factor = (answerId > 0) ? Service.FactorGet(answerId) : new FactorEntity(),
            };
            if (answerId > 0)
            {
                factorTypeId = model.Factor.FactorTypeId;
            }
            IList<RowsourceEntity> factorTypes = Service.FactorType();
            IList<RowsourceEntity> scores = null;
            if (factorTypeId > 0)
            {
                var factorType = factorTypes.Where(x => x.Key == factorTypeId).FirstOrDefault();
                factorTypes = new List<RowsourceEntity>();
                factorTypes.Add(factorType);
                scores = Service.FactorScore(model.Factor.FactorTypeId);
            }
            model.FactorTypes = factorTypes.OrderBy(x => x.Value).ToList();
            model.Scores = scores;
            model.Factor.FactorTypeId = factorTypeId;
            return PartialView("_EditAnswer", model);
        }

        [HttpPost, ValidateInput(false)]
        public JsonNetResult EditAnswer(int id, int answerId, string title, int factorTypeId, int scoreId, string keyInfo, string rationale)
        {
            FactorEntity temp = null;
            if (answerId > 0)
            {
                temp = Service.FactorUpdate(id, answerId, factorTypeId, scoreId, title, keyInfo, rationale);
            }
            else
            {
                temp = Service.FactorAdd(id, factorTypeId, scoreId, title, keyInfo, rationale);
                answerId = temp.Id;
            }
            string label = temp.Label;
            return new JsonNetResult(new { FactorId = answerId, Success = true, Label = label, Title = title });
        }

        [HttpPost]
        public JsonNetResult Select(int id, int answerId, int recommendationId, bool applyAll = false)
        {
			Service.FactorAssociate(id, recommendationId, answerId, applyAll); //BB hard coded false here, you'll want to bind to a checkbox ***********************
            FactorEntity entity = Service.FactorGet(answerId);
            return new JsonNetResult(entity);
        }

        [HttpPost]
        public JsonNetResult SelectForMultipleRecs(int id, int answerId, int locationId, int bodyOfWaterId, int methodId, int factorTypeId)
        {
            Service.FactorAssociate(id, locationId, bodyOfWaterId, methodId, answerId, factorTypeId); 
            FactorEntity entity = Service.FactorGet(answerId);
            return new JsonNetResult(entity);
        }

        [HttpGet]
        public JsonNetResult Scores(int factorTypeId)
        {
            IList<RowsourceEntity> scores = Service.FactorScore(factorTypeId);
            return new JsonNetResult(scores);
        }
        #endregion

    }
}
