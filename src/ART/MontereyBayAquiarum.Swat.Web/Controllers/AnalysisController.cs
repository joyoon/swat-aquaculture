﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ART;
using MontereyBayAquarium.Swat.Web.Models.Analysis;
using MontereyBayAquarium.Swat.Web.ActionResults;
using MontereyBayAquarium.Swat.Web.Models;
using System.Net;
using System.Net.Mail;
using ART.Reporting;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    [Authorize]
    public class AnalysisController : ControllerBase
    {

        #region Define Scope
        [HttpGet]
        public ViewResult DefineScope(int id)
        {
            ReportEntity report = new ReportEntity(id);
            DefineScopeViewModel model = new DefineScopeViewModel()
            {
                Recommendations = Service.RecsByReport(report),
                RecommendationTypes = Service.RecommendationType()
            };

            return View("DefineScope", model);
        }

        [HttpPost]
        public JsonNetResult SaveScope(int id, int recommendationId, int speciesId, int? locationId, int? bodyOfWaterId, int methodId, int typeId)
        {
            int result = 0;
            bool success = true;
            if (locationId == null)
            {
                locationId = 0;
            }
            if (bodyOfWaterId == null)
            {
                bodyOfWaterId = 0;
            }

            try
            {
                if (recommendationId == 0)
                    result = Service.RecommendationCreate(id, speciesId, (int)locationId, (int)bodyOfWaterId, methodId, typeId);
                else
                    result = Service.RecommendationUpdate(id, recommendationId, speciesId, (int)locationId, (int)bodyOfWaterId, methodId, typeId);
            }
            catch
            {
                result = 0;
            }
            if (result == 0)
            {
                success = false;
            }
            else
            {
                success = true;
            }
            return new JsonNetResult(new { Success = success, recId = result });
        }

        [HttpPost]
        public JsonNetResult UpdateRecommendation(int id, int recommendationId, int typeId)
        {
            //5 for no, 4 for yes
            int result = Service.RecommendationUpdateType(id, recommendationId, typeId);
            bool success;
            if (result == 0)
                success = false;
            else
                success = true;
            return new JsonNetResult(new { Success = success, typeId = typeId });
        }

        [HttpPost]
        public JsonNetResult DeleteScope(int id, int recommendationId)
        {
            bool result = Service.RecommendationDelete(id, recommendationId);
            return new JsonNetResult(new { Success = result });
        }

        #endregion

        #region StockHealth
        [HttpGet]
        public ViewResult StockHealth(int id)
        {
            IList<Criteria1Entity> model = Service.StockHealthOverview(id).OrderBy(x => x.CommonName).ToList();
            //Todo: need to pass in title/label of Factor, for now it's hardcoded in
            return View("StockHealth", model);
        }
        #endregion

        #region Criteria1
        [HttpGet]
        public ViewResult Criteria1(int id)
        {
            Criteria1ViewModel model = new Criteria1ViewModel()
            {
                Criteria = Service.Criteria1Overview(id).OrderBy(x => x.CommonName).ToList(),
                Synthesis = Service.SynthesisGet(id, 1)
            };
            return View("Criteria1", model);
        }
        #endregion

        #region Criteria2
        [HttpGet]
        public ViewResult Criteria2(int id)
        {
            IList<Criteria2Display> criteria = Helpers.BuildCriteria2List(Service.Criteria2Overview(id));

            Criteria2ViewModel model = new Criteria2ViewModel()
            {
                Criteria = criteria,
                Criteria21 = Service.Criteria21Overview(id).OrderBy(x => x.CommonName).ToList(), //TODO: Replace with the correct service method call when that's done
                Criteria24 = Service.Criteria24Overview(id).OrderBy(x => x.Location).ToList(),
                Synthesis = Service.SynthesisGet(id, 2)
            };
            return View("Criteria2", model);
        }
        #endregion

        #region Criteria3
        [HttpGet]
        public ViewResult Criteria3(int id)
        {
            Criteria3Display criteria = new Criteria3Display()
            {
                SummaryCriteria = Service.Criteria3Overview(id),
                Criteria31 = Service.Criteria31Overview(id),
                Criteria32 = Service.Criteria32Overview(id)
            };
            Criteria3ViewModel model = new Criteria3ViewModel()
            {
                Synthesis1 = Service.SynthesisGet(id, 31),
                Synthesis2 = Service.SynthesisGet(id, 32),
                Synthesis3 = Service.SynthesisGet(id, 33),
                Criteria = criteria
            };
            return View("Criteria3", model);
        }
        #endregion

        #region Criteria4
        [HttpGet]
        public ViewResult Criteria4(int id)
        {
            Criteria4ViewModel model = new Criteria4ViewModel()
            {
                Criteria = Service.Criteria4Overview(id),
                Synthesis = Service.SynthesisGet(id, 4)
            };
            return View("Criteria4", model);
        }
        #endregion

        #region ExecutiveSummary
        [HttpGet]
        public ViewResult ExecutiveSummary(int id)
        {
            ViewBag.ExecutiveSummary = Service.TextBlockGet(id, ARTService.TextBlock.ExecSummary);
            return View("ExecutiveSummary");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult ExecutiveSummary(int id, string executiveSummary, string nextAction)
        {
            //TODO: Client side form validation in javascript
            Service.TextBlockSet(id, ARTService.TextBlock.ExecSummary, executiveSummary);
            ViewBag.ExecutiveSummary = executiveSummary;
            return RedirectToAction(nextAction);
        }
        #endregion

        #region Introduction
        [HttpGet]
        public ViewResult Introduction(int id)
        {
            ViewBag.IntroductionScope = Service.TextBlockGet(id, ARTService.TextBlock.Scope);
            ViewBag.IntroductionSpeciesOverview = Service.TextBlockGet(id, ARTService.TextBlock.SpeciesOverview);
            ViewBag.ProductionStatistics = Service.TextBlockGet(id, ARTService.TextBlock.ProductionStatistics);
            ViewBag.ImportanceToMarket = Service.TextBlockGet(id, ARTService.TextBlock.ImportanceToMarket);
            ViewBag.CommonMarketNames = Service.TextBlockGet(id, ARTService.TextBlock.CommonMarketNames);
            ViewBag.ProductForms = Service.TextBlockGet(id, ARTService.TextBlock.ProductForms);
            return View("Introduction");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Introduction(int id, string introductionScope, string introductionSpeciesOverView, string productionStatistics, string importanceToMarket, string commonMarketNames, string productForms, string nextAction)
        {
            Service.TextBlockSet(id, ARTService.TextBlock.Scope, introductionScope);
            Service.TextBlockSet(id, ARTService.TextBlock.SpeciesOverview, introductionSpeciesOverView);
            Service.TextBlockSet(id, ARTService.TextBlock.ProductionStatistics, productionStatistics);
            Service.TextBlockSet(id, ARTService.TextBlock.ImportanceToMarket, importanceToMarket);
            Service.TextBlockSet(id, ARTService.TextBlock.CommonMarketNames, commonMarketNames);
            Service.TextBlockSet(id, ARTService.TextBlock.ProductForms, productForms);
            return RedirectToAction(nextAction);
        }
        #endregion

        #region Acknowledgements

        [HttpGet]
        public ViewResult Acknowledgements(int id)
        {
            ViewBag.Acknowledgements = Service.TextBlockGet(id, ARTService.TextBlock.Acknowledgements);
            return View("Acknowledgements");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Acknowledgements(int id, string acknowledgements, string nextAction)
        {
            Service.TextBlockSet(id, ARTService.TextBlock.Acknowledgements, acknowledgements);
            return RedirectToAction(nextAction);
        }
        #endregion

        #region SupplementalInfo

        [HttpGet]
        public ViewResult SupplementalInfo(int id)
        {
            @ViewBag.SupplementalInfo = Service.TextBlockGet(id, ARTService.TextBlock.SupplementalInfo);
            return View("SupplementalInfo");
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SupplementalInfo(int id, string supplementalInfo, string nextAction)
        {
            Service.TextBlockSet(id, ARTService.TextBlock.SupplementalInfo, supplementalInfo);
            return RedirectToAction(nextAction);
        }
        #endregion

        #region Appendix
        [HttpGet]
        public ViewResult Appendix(int id)
        {

            AppendixViewModel model = new AppendixViewModel();
            model.Appendices = Service.AppendixesGet(id);
            model.ReviewSchedule = Service.TextBlockGet(id, ARTService.TextBlock.ReviewSchedule);
            return View("Appendix", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Appendix(int id, string[] appendixTitles, string[] appendixDetails, int[] appendixId, string reviewSchedule, string nextAction)
        {  // should always return atleast one empty instance of an array
            //var newList = appendixTitles.ToList();
            for (var i = 0; i < appendixTitles.Length; i++)
            {
                if (appendixDetails[i].Length == 0 && appendixTitles[i].Length == 0)
                {
                    if(appendixId[i] > 0)
                        Service.AppendixRemove(id, appendixId[i]);
                    continue;
                }
                if (appendixId[i] > 0)
                {
                    Service.AppendixUpdate(id, appendixId[i], appendixTitles[i], appendixDetails[i]);
                }
                else //new appendix
                {
                    Service.AppendixAdd(id, appendixTitles[i], appendixDetails[i]);
                }
            }
            Service.TextBlockSet(id, ARTService.TextBlock.ReviewSchedule, reviewSchedule);
            return RedirectToAction(nextAction);
        }
        #endregion

        #region DraftOverallRecommendations

        [HttpGet]
        public ViewResult DraftOverallRecommendations(int id)
        {
            FinalRecGrid report = new FinalRecGrid();
            report.DataSource = new ReportSource().OverallRec(id);
            ViewBag.Report = report;
            ViewBag.TextSummary = Service.TextBlockGet(id, ARTService.TextBlock.TextSummary);
            ViewBag.EcoCertification = Service.TextBlockGet(id, ARTService.TextBlock.EcoCertification);
            return View("DraftOverallRecommendations");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DraftOverallRecommendations(int id, string textSummary, string ecoCertification, string nextAction)
        {
            Service.TextBlockSet(id, ARTService.TextBlock.TextSummary, textSummary);
            Service.TextBlockSet(id, ARTService.TextBlock.EcoCertification, ecoCertification);
            return RedirectToAction(nextAction);
        }

        #endregion 

        public JsonNetResult ChangeReportStatus(int id, int status)
        {
            /*public enum ReportStatus
		        {
			        InProgress = 1,
			        InternalReview1 = 3, 
			        ExternalPeerReview = 4,
			        InternalReview2 = 5,
			        Finalization = 6, 
			        Complete = 7
		        }*/
            Service.UpdateReportStatus(id, status);
            HttpContext.Session["Report"] = Service.LoadReport(id);
            return new JsonNetResult(true);
        }

        #region Preview
        [HttpGet]
        public ViewResult Preview(int id)
        {
            return View("Preview");
        }

        public ActionResult Export(int id)
        {
            FullReport report = new FullReport();
            string name = Service.LoadReport(id).Title;
            report.DataSources = new ReportSource().FullReport(id);
            report.Run();
            MemoryStream reportStream = new MemoryStream();
           
            DataDynamics.ActiveReports.Export.Pdf.PdfExport export = new DataDynamics.ActiveReports.Export.Pdf.PdfExport();
                export.Export(report.Document, reportStream);
            return File(reportStream.GetBuffer(), "application/pdf", name+".pdf");
            /*return BinaryResult(reportStream.GetBuffer(),
                "application/pdf", true, "SomeCoolReport.pdf");*/
        }
        #endregion
        public ViewResult Legend()
        {
            return View();
        }

        #region Autocomplete
        public JsonNetResult AutoComplete(string item, string keyword)
        {
            IList<RowsourceEntity> results = null;
            if (item == "species")
                results = Service.SpeciesAutocomplete(keyword);
            else if (item == "location")
                results = Service.LocationAutocomplete(keyword);
            else if (item == "bodyOfWater")
                results = Service.BodyOfWaterAutocomplete(keyword);
            else if (item == "method")
                results = Service.MethodAutocomplete(keyword);

            return new JsonNetResult(results);
        }
        #endregion

        #region Child Actions & Actions Returning Partial Views
        [ChildActionOnly]
        public ActionResult AnalysisHeader(int id, AuthenticatedUser user)
        {
            //if (HttpContext.Cache["Report"] == null) 
            HttpContext.Session["Report"] = Service.LoadReport(id);
            ViewBag.IsSFWStaff = user.UserEntity.IsSFWStaff;
            ReportEntity model = (ReportEntity)HttpContext.Session["Report"];
            return View("_AnalysisHeader", model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveSynthesis(int id, int criteriaId, string synthesis, string nextAction)
        {
            Service.SynthesisSet(id, criteriaId, synthesis);
            return RedirectToAction(nextAction);
        }

        [HttpPost]
        public ActionResult CreateCombinations(int id, int[] species, int[] locations, int[] bodiesOfWater, int[] methods)
        {
            //recommendationTypeId is set to 5
            //default to no for bulk add
            int recommendationTypeId = 5;
            if (locations == null)
            {
                locations = new int[1];
                locations[0] = 0;
            }
            if (bodiesOfWater == null)
            {
                bodiesOfWater = new int[1];
                bodiesOfWater[0] = 0;
            }
            bool success;
            try
            {
                success = Service.RecommendationCreateCombination(id, species, locations, bodiesOfWater, methods, recommendationTypeId);
            }
            catch
            {
                success = false;
            }
            return new JsonNetResult(new { success = success });
        }

        [HttpGet]
        public ActionResult StockAnalysis(int id, string context)
        {
            IList<Criteria1Entity> model = new List<Criteria1Entity>();
            if (context == "stock-health")
                model = Service.StockHealthOverview(id).OrderBy(x => x.CommonName).ToList();
            else if (context == "criteria1")
                model = Service.Criteria1Overview(id).OrderBy(x => x.CommonName).ToList();
            else if (context == "criteria2")
            {
                IList<Criteria2Display> criteria = Helpers.BuildCriteria2List(Service.Criteria2Overview(id));

                Criteria2ViewModel c2Model = new Criteria2ViewModel()
                {
                    Criteria = criteria,
                    Criteria21 = Service.Criteria21Overview(id).OrderBy(x => x.CommonName).ToList(), //TODO: Replace with the correct service method call when that's done
                    Criteria24 = Service.Criteria24Overview(id).OrderBy(x => x.Location).ToList()
                };
                return PartialView("_Criteria2", c2Model);
            }
            return PartialView("_StockAnalysis", model);
        }

        [HttpGet]
        public ActionResult Criteria2Data(int id)
        {
            IList<Criteria2Display> model = Helpers.BuildCriteria2List(Service.Criteria2Overview(id));
            return PartialView("_Criteria2", model);
        }

        [HttpGet]
        public ActionResult Criteria3Data(int id)
        {
            Criteria3Display model = new Criteria3Display()
            {
                SummaryCriteria = Service.Criteria3Overview(id),
                Criteria31 = Service.Criteria31Overview(id),
                Criteria32 = Service.Criteria32Overview(id)
            };
            return PartialView("_RegionMethodAnalysis", model);
        }

        [HttpGet]
        public ActionResult Criteria4Data(int id)
        {
            IList<Criteria4Entity> model = Service.Criteria4Overview(id);
            return PartialView("_Criteria4", model);
        }
        #endregion

        [HttpGet]
        public ActionResult SubmitReport(int id, AuthenticatedUser user)
        {
            Service.UpdateReportStatus(id, 3);
            ReportEntity report = Service.LoadReport(id);
            HttpContext.Session["Report"] = report;
            string subject = "Report analysis ready for review";
            string body = user.DisplayName + "has completed analysis of the following report:\n\n" + report.Title + "\n\n This report is now ready for Internal Review 1.";
            Helpers.SendMail(subject, body);
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        public ActionResult UpdatePageSize(int size)
        {
            HttpContext.Session["pageSize"] = size;
            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult GuidingPrinciple(string title, string key)
        {
            GuidingPrincipleViewModel model = new GuidingPrincipleViewModel()
            {
                Title = title,
                Key = key
            };
            return PartialView("_GuidingPrinciple", model);
        }

        [HttpGet]
        public ActionResult FactorInstruction(string title, int factorTypeId)
        {
            GuidingPrincipleViewModel model = new GuidingPrincipleViewModel()
            {
                Title = title,
                Key = LookUpFactorInstruction(factorTypeId)
            };
            return PartialView("_GuidingPrinciple", model);
        }

        #region Private Methods 

        public string LookUpFactorInstruction(int factorTypeId)
        {
            Dictionary<int, string> d = null;
            d = new Dictionary<int, string>()
            {
                {1, "Factor11"},
                {2, "Factor12"},
                {3, "Factor13"},
                {4, "Factor24"},
                {5, "Factor31"},
                {6, "Factor32"},
                {7, "Factor41"},
                {8, "Factor42"},
                {9, "Factor43"},
                {10, "Factor31"},
                {11, "Factor311"},
                {12, "Factor312"},
                {13, "Factor313"},
                {14, "Factor314"},
                {15, "Factor315"},
                {16, "Factor316"},
                {17, "Factor317"},
                {18, "Factor32AllKept"},
                {19, "Factor32"},
                {20, "Factor321"},
                {21, "Factor322"},
                {22, "Factor323"},
                {23, "Factor324"}
            };
            if (d.ContainsKey(factorTypeId))
                return d[factorTypeId];
            else
                return "";
        }

        #endregion
    }
}
