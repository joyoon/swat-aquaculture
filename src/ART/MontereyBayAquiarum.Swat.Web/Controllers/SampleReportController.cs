﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART.Reporting;
using ART;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class SampleReportController : Controller
    {
        //
        // GET: /SampleReport/

        public ActionResult Index()
        {
            ART.Reporting.Criteria2 rpt = new Criteria2();
            rpt.DataSource = new ReportSource().Criteria1(1)[0]; //Need to bind ReportId
            rpt.Run();
            ViewBag.Report = rpt;
            return View();
        }

    }
}
