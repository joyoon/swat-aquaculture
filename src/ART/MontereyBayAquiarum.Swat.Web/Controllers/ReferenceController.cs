﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;
using MontereyBayAquarium.Swat.Web.Models.References;
using MontereyBayAquarium.Swat.Web.ActionResults;
using System.Text.RegularExpressions;



namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class ReferenceController : ControllerBase
    {

        #region Index

        public ViewResult Index(int id, string keywords)
        {
            string Keywords = "";
            if (keywords != null) { Keywords = keywords; }
            ReferencesViewModel model = new ReferencesViewModel();
            model.References = Service.ReferenceList(id, Keywords);
            return View("Index", model);
        }
        #endregion

        #region Edit
        [HttpGet]
        public ViewResult Edit(int id, int referenceId)
        {
            ReferenceEntity model = Service.ReferenceGet(id, referenceId);
            return View("Edit", model);
        }
        
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, int referenceId, ReferenceEntity reference, HttpPostedFileBase documentation, string documentationType, string link)
        {
            //reference.Title = ""; //title is no longer needed TODO: Update Service Layer to take out title field
            bool isUrl = false;
            string path = "";
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            reference.ShortName = regex.Replace(reference.ShortName, @" ");
            if (reference.ReferenceId > 0)
            {
                ReferenceEntity oldReference = Service.ReferenceGet(reference.ReportId, reference.ReferenceId); // get the actual reference because there's no "path" or "isUrl" in the form
                isUrl = oldReference.isUrl;
                path = (oldReference.Path != null) ? oldReference.Path : "";
            }
            if (documentationType == "link") //documentationType = 0 unless the user purposefully want to upload something 
            {
                isUrl = true;
                path =  new UriBuilder(link).Uri.AbsoluteUri;
            }
           
			if (documentationType == "document") path = Helpers.SaveFile(documentation, Server, "references/"+id.ToString()+"/");

            bool shortNameIsValid = true;
            int updatedReferenceId = 0;

			if (reference.ReferenceId > 0)
                shortNameIsValid = Service.ReferenceUpdate(id, reference.ReferenceId, reference.ShortName, reference.Bibliography, reference.Authors, reference.Year, path, isUrl); //TODO GenEx
			else 
			    updatedReferenceId = Service.ReferenceAdd(id, reference.ShortName, reference.Bibliography, reference.Authors, reference.Year, path, isUrl); //TODO GenEx

            if (!shortNameIsValid || updatedReferenceId == -1)
            {
                TempData["error"] = "An existing reference already exists with the same shortname.";
                return RedirectToAction("Edit", new { id = id, referenceId = referenceId });
            }
			
			return RedirectToAction("Index");
        }
        #endregion

        [HttpPost]
        public JsonNetResult Delete(int id, int referenceId)
        {
            Service.ReferenceDelete(id, referenceId);
            return new JsonNetResult(new {success = true});
        }
        [HttpPost]
        public ActionResult Search(int id, string filter, string context)
        {
            ViewBag.Context = context;
            string Keywords = "";
            if (filter != null) { Keywords = filter; }
            ReferencesViewModel model = new ReferencesViewModel();
            model.References = Service.ReferenceList(id, Keywords);
            //return View("Index", model);
            return View("_References", model);
        }

        [HttpGet]
        public ViewResult Search(int id)
        {
            ViewBag.Context = "Modal";
            string Keywords = "";
            ReferencesViewModel model = new ReferencesViewModel();
            model.References = Service.ReferenceList(id, Keywords);
            return View("_References", model);
        }

    }
}
