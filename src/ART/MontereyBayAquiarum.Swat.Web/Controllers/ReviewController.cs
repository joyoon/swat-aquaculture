﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART.Reporting;
using ART;
using System.Data;
using MontereyBayAquarium.Swat.Web.Models.References;
using MontereyBayAquarium.Swat.Web.Models.Shared;
using MontereyBayAquarium.Swat.Web.Models.Review;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    [Authorize]
    public class ReviewController : ControllerBase
    {
        #region Final Recommendation
        [HttpGet]
        public ViewResult FinalRecommendation(int id)
        {
            return View("FinalRecommendation");
        }

        [ChildActionOnly]
        public ViewResult FinalRecommendationTable(int id)
        {
            RecommendationViewModel model = new RecommendationViewModel()
            {
                DataTable = new ReportSource().OverallRec(id),
                EcoCertification = Service.TextBlockGet(id, ARTService.TextBlock.EcoCertification),
                Summary = Service.TextBlockGet(id, ARTService.TextBlock.TextSummary)
            };
            return View("_FinalRecommendation", model);
        }
        #endregion

        #region Executive Summary
        [HttpGet]
        public ViewResult ExecutiveSummary(int id)
        {
            return View("ExecutiveSummary");
        }

        [ChildActionOnly]
        public ViewResult ExecutiveSummaryText(int id)
        {
            string summary = Service.TextBlockGet(id, ARTService.TextBlock.ExecSummary);
            GenericViewModel model = new GenericViewModel() { TextBlock = summary };
            return View("_ExecutiveSummary", model);
        }
        #endregion

        #region Introduction
        [HttpGet]
        public ViewResult Introduction(int id)
        {
            return View("Introduction");
        }

        [ChildActionOnly]
        public ViewResult IntroductionText(int id)
        {
            IntroductionViewModel model = new IntroductionViewModel()
            {
                IntroductionScope = Service.TextBlockGet(id, ARTService.TextBlock.Scope),
                IntroductionSpeciesOverview = Service.TextBlockGet(id, ARTService.TextBlock.SpeciesOverview),
                ProductionStatistics = Service.TextBlockGet(id, ARTService.TextBlock.ProductionStatistics),
                ImportanceToMarket = Service.TextBlockGet(id, ARTService.TextBlock.ImportanceToMarket),
                CommonMarketNames = Service.TextBlockGet(id, ARTService.TextBlock.CommonMarketNames),
                ProductForms = Service.TextBlockGet(id, ARTService.TextBlock.ProductForms)
            };
            return View("_Introduction", model);
        }
        #endregion

        #region Criteria1
        [HttpGet]
        public ViewResult Criteria1(int id)
        {
            TempData["Context"] = "Review";
            return View("Criteria1");
        }

        [ChildActionOnly]
        public ActionResult Criteria1Review(int id)
        {
            IList<Criteria1Entity> criteria1Entities = Service.Criteria1Overview(id).OrderBy(x => x.CommonName).ToList();
            Criteria1ViewModel model = new Criteria1ViewModel()
            {
                Criteria = criteria1Entities,
                Synthesis = Service.SynthesisGet(id, 1)
            };

            IList<IGrouping<string, Criteria1Entity>> groupedEntities = criteria1Entities.GroupBy(x => x.CommonName).ToList();
            IList<GroupedFactorColumnEntity> results = new List<GroupedFactorColumnEntity>();
            //grouped by common name
            foreach (IGrouping<string, Criteria1Entity> group in groupedEntities)
            {
                HashSet<FactorColumnEntity> uniqueFactor1Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                HashSet<FactorColumnEntity> uniqueFactor2Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                HashSet<FactorColumnEntity> uniqueFactor3Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                IList<CriteriaEntityBase> allRows = new List<CriteriaEntityBase>();
                foreach (Criteria1Entity set in group)
                {
                    if (set.Factor1.FactorId != 0)
                        uniqueFactor1Entities.Add(set.Factor1);
                    if (set.Factor2.FactorId != 0)
                        uniqueFactor2Entities.Add(set.Factor2);
                    if (set.Factor3.FactorId != 0)
                        uniqueFactor3Entities.Add(set.Factor3);
                    allRows.Add(set);
                }
                GroupedFactorColumnEntity result = new GroupedFactorColumnEntity();
                result.CommonName = group.Key;
                result.Factor1Entities = ProcessUniqueAnswers(uniqueFactor1Entities, allRows, (f, c) => (f.FactorId == c.Factor1.FactorId));
                result.Factor2Entities = ProcessUniqueAnswers(uniqueFactor2Entities, allRows, (f, c) => (f.FactorId == c.Factor2.FactorId));
                result.Factor3Entities = ProcessUniqueAnswers(uniqueFactor3Entities, allRows, (f, c) => (f.FactorId == c.Factor3.FactorId));
                results.Add(result);
            }

            model.GroupedAnswers = results;

            return View("_Criteria1", model);
        }



        #endregion

        #region Criteria2
        [HttpGet]
        public ViewResult Criteria2(int id)
        {
            //get the criteria 2 grid report
            Criteria2Grid grid = new Criteria2Grid();
            grid.DataSource = Service.Criteria1ReportData(id);
            grid.Criteria2Synthesis = Service.SynthesisGet(id, 2);
            ViewBag.GridReport = grid;

            //justification reports
            IList<FactorReportViewModel> justificationReports = new List<FactorReportViewModel>();
            IList<DataTable> results = new ReportSource().Criteria2(id);
            for (var x = 0; x < results.Count(); x++)
            {
                Criteria2 rpt = new Criteria2();
                rpt.DataSource = results[x];
                rpt.Run();
                if (results[x].Rows.Count != 0)
                {
                    FactorReportViewModel model = new FactorReportViewModel()
                                                      {
                                                          Report = rpt,
                                                          AnswerId =
                                                              Convert.ToInt32(
                                                                  results[x].Rows[0]["factorId"])
                                                      };
                    justificationReports.Add(model);
                }
            }
            TempData["Context"] = "Review";
            ViewBag.JustificationReports = justificationReports;
            return View("Criteria2");
        }

        [ChildActionOnly]
        public ActionResult Criteria2Review(int id)
        {
            IList<Criteria2Entity> criteria2Entities = Service.Criteria2Overview(id);
            IList<Criteria24Entity> criteria24Entities = Service.Criteria24Overview(id).OrderBy(x => x.Location).ToList();
            Criteria2ViewModel model = new Criteria2ViewModel()
            {
                Criteria = Helpers.BuildCriteria2List(criteria2Entities),
                Criteria24 = criteria24Entities,
                Synthesis = Service.SynthesisGet(id, 2)
            };

            IList<GroupedFactorColumnEntity> results = new List<GroupedFactorColumnEntity>();
            //grouped by common name

            HashSet<FactorColumnEntity> uniqueFactor4Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            IList<CriteriaEntityBase> allRows = new List<CriteriaEntityBase>();
            foreach (Criteria24Entity set in criteria24Entities)
            {
                if (set.Factor4.FactorId != 0)
                    uniqueFactor4Entities.Add(set.Factor4);
                allRows.Add(set);
            }
            GroupedFactorColumnEntity result = new GroupedFactorColumnEntity();
            result.Factor1Entities = ProcessUniqueAnswers(uniqueFactor4Entities, allRows, (f, e) => f.FactorId == e.Factor4.FactorId);
            results.Add(result);

            model.GroupedAnswers = results;

            IList<Criteria1Entity> criteria1Entities = Service.Criteria1Overview(id).OrderBy(x => x.CommonName).ToList();
            IList<IGrouping<string, Criteria1Entity>> groupedEntities1 = criteria1Entities.GroupBy(x => x.CommonName).ToList();
            IList<string> speciesFromCriteria1 = new List<string>();
            foreach (IGrouping<string, Criteria1Entity> criteria1 in groupedEntities1)
            {
                speciesFromCriteria1.Add(criteria1.Key);
            }
            IList<IGrouping<string, Criteria2Entity>> groupedEntities = criteria2Entities.GroupBy(x => x.CommonName).ToList();
            groupedEntities = groupedEntities.Where(x => !speciesFromCriteria1.Contains(x.Key)).ToList();
            IList<GroupedFactorColumnEntity> results2 = new List<GroupedFactorColumnEntity>();
            //grouped by common name
            foreach (IGrouping<string, Criteria2Entity> group in groupedEntities)
            {
                HashSet<FactorColumnEntity> uniqueFactor1Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                HashSet<FactorColumnEntity> uniqueFactor2Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                HashSet<FactorColumnEntity> uniqueFactor3Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
                IList<CriteriaEntityBase> allRows2 = new List<CriteriaEntityBase>();
                foreach (Criteria2Entity set in group)
                {
                    if (set.Factor1.FactorId != 0)
                        uniqueFactor1Entities.Add(set.Factor1);
                    if (set.Factor2.FactorId != 0)
                        uniqueFactor2Entities.Add(set.Factor2);
                    if (set.Factor3.FactorId != 0)
                        uniqueFactor3Entities.Add(set.Factor3);
                    allRows2.Add(set);
                }
                GroupedFactorColumnEntity result2 = new GroupedFactorColumnEntity();
                result2.CommonName = group.Key;
                result2.Factor1Entities = ProcessUniqueAnswers(uniqueFactor1Entities, allRows2, (f, c) => (f.FactorId == c.Factor1.FactorId));
                result2.Factor2Entities = ProcessUniqueAnswers(uniqueFactor2Entities, allRows2, (f, c) => (f.FactorId == c.Factor2.FactorId));
                result2.Factor3Entities = ProcessUniqueAnswers(uniqueFactor3Entities, allRows2, (f, c) => (f.FactorId == c.Factor3.FactorId));
                results2.Add(result2);
            }

            model.GroupedAnswers2 = results2.OrderBy(x => x.CommonName).ToList();

            return View("_Criteria2", model);
        }

        #endregion

        #region Criteria3
        [HttpGet]
        public ViewResult Criteria3(int id)
        {
            //get the criteria 3 grid report
            Criteria3Grid grid = new Criteria3Grid();
            grid.DataSource = Service.Criteria1ReportData(id);
            grid.DataSource31Grid = Service.Criteria31ReportData(id);
            grid.DataSource32Grid = Service.Criteria32ReportData(id);
            //Next 3 lines added by Bryan Banks
            ReportSource rs = new ReportSource();
            grid.DataSource31 = rs.Criteria31All(id);
            grid.DataSource32 = rs.Criteria32All(id);

            grid.Criteria3Synthesis = Service.SynthesisGet(id, 3);
            grid.Criteria31Synthesis = Service.SynthesisGet(id, 5);
            grid.Criteria32Synthesis = Service.SynthesisGet(id, 6);
            ViewBag.GridReport = grid;

            //justification reports for 3.1
            IList<FactorReportViewModel> justificationReports31 = new List<FactorReportViewModel>();
            IList<DataTable> results = new ReportSource().Criteria31(id);
            for (var x = 0; x < results.Count(); x++)
            {
                Criteria34 rpt = new Criteria34();
                rpt.DataSource = results[x];
                rpt.Run();
                FactorReportViewModel model = new FactorReportViewModel()
                {
                    Report = rpt,
                    AnswerId = Convert.ToInt32(results[x].Rows[0]["factorId"])
                };
                justificationReports31.Add(model);
            }

            ViewBag.JustificationReports31 = justificationReports31;

            //justification reports for 3.2
            IList<FactorReportViewModel> justificationReports32 = new List<FactorReportViewModel>();
            results = new ReportSource().Criteria32(id);
            for (var x = 0; x < results.Count(); x++)
            {
                Criteria34 rpt = new Criteria34();
                rpt.DataSource = results[x];
                rpt.Run();
                FactorReportViewModel model = new FactorReportViewModel()
                {
                    Report = rpt,
                    AnswerId = Convert.ToInt32(results[x].Rows[0]["factorId"])
                };
                justificationReports32.Add(model);
            }
            ViewBag.JustificationReports32 = justificationReports32;
            TempData["Context"] = "Review";
            return View("Criteria3");
        }

        [ChildActionOnly]
        public ActionResult Criteria3Review(int id)
        {
            IList<Criteria31Entity> criteria31Entities = Service.Criteria31Overview(id);
            IList<Criteria32Entity> criteria32Entities = Service.Criteria32Overview(id);
            Criteria3ViewModel model = new Criteria3ViewModel()
            {
                Synthesis1 = Service.SynthesisGet(id, 31),
                Synthesis2 = Service.SynthesisGet(id, 32),
                Synthesis3 = Service.SynthesisGet(id, 33),
                SummaryCriteria = Service.Criteria3Overview(id),
                Criteria31 = criteria31Entities,
                Criteria32 = criteria32Entities
            };

            IList<GroupedFactorColumnEntity> results = new List<GroupedFactorColumnEntity>();
            //grouped by common name
            HashSet<FactorColumnEntity> uniqueFactor1Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor2Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor3Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor4Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor5Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor6Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor7Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor8Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            IList<CriteriaEntityBase> allRows = new List<CriteriaEntityBase>();
            foreach (Criteria31Entity set in criteria31Entities)
            {
                if (set.Factor1.FactorId != 0)
                    uniqueFactor1Entities.Add(set.Factor1);
                if (set.Factor2.FactorId != 0)
                    uniqueFactor2Entities.Add(set.Factor2);
                if (set.Factor3.FactorId != 0)
                    uniqueFactor3Entities.Add(set.Factor3);
                if (set.Factor4.FactorId != 0)
                    uniqueFactor4Entities.Add(set.Factor4);
                if (set.Factor5.FactorId != 0)
                    uniqueFactor5Entities.Add(set.Factor5);
                if (set.Factor6.FactorId != 0)
                    uniqueFactor6Entities.Add(set.Factor6);
                if (set.Factor7.FactorId != 0)
                    uniqueFactor7Entities.Add(set.Factor7);
                if (set.Factor8.FactorId != 0)
                    uniqueFactor8Entities.Add(set.Factor8);
                allRows.Add(set);
            }
            GroupedFactorColumnEntity result = new GroupedFactorColumnEntity();
            result.Factor1Entities = ProcessUniqueAnswers(uniqueFactor1Entities, allRows, (f, c) => f.FactorId == c.Factor1.FactorId);
            result.Factor2Entities = ProcessUniqueAnswers(uniqueFactor2Entities, allRows, (f, c) => f.FactorId == c.Factor2.FactorId);
            result.Factor3Entities = ProcessUniqueAnswers(uniqueFactor3Entities, allRows, (f, c) => f.FactorId == c.Factor3.FactorId);
            result.Factor4Entities = ProcessUniqueAnswers(uniqueFactor4Entities, allRows, (f, c) => f.FactorId == c.Factor4.FactorId);
            result.Factor5Entities = ProcessUniqueAnswers(uniqueFactor5Entities, allRows, (f, c) => f.FactorId == c.Factor5.FactorId);
            result.Factor6Entities = ProcessUniqueAnswers(uniqueFactor6Entities, allRows, (f, c) => f.FactorId == c.Factor6.FactorId);
            result.Factor7Entities = ProcessUniqueAnswers(uniqueFactor7Entities, allRows, (f, c) => f.FactorId == c.Factor7.FactorId);
            result.Factor8Entities = ProcessUniqueAnswers(uniqueFactor8Entities, allRows, (f, c) => f.FactorId == c.Factor8.FactorId);
            results.Add(result);

            model.GroupedAnswers = results;

            IList<GroupedFactorColumnEntity> results2 = new List<GroupedFactorColumnEntity>();
            //grouped by common name
            HashSet<FactorColumnEntity> uniqueFactor21Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor22Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor23Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor24Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor25Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor26Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            IList<CriteriaEntityBase> allRows2 = new List<CriteriaEntityBase>();
            foreach (Criteria32Entity set in criteria32Entities)
            {
                if (set.Factor1.FactorId != 0)
                    uniqueFactor21Entities.Add(set.Factor1);
                if (set.Factor2.FactorId != 0)
                    uniqueFactor22Entities.Add(set.Factor2);
                if (set.Factor3.FactorId != 0)
                    uniqueFactor23Entities.Add(set.Factor3);
                if (set.Factor4.FactorId != 0)
                    uniqueFactor24Entities.Add(set.Factor4);
                if (set.Factor5.FactorId != 0)
                    uniqueFactor25Entities.Add(set.Factor5);
                if (set.Factor6.FactorId != 0)
                    uniqueFactor26Entities.Add(set.Factor6);
                allRows2.Add(set);
            }
            GroupedFactorColumnEntity result2 = new GroupedFactorColumnEntity();
            result2.Factor1Entities = ProcessUniqueAnswers(uniqueFactor21Entities, allRows2, (f, c) => f.FactorId == c.Factor1.FactorId);
            result2.Factor2Entities = ProcessUniqueAnswers(uniqueFactor22Entities, allRows2, (f, c) => f.FactorId == c.Factor2.FactorId);
            result2.Factor3Entities = ProcessUniqueAnswers(uniqueFactor23Entities, allRows2, (f, c) => f.FactorId == c.Factor3.FactorId);
            result2.Factor4Entities = ProcessUniqueAnswers(uniqueFactor24Entities, allRows2, (f, c) => f.FactorId == c.Factor4.FactorId);
            result2.Factor5Entities = ProcessUniqueAnswers(uniqueFactor25Entities, allRows2, (f, c) => f.FactorId == c.Factor5.FactorId);
            result2.Factor6Entities = ProcessUniqueAnswers(uniqueFactor26Entities, allRows2, (f, c) => f.FactorId == c.Factor6.FactorId);

            results2.Add(result2);

            model.GroupedAnswers2 = results2;

            return View("_Criteria3", model);
        }
        #endregion

        #region Criteria4
        [HttpGet]
        public ViewResult Criteria4(int id)
        {
            //get the criteria 4 grid report
            Criteria4Grid grid = new Criteria4Grid();
            grid.DataSource = Service.Criteria1ReportData(id);
            grid.Criteria4Synthesis = Service.SynthesisGet(id, 4);
            ViewBag.GridReport = grid;

            //justification reports
            IList<FactorReportViewModel> justificationReports = new List<FactorReportViewModel>();
            IList<DataTable> results = new ReportSource().Criteria4(id);
            for (var x = 0; x < results.Count(); x++)
            {
                Criteria34 rpt = new Criteria34();
                rpt.DataSource = results[x];
                rpt.Run();
                FactorReportViewModel model = new FactorReportViewModel()
                {
                    Report = rpt,
                    AnswerId = Convert.ToInt32(results[x].Rows[0]["factorId"])
                };
                justificationReports.Add(model);
            }
            TempData["Context"] = "Review";
            ViewBag.JustificationReports = justificationReports;
            return View("Criteria4");
        }

        [ChildActionOnly]
        public ActionResult Criteria4Review(int id)
        {
            IList<Criteria4Entity> criteria4Entities = Service.Criteria4Overview(id);
            Criteria4ViewModel model = new Criteria4ViewModel()
            {
                Criteria = criteria4Entities,
                Synthesis = Service.SynthesisGet(id, 4)
            };
            IList<GroupedFactorColumnEntity> results = new List<GroupedFactorColumnEntity>();
            //grouped by common name
            HashSet<FactorColumnEntity> uniqueFactor1Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor2Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            HashSet<FactorColumnEntity> uniqueFactor3Entities = new HashSet<FactorColumnEntity>(new EntityEqualityComparer());
            IList<CriteriaEntityBase> allRows = new List<CriteriaEntityBase>();
            foreach (Criteria4Entity set in criteria4Entities)
            {
                if (set.Factor1.FactorId != 0)
                    uniqueFactor1Entities.Add(set.Factor1);
                if (set.Factor2.FactorId != 0)
                    uniqueFactor2Entities.Add(set.Factor2);
                if (set.Factor3.FactorId != 0)
                    uniqueFactor3Entities.Add(set.Factor3);
                allRows.Add(set);
            }
            GroupedFactorColumnEntity result = new GroupedFactorColumnEntity();
            result.Factor1Entities = ProcessUniqueAnswers(uniqueFactor1Entities, allRows, (f, c) => f.FactorId == c.Factor1.FactorId);
            result.Factor2Entities = ProcessUniqueAnswers(uniqueFactor2Entities, allRows, (f, c) => f.FactorId == c.Factor2.FactorId);
            result.Factor3Entities = ProcessUniqueAnswers(uniqueFactor3Entities, allRows, (f, c) => f.FactorId == c.Factor3.FactorId);
            results.Add(result);
            model.GroupedAnswers = results;

            return View("_Criteria4", model);
        }


        #endregion

        #region OverallRecommendation
        [HttpGet]
        public ViewResult OverallRecommendation(int id)
        {
            return View("OverallRecommendation");
        }

        [ChildActionOnly]
        public ViewResult OverallRecommendationTable(int id)
        {
            DataTable dt = new ReportSource().OverallRec(id);
            return View("_OverallRecommendation", dt);
        }

        #endregion

        public ViewResult References(int id, string keywords)
        {
            return View("References", keywords);
        }

        [ChildActionOnly]
        public ViewResult ReferencesTable(int id, string keywords)
        {
            string Keywords = "";
            if (keywords != null) { Keywords = keywords; }
            ReferencesViewModel model = new ReferencesViewModel();
            model.References = Service.ReferenceList(id, Keywords);
            ViewBag.Context = "Review";
            return View("_ReferencesTable", model);
        }

        #region Appendix
        [HttpGet]
        public ViewResult Appendix(int id)
        {
            return View("Appendix");
        }

        [ChildActionOnly]
        public ViewResult AppendixList(int id)
        {
            DataTable dt = Service.AppendixAReportData(id);
            IList<DataRow> rows = new List<DataRow>();
            foreach (DataRow row in dt.Rows)
            {
                rows.Add(row);
            }

            MontereyBayAquarium.Swat.Web.Models.Analysis.AppendixViewModel model = new MontereyBayAquarium.Swat.Web.Models.Analysis.AppendixViewModel();
            model.Appendices = Service.AppendixesGet(id);
            model.AppendixA = rows.GroupBy(x => x.ItemArray[6].ToString());
            model.ReviewSchedule = Service.TextBlockGet(id, ARTService.TextBlock.ReviewSchedule);
            return View("_Appendix", model);
        }
        #endregion

        private IList<ReviewFactorColumnEntity> ProcessUniqueAnswers(HashSet<FactorColumnEntity> entities, IList<CriteriaEntityBase> allRows, Func<FactorColumnEntity, CriteriaEntityBase, bool> predicate)
        {
            IList<ReviewFactorColumnEntity> results = new List<ReviewFactorColumnEntity>();
            foreach (FactorColumnEntity entity in entities)
            {
                //find all rows that have this answer
                ReviewFactorColumnEntity reviewEntity = new ReviewFactorColumnEntity();
                reviewEntity.FactorColumnEntity = entity;
                IList<CriteriaEntityBase> list = allRows.Where(x => predicate(entity, x)).ToList();
                foreach (CriteriaEntityBase innerEntity in list)
                {
                    reviewEntity.RelevantRows.Add(innerEntity.Location + "/" + innerEntity.BodyOfWater + ", " + innerEntity.Method);
                }

                results.Add(reviewEntity);
            }
            return results;
        }

    }
}
