﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ART;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class ControllerBase : Controller
    {
        #region Members
        protected ARTService Service;
        protected PublicBL PublicService;
        #endregion

        #region Constructors
        public ControllerBase()
        {
            Service = new ARTService();
            PublicService = new PublicBL();
        }
        #endregion
    }
}
