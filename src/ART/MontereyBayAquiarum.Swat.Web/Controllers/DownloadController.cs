﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Microsoft.Win32;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure;

namespace MontereyBayAquarium.Swat.Web.Controllers
{
    public class DownloadController : Controller
    {
        //
        // GET: /Download/

        public ActionResult Index(string filename)
        {
            filename = HttpUtility.UrlDecode(filename);
            // Retrieve storage account from connection string.
            Microsoft.WindowsAzure.Storage.CloudStorageAccount storageAccount = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("sfwart");

            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

            // Save blob contents to disk.
            Response.AddHeader("Content-Disposition", "attachment; filename='" + filename+"'");
            blockBlob.DownloadToStream(Response.OutputStream);
            return new EmptyResult();

            /*
            blockBlob.DownloadToStream(s);
            return File(s, contentType);*/
        }

        /*
        public ActionResult Report()
        {
            Generator gen = new Generator();
            MemoryStream ms = new MemoryStream();
            //gen.CreateDoc("~/Content/documents/report.docx", 1);
            //CopyStream(gen.CreateStream(1), ms);
            var stream = gen.CreateStream(1);
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "report.docx");
            /*Response.AddHeader("content-disposition", "attachment;filename=labtest.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();*//*
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[16*1024];
            int read;
            while(true){
                read = input.Read(buffer, 0, buffer.Length);
                if (read < 0)
                {
                    break;
                }
                output.Write (buffer, 0, read);
            }
        }*/
    }
}
