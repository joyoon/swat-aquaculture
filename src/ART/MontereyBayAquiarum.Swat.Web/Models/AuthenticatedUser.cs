﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SquareOne.Infrastructure.Mvc.Authentication;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        #region Constructors
        public AuthenticatedUser(UserEntity user)
        {
            this.UserEntity = user;
        }
        #endregion

        #region Properties
        public UserEntity UserEntity { get; private set; }
        #endregion

        #region IAuthenticatedUser Members

        public string DisplayName
        {
            get { return this.UserEntity.DisplayName; }
        }

        public int Id
        {
            get { return this.UserEntity.Id; }
        }

        #endregion
    }
}