﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class GuidingPrincipleViewModel
    {
        public string Title { get; set; }
        public string Key { get; set; }
    }
}