﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class Criteria1ViewModel
    {
        public IList<Criteria1Entity> Criteria { get; set; }
        public string Synthesis { get; set; }
    }
}