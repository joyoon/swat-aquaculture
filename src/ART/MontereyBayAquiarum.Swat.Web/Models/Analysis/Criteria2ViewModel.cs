﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class Criteria2ViewModel
    {
        public IList<Criteria2Display> Criteria { get; set; }        public IList<Criteria21Entity> Criteria21 { get; set; }
        public IList<Criteria24Entity> Criteria24 { get; set; }
        public IList<Criteria2Entity> Criteria2 { get; set; }
        public string Synthesis { get; set; }
    }
}