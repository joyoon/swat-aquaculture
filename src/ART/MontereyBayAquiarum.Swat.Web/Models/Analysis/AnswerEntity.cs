﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class AnswerEntity
    {
        public int FactorId { get; set; }
        public string Score { get; set; }
        public string ScoreDescription { get; set; }
        public string Label { get; set; }
        public int CommentCount { get; set; }
        public string Title { get; set; }
        public string KeyInfo { get; set; }
        public int FactorTypeId { get; set; }
        public string CurSpecies { get; set; }
    }
}