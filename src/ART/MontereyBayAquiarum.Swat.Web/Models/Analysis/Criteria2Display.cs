﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class Criteria2Display
    {

        public Criteria2Entity LimitingStock { get; set; }
        public IList<Criteria2Entity> OtherStock { get; set; }
        public Criteria2Display()
        {
            this.OtherStock = new List<Criteria2Entity>();
        }
    }
}