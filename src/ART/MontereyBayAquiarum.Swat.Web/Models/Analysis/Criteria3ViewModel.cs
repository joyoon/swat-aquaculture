﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class Criteria3ViewModel
    {
        public Criteria3Display Criteria { get; set; }
        public string Synthesis1 { get; set; }
        public string Synthesis2 { get; set; }
        public string Synthesis3 { get; set; }
    }
}