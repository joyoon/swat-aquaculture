﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class Criteria3Display
    {
        public IList<Criteria3Entity> SummaryCriteria { get; set; }
        public IList<Criteria31Entity> Criteria31 { get; set; }
        public IList<Criteria32Entity> Criteria32 { get; set; }
    }
}