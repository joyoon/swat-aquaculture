﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;
using System.Data;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class AppendixViewModel
    {
        public IList<AppendixEntity> Appendices { get; set; }
        public string ReviewSchedule { get; set; }
        public string GetAppendixItemHTML(int i)
        {
            return "<div class='appendix-form'><div class='appendix-item'>" +
                   "<h2>Appendix <span>@Model.Appendicies["+i+"].Letter</span> <a class='remove'>remove</a></h2><div class='clear'></div>" +
                   "<label>Title: </label><input type='text' name='appendixTitles[" + i + "]' id='appendixTitle" + i.ToString() + "' + value=@Html.raw(@Model.Appendicies[" + i + "].Title)  />" +
                   "<textarea style='width:917px' class='mceEditor' name='appendixDetails["+i+"]' id='appendixBody" + i.ToString() + "'>@Html.raw(@Model.Appendices[" + i + "].Appendix</textarea></div></div>";
        }
        public IEnumerable<IGrouping<string, DataRow>> AppendixA { get; set; }
    }
}
