﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Analysis
{
    public class DefineScopeViewModel
    {
        public IList<RecEntity> Recommendations { get; set; }
        public IList<RowsourceEntity> RecommendationTypes { get; set; }
    }
}