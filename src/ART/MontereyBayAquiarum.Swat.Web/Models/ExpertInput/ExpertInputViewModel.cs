﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.ExpertInput
{
    public class ExpertInputViewModel
    {
        public FeedbackEntity Feedback { set; get; }
        public IList<string> Documents { set; get; }
    }
}