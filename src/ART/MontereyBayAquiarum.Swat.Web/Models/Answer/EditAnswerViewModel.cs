﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Answer
{
    public class EditAnswerViewModel
    {
        public FactorEntity Factor { get; set; }
        public IList<RowsourceEntity> FactorTypes { get; set; }
        public IList<RowsourceEntity> Scores { get; set; }
    }
}