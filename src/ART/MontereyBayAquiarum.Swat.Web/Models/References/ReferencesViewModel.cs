﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.References
{
    public class ReferencesViewModel
    {
        public IList<ReferenceEntity> References { get; set; }
  
    }
}