﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;
using MontereyBayAquarium.Swat.Web.Models.Analysis;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class Criteria3ViewModel
    {
        public Criteria3Display Criteria { get; set; }
        public IList<Criteria3Entity> SummaryCriteria { get; set; }
        public IList<Criteria31Entity> Criteria31 { get; set; }
        public IList<Criteria32Entity> Criteria32 { get; set; }
        public string Synthesis1 { get; set; }
        public string Synthesis2 { get; set; }
        public string Synthesis3 { get; set; }
        public IList<GroupedFactorColumnEntity> GroupedAnswers { get; set; }
        public IList<GroupedFactorColumnEntity> GroupedAnswers2 { get; set; }
    }
}