﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART.Reporting;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class Criteria4ViewModel
    {
        public IList<Criteria4Entity> Criteria { get; set; }
        public string Synthesis { get; set; }
        public IList<GroupedFactorColumnEntity> GroupedAnswers { get; set; }
    }
}