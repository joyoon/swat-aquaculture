﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART.Reporting;
using ART;
using System.Data;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class RecommendationViewModel
    {
        public string Summary { get; set; }
        public string EcoCertification { get; set; }
        public DataTable DataTable { get; set; }
    }
}