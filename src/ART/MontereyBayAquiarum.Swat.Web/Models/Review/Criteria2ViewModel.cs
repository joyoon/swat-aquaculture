﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART.Reporting;
using ART;
using MontereyBayAquarium.Swat.Web.Models.Analysis;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class Criteria2ViewModel
    {
        public IList<Criteria2Display> Criteria { get; set; }
        public string Synthesis { get; set; }
        public IList<GroupedFactorColumnEntity> GroupedAnswers { get; set; }
        public IList<GroupedFactorColumnEntity> GroupedAnswers2 { get; set; }
        public IList<Criteria24Entity> Criteria24 { get; set; }
    }
}