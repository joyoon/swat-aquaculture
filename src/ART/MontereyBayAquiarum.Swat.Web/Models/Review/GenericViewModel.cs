﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART.Reporting;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class GenericViewModel
    {
        public string TextBlock { get; set; }
    }
}