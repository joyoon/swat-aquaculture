﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART.Reporting;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Review
{
    public class IntroductionViewModel
    {
        public string IntroductionScope { get; set; }
        public string IntroductionSpeciesOverview { get; set; }
        public string ProductionStatistics { get; set; }
        public string ImportanceToMarket { get; set; }
        public string CommonMarketNames { get; set; }
        public string ProductForms { get; set; }
    }
}