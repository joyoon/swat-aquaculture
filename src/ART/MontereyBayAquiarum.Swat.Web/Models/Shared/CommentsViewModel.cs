﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Shared
{
    public class CommentsViewModel
    {
        public IList<FactorCommentEntity> Comments { get; set; }
        public UserEntity User { get; set; }
    }
}