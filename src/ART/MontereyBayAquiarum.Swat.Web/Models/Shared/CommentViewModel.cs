﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Shared
{
    public class CommentViewModel
    {
        public FactorCommentEntity Comment { get; set; }
        public UserEntity User { get; set; }
        public IList<FactorCommentEntity> SubComments { get; set; }
        public bool IsRoot { get; set; }
        public string NewPath { get; set; }
    }
}