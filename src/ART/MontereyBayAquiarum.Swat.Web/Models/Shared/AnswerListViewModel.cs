﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models.Shared
{
    public class AnswerListViewModel
    {
        public IList<FactorEntity> Answers { get; set; }
        public IList<RowsourceEntity> FactorTypes { get; set; }
        public int CurrentFactorTypeId { get; set; }
    }
}