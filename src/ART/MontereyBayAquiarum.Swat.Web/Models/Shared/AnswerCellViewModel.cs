﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MontereyBayAquarium.Swat.Web.Models.Analysis;

namespace MontereyBayAquarium.Swat.Web.Models.Shared
{
    public class AnswerCellViewModel
    {
        public AnswerEntity Answer { get; set; }
        public string FactorTypeName { get; set; }
        public string Species { get; set; }
        public string Location { get; set; }
        public string BodyOfWater { get; set; }
        public string Method { get; set; }
        public bool IsEditable { get; set; }

        public AnswerCellViewModel()
        {
            IsEditable = true;
        }
    }
}