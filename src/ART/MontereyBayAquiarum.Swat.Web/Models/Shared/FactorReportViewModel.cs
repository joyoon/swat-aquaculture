﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataDynamics.ActiveReports;

namespace MontereyBayAquarium.Swat.Web.Models.Shared
{
    public class FactorReportViewModel
    {
        public ActiveReport Report { get; set; }
        public int AnswerId { get; set; }
    }
}