﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ART;

namespace MontereyBayAquarium.Swat.Web.Models
{
    public class ReportsViewModel
    {
        public IList<ReportEntity> Reports { get; set; }
        public IList<RecSummaryEntity> RecSummaries { get; set; }
        public UserEntity User { get; set; }
    }
}