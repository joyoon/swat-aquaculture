﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register assembly="ActiveReports.Web, Version=6.2.3681.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" namespace="DataDynamics.ActiveReports.Web" tagprefix="ActiveReportsWeb" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Index</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
    </div>
    <ActiveReportsWeb:WebViewer ID="WebViewer1" runat="server" height="442px" onload="WebViewer1_Load"
        width="958px" ViewerType="RawHtml">
    </ActiveReportsWeb:WebViewer>
    </form>
</body>
</html>

<script runat="server">
    protected void WebViewer1_Load(object sender, EventArgs e)
    {
        WebViewer1.Report = (DataDynamics.ActiveReports.ActiveReport)ViewBag.Report;
    }
</script>