﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data" %>
<script runat="server">
  private void Page_Load(object sender, System.EventArgs e)
  {

      SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MontereyBay"].ConnectionString);
      DataSet ds = new DataSet();
      SqlDataAdapter adapter = new SqlDataAdapter("SELECT NAME, STATUS FROM REPORT", conn);
      adapter.Fill(ds);
      
      ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/TestReport.rdlc");
      ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("TestDataSet", ds.Tables[0]));
      ReportViewer1.LocalReport.Refresh();
  }
</script>

    <form id="Form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false" Font-Names="Verdana" 
        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" 
        ShowToolBar="False">
</rsweb:ReportViewer>

</form>