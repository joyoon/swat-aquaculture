﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ActiveReport>" %>
<%@ Import Namespace="DataDynamics.ActiveReports" %>
<%@ Register assembly="ActiveReports.Web, Version=6.2.3681.0, Culture=neutral, PublicKeyToken=cc4967777c49a3ff" namespace="DataDynamics.ActiveReports.Web" tagprefix="ActiveReportsWeb" %>


<ActiveReportsWeb:WebViewer ID="WebViewer" runat="server" ViewerType="RawHtml" OnLoad="WebViewer_Load"  MaxReportRunTime="6000000">
</ActiveReportsWeb:WebViewer>

 <asp:LinkButton></asp:LinkButton> <!--Added here to get the __doPostBack function for super long reports that requires it-->
<script runat="server">
    protected void WebViewer_Load(object sender, EventArgs e)
    {
        WebViewer.Report = (DataDynamics.ActiveReports.ActiveReport)Model;
    }
</script>