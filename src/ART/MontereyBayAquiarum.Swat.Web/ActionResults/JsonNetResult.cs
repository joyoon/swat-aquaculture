﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MontereyBayAquarium.Swat.Web.ActionResults
{
    public class JsonNetResult : ActionResult
    {
        public dynamic Data { get; private set; }

        public JsonNetResult(dynamic data)
        {
            this.Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var response = context.HttpContext.Response;
            response.ContentType = "application/json";

            var result = "";
            if (Data != null)
                result = JsonConvert.SerializeObject(Data);

            response.Write(result);
        }
    }
}