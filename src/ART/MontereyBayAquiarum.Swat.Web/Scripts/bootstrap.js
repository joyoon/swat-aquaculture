﻿var require = {
    baseUrl: "/Scripts",
    priority: ["jquery", "underscore", "tiny_mce", "jquery.tablesorter"],
    paths: {
        "jquery": "libs/jquery-1.7.2.min",
        "underscore": "libs/underscore-min",
        "backbone": "libs/backbone-min",
        "jquery.validate": "libs/jquery.validate.min",
        "jquery.ui": "libs/jquery-ui.min",
        "jquery.template": "libs/jquery.template",
        "jquery.qtip": "libs/jquery.qtip-1.0.0-rc3.min",
        "jquery.tiny_mce": "libs/tiny_mce/jquery.tinymce",
        "tiny_mce": "libs/tiny_mce/tiny_mce",
        "tiny_mce_full": "libs/tiny_mce/tiny_mce",
        "boostrap_tiny_mce": "utils/bootstrap.tinymce",
        "jquery.predictiveTyping": "libs/jquery.predictiveTyping",
        "jquery.tablesorter": "libs/jquery.tablesorter.min",
        "jquery.tablesorter.pager": "libs/jquery.tablesorter.pager",
        "custom-form-elements": "libs/custom-form-elements"
    }
};