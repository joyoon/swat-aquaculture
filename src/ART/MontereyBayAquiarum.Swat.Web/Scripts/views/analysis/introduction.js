﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function ($) {
    "use strict";
    var introduction = {
        currentReference: null,
        init: function () {
            $(".subheader span").hover(
                             function () {
                                 var offset = $(this).position();
                                 $(this).parents(".subheader:eq(0)").find(".tip").css({ "top": (offset.top + 15) + "px", "left": (offset.left + 15) + "px" }).show();
                             },
                             function () {
                                 $(this).parents(".subheader:eq(0)").find(".tip").hide();
                             });
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(2)").addClass("on")
            $(".introduction-button").bind("click", this.submitForm);
            $(".document-insert, .image-insert").bind("click", this.openModal);
            $(".document-insert").bind("click", this.showReference);
            $(".image-insert").bind("click", this.showImage);
            $("#insert-reference").bind("click", this.insertReference);
            $("#insert-image").bind("click", this.insertImage);
            //code here is to address reusing the same answer modal
            $("#answer-modal").addClass("reference");
            $(".go-back-button").hide();
            $(".cancel-add-reference").hide();
        },
        showReference: function () {
            $(".images").hide();
            $(".references").show();
            $("#ui-dialog-title-answer-modal").text("Insert Reference");
        },
        showImage: function () {
            $(".references").hide();
            $(".images").show();
            $("#ui-dialog-title-answer-modal").text("Insert Image");
        },
        submitForm: function () {
            $("#nextAction").val("Acknowledgements");
            $("#introductionForm").submit();
        },
        openModal: function () {
            $("#answer-modal").dialog("open");
            introduction.currentReference = $(this).next().attr("name");

            $(".references h2").hide();
        },
        insertReference: function (e) {
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                textToInsert += "{" + $(this).parents("tr:eq(0)").find("td:eq(3)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = answerModal.currentReference,
                editor = tinyMCE.get(currentReference);
            //
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#answer-modal").dialog("close");
        },
        insertImage: function () {
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                var id = $(this).closest("tr").data("image-id");
                textToInsert += "{IMG-" + id + ": " + $(this).parents("tr:eq(0)").find("td:eq(2)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = answerModal.currentReference,
                editor = tinyMCE.get(currentReference);
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#answer-modal").dialog("close");

        }
    };
    introduction.init()
});