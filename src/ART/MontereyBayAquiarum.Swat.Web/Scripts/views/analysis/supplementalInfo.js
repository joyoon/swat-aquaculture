﻿/*global require*/
require(["utils/bootstrap.tinymce", "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var supplementalInfo = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(2)").addClass("on")
            $(".supplementalinfo-button").bind("click", this.submitForm);
        },
        submitForm: function () {
            $("#nextAction").val("Appendix");
            $("#supplementalInfoForm").submit();
        }
    }
    supplementalInfo.init();
});