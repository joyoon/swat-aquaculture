﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function ($) {
    "use strict";
    var criteria4 = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on");
            $(".tabbed-wrapper .tabs li:eq(1)").addClass("on");
            $(".criteria4-button").bind("click", this.submitForm);
        },
        submitForm: function () {
            $("#nextAction").val("ExecutiveSummary");
            $("#criteria4Form").submit();
        }
    }
    criteria4.init()
});