﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer", "jquery.template", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var appendixForm = {
        appendixCount: $(".appendix-item").length,
        currentEditor: null,
        init: function () {
            $(".add-appendix").bind("click", this.addAppendix)
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(2)").addClass("on")
            $(".appendix-button").bind("click", this.submitForm);
            $(".appendix-item .remove").live("click", this.removeAppendix);
            $("#appendixForm").on("click", ".image-insert, .document-insert", this.openModal);
            $("#insert-image").bind("click", this.insertImage);
            //code here is to address reusing the same answer modal
            $("#answer-modal").addClass("reference");
            $(".go-back-button").hide();
            $(".cancel-add-reference").hide();
            $("#insert-reference").bind("click", this.insertReference);
            $(".document-insert").bind("click", this.showReference);
            $(".image-insert").bind("click", this.showImage);
            //$(".document-insert, .image-insert").bind("click", this.openModal);
        },
        openModal: function () {
            $("#answer-modal").dialog("open");
            appendixForm.currentEditor = $(this).siblings("textarea").first().attr("id");
            $(".references h2").hide();
        },
        showReference: function () {
            $(".images").hide();
            $(".references").show();
            $("#ui-dialog-title-answer-modal").text("Insert Reference");
        },
        showImage: function () {
            $(".references").hide();
            $(".images").show();
            $("#ui-dialog-title-answer-modal").text("Insert Image");
        },
        submitForm: function () {
            $("#nextAction").val("DraftOverallRecommendations");
            $("#appendixForm").submit();
        },
        addAppendix: function () {
            var lastLetter = $(".appendix-item").last().find(".letter").val();
            var newLetter = String.fromCharCode(lastLetter.charCodeAt() + 1)
            var template = $.template($('#appendix-item-form').html());
            var itemHTML = template({ 'appendixCount': appendixForm.appendixCount, 'newLetter': newLetter });
            $(".appendix-form").append(itemHTML);
            appendixForm.createEditor();
            appendixForm.appendixCount++;
        },
        removeAppendix: function () {
            var $appendixItem = $(this).parents(".appendix-item");
            $appendixItem.hide()
                .find(".title").val("")
                .end().find("textarea").text(""); //remove text but leave element in place to delete in DB
            //var listItem = document.getElementById('bar');
            var index = $('.appendix-item').index($appendixItem);
            var className = $appendixItem.find(".mceEditor").attr('id');
            tinyMCE.getInstanceById(className).getBody().innerHTML = ' ';

        },
        createEditor: function () {
            tinyMCE.init({
                mode: "specific_textareas",
                editor_selector: "newEditor",
                theme: "advanced",
                plugins: "table, autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                setup: function (ed) {
                    ed.onKeyUp.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                    ed.onClick.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                },
                // Theme options
                theme_advanced_buttons1: "save,pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,|,sub,sup,|, tablecontrols, code",
                theme_advanced_buttons2: "",
                width: "870px",
                paste_auto_cleanup_on_paste: true,
                paste_remove_styles: true,
                paste_remove_styles_if_webkit: true,
                paste_strip_class_attributes: true,
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                theme_advanced_resizing_use_cookie: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: false

            });
            //after we init an editor, we want to remove the class because tinyMCE editors can't be initialized multiple times.
            $(".newEditor").removeClass("newEditor");
        },
        /*openModal: function () {
            $("#answer-modal").dialog("open");
            appendixForm.currentEditor = $(this).next().attr("id");
            $("#ui-dialog-title-answer-modal").text("Insert Image");
            $(".references h2").hide();
            $("#answer-modal .tabs").hide();
            $(".images").show();
        },*/
        insertImage: function () {
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                var id = $(this).closest("tr").data("image-id");
                textToInsert += "{IMG-"+ id + ": " + $(this).parents("tr:eq(0)").find("td:eq(2)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = appendixForm.currentEditor,
                editor = tinyMCE.get(currentReference);
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#answer-modal").dialog("close");
        },
        insertReference: function (e) {
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                textToInsert += "{" + $(this).parents("tr:eq(0)").find("td:eq(3)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = appendixForm.currentEditor,
                editor = tinyMCE.get(currentReference);
            //
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#answer-modal").dialog("close");
        },
    }
    appendixForm.init()
});