﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var criteria3 = {
        init: function () {
            $(".guiding-principles .information-box a").bind("click", this.hideGuide);
            $(".guiding-principles .button-review").bind("click", this.showGuide);
            $(".tabbed-wrapper .tabs li").removeClass("on");
            $(".tabbed-wrapper .tabs li:eq(1)").addClass("on");
            $(".synthesis").hide().eq(0).show();
            $(".criteria-content:eq(0)").show();
            $(".criteria-tabs li").live("click", this.tabCriteria);
            $(".criteria3-button").bind("click", this.submitForm);
            $(".cell-wrapper").live("mouseenter", this.showPopout); // the popouts need to be detached, because it's inside a div with overflow:scroll and z-index wouldn't show the popout
            $(".cell-wrapper").live("mouseleave", this.hidePopout);
            this.pullOutElements(1); //this method is used to detach the elements because the first 2 columns doesnt move on scroll
            this.popups = new Array()
            $(".criteria3-button").bind("click", this.submitForm);
            this.addClassToAnswerModal();
        },
        showGuide: function () {
            $(".guiding-principles .information-box").show();
            $(".guiding-principles .button-review").hide();
            var index = $(".criteria-tabs li").index($(".criteria-tabs li.on"));
            criteria3.pullOutElements(index+1);
        },
        hideGuide: function () {
            $(".guiding-principles .information-box").hide();
            $(".guiding-principles .button-review").show();
            var index = $(".criteria-tabs li").index($(".criteria-tabs li.on"));
            criteria3.pullOutElements(index+1);
        },
        tabCriteria: function () {
            var index = $(".criteria-tabs li").index($(this));
            $(".criteria-content").hide();
            $(".criteria-tabs li").removeClass("on");
            $(this).addClass("on")
            $(".criteria-content:eq(" + index + ")").show();
            $(".synthesis").hide().eq(index).show();
            $(".criteria-scroll").scrollLeft(0);
            criteria3.pullOutElements(index + 1);
        },
        submitForm: function () {
            $("#criteria33Form #nextAction").val("Criteria4");
            //have to submit 3 different forms, quick work around submit first two forms by ajax
            $.post($("#criteria31Form").attr("action"),
            {
                criteriaId: $("#criteria31Form").find("#criteriaId").val(),
                nextAction: "Criteria4",
                synthesis: tinyMCE.get('synthesis31').getContent()
            });
            $.post($("#criteria32Form").attr("action"),
            {
                criteriaId: $("#criteria32Form").find("#criteriaId").val(),
                nextAction: "Criteria4",
                synthesis: tinyMCE.get('synthesis32').getContent()
            });
            $("#criteria33Form").submit();
        },
        showPopout: function () {
            if ($(this).parent().hasClass("no-popup")) return false;
            var $popup = $(this).find(".risk-factor-popup").addClass("custom-popup");
            if ($popup.length > 0) {
                var left, top;
                left = $popup.offset().left;
                top = $popup.offset().top;
                var $clone = $popup.clone();
                $clone
                .appendTo('body')
                .css({
                    'position': 'absolute',
                    'left': left + 'px',
                    'top': top + 'px'
                }).show();
                criteria3.popups.push($clone);
            }
        },
        hidePopout: function () {
            while (criteria3.popups.length > 0) {
                criteria3.popups.pop().remove();
            }
        },
        pullOutElements: function (tab) {
            $(".immovable-td").remove();
            var $currentTable;
            var index = tab -1;
            if (tab == 1) {
                $currentTable = $(".criteria-31");
            }
            if (tab == 2) {
                $currentTable = $(".criteria-32");
            }
            if (tab == 3) {
                return false;
            }
            $(".immovable-td").remove();
            $currentTable.find(".dark-bg").each(function () {
                var left, top, height, width;
                left = $(this).position().left ;
                top = $(this).position().top +40; //35 pixels to offset the tabs
                height = $(this).outerHeight() - 11; //10 for the padding and 1 for the border
                width = $(this).outerWidth() - 11;
                $(this).clone().appendTo('.parentTable').css({
                    'position': 'absolute',
                    'left': left + 'px',
                    'top': top + 'px',
                    'width': width + 'px',
                    'height': height + 'px'
                }).addClass("immovable-td");
            });
        },
        addClassToAnswerModal: function () {
            $("#answer-modal").addClass("criteria3");
        }
    }
    criteria3.init()
});