﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.stockAnalysis"], function ($) {
    "use strict";
    var stockAnalysis = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on");
            $(".tabbed-wrapper .tabs li:eq(1)").addClass("on");
        }
    };
    stockAnalysis.init();
});