﻿/*global require*/
require(["jquery", "jquery.ui", "jquery.template", "jquery.predictiveTyping", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    window.defineScope = {
        scopeItem: { species: "", location: "", bow: "", gear: "", type: "" },
        reportId: 0,
        editorOpened: false,
        bulkEditorOpened: false,
        currentInput: null,
        currentRowChecked: null,
        init: function () {
            defineScope.reportId = $("#reportId").html();
            $("#bulk-add").dialog({ autoOpen: false, modal: true, resizable: false, title: "Bulk add Stock, Region and Gear Type", width: 920 });
            $("#notification-modal").dialog({ autoOpen: false, modal: true });
            $(".bulk-add").bind("click", this.openBulkAddModal);
            $("#bulk-add").on("click", ".add-button", this.bulkAddEdit);
            $("#bulk-add").on("click", ".save", this.bulkAddSave);
            $("#bulk-add").on("click", ".predictive-typing-results li", this.bulkAddSave);
            $("#bulk-add").on("click", ".delete", this.bulkAddDelete);
            $("#bulk-add").on("click", ".cancel", this.bulkAddCancel);
            $("#bulk-add").on("focus", ".add-new-item", this.setInput);
            $(".define-scope").on("click", ".edit-row", this.editScope);
            $(".define-scope").on("click", ".delete-row", this.deleteScope);
            $(".define-scope").on("click", ".save-row", this.saveScope);
            $(".add-row").bind("click", this.addScope);
            $(".define-scope").on("click", ".cancel-row", this.cancelSaveScope);
            $("input").predictiveTyping({ parentContainer: "li" });
            $(".tabs li").removeClass("on");
            $(".tabs li").first().addClass("on");
            $(".cancel-add").bind("click", this.closeBulkAddModal);
            $(".create-combination").bind("click", this.createCombinations);
            $(".define-scope").on("change", ".edit input", this.updateSaveButton);
            $('#notification-modal').bind('dialogclose', function () {
                $(".ajax-spinner").hide();
            });
            $('.define-scope-table').on('change', '.recCheckbox', this.updateRecommendation);


        },
        setInput: function () {
            defineScope.currentInput = $(this).parents("li").first();
        },
        openBulkAddModal: function () {
            if (defineScope.editorOpened) {
                $("#notification-modal").children(".on").removeClass("on")
                    .end().find(".edit").addClass("on")
                    .end().dialog("open");
                return false;
            }
            $("#bulk-add").dialog('open');
        },
        closeBulkAddModal: function () {
            $("#bulk-add").dialog('close');
        },
        bulkAddEdit: function () {
            if (defineScope.bulkEditorOpened) {
                $("#notification-modal").children(".on").removeClass("on")
                    .end().find(".edit").addClass("on")
                    .end().dialog("open");
                return false;
            }
            $(this).parents("li").first().addClass('edit')
                .end().find(".add-new-item").focus();
            defineScope.bulkEditorOpened = true;
        },
        bulkAddSave: function () {
            var $parent = defineScope.currentInput,
                value = $(this).text(),
                key = $(this).attr("key"),
                template = $.template($('#bulk-add-item').html()),
                html = template({ 'value': value, 'key': key });
            $parent.find("input").val('');
            $parent.before(html).removeClass("edit")
                .parents("ul").first().find(".none").addClass("hidden");
            defineScope.bulkEditorOpened = false;
        },
        bulkAddCancel: function () {
            $(this).parents("li:eq(0)").removeClass('edit');
            defineScope.bulkEditorOpened = false;
        },
        bulkAddDelete: function () {
            var $parentUl = $(this).parents("ul").first(),
                count = $parentUl.find("li").size();
            if (count === 3) { //3 items mean one template one add button and only one item in the list
                $(this).parents("ul").find(".none").removeClass("hidden");
            }
            $(this).parents("li").first().remove();
        },
        editScope: function () {
            if (defineScope.editorOpened) {
                $("#notification-modal").children(".on").removeClass("on")
                    .end().find(".edit").addClass("on")
                    .end().dialog("open");
                return false;
            }
            defineScope.editorOpened = true;

            var row, template, itemHTML, isNew = false;
            row = $(this).parents("tr:eq(0)");
            defineScope.item = { recommendationId: row.attr("recommendationId"),
                species: row.find("td:eq(1)").text(), speciesId: row.find("td:eq(1)").attr("key"),
                location: row.find("td:eq(2)").html(), locationId: row.find("td:eq(2)").attr("key"),
                bow: row.find("td:eq(3)").html(), bodyOfWaterId: row.find("td:eq(3)").attr("key"),
                gear: row.find("td:eq(4)").html(), methodId: row.find("td:eq(4)").attr("key"),
                recommendation: row.find("td:eq(0)").find("input").attr("checked")
                //type: row.find("td:eq()").text(), typeId: row.find("td:eq(4)").attr("key")
            };

            template = $.template($('#edit-scope').html());
            itemHTML = template({ 'item': defineScope.item });

            row.before(itemHTML);
            $(".define-scope input").predictiveTyping();
            row.remove();
            $(".save-row").addClass("available"); //no changes
        },
        saveScope: function () {
            if (!$(this).hasClass("available")) {
                return false;
            }
            var row = $(this).parents("tr").first(),
                longName = row.find(".species").val(),
                lastIndex = longName.lastIndexOf("("),
                scienceName = longName.substr(lastIndex, longName.length),
                regularName = longName.substr(0, lastIndex),
                styledName = $.trim(regularName) + " <span class='scientific-name'>" + scienceName + "</span>",
                typeId = row.find("td:eq(0)").find("input").attr("checked") == "checked" ? 4 : 5,
                recommendationChanged = false;
            defineScope.keys = {
                recommendationId: row.attr("recommendationId"),
                speciesId: row.find(".species").attr("key"),
                locationId: row.find(".location").attr("key"),
                bodyOfWaterId: row.find(".bow").attr("key"),
                methodId: row.find(".gear").attr("key"),
                typeId: typeId
            };
            if (row.attr("recommendationId") === "0") {//new item 
                if (row.find("td:eq(0)").find("input").attr("checked") === "checked") {
                    recommendationChanged = true;
                }
            }
            else if (defineScope.item.recommendation != row.find("td:eq(0)").find("input").attr("checked")) {
                recommendationChanged = true;
            }
            defineScope.item = {
                recommendationId: row.attr("recommendationId"),
                species: row.find(".species").val(), speciesId: row.find(".species").attr("key"),
                location: row.find(".location").val(), locationId: row.find(".location").attr("key"),
                bow: row.find(".bow").val(), bodyOfWaterId: row.find(".bow").attr("key"),
                gear: row.find(".gear").val(), methodId: row.find(".gear").attr("key"),
                recommendation: row.find("td:eq(0)").find("input").attr("checked")
            };
            $.ajax({
                type: "POST",
                url: "/reports/" + defineScope.reportId + "/analysis/savescope",
                async: false,
                data: defineScope.keys,
                success: function (data) {
                    var template,
                        itemHTML;

                    if (data.Success === true) {
                        //add the row to the table
                        template = $.template($('#view-scope').html());
                        itemHTML = template({ 'item': defineScope.item });
                        row.before(itemHTML);
                        row.prev().find(".specie-name").html(styledName)
                            .end().attr("recommendationId", data.recId);
                        if (recommendationChanged) {
                            if (typeId === 4) {
                                row.prev().insertAfter($(".define-scope-table").find(".yes").last()).addClass("yes");
                            }
                            else {
                                row.prev().insertAfter($(".define-scope-table").find("tr").last()).removeClass("yes");
                            }
                        }
                        else if (typeId === 4) {
                            row.prev().addClass("yes");
                        }

                        defineScope.editorOpened = false;

                        row.remove();
                    }
                    else {
                        $("#notification-modal").children(".on").removeClass("on")
                            .end().find(".combination").addClass("on")
                            .end().dialog("open");
                    }
                }
            });

        },
        cancelSaveScope: function () {

            var row = $(this).parents("tr").eq(0);
            defineScope.editorOpened = false;
            if (row.attr("recommendationId") === "0") { //new element
                defineScope.cancelAddScope.apply(this);
                return;
            }

            var longName = row.find(".species").val(),
                lastIndex = longName.lastIndexOf("("),
                scienceName = longName.substr(lastIndex, longName.length),
                regularName = longName.substr(0, lastIndex),
                styledName = $.trim(regularName) + " <span class='scientific-name'>" + scienceName + "</span>",
                template = $.template($('#view-scope').html()),
                itemHTML = template({ 'item': defineScope.item });

            row.before(itemHTML);
            row.prev().find(".specie-name").html(styledName);
            row.remove();

        },
        deleteScope: function () {
            var row = $(this).parents("tr:eq(0)");
            //ajax call to delete a recommendation
            $.ajax({
                type: "POST",
                url: "/reports/" + defineScope.reportId + "/analysis/deletescope",
                async: false,
                data: "recommendationId=" + row.attr("recommendationId"),
                success: function (data) {
                    if (data.Success === true) {
                        row.remove();
                    }
                }
            });
        },
        addScope: function () {
            if (defineScope.editorOpened) {
                $("#notification-modal").children(".on").removeClass("on")
                    .end().find(".edit").addClass("on")
                    .end().dialog("open");
                return false;
            }
            defineScope.editorOpened = true;
            var template = $.template($('#add-scope').html()),
                itemHTML = template();
            $(".define-scope-table tbody").append(itemHTML);
            $(".define-scope input").predictiveTyping();
            window.scrollTo(0, document.body.scrollHeight);
        },
        cancelAddScope: function () {
            $(this).parents("tr:eq(0)").remove();
        },
        createCombinations: function () {
            //get the species, locations, bows, and methods into arrays

            //var recommendationTypeId = $(".recType").children(":selected").attr("id"),
            //checks to see if any of the fields are not filled out, none is always hidden when selected
            var notSelected = $("#bulk-add").find(".none").not(".hidden"),
                validated = true,
                species = [],
                locations = [],
                bodiesOfWater = [],
                methods = [];

            if (notSelected.length > 1) {
                validated = false;
            }

            if (notSelected.length === 1) { //if there are one field not fill out it has to be either location or bow
                if (notSelected.first().parent().attr("id") === "locationList" || notSelected.first().parent().attr("id") === "bowList") {
                    //do nothing
                }
                else {
                    validated = false;
                }
            }

            if (defineScope.bulkEditorOpened === true) {
                validated = false;
            }

            if ($(".predictive-typing-results").length > 0) {
                validated = false;
            }

            if (!validated) {
                $("#notification-modal").children(".on").removeClass("on")
                    .end().find(".type").addClass("on")
                    .end().dialog("open");
                return false;
            }

            $("#speciesList > li[key]").each(function () {
                species.push($(this).attr("key"));
            });

            $("#locationList > li[key]").each(function () {
                locations.push($(this).attr("key"));
            });

            $("#bowList > li[key]").each(function () {
                bodiesOfWater.push($(this).attr("key"));
            });

            $("#methodList > li[key]").each(function () {
                methods.push($(this).attr("key"));
            });
            //ajax call to create all combinations

            $(".ajax-spinner").show();
            $.ajax({
                type: "POST",
                url: "/reports/" + defineScope.reportId + "/analysis/createcombinations",
                async: true,
                data: { species: species, locations: locations, bodiesOfWater: bodiesOfWater, methods: methods },
                traditional: true,
                success: function (data) {
                    if (data.success === true) {
                        window.location.reload();
                    }
                    else {
                        $("#notification-modal").children(".on").removeClass("on")
                            .end().find(".combination").addClass("on")
                            .end().dialog("open");
                        $(".ajax-spinner").hide();
                    }
                }
            });

        },
        validateFields: function () {
            var $edit = $(".edit"),
                validated = true;

            if ($edit.find(".species").val() === "" || $edit.find(".gear").val() === "") {
                validated = false;
            }

            if ($edit.find(".location").val() === "" && $edit.find(".bow").val() === "") {
                validated = false;
            }

            return validated;
        },
        updateSaveButton: function () {
            if (defineScope.validateFields() === true) {
                $(".save-row").addClass("available");
            }
            else {
                $(".save-row").removeClass("available");
            }
        },
        updateRecommendation: function () {
            var row = $(this).parent().parent(),
                recommendationId = row.attr("recommendationId"),
                recommendation;

            if (row.hasClass("edit")) {
                return false;
            }

            //5 for no, 4 for yes
            if ($(this).attr("checked") == "checked") {
                recommendation = 4;
            }
            else {
                recommendation = 5;
            }
            if (recommendationId != 0) { //not new
                $.ajax({
                    type: "POST",
                    url: "/reports/" + defineScope.reportId + "/analysis/update-recommendation",
                    data: { id: defineScope.reportId, recommendationId: recommendationId, typeId: recommendation },
                    success: function (data) {
                        if (data.Success === true) {
                            if (data.typeId == 4) {
                                row.insertAfter($(".define-scope-table").find(".yes").last()).addClass("yes");
                            }
                            else {
                                row.insertAfter($(".define-scope-table").find("tr").last()).removeClass("yes");
                            }
                        }
                    }
                });
            }
            else {
                if (recommendation == 4) {
                    row.insertAfter($(".define-scope-table").find(".yes").last()).addClass("yes");
                }
                else {
                    row.insertAfter($(".define-scope-table").find("tr").last()).removeClass("yes");
                }
            }

        }
    };
    defineScope.init();
});