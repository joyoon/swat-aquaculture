﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var criteria2 = {
        init: function () {
            $(".guiding-principles .information-box a").bind("click", this.hideGuide)
            $(".guiding-principles .button-review").bind("click", this.showGuide)
            $(".toggle-all-species").live("click", this.toggleDetails)
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(1)").addClass("on")
            $(".criteria2-button").bind("click", this.submitForm);
            $(".criteria-tabs li").live("click", this.tabCriteria);
        },
        showGuide: function () {
            $(".guiding-principles .information-box").show();
            $(".guiding-principles .button-review").hide();
        },
        hideGuide: function () {
            $(".guiding-principles .information-box").hide();
            $(".guiding-principles .button-review").show();
        },
        toggleDetails: function (e) {
            var parent = $(this).parents("table:eq(0)");
            var $row = $(this).parent().parent();
            var parentTR;
            var $temp = $row.next();
            if ($(this).hasClass("on")) {//collapse
                $temp = $row;
                $(this).removeClass("on");
                while ($temp.hasClass("collapse")) {
                    $temp.hide();
                    $temp = $temp.next();
                }
                //parent.find(".collapse").hide();
                $(this).find("span:eq(0)").html("Show all other species<span></span>")
                parentTR = $(this).parents("tr:eq(0)");
                parentTR.prev("tr").show().find("td:eq(0)").append($(this))
            } else {
                //$temp = $row;
                parentTR = $(this).parents("tr:eq(0)");
                $(this).addClass("on");
                while ($temp.hasClass("collapse")) {
                    $temp.show();
                    $temp = $temp.next();
                }
                parentTR.next("tr").find("td:eq(0)").append($(this))
                parentTR.hide();
                $(this).find("span:eq(0)").html("Hide all other species<span></span>")
            }
        },
        submitForm: function () {
            $("#nextAction").val("Criteria3");
            $("#criteria2Form").submit();
        },
        tabCriteria: function () {
            var index = $(".criteria-tabs li").index($(this));
            $(".criteria-content").hide();
            $(".criteria-tabs li").removeClass("on");
            $(this).addClass("on")
            $(".criteria-content:eq(" + index + ")").show();
        }
    }
    criteria2.init()
});