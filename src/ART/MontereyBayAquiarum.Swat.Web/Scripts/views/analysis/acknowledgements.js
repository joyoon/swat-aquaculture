﻿/*global require*/
require(["utils/bootstrap.tinymce", , "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var acknowledgements = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(2)").addClass("on")
            $(".acknowledgements-button").bind("click", this.submitForm);
        },
        submitForm: function () {
            $("#nextAction").val("Appendix");
            $("#acknowledgementsForm").submit();
        }
    }
    acknowledgements.init();
});