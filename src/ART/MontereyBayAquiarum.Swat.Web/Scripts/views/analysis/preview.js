﻿/*global require*/
require(["jquery","utils/bootstrap.stockAnalysis" , "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var preview = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(3)").addClass("on")
        }
    }
    preview.init()
});