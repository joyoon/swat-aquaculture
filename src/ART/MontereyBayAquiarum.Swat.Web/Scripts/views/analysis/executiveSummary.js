﻿/*global require*/
require(["jquery", "jquery.tiny_mce","utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var executiveSummary = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on");
            $(".tabbed-wrapper .tabs li:eq(2)").addClass("on");
            $(".executive-summary-button").bind("click", this.submitForm);

            //this page requires taller editor
            $(".mceFullEditor").not(".initialized").not("span").tinymce({
                mode: "specific_textareas",
                theme: "advanced",
                plugins: "table, autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

                // Theme options
                theme_advanced_buttons1: "save,|pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,|,sub,sup,|, tablecontrols, code",
                theme_advanced_buttons2: "",
                width: "870px",
                height:"480",
                paste_auto_cleanup_on_paste: true,
                paste_remove_styles: true,
                paste_remove_styles_if_webkit: true,
                paste_strip_class_attributes: true,
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                theme_advanced_resizing_use_cookie: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: false
            });

            $(".mceEditor, .mceFullEditor").addClass("initialized");
        },
        submitForm: function () {
            $("#nextAction").val("Introduction");
            $("#executiveSummaryForm").submit();
        }
    }
    executiveSummary.init();
});