﻿/*global require*/
require(["jquery", "views/shared/answerModal", "utils/bootstrap.stockAnalysis"], function ($) {
    "use strict";
    var criteria1 = {
        init: function () {
            $(".guiding-principles .information-box a").bind("click", this.hideGuide);
            $(".guiding-principles .button-review").bind("click", this.showGuide);
            $(".tabbed-wrapper .tabs li").removeClass("on");
            $(".tabbed-wrapper .tabs li:eq(1)").addClass("on");
            $(".criteria1-button").bind("click", this.submitForm);
        },
        showGuide: function () {
            $(".guiding-principles .information-box").show();
            $(".guiding-principles .button-review").hide();
        },
        hideGuide: function () {
            $(".guiding-principles .information-box").hide();
            $(".guiding-principles .button-review").show();
        },
        submitForm: function () {
            $("#nextAction").val("Criteria2");
            $("#criteria1Form").submit();
        }
    }
    criteria1.init()
});