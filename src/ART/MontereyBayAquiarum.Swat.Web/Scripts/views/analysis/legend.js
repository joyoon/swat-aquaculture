﻿/*global require*/
require(["jquery", "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var legend = {
        init: function () {
            $(".legend .content li").hide();
            $(".legend .content li:eq(0)").show();
            $(".legend .tabs li").bind("click", this.showContent)
        },
        showContent: function (e) {
            var index = $(".legend .tabs li").index($(this));
            $(".legend .content li").hide();
            $(".legend .content li:eq(" + index + ")").show();
            $(".legend .tabs li").removeClass("on");
            $(this).addClass("on")
        }
    }
    legend.init()
});