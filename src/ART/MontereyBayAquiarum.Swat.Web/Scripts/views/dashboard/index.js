﻿/*global require*/
require(["jquery", "jquery.ui", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var dashboard = {
        init: function () {
            $(".tabbed-wrapper .tabs li").removeClass("on")
            $(".tabbed-wrapper .tabs li:eq(3)").addClass("on")
            this.checkNoReport();
            $("#notification-modal").dialog({
                width: 560,
                position: { my: "center top", at: "center bottom-200%", of: $(".dashboard-reports-container") }
            });
            $(".ui-dialog").css("top", "+=150");
            this.showDashboard();
        },
        showDashboard: function () {
            var type = this.getParameterByName("type")
            if (type == "analyst") {
                $(".report-table").show();
                $(".review-table").hide();
                $(".analysis-table").hide();
            } else if (type == "reviewer") {
                $(".report-table").hide();
                $(".review-table").show();
                $(".analysis-table").hide();
            } else if (type == "sfw") {
                $(".report-table").hide();
                $(".review-table").hide();
                $(".analysis-table").show();
            }
        },
        getParameterByName: function (name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        checkNoReport: function () {
            if ($("table").length === 0) {
                $(".dashboard-reports-container").append("<h1 class='no-reports'>You are currently not assigned to any report.</h1>");
            }
        }
    }
    dashboard.init()
});


