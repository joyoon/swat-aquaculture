﻿/*global require*/
require(["jquery", "jquery.validate", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var editReference = {
        init: function () {
            //this.validateForm();
            $(".submit").bind("click", this.submitForm)
            $(".update-link").bind("click", this.updateLink)
            $("#documentationType").bind("change", this.toggleDocumentInput)
            $("#author").bind("keyup", this.generateShortName);
            $("#year").bind("keyup", this.generateShortName);
            if ($("#referenceid").val() == 0) this.updateLink();
        },
        validateForm: function () {
            $("#reference-form").validate({
                errorPlacement: function (error, element) {
                    element.after(error);
                    element.parent("li").addClass("error")
                },
                success: function (element) {
                    element.parent("li").removeClass("error")
                },
                rules: {
                    shortname: "required",
                    bibliography: "required"
                },
                messages: {
                    shortname: "Please enter a shortname",
                    bibliography: "Please enter content for the bibliography"
                }
            });

        },
        submitForm: function (e) {
            // check if validation working ??
            e.preventDefault();
            if ($("#reference-form").valid()) {
            $("#reference-form").submit();
            }
        },
        toggleDocumentInput: function () {
            var selectedVal = $(this).val();
            if (selectedVal === "link") {
                $("#documentation").hide();
                $("#link").show();
            } else {
                $("#documentation").show();
                $("#link").hide();
            }
        },
        updateLink: function () {
            $(".edit-doc").show();
            $(".view-doc").hide();
        },
        generateShortName: function () {
            var shortName = $("#authors").val() + " " + $("#year").val();
            $("#shortname").val(shortName)
        }
    }
    editReference.init()
});