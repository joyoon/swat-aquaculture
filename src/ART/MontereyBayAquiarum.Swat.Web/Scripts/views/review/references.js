﻿/*global require*/
require(["jquery", "utils/bootstrap.comments", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var references = {
        init: function () {
            var numRows = $(".tablesorter tbody tr").size();
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter")
            .tablesorter();
            if (numRows > 0) {
                $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
            }
            $(".pagesize").change(this.updatePageSize);
            $(".tabs li:eq(4)").addClass("on");
            $(".search-reference").live("click", this.submitForm);
            $("#search-reference").live("keyup", function (event) {
                if (event.keyCode == 13) {
                    references.submitForm();
                }
            });
        },
        submitForm: function () {
            //$("#search-form").submit();
            var reportId = $(".search").attr("reportId");
            var searchTerm = $("#search-reference").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/references/search",
                data: { filter: searchTerm, context: "ReferenceList" },
                success: function (data) {
                    $("#reference-container").html(data);
                    $("#search-reference").val(searchTerm);
                    var numRows = $(".tablesorter tbody tr").size();
                    var pageSize = $("#pager").data().pagesize;
                    if (pageSize === '') {
                        pageSize = 10;
                    }
                    $(".tablesorter")
            .tablesorter();
                    if (numRows > 0) {
                        $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
                    }
                    $(".pagesize").change(references.updatePageSize);
                }
            });
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        }
    }
    references.init()
});