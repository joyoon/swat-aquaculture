﻿/*global require*/
require(["jquery", "utils/bootstrap.stockAnalysis", "utils/bootstrap.tinymce", "utils/bootstrap.comments", "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var criteria2 = {
        init: function () {
            $(".guiding-principles .information-box a").bind("click", this.hideGuide)
            $(".guiding-principles .button-review").bind("click", this.showGuide)
            $(".tabs li:eq(2)").addClass("on")
        },
        showGuide: function () {
            $(".guiding-principles .information-box").show();
            $(".guiding-principles .button-review").hide();
        },
        hideGuide: function () {
            $(".guiding-principles .information-box").hide();
            $(".guiding-principles .button-review").show();
        }
    }
    criteria2.init()
});