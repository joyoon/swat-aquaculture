﻿/*global require */
require(["jquery", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tabContainer"], function ($) {
    "use strict";
    console.log($.fn.tablesorter);
    var expertList = {
        init: function () {
            var numRows = $(".tablesorter tbody tr").size();
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter")
            .tablesorter();
            if (numRows > 0) {
                $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
            }
            $(".pagesize").change(this.updatePageSize);
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        }
    }
    expertList.init()
});