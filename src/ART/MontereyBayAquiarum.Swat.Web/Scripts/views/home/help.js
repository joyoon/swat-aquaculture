﻿/*global require */
require(["jquery", "jquery.ui"], function ($) {
    "use strict";
    // can use backbone and underscore now.
    var help = {
        init: function () {
            $("#video-modal").dialog({ width: 1000, autoOpen: false, resizable: false, modal: true });
            $(".video-link").click(this.openModal);
            $('#video-modal').bind('dialogclose', this.stopVideo);
        },
        openModal: function () {
            $("#video-modal iframe").attr("src", $(this).data("video-link"));
            $("#video-modal").dialog("open");
            $(".ui-dialog-title").text($(this).text());
        },
        stopVideo: function () {
            $("#video-modal iframe").attr("src", "");
        }
    };
    $(function () {
        help.init();
    });
});