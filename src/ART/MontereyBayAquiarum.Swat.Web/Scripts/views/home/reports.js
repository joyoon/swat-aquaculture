﻿/*global require */
require(["jquery", "backbone", "underscore", "jquery.validate", "utils/bootstrap.tabContainer"], function ($) {
    "use strict";
    // can use backbone and underscore now.
    var reports = {
        init: function () {
            $(".button-review").click(this.submitForm);
        },
        submitForm: function () {
            $("#search-form").submit();
        }

    }

    reports.init()
});