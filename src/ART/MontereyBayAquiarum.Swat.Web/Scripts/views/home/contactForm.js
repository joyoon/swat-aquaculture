﻿/*global require */
require(["jquery", "backbone", "underscore", "jquery.validate", "utils/bootstrap.tabContainer"], function ($) {
    "use strict";
    // can use backbone and underscore now.
    var contactForm = {
        init: function () {
            this.validateForm();
            $(".submit").bind("click", this.submitForm);
            $(".attach").bind("click", this.attachFile);
            $(".file").live("change", this.attachAnotherFile);
        },
        validateForm: function () {
            $("#contact-form").validate({
                errorPlacement: function (error, element) {
                    element.after(error);
                    element.parent("li").addClass("error")
                },
                success: function (element) {
                    element.parent("li").removeClass("error")
                },
                rules: {
                    firstname: "required",
                    lastname: "required",
                    affiliation: "required",
                    title: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    comments: "required"
                },
                messages: {
                    firstname: "Please enter your firstname",
                    lastname: "Please enter your lastname",
                    affiliation: "Please enter your affiliation",
                    title: "Please enter your title",
                    email: "Please enter a valid email address",
                    comments: "Please enter your comments"
                }
            });

        },
        submitForm: function () {
            if ($("#contact-form").valid()) {
                $("#contact-form").submit();
            }
        },
        attachFile: function () {
            $(".attach-file").show();
            $(".attach").hide();
        },
        attachAnotherFile: function () {
            var $attachfile = $(".attach-file");
            var count = $attachfile.data().count;
            count++;
            $attachfile.data("count", count);
            $attachfile.append("<input title='' type='file' class='file' name='files[" + count + "]'>");
        }
    }

    contactForm.init();
});