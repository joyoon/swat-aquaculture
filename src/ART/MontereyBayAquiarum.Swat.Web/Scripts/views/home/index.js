﻿/*global require */
require(["jquery", "jquery.validate", "utils/bootstrap.tabContainer"], function ($) {
    "use strict";
    // can use backbone and underscore now.
    var home = {
        init: function () {
            this.validateForm();
            $(".login-button").bind("click", this.submitForm);
            $("#password").keyup(this.pressedEnter);
        },
        validateForm: function () {
            $("#login").validate({
                errorPlacement: function (error, element) {
                    element.parents("ul").find(".login-validation").append(error).show();
                    element.parent("li").addClass("error");
                },
                success: function (element) {
                    if (element.parents("ul").find(".error").size() === 0) {
                        element.parents("ul").find(".login-validation").hide();
                    }
                    element.parent("li").removeClass("error");
                },
                rules: {
                    username: "required",
                    password: "required"
                },
                messages: {
                    username: "Please enter your username",
                    password: "Please enter your password"
                }
            });
        },
        submitForm: function () {
            if ($("#login").valid()) {
                $("#login").submit();
            }
        },
        pressedEnter: function (e) {
            if (e.keyCode === 13) {
                home.submitForm();
            }
        }
    };

    home.init();
});