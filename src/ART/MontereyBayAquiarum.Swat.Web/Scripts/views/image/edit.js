﻿/*global require*/
require(["jquery", "jquery.validate", "utils/bootstrap.tabContainer", "jquery.tiny_mce", "utils/bootstrap.tinymce", "utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var editImage = {
        init: function () {
            //this.validateForm();
            $(".submit").bind("click", this.submitForm)
            $(".update-link").bind("click", this.updateLink)
            if ($("#imageId").val() == 0) this.updateLink();
            $(".mceEditor").tinymce({
                script_url: '../../tiny_mce/tiny_mce.js',
                mode: "specific_textareas",
                theme: "advanced",
                plugins: "lists,spellchecker,pagebreak,table,save,advhr,advlink,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking",
                // Theme options
                theme_advanced_buttons1: "pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull|,sub,sup, code",
                theme_advanced_buttons2: "",
                setup: function (ed) {
                    ed.onKeyUp.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                    ed.onClick.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                },
                width: "870px",
                paste_auto_cleanup_on_paste: true,
                paste_remove_styles: true,
                paste_remove_styles_if_webkit: true,
                paste_strip_class_attributes: true,

                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                theme_advanced_resizing_use_cookie: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: false
            });
        },
        validateForm: function () {
            $("#reference-form").validate({
                errorPlacement: function (error, element) {
                    element.after(error);
                    element.parent("li").addClass("error")
                },
                success: function (element) {
                    element.parent("li").removeClass("error")
                },
                rules: {
                    shortname: "required",
                    bibliography: "required"
                },
                messages: {
                    shortname: "Please enter a shortname",
                    bibliography: "Please enter content for the bibliography"
                }
            });

        },
        submitForm: function (e) {
            e.preventDefault();
            $("#description").val(tinyMCE.activeEditor.getContent());
            if ($("#reference-form").valid()) {
                $("#reference-form").submit();
            }
        },
        updateLink: function () {
            $(".edit-doc").show();
            $(".view-doc").hide();
        }
    }
    editImage.init()
});