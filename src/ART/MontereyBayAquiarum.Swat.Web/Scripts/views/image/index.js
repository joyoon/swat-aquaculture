﻿/* called by views/reference/index.cshtml */
/*global require */
require(["jquery", "jquery.ui", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tabContainer", "utils/bootstrap.stockAnalysis"], function ($) {
    "use strict";
    var imageList = {
        init: function () {
            var numRows = $(".tablesorter tbody tr").size();
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter")
            .tablesorter();
            if (numRows > 0) {
                $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
            }
            $(".pagesize").change(this.updatePageSize);

            $(".bulk-add").live("click", this.searchreferences);
            $(".delete-row").live("click", this.deleteReference);
            $("#delete-modal").dialog({ autoOpen: false, width: 300 });
            $("#delete-modal").find(".no").live("click", this.closeDeleteModal);
            $("#delete-modal").find(".yes").live("click", this.confirmDelete);
            $(".search-image").live("click", this.submitForm);
            $("#search-image").live("keyup", function (event) {
                if (event.keyCode == 13) {
                    imageList.submitForm();
                }
            });
            var currentTr;
        },
        searchreferences: function () {
            //var url = window.location.toString();
            // strip any previous/leftover search string from end of references url and append new search terms, length property to obviate various char-encodings
            //window.location = url.substr(0, url.indexOf('\/references') + '\/references'.length) + '/' + $("#search-reference").val();
        },
        deleteReference: function () {
            var deleteUrl = $(this).attr("href");
            imageList.currentTr = $(this).parent().parent();
            $("#delete-modal").dialog("open").data("deleteUrl", deleteUrl);
            return false;
        },
        closeDeleteModal: function () {
            $("#delete-modal").dialog("close");
        },
        confirmDelete: function () {
            console.log($("#delete-modal").data().deleteUrl);
            $.ajax({
                type: "POST",
                url: $("#delete-modal").data().deleteUrl,
                async: true,
                success: function (data) {
                    $("#delete-modal").dialog('close');
                    imageList.currentTr.slideUp("slow");
                }
            });
            //delete logic will go here after it's implemented in the backend.
        },
        submitForm: function () {
            //$("#search-form").submit();
            var reportId = $(".search").attr("reportId");
            var searchTerm = $("#search-image").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/images/search",
                data: { filter: searchTerm, context: "ImageList" },
                success: function (data) {
                    $("#reference-container").html(data);
                    $("#search-image").val(searchTerm);
                    var numRows = $(".tablesorter tbody tr").size();
                    var pageSize = $("#pager").data().pagesize;
                    if (pageSize === '') {
                        pageSize = 10;
                    }
                    $(".tablesorter")
            .tablesorter();
                    if (numRows > 0) {
                        $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
                    }
                    $(".pagesize").change(imageList.updatePageSize);
                }
            });
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        }

    }
    imageList.init()
});