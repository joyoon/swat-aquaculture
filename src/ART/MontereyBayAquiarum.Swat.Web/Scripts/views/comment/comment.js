var comment = {
    init: function () {

        var comment = {
            content: $("#content").html(),
            commentId: $("#commentId").html(),
            newPath: null, //init as null
            newFileName: $("#newFileName").text() 
        };
        if ($("#newPath").length > 0) {
            comment.newPath = $("#newPath").text()
        }
        parent.comments.saveCommentSuccess(comment);
    }
};
