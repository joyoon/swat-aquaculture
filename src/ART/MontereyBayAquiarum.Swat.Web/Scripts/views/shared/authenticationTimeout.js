﻿/*global require*/
define(["jquery", "jquery.ui"], function ($) {
    "use strict";
    var authenticationTimeout = {
        warningTime: 603900000, //15mins = 900,000
        logoutTime: 604800000, //20min = 1,600,000
        fifteenTimeout: null,
        twentyTimeout: null,
        init: function () {
            $("#timeout-modal").dialog({ autoOpen: false, modal: true, resizable: false, width: 535, closeOnEscape: false,
                open: function (event, ui) {
                    $(this).parent().find(".ui-dialog-titlebar-close").hide(); 
              } 
            }).show();
            $("#resetTimeout").click(this.resetTimeouts);
            this.initTimeouts();
        },
        showReauthentication: function () {
            $("#timeout-modal").dialog("open");
        },
        kickUserOut: function () {
            window.location = "/logout";
        },
        resetTimeouts: function () {
            $.post("/home/extendtimeout", function () {
                window.clearTimeout(authenticationTimeout.fifteenTimeout);
                window.clearTimeout(authenticationTimeout.twentyTimeout);
                $("#timeout-modal").dialog("close");
                authenticationTimeout.initTimeouts();
            });
        },
        initTimeouts: function () {
            authenticationTimeout.fifteenTimeout = window.setTimeout(this.showReauthentication, this.warningTime);
            authenticationTimeout.twentyTimeout = window.setTimeout(this.kickUserOut, this.logoutTime);
        }
    };
    authenticationTimeout.init();
});