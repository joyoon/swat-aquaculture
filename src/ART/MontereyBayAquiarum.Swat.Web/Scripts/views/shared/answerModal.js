﻿/*global require*/
var answerModal;
define(["jquery", "jquery.ui", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tabContainer", "utils/bootstrap.comments", "jquery.tiny_mce", "utils/bootstrap.tinymce", "jquery.template", "custom-form-elements"], function ($) {
    "use strict";
    answerModal = {
        currentFactorTypeId: 0,
        reportId: 0,
        currentFactorId: 0, //currentFactorId = answerId
        currentRecId: 0,
        currentCell: undefined,
        callingPage: "",
        currentReference: "",
        currentOpenTables: [],
        init: function () {
            if ($("#callingPage") != null) answerModal.callingPage = $("#callingPage").html();
            answerModal.reportId = $("#answer-modal").attr("reportId");
            $("#factor").attr("disabled", "true")
            $("#answer-modal").dialog({ autoOpen: false, modal: true, resizable: false, title: "Cod : North : Trawl : Inherent Resillience", width: 920, height: 630 });

            answerModal.initTableSorter();
            $(".pagesize").change(this.updatePageSize);

            $(".risk-factor").live("click", this.showAnswerModalWithDetail);
            $("#answer-modal .tabs li").bind("click", this.showModalContent)
            $(".select-different-answer").live("click", this.showAnswerList)
            //$(".answer-amount").html(numRows + " Answers")
            $("#answer-list .answer-info a").live("click", this.toggleDetails)
            $("#answer-list tr").live("click", this.replaceAnswer);
            $(".edit-answer").live("click", this.editAnswer);
            $(".add-answer").live("click", this.newAnswer);
            $(".document-insert").live("click", this.showReferenceList);
            $("#answer-modal").on("click", ".image-insert", this.showImageList);
            $(".image-insert").click(this.showImageList); //for standalone page
            $(".save-area").live("click", this.saveAnswer)
            $(".scoring-legend").bind("click", this.showScoringLegend);
            $("#answer-modal .save-area.submit").bind("click", this.showAnswer);
            $(".add-score").live("click", this.showAnswerModalWithNew);
            $("#factor").live("change", this.populateScores);
            $(".select-answer").live("click", this.selectAnswer);
            $(".goToAnswerList").live("click", this.showAnswerList);
            $(".search-reference").live("click", this.searchReference);
            $("#answer-modal").on("click", ".search-image", this.searchImage)
                .on("keyup", "#search-image", function (event) {
                    if (event.keyCode == 13) { //enter key
                        answerModal.searchImage();
                    }
                });
            $("#search-reference").live("keyup", function (event) {
                if (event.keyCode == 13) { //enter key
                    answerModal.searchReference();
                }
            });
            $(".search-answer").live("click", this.searchAnswer);
            $("#search-answer").live("keyup", function (event) {
                if (event.keyCode == 13) { //enter key
                    answerModal.searchAnswer();
                }
            });
            $("#insert-reference").live("click", this.insertReference);
            $("#answer-modal .images").on("click", "#insert-image", this.insertImage);
            $(".cancel-add-reference").live("click", this.cancelAddReference);
            $("#answer-modal .images").on("click", ".cancel-add-image", this.cancelAddImage);
            $(".cancel-modal").live("click", function () { $("#answer-modal").dialog("close"); return false; });
            window.onbeforeunload = this.navigateToAnotherPageCheck;
            $("#factor").live("change", this.populateHelp);
            $("#answer-modal").on("click", "#go-back", this.showAnswer);
            $.ajaxSetup({
                // Disable caching of AJAX responses for IE in tinyMCE
                cache: false
            });

        },
        showAnswerModalWithDetail: function () {
            if ($(this).hasClass("not-editable")) return false;
            answerModal.currentFactorTypeId = $(this).attr("factorTypeId");
            answerModal.currentRecId = $(this).parents(".rec").attr("id");
            answerModal.currentFactorId = $(this).attr("factorId");
            answerModal.currentCell = $(this).parents("td:eq(0)");
            var factor = $(this).data().factor;
            var method = $(this).data().method;
            var region = $(this).data().region;
            var specie = $(this).data().specie;
            //criterion 4 does not have species
            if (specie !== "")
                specie += " : ";
            var answerCode = $(this).parent().find(".risk-factor-popup").find(".popup-head").text();
            $("#answer-modal").find("#modal-title").text(answerCode)
                .end().find(".tabs, .tab-rule, .details, .select-different-answer").show() //cleanup here to reset modal back to default state before displaying
                .end().find(".select-different-answer").css("top", "-75px")
                .end().find(".comments").removeClass("on")
                .end().find(".tabs").find(".on").removeClass("on")
                        .end().find("li").first().addClass("on");
            $(".references").hide();
            $(".images").hide();
            $("#answer-modal").dialog("option", "title", specie + region + " : " + method + " : " + factor);
            $("#answer-modal").dialog("open");
            answerModal.showAnswer();
            answerModal.loadComments();
        },
        closeModal: function () {
            $("#answer-modal").dialog("close");
        },
        showAnswer: function () {
            //$("#answer-modal .details").show();
            $("#answer-modal .details").load("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/detail?callingPage=" + answerModal.callingPage, function () {
                var $answerModal = $("#answer-modal");
                $answerModal.find(".tabs").css("display", "block");
            });
            //$(".answer-form").hide();
        },
        loadComments: function () {
            var $comments = $("#answer-modal .comments");
            $comments.load("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/comments", function () {
                var commentCount = $(this).find(".comment").attr("commentCount");
                $("#count").text("(" + commentCount + ")");
            });
        },
        showModalContent: function () {
            $("#answer-modal .tabs li").removeClass("on")
            $("#answer-modal .content").removeClass("on").removeAttr("style")
            var index = $("#answer-modal .tabs li").index($(this))
            $(this).addClass("on")
            $("#answer-modal .content:eq(" + index + ")").addClass("on")
        },
        showAnswerList: function () {
            var $answerModal = $("#answer-modal");
            $answerModal.find(".details").load("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorTypeId, function () {
                answerModal.initTableSorter();
            });
            //$answerModal.find(".tabs").hide();
            $("#modal-title").text("Select an Answer");
            $answerModal.find(".select-different-answer").hide();

            //$(".answer-list").show();
            return false; //dont follow link
        },
        showAnswerModalWithList: function () {
            answerModal.currentFactorTypeId = $(this).attr("factorTypeId");
            answerModal.currentRecId = $(this).parents(".rec").attr("id");
            answerModal.currentCell = $(this).parents("td:eq(0)");
            var factor = $(this).data().factor;
            var method = $(this).data().method;
            var region = $(this).data().region;
            var specie = $(this).data().specie;
            //criterion 4 does not have species
            if (specie !== "")
                specie += " : ";
            //var answerCode = $(this).parent().find(".risk-factor-popup").find(".popup-head").text();
            //$("#answer-modal").find("h2").text(answerCode);

            $("#answer-modal").dialog("option", "title", specie + region + " : " + method + " : " + factor);
            $("#answer-modal").dialog("open");
            $("#answer-modal").find(".comments").removeClass("on")
                .end().find(".details").addClass("on");
            answerModal.showAnswerList();
        },
        showAnswerModalWithNew: function () {
            answerModal.currentFactorTypeId = $(this).attr("factorTypeId");
            answerModal.currentRecId = $(this).parents(".rec").attr("id");
            answerModal.currentCell = $(this).parents("td:eq(0)");
            var factor = $(this).data().factor;
            var method = $(this).data().method;
            var region = $(this).data().region;
            var specie = $(this).data().specie;
            //criterion 4 does not have species
            if (specie !== "")
                specie += " : ";
            //var answerCode = $(this).parent().find(".risk-factor-popup").find(".popup-head").text();
            //$("#answer-modal").find("h2").text(answerCode);

            $("#answer-modal").dialog("option", "title", specie + region + " : " + method + " : " + factor);
            $("#answer-modal").dialog("open");
            $("#answer-modal").find(".comments").removeClass("on")
                .end().find(".details").addClass("on");
            answerModal.newAnswer();
        },
        toggleDetails: function () {
            if ($(this).hasClass("collapse")) {
                $(this).parents("td:eq(0)").find(".detail-rationale").hide();
                $(this).removeClass("collapse")
                $(this).html("Show More")
            } else {
                $(this).parents("td:eq(0)").find(".detail-rationale").show();
                $(this).addClass("collapse")
                $(this).html("Show Less")
            }
        },
        replaceAnswer: function () {
            //set the current factorID to the one that was selected
            var $row = $(this);
            var title = $row.find(".left-cap").find("a").text() + " : " + $row.find("td").eq(1).text();
            answerModal.currentFactorId = $(this).find(".factor-id").attr("factorId");
            answerModal.showAnswer();
            $("#modal-title").text(title);
            $("#answer-modal").find(".select-different-answer").css("top", "-45px");

        },
        editAnswer: function () {

            var url = document.URL; //just looking for criteria2
            var criteria = url.substr(url.lastIndexOf('/') + 1, 9);
            $("#answer-modal .details").load("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/editanswer/" + answerModal.currentFactorTypeId + "/" + criteria, answerModal.tinyMCEInit);
            $("#reference-form.details").show();
        },
        newAnswer: function () {

            answerModal.currentFactorId = 0;
            $("#modal-title").text("Create a New Answer");
            $("#answer-modal .details").load("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/editanswer/" + answerModal.currentFactorTypeId, function () {
                answerModal.tinyMCEInit();
                $("#answer-modal .select-different-answer").show();
                $("#answer-modal .cancel-area").addClass("goToAnswerList");
                answerModal.populateScores();
            });
            $("#reference-form.details").show();
            return false; // don't travel with the link
        },
        showReferenceList: function () {
            $(".references").show();
            $(".details, #answer-modal .tabs, .tab-rule").hide();
            $(".answer-header").hide();
            answerModal.currentReference = $(this).nextAll("textarea").first().attr("name");
        },
        showImageList: function () {
            $(".images").show();
            $(".details,#answer-modal .tabs, .tab-rule").hide();
            $(".answer-header").hide();
            answerModal.currentReference = $(this).nextAll("textarea").first().attr("name");
        },
        cancelAddReference: function () {
            $(".references").hide()
            $(".details, .tabs, .tab-rule").show();
            return false;
        },
        cancelAddImage: function () {
            $(".images").hide()
            $(".details, .tabs, .tab-rule").show();
            return false;
        },
        insertReference: function () {
            if ($("#answer-modal").hasClass("reference")) {
                return false; // on a page where reference should not insert to modal, let that page handle this function
            }
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                textToInsert += "{" + $(this).parents("tr:eq(0)").find("td:eq(3)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = answerModal.currentReference,
                editor = tinyMCE.get(currentReference);
            editor.focus();
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content, { format: 'raw' });
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $(".references").hide();
            $(".details, .tabs, .tab-rule").show();
        },
        insertImage: function () {
            if ($("#answer-modal").hasClass("reference")) {
                return false; // on a page where reference should not insert to modal, let that page handle this function
            }
            var textToInsert = "";
            $(".tablesorter").find("input:checked").each(function () {
                var id = $(this).closest("tr").data("image-id");
                textToInsert += "{IMG-" + id + ": " + $(this).parents("tr:eq(0)").find("td:eq(2)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = answerModal.currentReference,
                editor = tinyMCE.get(currentReference);
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $(".images").hide();
            $(".details, .tabs, .tab-rule").show();
        },
        saveAnswer: function () {
            //get all data
            var title, factorTypeId, scoreId, keyInfo, rationale;
            title = $("#title").val();
            factorTypeId = $("#factor").val();
            scoreId = $("#score").val();
            keyInfo = tinyMCE.get('keyInfo').getContent();
            rationale = tinyMCE.get('rationale').getContent();
            //post all data to the server
            var async = true;
            if ($(window.location.pathname.split("/")).last()[0] === "criteria3") {
                async = false;
            }
            $.ajax({
                type: "POST",
                url: "/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/editanswer/" + answerModal.currentFactorTypeId,
                async: async,
                data: { title: title, factorTypeId: factorTypeId, scoreId: scoreId, keyInfo: keyInfo, rationale: rationale },
                success: function (data) {
                    if (data.Success == true) {
                        //load the answer detail display
                        var modalTitle = data.Label + ": " + data.Title;
                        $("#modal-title").text(modalTitle);
                        answerModal.currentFactorId = data.FactorId;
                        answerModal.showAnswer();
                    }
                }
            });
            //$(".answer-form").hide();
            //$("#answer-modal .details").show();
        },
        populateScores: function () {
            var factorTypeId = $("#factor").val();
            var score = $("#score");
            //ajax get call to get all scores for the selected factortype
            $.getJSON("/scores/" + factorTypeId, function (data) {
                //delete all previous scores
                var scoreInnerHtml = "";
                $("#score option[value!='0']").remove();
                $.each(data, function (key, item) {
                    scoreInnerHtml += "<option value='" + item.Key.toString() + "'>" + item.Value.toString() + "</option>";
                });
                score.append(scoreInnerHtml);
            });
        },
        selectAnswer: function () {
            //find out if the selection is for a specific recommendation or for a location/body of water/method
            var ids = answerModal.currentRecId.split('|');
            var index;
            var applyStr = "";
            var $applyAll = $(this).prev();
            var that = this;
            if ($applyAll.hasClass("stockhealth") && $applyAll.find("input").is(":checked")) {
                applyStr = "&applyAll=true";
            }
            if ($(".criteria-tabs").length > 0) {
                index = $(".criteria-tabs li").index($(".criteria-tabs li.on"));
            }
            if (ids.length == 1) {
                $(".ajax-spinner").show();
                answerModal.noteOpenTables();
                //ajax post to save the selected answer for a specific recommendation
                $.post("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/select",
                    "recommendationId=" + answerModal.currentRecId + applyStr, function (data) {

                        $(".parentTable").load($(".parentTable").attr("url"), function () {
                            $(".ajax-spinner").hide();
                            answerModal.reOpenTables();
                            if ($(".criteria-tabs").length > 0) {
                                $(".criteria-tabs").find("li").eq(index).click();
                            }
                            $("#answer-modal").dialog("close");
                        });

                    });
            } else {
                //ajax post to save the selected answer for a location/body of water/method combination
                $(".ajax-spinner").show();
                $.post("/reports/" + answerModal.reportId + "/answers/" + answerModal.currentFactorId + "/selectformultiplerecs",
                    "locationId=" + ids[0] + "&bodyOfWaterId=" + ids[1] + "&methodId=" + ids[2] +
                    "&factorTypeId=" + answerModal.currentFactorTypeId, function (data) {
                        $(".parentTable").load($(".parentTable").attr("url"), function () {
                            $(".ajax-spinner").hide();
                            answerModal.reOpenTables();
                            if ($(".criteria-tabs").length > 0) {
                                $(".criteria-tabs").find("li").eq(index).click();
                            }
                            $("#answer-modal").dialog("close");
                        });
                    });
            }
        },
        populateAnswer: function (data) {
            //populate the table cell with the appropriate answer
            var criteria = {
                factorId: data.Id,
                factorTypeId: data.FactorTypeId,
                score: data.FactorScore,
                scoreDescription: data.FactorScoreDescription,
                label: data.Label,
                commentCount: 0,  //TODO: Implement the comment count here
                title: data.Title,
                keyInfo: data.KeyInfo
            };
            var template = $.template($('#risk-factor').html());
            var html = template({ 'criteria': criteria });
            answerModal.currentCell.html(html);
            $("#answer-modal").dialog("close");
        },
        showScoringLegend: function () {
            window.open("../../../legend", "mywindow", "location=0,status=0,scrollbars=1, width=850,height=500");
        },
        tinyMCEInit: function () {
            $(".mceFullEditor").not(".initialized").not("span").tinymce({

                script_url: '../../tiny_mce/tiny_mce.js',
                mode: "specific_textareas",
                theme: "advanced",
                plugins: "table, autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                setup: function (ed) {
                    ed.onKeyUp.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                    ed.onClick.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                },
                // Theme options
                theme_advanced_buttons1: "pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,|,sub,sup,|, tablecontrols, code",
                theme_advanced_buttons2: "",
                width: "870px",
                paste_auto_cleanup_on_paste: true,
                paste_remove_styles: true,
                paste_remove_styles_if_webkit: true,
                paste_strip_class_attributes: true,
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                theme_advanced_resizing_use_cookie: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: false
            });
            $(".mceEditor").not(".initialized").not("span").tinymce({
                script_url: '../../tiny_mce/tiny_mce.js',
                mode: "specific_textareas",
                theme: "advanced",
                plugins: "lists,spellchecker,pagebreak,table,save,advhr,advlink,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking",
                // Theme options
                theme_advanced_buttons1: "pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull|,sub,sup, code",
                theme_advanced_buttons2: "",
                setup: function (ed) {
                    ed.onKeyUp.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                    ed.onClick.add(function (ed, e) {
                        tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                        tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                        tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
                    });
                },
                width: "870px",
                paste_auto_cleanup_on_paste: true,
                paste_remove_styles: true,
                paste_remove_styles_if_webkit: true,
                paste_strip_class_attributes: true,

                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                theme_advanced_resizing_use_cookie: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: false
            });

            $(".mceEditor, .mceFullEditor").addClass("initialized");
        },
        searchReference: function () {
            var reportId = $(".search").attr("reportId");
            var searchTerm = $("#search-reference").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/references/search",
                data: { filter: searchTerm, context: "Modal" },
                success: function (data) {
                    $("#reference-container").html(data);
                    $("#search-reference").val(searchTerm);
                    answerModal.initTableSorter();
                }
            });
        },
        searchImage: function () {
            var reportId = $(".search").attr("reportId");
            var searchTerm = $("#search-image").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/images/search",
                data: { filter: searchTerm, context: "Modal" },
                success: function (data) {
                    $("#image-container").html(data);
                    $("#search-image").val(searchTerm);
                    answerModal.initTableSorter();
                }
            });
        },
        searchAnswer: function () {
            var reportId = $(".search").attr("reportId");
            var factorTypeId = $(".search").attr("factorTypeId");
            var searchTerm = $("#search-answer").val();
            $.ajax({
                type: "POST", //reports/{id}/answers/{factorTypeId}
                url: "/reports/" + reportId + "/answers/" + factorTypeId,
                data: { filter: searchTerm },
                success: function (data) {
                    $(".details").html(data); //using details instead of "#answer-list" here because inside modal, thats where the list get populated per the original code
                    $("#search-answer").val(searchTerm);
                    answerModal.initTableSorter();
                }
            });
        },
        navigateToAnotherPageCheck: function (e) {
            if ($("#answer-modal").parent().css("display") === "block") {
                return "Any unsaved data may be lost.";
            }
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        },
        noteOpenTables: function () {
            var $stockAnalysis = $(".stock-analysis"),
                numberOfTables = $stockAnalysis.length;
            answerModal.currentOpenTables = [];
            for (var x = 0; x < numberOfTables; x++) {
                if ($stockAnalysis.eq(x).find(".click-to-collapse").hasClass("on")) {
                    answerModal.currentOpenTables.push(1);
                }
                else {
                    answerModal.currentOpenTables.push(0);
                }
            }
        },
        reOpenTables: function () {
            var $stockAnalysis = $(".stock-analysis"),
                numberOfTables = $stockAnalysis.length,
                $clickToCollapse = $stockAnalysis.find(".click-to-collapse");
            for (var x = 0; x < numberOfTables; x++) {
                if (answerModal.currentOpenTables[x] == 1) {
                    $clickToCollapse.eq(x).click();
                }
            }
        },
        populateHelp: function () {
            var factorId = $(this).val();
            $("#factor-instruction-container").load("/analysis/factor-instruction/Help/" + factorId, function () {
                $(".guiding-principles").show();
            });
        },
        initTableSorter: function () {
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter").each(function () {
                var numRows = $(this).find("tbody tr").size();
                $(this).tablesorter();
                if (numRows > 0) {
                    $(this).tablesorterPager({ container: $(this).prev(), positionFixed: false, seperator: " of ", size: pageSize });
                }
            });
        }
    };
    answerModal.init();
});