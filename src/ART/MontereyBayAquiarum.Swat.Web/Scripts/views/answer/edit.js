﻿/*global require*/
require(["jquery", "jquery.ui", "jquery.validate", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tinymce", "utils/bootstrap.tabContainer"], function () {
    "use strict";
    var editAnswer = {
        init: function () {
            var numRows = $(".tablesorter tbody tr").size();
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter")
            .tablesorter();
            if (numRows > 0) {
                $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
            }
            $(".pagesize").change(this.updatePageSize);

            $(".submit").bind("click", this.submitForm)
            $(".document-insert").bind("click", this.showReferenceList);
            $(".image-insert").click(this.showImageList);
            $("#insert-reference").live("click", this.insertReference);
            $("#image-modal").on("click", "#insert-image", this.insertImage);
            $(".cancel-add-reference").live("click", this.closeModal);
            $("#factor").live("change", this.populateScores);
            $("#factor").change(this.populateHelp);
            $(".search-reference").live("click", this.searchReference);
            $("#image-modal").on("click", ".search-image", this.searchImage);
            $("#search-reference").live("keyup", function (event) {
                if (event.keyCode == 13) { //enter key
                    editAnswer.searchReference();
                }
            });
            $("#image-modal").on("keyup", "#search-image", function (event) {
                if (event.keyCode == 13) { //enter key
                    editAnswer.searchImage();
                }
            });
            $('form').bind('submit', function () {
                $(this).find(':input').removeAttr('disabled'); //the disabled attribute prevents the value from getting submitted, remove before submit
            });

            $("#reference-modal").dialog({ autoOpen: false, width: 920, modal: true, height: 630 });
            $("#image-modal").dialog({ autoOpen: false, width: 920, modal: true, height: 630 });
            $(".guiding-principles .information-box .hide-information-box").live("click", this.hideGuide);
            $(".guiding-principles .button-review").live("click", this.showGuide);

            if ($("#score").find("option").length === 1) //new answer
            {
                $("#factor").parent().show();
            }
            $("#validation-modal").dialog({ autoOpen: false });
            //code here is to address reusing the same answer modal
            $("#image-modal, #reference-modal").addClass("reference");
        },
        validForm: function () {
            var valid = true;
            if ($("#score").val() === "0") {
                valid = false;
            }
            return valid;
        },
        submitForm: function () {
            if (editAnswer.validForm()) {
                $("#reference-form").submit();
                // window.location = "../../answers";
            }
            else {
                $("#validation-modal").dialog("open");
            }
        },
        showReferenceList: function () {
            $("#reference-modal").dialog("open");
            editAnswer.currentReference = $(this).siblings("textarea").attr("name");
        },
        showImageList: function () {
            $("#image-modal").dialog("open");
            editAnswer.currentReference = $(this).siblings("textarea").attr("name");
        },
        insertReference: function () {
            var textToInsert = "";
            $("#reference-modal .tablesorter").find("input:checked").each(function () {
                textToInsert += "{" + $(this).parents("tr:eq(0)").find("td:eq(3)").html() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = editAnswer.currentReference,
                editor = tinyMCE.get(currentReference);
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#reference-modal").dialog("close");
        },
        insertImage: function () {
            var textToInsert = "";
            $("#image-modal").find(".tablesorter").find("input:checked").each(function () {
                var id = $(this).closest("tr").data("image-id");
                textToInsert += "{IMG-" + id + ": " + $(this).parents("tr:eq(0)").find("td:eq(2)").text() + "}";
                $(this).removeAttr("checked");
            });
            var currentReference = editAnswer.currentReference,
                editor = tinyMCE.get(currentReference);
            //forced to do it this way because IE doesnt get the bookmark
            if (currentReference !== tinyMCE.currentActiveEditor || tinyMCE.isIE) {
                var content = editor.getContent();
                content += textToInsert;
                editor.setContent(content);
            }
            else {
                editor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                editor.selection.setContent(textToInsert);
            }
            $("#image-modal").dialog("close");
        },
        populateScores: function () {
            var factorTypeId = $("#factor").val();
            var score = $("#score");
            //ajax get call to get all scores for the selected factortype
            $.getJSON("/scores/" + factorTypeId, function (data) {
                //delete all previous scores
                var scoreInnerHtml = "";
                $("#score option[value!='0']").remove();
                $.each(data, function (key, item) {
                    scoreInnerHtml += "<option value='" + item.Key.toString() + "'>" + item.Value.toString() + "</option>";
                });
                score.append(scoreInnerHtml);
            });
        },
        closeModal: function () {
            $("#reference-modal").dialog("close");
            return false;
        },
        searchReference: function () {
            var reportId = $(".search").attr("reportId");
            var searchTerm = $("#search-reference").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/references/search",
                data: { filter: searchTerm, context: "Modal" },
                success: function (data) {
                    $("#reference-container").html(data);
                    $("#search-reference").val(searchTerm);
                }
            });
        },
        searchImage: function () {
            var reportId = $(".search").attr("reportId"),
                searchTerm = $("#search-image").val();
            $.ajax({
                type: "POST",
                url: "/reports/" + reportId + "/images/search",
                data: { filter: searchTerm, context: "Modal" },
                success: function (data) {
                    $("#image-container").html(data);
                    $("#search-image").val(searchTerm);
                }
            });
        },
        showGuide: function () {
            var $guidingPrinciples = $(this).parents(".guiding-principles").first();
            $guidingPrinciples.find(".information-box").show();
            $guidingPrinciples.find(".button-review").hide();
        },
        hideGuide: function () {
            var $guidingPrinciples = $(this).parents(".guiding-principles").first();
            $guidingPrinciples.find(".information-box").hide();
            $guidingPrinciples.find(".button-review").show();
        },
        populateHelp: function () {
            var factorId = $(this).val();
            $("#factor-instruction-container").load("/analysis/factor-instruction/Help/" + factorId, function () {
                $(".guiding-principles").show();
            });
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        }
    };
    $(document).ready(function () {
        editAnswer.init()
    });
});