﻿/*global require */
require(["jquery", "jquery.ui", "jquery.tablesorter", "jquery.tablesorter.pager", "utils/bootstrap.tabContainer"], function ($) {
    "use strict";
    // console.log($.fn.tablesorter);
    var answerList = {
        init: function () {

            var numRows = $(".tablesorter tbody tr").size();
            var pageSize = $("#pager").data().pagesize;
            if (pageSize === '') {
                pageSize = 10;
            }
            $(".tablesorter")
            .tablesorter();
            if (numRows > 0) {
                $(".tablesorter").tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of ", size: pageSize });
            }
            $(".pagesize").change(this.updatePageSize);



            $(".answer-amount").html(numRows + " Answers")
            $("#answer-list .answer-info a").live("click", this.toggleDetails);
            $(".delete-row").live("click", this.deleteReference);
            $("#delete-modal").dialog({ autoOpen: false, width: 300 });
            $("#delete-modal").find(".no").live("click", this.closeDeleteModal);
            $("#delete-modal").find(".yes").live("click", this.confirmDelete);
            $(".search-answer").live("click", this.searchAnswer);
            $("#search-answer").live("keyup", function (event) {
                if (event.keyCode == 13) { //enter key
                    answerList.searchAnswer();
                }
            });

        },
        toggleDetails: function () {
            if ($(this).hasClass("collapse")) {
                $(this).parents("td:eq(0)").find(".detail-rationale").hide();
                $(this).removeClass("collapse")
                $(this).html("Show More")
            } else {
                $(this).parents("td:eq(0)").find(".detail-rationale").show();
                $(this).addClass("collapse")
                $(this).html("Show Less")
            }
        },
        deleteReference: function () {
            var deleteUrl = $(this).attr("href"),
                $currentTr = $(this).parents("tr");
            $("#delete-modal").data({ "deleteUrl": deleteUrl, "$currentTr": $currentTr }).dialog("open");
            return false;
        },
        closeDeleteModal: function () {
            $("#delete-modal").dialog("close");
        },
        confirmDelete: function () {
            console.log($("#delete-modal").data().deleteUrl);
            $.ajax({
                type: "POST",
                url: $("#delete-modal").data().deleteUrl,
                async: true,
                success: function (data) {
                    if (data.Success == true) {
                        //load the answer detail display
                        var $deleteModal = $("#delete-modal");
                        var originalHtml = $deleteModal.html();
                        $deleteModal.data("originalHtml", originalHtml);
                        $deleteModal.html("<p>Score Deleted</p>");
                        setTimeout("var data = $('#delete-modal').data();$('#delete-modal').dialog('close').html(data.originalHtml); data.$currentTr.remove();", 1000);
                    }
                }
            });
        },
        searchAnswer: function () {
            var reportId = $(".search").attr("reportId");
            var factorTypeId = $(".search").attr("factorTypeId");
            var searchTerm = $("#search-answer").val();
            $.ajax({
                type: "POST", //reports/{id}/answers/{factorTypeId}
                url: "/reports/" + reportId + "/answers/" + factorTypeId,
                data: { filter: searchTerm },
                success: function (data) {
                    $("#answer-list").html(data);
                    $("#search-answer").val(searchTerm);
                    $(".tablesorter")
                    .tablesorter()
                    .tablesorterPager({ container: $("#pager"), positionFixed: false, seperator: " of " });
                }
            });
        },
        updatePageSize: function () {
            $.post("/analysis/updatepagesize", { size: $(this).val() });
        }
    }
    answerList.init()
});