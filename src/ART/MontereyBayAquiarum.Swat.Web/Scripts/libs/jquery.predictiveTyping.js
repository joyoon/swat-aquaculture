﻿(function ($) {

    $.fn.predictiveTyping = function (options) {

        // Create some defaults, extending them with any options that were provided
        var defaults = {
            postURL: null,
            parentContainer: "td"
        };
        var options = $.extend(defaults, options);

        return this.each(function () {
            $(this).bind("keyup", getResults)
        });
        function getResults(e) {
            var value = $(this).val();
            var obj = $(this);
            if (value == "")
                return;

            //find out which item needs to be autocompleted
            var item = $(this).attr("item");
            //ajax post
            $.ajax({
                type: "POST",
                url: "/autocomplete",
                //dataType: "json",
                async: true,
                data: "item=" + item + "&keyword=" + $.trim(value),
                success: function (results) {
                    //I am going to fake the results
                    $(".predictive-typing-results").remove();
                    if (results.length == 0 && value != "") {
                        var html = "<div class=\"predictive-typing-results\"><ul>"
                        html += "<li>No results found. Please try again</li>"
                        html += "</ul></div>"
                        obj.after(html)
                        //obj.val("");
                    } else {
                        var html = "<div class=\"predictive-typing-results\"><ul>"
                        for (var i = 0; i < results.length; i++) {
                            html += "<li key='" + results[i].Key + "'>" + results[i].Value + "</li>"
                        }
                        html += "</ul></div>"
                        obj.after(html)
                        $(".predictive-typing-results li").bind("click", selectOption)
                    }
                }
            });

        }
        function selectOption(e) {
            var input = $(this).parents(options.parentContainer + ":eq(0)").find("input");
            input.val($(this).html());
            input.attr("key", $(this).attr("key"));
            $(this).parents(".predictive-typing-results").remove();
            if (defineScope) {
                defineScope.updateSaveButton();
            }
        }
    };
})(jQuery);