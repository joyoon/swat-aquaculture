﻿//  jquery plugin extend Underscore _.template() to jquery

//     Underscore.js 1.2.1
//     (c) 2011 Jeremy Ashkenas, DocumentCloud Inc.
//     Underscore is freely distributable under the MIT license.
//     Portions of Underscore are inspired or borrowed from Prototype,
//     Oliver Steele's Functional, and John Resig's Micro-Templating.
//     For all details and documentation:
//     http://documentcloud.github.com/underscore


(function ($) {

    // JavaScript micro-templating, similar to John Resig's implementation.
    // Underscore templating handles arbitrary delimiters, preserves whitespace,
    // and correctly escapes quotes within interpolated code.
    jQuery.template = function (str, data) {

        // By default, Underscore uses ERB-style template delimiters, change the
        // following template settings to use alternative delimiters.
        var templateSettings = {
            evaluate: /<%([\s\S]+?)%>/g,
            interpolate: /<%=([\s\S]+?)%>/g,
            escape: /<%-([\s\S]+?)%>/g
        };

        var c = templateSettings;

        var tmpl = 'var __p=[],print=function(){__p.push.apply(__p,arguments);};' +
                       'with(obj||{}){__p.push(\'' +
                        str.replace(/\\/g, '\\\\')
                          .replace(/'/g, "\\'")
                          .replace(c.escape, function (match, code) {
                              return "',_.escape(" + code.replace(/\\'/g, "'") + "),'";
                          })
                         .replace(c.interpolate, function (match, code) {
                             return "'," + code.replace(/\\'/g, "'") + ",'";
                         })
                         .replace(c.evaluate || null, function (match, code) {
                             return "');" + code.replace(/\\'/g, "'")
                                              .replace(/[\r\n\t]/g, ' ') + "__p.push('";
                         })
                         .replace(/\r/g, '\\r')
                         .replace(/\n/g, '\\n')
                         .replace(/\t/g, '\\t')
                         + "');}return __p.join('');";

        var func = new Function('obj', tmpl);

        return data ? func(data) : func;

    };   // jQuery.template
})(jQuery);


 
