﻿/*global require*/
var comments;
require(["utils/bootstrap.comments"], function () {
    "use strict";
    comments = {
        reportId: 0,
        answerId: 0,
        currentCommentId: 0,
        currentCommentElem: $,
        newComment: false,
        init: function () {
            this.reportId = $(".comment").attr("reportId");
            this.answerId = $(".comment").attr("answerId");
            $(".add-comment").live("click", this.addComment);
            $(".comment-record .save-comment").live("click", this.saveComment);
            $(".new-comment .save-comment").live("click", this.saveNewComment);
            $(".comment textarea").live("keyup", this.updateViewMode);
            $(".edit-comment").live("click", this.editComment);
            $(".attach").live("click", this.showAttachForm)
            $(".files .upload").live("click", this.uploadFile);
            $(".files .remove").live("click", this.removeFile);
            $(".cancel-area a").live("click", this.cancelComment);
            $(".cancel-edit-comment").live("click", this.cancelEditComment);
            $(".add-nested-comment").live("click", this.addSubComment);
            $(".edit-nested-comment").live("click", this.editSubComment);
            $(".save-edit-nested-comment").live("click", this.saveSubComment);
            $(".save-new-nested-comment").live("click", this.saveNewSubComment);
            $(".cancel-edit-nested-comment").live("click", this.cancelSubComment);
            $(".cancel-add-nested-comment").live("click", this.cancelAddSubComment);
            $(".checkbox").live("click", this.toggleActiveState);
            //this.currentNewComment;
        },
        addComment: function (e) {
            comments.reportId = $(".comment").attr("reportId");
            comments.answerId = $(".comment").attr("answerId");
            $(this).parents(".comment:eq(0)").find(".new-comment").show();
            $(".add-comment").hide(); //hide button
        },
        editComment: function () {
            comments.reportId = $(".comment").attr("reportId");
            comments.answerId = $(".comment").attr("answerId");
            comments.currentCommentId = $(this).parents(".comment-record").attr("commentId");
            $(this).parents("ul:eq(0)").find(".edit-mode").show()
                .end().children(".view-mode").hide();
            //$(this).parents("ul:eq(0)").find(".view-mode").hide();
        },
        cancelEditComment: function () {
            $(this).parents("ul").first().find(".edit-mode").hide()
            .end().find(".view-mode").show();
            if ($(this).parent().find(".word").length > 0) {
                $(this).parent().parent().find(".word").last().show(); //last one cause all the other ones are hidden
                $(this).parent().parent().find(".attach").hide()
                .end().find(".add-new-file").hide();
            }
        },
        cancelComment: function () {
            $(this).parents(".new-comment").hide();
            $(".add-comment").show();
            //$(this).parents("ul:eq(0)").children(".view-mode").show();
        },
        saveComment: function () { //editing
            var commentElem = $(this).parents(".comment-record");
            var comment = commentElem.find("textarea").val();
            //ajax call to save the edited comment

            var $form = $(this).parent();
            $form.attr("action", "/reports/" + comments.reportId + "/answers/" + comments.answerId + "/comments/" + comments.currentCommentId);
            $form.submit();
            comments.currentCommentElem = commentElem;
            commentElem.find(".comment-content p:eq(0)").html(comment);
            commentElem.find(".edit-mode").hide();
            commentElem.find(".view-mode").show();


        },
        saveNewComment: function () {
            var newCommentElem = $(this).parents(".new-comment:eq(0)");
            var $form = $(this).parent();
            if (typeof answerModal != 'undefined') {
                answerModal.noteOpenTables();
            }
            //commented out for ticket # 228 active collab, comments were being associated with first comment, this line was changing the correct Url
            //$form.attr("action", "/reports/" + comments.reportId + "/answers/" + comments.answerId + "/comments/");
            $form.submit();
            comments.currentCommentElem = newCommentElem;
            $(".add-comment").show();
            var count = parseInt($("#count").text().substr(1, $("#count").text().length - 2));
            count++;
            $("#count").text("(" + count + ")");

        },
        updateViewMode: function (e) {
            $(this).parent().parent().siblings(".view-mode").find(".comment-content").find("p").html($(this).val());
        },
        showAttachForm: function (e) {
            $(this).parent().find(".files .add-new-file").show()
                .end().find(".fileModified").val("1")
                .end().find(".attach").show();
            $(this).hide();
        },
        uploadFile: function (e) {
            //ajax upload!
            //faking the filename!
            var filename = "myaweseomfile.doc"
            $(this).parents("li:eq(0)").before("<li class=\"word\"><span></span><a href=\"myaweseomfile.doc\">" + filename + "</a> <a class=\"remove\">remove</a></li>");
            $(this).parents("ul:eq(1)").find(".view-mode .files").append("<li class=\"word\"><span></span><a href=\"myaweseomfile.doc\">" + filename + "</li>");
            $(this).parents("li:eq(0)").find("input").val("")
        },
        removeFile: function (e) {
            //ajax remove!
            var files = $(this).parents(".files:eq(0)").find("li")
            var index = files.index($(this).parents("li:eq(0)"))
            $(this).parents("form").find(".hasFile").removeClass("hasFile")
                .end().find(".fileModified").val("1")
                .end().find(".attach").show()
                .end().find("input[name='file']").val("");
            //$(this).parents("ul:eq(1)").find(".view-mode .files li:eq(" + index + ")").remove();
            $(this).parents("li:eq(0)").hide();
        },
        editSubComment: function () {
            comments.reportId = $(".comment").attr("reportId");
            comments.answerId = $(".comment").attr("answerId");
            var $li = $(this).parents("li:eq(0)");
            var commentContent = $.trim($li.find(".nested-comment-content").text());
            comments.currentCommentId = $li.attr("commentid");
            $li.find(".nested-comment").hide();
            $li.find(".nested-comment-edit-mode").show().find("textarea").val(commentContent);
        },
        cancelSubComment: function () {
            var $li = $(this).parents("li:eq(0)");
            $li.find(".nested-comment").show();
            $li.find(".nested-comment-edit-mode").hide();
        },
        addSubComment: function () {
            comments.reportId = $(".comment").attr("reportId");
            comments.answerId = $(".comment").attr("answerId");
            $(this).parents("ul:eq(0)").find(".new-nested-comment").show();
            $(this).hide();
        },
        updateSubCommentViewMode: function (e) {
            $(this).parents("li:eq(0)").find(".nested-comment-content").html($(this).val());
        },
        saveNewSubComment: function () {
            var $parent = $(this).parents(".comment-record");
            var description = $parent.find(".new-nested-comment .nested-comment-edit-mode textarea").val();
            var newCommentElem = $(this).parents(".new-comment:eq(0)");
            var $form = $(this).parent();
            comments.newComment = true;
            $form.attr("action", "/reports/" + comments.reportId + "/answers/" + comments.answerId + "/comments/");
            $form.submit();

            $parent.find(".new-nested-comment .nested-comment-content").html(description);
            var $comment = $parent.find(".new-nested-comment").clone();
            $comment.removeClass("new-nested-comment");
            comments.newCurrentComment = $comment;
            $parent.find(".add-nested-comment").before($comment) //need to rethink this
            $comment.find(".cancel-add-nested-comment").removeClass("cancel-add-nested-comment").addClass("cancel-edit-nested-comment");
            //$comment.attr('commentid', newid);
            $comment.find("textarea").val($(".new-nested-comment textarea").val())
            $parent.find(".new-nested-comment").hide().find("textarea").val("");
            $comment.find(".save-new-nested-comment").addClass("save-edit-nested-comment").removeClass("save-new-nested-comment")
            $parent.find(".add-nested-comment").show(); //need to give the comment its id

        },
        cancelAddSubComment: function () {
            var $parent = $(this).parents("ul:eq(0)")
            $parent.find(".add-nested-comment").show();
            $parent.find(".new-nested-comment").hide().find("textarea").val("");
        },
        saveSubComment: function () { //for edit
            var $li = $(this).parents("li:eq(0)");
            var comment = $li.find("textarea").val();
            var $form = $(this).parent();
            $form.attr("action", "/reports/" + comments.reportId + "/answers/" + comments.answerId + "/comments/" + comments.currentCommentId);
            $form.submit();
            $li.find(".nested-comment-content").html(comment);
            $li.find(".nested-comment").show();
            $li.find(".nested-comment-edit-mode").hide();
        },
        toggleActiveState: function () {
            var $ul = $(this).parents("ul:eq(0)"),
                isReviewed = $(this).is(":checked"),
                reportId = $(this).closest(".comment").attr("reportId");
            if (isReviewed) {
                $ul.addClass("completed");
                var count = parseInt($("#count").text().substr(1, $("#count").text().length - 2));
                count--;
                $("#count").text("(" + count + ")");
            } else {
                $ul.removeClass("completed")
                var count = parseInt($("#count").text().substr(1, $("#count").text().length - 2));
                count++;
                $("#count").text("(" + count + ")");
            }
            var commentId = $ul.attr("commentId");
            $.ajax({
                type: "POST",
                url: "/comment/UpdateStatus/",
                async: true,
                data: { reportId: reportId, commentId: commentId, isReviewed: isReviewed },
                success: function (data) {
                    comments.reloadBackgroundCommentCounts();
                    if (data.Success) {
                        //load the answer detail display
                        var modalTitle = data.Label + ": " + data.Title;
                        $("#modal-title").text(modalTitle);
                        answerModal.currentFactorId = data.FactorId;
                        answerModal.showAnswer();
                    }
                }
            });
        },
        saveCommentSuccess: function (comment) {
            if (comments.newComment == true) {
                comments.reloadBackgroundCommentCounts();
                comments.newComment = false;
            }
            if (comment.commentId == null) {
                comments.reloadBackgroundCommentCounts();
            }
            if ($.trim(comment.commentId) != "") {//only save new comment use this
                //$(".new-comment").before(data).hide().find("textarea").val(""); //do this for new comment
                //$parent.find(".add-nested-comment").before($comment)
                comments.newCurrentComment.attr('commentid', comment.commentId);

            }
            if ($.trim(comment.content) != "") {  //pushing new comment
                comments.currentCommentElem.before(comment.content)
                    .hide()
                    .find("textarea").val("");
            }
            if (comment.newPath != null) { //comment edit needs attachment
                if ($.trim(comment.newPath).length > 0) {
                    var htmlElement = "<li class='word'><span></span><a target='_blank' href='" + comment.newPath + "'>" + comment.newFileName + "</a></li>";
                    var htmlRemove = "<li class='word'><span></span><a target='_blank' href='" + comment.newPath + "'>" + comment.newFileName + "</a><a class='remove'>remove/replace</a></li>";
                    comments.currentCommentElem.find(".files").eq(0).html(htmlElement); //first one is the view one, no need for remove <a class='remove'>remove/replace</a>
                    comments.currentCommentElem.find(".files").eq(1).append(htmlRemove) //edit mode, need remove
                        .end().find(".add-new-file").hide();
                }
                else { //removed file, no path to be shown
                    comments.currentCommentElem.find(".files").eq(0).find(".word").hide();
                    comments.currentCommentElem.find(".add-new-file").hide();
                }
                return; //dont increment count if it's an edit
            }
        },
        reloadBackgroundCommentCounts: function () {
            var index;
            if (typeof answerModal != "undefined") {
                if ($(".criteria-tabs").length > 0) {
                    index = $(".criteria-tabs li").index($(".criteria-tabs li.on"));
                }
                answerModal.noteOpenTables();
                $(".parentTable").load($(".parentTable").attr("url"), function () {
                    answerModal.reOpenTables();
                    if ($(".criteria-tabs").length > 0) {
                        $(".criteria-tabs").find("li").eq(index).click();
                    }
                });
            }
        }
    }
    comments.init();
});