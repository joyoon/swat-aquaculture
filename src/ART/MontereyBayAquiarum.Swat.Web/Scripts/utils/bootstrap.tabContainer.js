﻿/*global require*/
require(["utils/bootstrap.tabContainer"], function () {
    "use strict";
    var tabContainer = {
        init: function () {
            $(".resources").bind("click", this.showResources);
            $('body').bind("click", this.clickAway)
            $("#report-status-select").change(this.changeStatus);

        },
        showResources: function () {
            $(".resources-drop").show();
        },
        clickAway: function (e) {
            if (!$(e.target).closest('.resources-drop').length
            && !$(e.target).closest('.resources').length) {
                $(".resources-drop").hide();
            }
        },
        changeStatus: function () {
            /*InProgress = 1,
            InternalReview1 = 3,
            ExternalPeerReview = 4,
            InternalReview2 = 5,
            Finalization = 6,
            Complete = 7*/
            var reportId = $(this).attr("reportId"),
                status = $(this).val();
            $.post("/reports/" + reportId + "/analysis/change-status", { "status": status }, function (data) {
                //success call not needed?
                console.log(data);
            });
        }
    }
    tabContainer.init();
});