﻿/*global define,tinyMCE */
define(["tiny_mce_full"], function () {
    "use strict";
    tinyMCE.init({
        mode: "specific_textareas",
        editor_selector: "mceFullEditor",
        theme: "advanced",
        plugins: "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1: "pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,|,sub,sup,|,save",
        theme_advanced_buttons2: "",
        width: "870px",
        paste_auto_cleanup_on_paste: true,
        paste_remove_styles: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: true,
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: false,
        theme_advanced_resizing_use_cookie: false,
        force_br_newlines: true,
        force_p_newlines: false,
        forced_root_block: false
    });
});