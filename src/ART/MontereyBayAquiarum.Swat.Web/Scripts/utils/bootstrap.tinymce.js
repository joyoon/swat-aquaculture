﻿/*global define,tinyMCE */
require(["jquery", "jquery.tiny_mce"], function () {
    "use strict";
    $(".mceEditor").not(".initialized").not("span").tinymce({

        script_url: '../tiny_mce/tiny_mce.js',

        mode: "specific_textareas",
        theme: "advanced",
        plugins: "lists,spellchecker,pagebreak,table,save,advhr,advlink,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking",
        // Theme options
        theme_advanced_buttons1: "save,|pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,sub,sup, code",
        theme_advanced_buttons2: "",
        setup: function (ed) {
            ed.onKeyUp.add(function (ed, e) {
                tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
            });
            ed.onClick.add(function (ed, e) {
                tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
            });
        },
        width: "870px",
        paste_auto_cleanup_on_paste: true,
        paste_remove_styles: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: true,

        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: false,
        theme_advanced_resizing_use_cookie: false,
        force_br_newlines: true,
        force_p_newlines: false,
        forced_root_block: false
    });
    $(".mceFullEditor").not(".initialized").not("span").tinymce({
        mode: "specific_textareas",
        theme: "advanced",
        plugins: "table, autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1: "save,|pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,|,sub,sup,|, tablecontrols, code",
        theme_advanced_buttons2: "",
        setup: function (ed) {
            ed.onKeyUp.add(function (ed, e) {
                tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
            });
            ed.onClick.add(function (ed, e) {
                tinyMCE.actualCaretPositionBookmark = tinyMCE.activeEditor.selection.getBookmark();
                tinyMCE.activeEditor.selection.moveToBookmark(tinyMCE.actualCaretPositionBookmark);
                tinyMCE.currentActiveEditor = tinyMCE.activeEditor.id;
            });
        },
        width: "870px",
        paste_auto_cleanup_on_paste: true,
        paste_remove_styles: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: true,
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: false,
        theme_advanced_resizing_use_cookie: false,
        force_br_newlines: true,
        force_p_newlines: false,
        forced_root_block: false
    });

    $(".mceEditor, .mceFullEditor").addClass("initialized");

});