﻿/*global require*/
require(["utils/bootstrap.stockAnalysis"], function () {
    "use strict";
    var stockAnalysis = {
        init: function () {
            $(".click-to-collapse").live("click", this.expandCollapse)
            $(".expand-all").live("click", this.expandAll)
            $(".collapse-all").live("click", this.collapseAll);
            $(".guiding-principles .information-box .hide-information-box").live("click", this.hideGuide);
            $(".guiding-principles .button-review").live("click", this.showGuide);

        },
        expandCollapse: function (e) {
            if ($(".review-layout").length > 0) {
                return false;
            }
            var parent = $(this).parents("table:eq(0)");
            if ($(this).hasClass("on")) {
                $(this).removeClass("on");
                parent.find(".collapse").hide();
            } else {
                $(this).addClass("on");
                parent.find(".collapse").show();
            }
        },
        expandAll: function () {
            $(".stock-analysis .click-to-collapse").addClass("on")
            $(".stock-analysis .collapse").show();
        },
        collapseAll: function () {
            $(".stock-analysis .click-to-collapse").removeClass("on")
            $(".stock-analysis .collapse").hide();
        },
        showGuide: function () {
            var $guidingPrinciples = $(this).parents(".guiding-principles").first();
            $guidingPrinciples.find(".information-box").show();
            $guidingPrinciples.find(".button-review").hide();
        },
        hideGuide: function () {
            var $guidingPrinciples = $(this).parents(".guiding-principles").first();
            $guidingPrinciples.find(".information-box").hide();
            $guidingPrinciples.find(".button-review").show();
        }
    }
    stockAnalysis.init();
});