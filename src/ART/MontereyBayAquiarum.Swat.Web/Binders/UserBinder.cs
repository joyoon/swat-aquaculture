﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SquareOne.Infrastructure.Mvc.Authentication;
using MontereyBayAquarium.Swat.Web.Models;

namespace MontereyBayAquarium.Swat.Web.Binders
{
    public class UserBinder : IModelBinder
    {
        #region IModelBinder Members

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null) throw new InvalidOperationException("Cannot bind the user object");

            if (controllerContext.HttpContext.User == null) return null;

            if (!(controllerContext.HttpContext.User.Identity is CustomIdentity<IAuthenticatedUser>)) return null;

            return ((CustomIdentity<IAuthenticatedUser>)controllerContext.HttpContext.User.Identity).User;

        }

        #endregion
    }
}