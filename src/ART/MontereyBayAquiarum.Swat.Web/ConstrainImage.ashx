<%@ WebHandler Language="C#" Class="ConstrainImage" %>

using System;
using System.IO;
using System.Configuration;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure;
public class ConstrainImage : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        Image img = GetImage(context);
        
        string imagePath = context.Server.MapPath(context.Request.QueryString["imgUrl"]);
        int maxWidth = (context.Request.QueryString["mw"] == null) ? 700 : int.Parse(context.Request.QueryString["mw"]);
        int maxHeight = (context.Request.QueryString["mh"] == null) ? 700 : int.Parse(context.Request.QueryString["mh"]);

        if (img == null) return;

        decimal ratio;

        if (img.Height > img.Width)
        {
            if (img.Height > maxHeight)
                ratio = Convert.ToDecimal(maxHeight) / Convert.ToDecimal(img.Height);
            else if (img.Height < maxHeight && img.Width > maxWidth)
                ratio = Convert.ToDecimal(maxWidth) / Convert.ToDecimal(img.Width);
            else
                ratio = 1m;
        }
        else
        {
            if (img.Width > maxWidth)
                ratio = Convert.ToDecimal(maxWidth) / Convert.ToDecimal(img.Width);
            else if (img.Width < maxWidth && img.Height > maxHeight)
                ratio = Convert.ToDecimal(maxHeight) / Convert.ToDecimal(img.Height);
            else
                ratio = 1m;
        }

        int newWidth = Convert.ToInt32(img.Width * ratio);
        int newHeight = Convert.ToInt32(img.Height * ratio);

        img.RotateFlip(RotateFlipType.Rotate180FlipNone);
        img.RotateFlip(RotateFlipType.Rotate180FlipNone);


        Image resized;

        if (ratio >= 1m)
            resized = img;
        else
            resized = img.GetThumbnailImage(newWidth, newHeight, delegate() { return false; }, IntPtr.Zero);


        SendImage(context, resized);

        resized.Dispose();
        img.Dispose();
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    private Image GetImage(HttpContext context)
    {
        string imgUrl =  HttpUtility.UrlDecode(context.Request.QueryString["imgUrl"]);
        if (imgUrl == null || imgUrl == "")
            imgUrl = ConfigurationManager.AppSettings["NoImageUrl"];

        Stream stream = new MemoryStream();

                    Microsoft.WindowsAzure.Storage.CloudStorageAccount storageAccount = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("sfwart");

            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(imgUrl);

            // Save blob contents to disk.

            blockBlob.DownloadToStream(stream);



            Image img = Image.FromStream(stream);

        return img;
    }

    private void SendImage(HttpContext context, Image image)
    {
        string contentType = "image/jpeg";
        ImageFormat format = ImageFormat.Jpeg;


        context.Response.ContentType = contentType;

        image.Save(context.Response.OutputStream, format);

    }

}