﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using ART;
using System.Security.Principal;
using MontereyBayAquarium.Swat.Web.Binders;
using SquareOne.Infrastructure.Mvc.Authentication;
using MontereyBayAquarium.Swat.Web.Models;

namespace MontereyBayAquarium.Swat.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /* Note: I discovered I needed this from several blog posts:
             * here: http://blogs.gcpowertools.co.in/2011/11/using-active-reports-in-mvc-3.html
             * and here: http://stackoverflow.com/questions/668328/how-to-render-active-reports-webviewer-in-asp-net-mvc 
             * and here: http://www.datadynamics.com/forums/ShowPost.aspx?PostID=121907#121907 */
            routes.IgnoreRoute("{*allarcachitems}", new { allarcachitems = @".*\.ArCacheItem(/.*)?" });
            routes.IgnoreRoute("{*allActiveReport}", new { allActiveReport = @".*\.ar7(/.*)?" }); 
            #region Dashboard Route
            routes.MapRoute(
                "Dashboard",
                "dashboard",
                new { controller = "Dashboard", action = "Index" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            #endregion

            #region Analysis Routes
            routes.MapRoute(
                "Define Scope",
                "reports/{id}/analysis/define-scope",
                new { controller = "Analysis", action = "DefineScope" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );

            routes.MapRoute(
                "Update Recommendation",
                "reports/{id}/analysis/update-recommendation",
                new { controller = "Analysis", action = "UpdateRecommendation" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Stock Health",
                "reports/{id}/analysis/stock-health",
                new { controller = "Analysis", action = "StockHealth" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria1",
                "reports/{id}/analysis/criteria1",
                new { controller = "Analysis", action = "Criteria1" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria2",
                "reports/{id}/analysis/criteria2",
                new { controller = "Analysis", action = "Criteria2" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria3",
                "reports/{id}/analysis/criteria3",
                new { controller = "Analysis", action = "Criteria3" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria4",
                "reports/{id}/analysis/criteria4",
                new { controller = "Analysis", action = "Criteria4" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Executive Summary",
                "reports/{id}/analysis/executive-summary",
                new { controller = "Analysis", action = "ExecutiveSummary" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Introduction",
                "reports/{id}/analysis/introduction",
                new { controller = "Analysis", action = "Introduction" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Acknowledgements",
                "reports/{id}/analysis/acknowledgements",
                new { controller = "Analysis", action = "Acknowledgements" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Supplemental Info",
                "reports/{id}/analysis/supplemental-info",
                new { controller = "Analysis", action = "SupplementalInfo" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Appendix",
                "reports/{id}/analysis/appendix",
                new { controller = "Analysis", action = "Appendix" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );

            routes.MapRoute(
                "Draft Overall Recommendations",
                "reports/{id}/analysis/draft-overall-recommendations",
                new { controller = "Analysis", action = "DraftOverallRecommendations" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Preview",
                "reports/{id}/analysis/preview",
                new { controller = "Analysis", action = "Preview" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Export",
                "reports/{id}/analysis/export",
                new { controller = "Analysis", action = "Export" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "SubmitReport",
                "reports/{id}/analysis/submit",
                new { controller = "Analysis", action = "SubmitReport" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Legend",
                "Legend",
                new { controller = "Analysis", action = "Legend" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Autocomplete",
                "AutoComplete",
                new { controller = "Analysis", action = "AutoComplete" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Save Scope",
                "reports/{id}/analysis/savescope",
                new { controller = "Analysis", action = "SaveScope" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Delete Scope",
                "reports/{id}/analysis/deletescope",
                new { controller = "Analysis", action = "DeleteScope" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Create Combinations",
                "reports/{id}/analysis/createcombinations",
                new { controller = "Analysis", action = "CreateCombinations" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Stock Analysis",
                "reports/{id}/analysis/stock-analysis/{context}",
                new { controller = "Analysis", action = "StockAnalysis" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria2 Data",
                "reports/{id}/analysis/criteria2data",
                new { controller = "Analysis", action = "Criteria2Data" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria3 Data",
                "reports/{id}/analysis/criteria3data",
                new { controller = "Analysis", action = "Criteria3Data" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );
            routes.MapRoute(
                "Criteria4 Data",
                "reports/{id}/analysis/criteria4data",
                new { controller = "Analysis", action = "Criteria4Data" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );

            routes.MapRoute(
                "Change Report Status",
                "reports/{id}/analysis/change-status",
                new { controller = "Analysis", action = "ChangeReportStatus" },
                new { id = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );

            routes.MapRoute(
                "Factor Instruction",
                "analysis/factor-instruction/{title}/{factorTypeId}",
                new { controller = "Analysis", action = "FactorInstruction" },
                new { factorTypeId = @"\d+" },
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" } 
            );

            #endregion

            #region Reference Routes
            /*routes.MapRoute(
                "References",
                "reports/{id}/references/{keywords}",
                new { controller = "Reference", action = "Index", keywords = UrlParameter.Optional },
                new { id = @"\d+" }
            );*/
            routes.MapRoute(
                "References",
                "reports/{id}/references/",
                new { controller = "Reference", action = "Index"},
                new { id = @"\d+" }
            );

            routes.MapRoute(
               "Reference Search",
               "reports/{id}/references/search",
               new { controller = "Reference", action = "Search" },
               new { id = @"\d+"}
            );

            routes.MapRoute(
                "Reference Add / Edit",
                "reports/{id}/references/{referenceId}/edit",
                new { controller = "Reference", action = "Edit" },
                new { id = @"\d+", referenceId = @"\d+" }
            );

            routes.MapRoute(
               "Reference Delete",
               "reports/{id}/references/{referenceId}/delete",
               new { controller = "Reference", action = "Delete" },
               new { id = @"\d+", referenceId = @"\d+" }
            );

            #endregion

            #region Image Routes
            routes.MapRoute(
                "Images",
                "reports/{id}/images/",
                new { controller = "Image", action = "Index" },
                new { id = @"\d+" }
            );

            routes.MapRoute(
               "Image Search",
               "reports/{id}/images/search",
               new { controller = "Image", action = "Search" },
               new { id = @"\d+" }
            );

            routes.MapRoute(
                "Image Add / Edit",
                "reports/{id}/images/{imageId}/edit",
                new { controller = "Image", action = "Edit" },
                new { id = @"\d+", imageId = @"\d+" }
            );

            routes.MapRoute(
               "Image Delete",
               "reports/{id}/images/{imageid}/delete",
               new { controller = "Image", action = "Delete" },
               new { id = @"\d+", imageId = @"\d+" }
            );

            #endregion

            #region Answer Routes
            routes.MapRoute(
                "Answers",
                "reports/{id}/answers",
                new { controller = "Answer", action = "Index" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Answers Add / Edit",
                "reports/{id}/answers/{answerId}/edit",
                new { controller = "Answer", action = "Edit" },
                new { id = @"\d+", answerId = @"\d+" }
            );
            routes.MapRoute(
                "Answer List",
                "reports/{id}/answers/{factorTypeId}",
                new { controller = "Answer", action = "AnswerList" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Answer Detail",
                "reports/{id}/answers/{answerId}/detail",
                new { controller = "Answer", action = "AnswerDetail" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Edit Answer",
                "reports/{id}/answers/{answerId}/editanswer/{factorTypeId}/{context}",
                new { controller = "Answer", action = "EditAnswer", context = UrlParameter.Optional },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Select Answer",
                "reports/{id}/answers/{answerId}/select",
                new { controller = "Answer", action = "Select" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Select Answer For Multiple Recs",
                "reports/{id}/answers/{answerId}/selectformultiplerecs",
                new { controller = "Answer", action = "SelectForMultipleRecs" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Answer Delete",
                "reports/{id}/answers/{answerId}/delete",
                new { controller = "Answer", action = "Delete" },
                new { id = @"\d+" }
            );
            #endregion

            #region Comment routes
            routes.MapRoute(
                "Answer Comments List & Create",
                "reports/{id}/answers/{answerId}/comments",
                new { controller = "Comment", action = "Index" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Answer Comment Edit",
                "reports/{id}/answers/{answerId}/comments/{commentId}",
                new { controller = "Comment", action = "Edit" },
                new { id = @"\d+" }
            );

            #endregion

            #region Expert Input Routes
            routes.MapRoute(
                "Expert Input List",
                "reports/{id}/expert-input",
                new { controller = "ExpertInput", action = "Index" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Expert Input Details",
                "reports/{id}/expert-input/{expertInputId}",
                new { controller = "ExpertInput", action = "Details" },
                new { id = @"\d+", expertInputId = @"\d+" }
            );
            #endregion

            #region Review Routes
            routes.MapRoute(
                "Review - Final Recommendation",
                "reports/{id}/review/final-recommendation",
                new { controller = "Review", action = "FinalRecommendation" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Executive Summary",
                "reports/{id}/review/executive-summary",
                new { controller = "Review", action = "ExecutiveSummary" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Introduction",
                "reports/{id}/review/introduction",
                new { controller = "Review", action = "Introduction" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Criteria1",
                "reports/{id}/review/criteria1",
                new { controller = "Review", action = "Criteria1" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Criteria2",
                "reports/{id}/review/criteria2",
                new { controller = "Review", action = "Criteria2" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Criteria3",
                "reports/{id}/review/Criteria3",
                new { controller = "Review", action = "Criteria3" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Criteria4",
                "reports/{id}/review/criteria4",
                new { controller = "Review", action = "Criteria4" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Overall Recommendation",
                "reports/{id}/review/overall-recommendation",
                new { controller = "Review", action = "OverallRecommendation" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - References",
                "reports/{id}/review/references",
                new { controller = "Review", action = "References" },
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Review - Appendix",
                "reports/{id}/review/appendix",
                new { controller = "Review", action = "Appendix" },
                new { id = @"\d+" }
            );

            #endregion

            #region Download Route
            //Download route
            routes.MapRoute(
                "Download", // Route name
                "download", // URL with parameters
                new { controller = "Download", action = "Index" }
            );
            #endregion

            #region Home Routes
            routes.MapRoute(
                "Public Contact Form", // Route name
                "contact-form/{id}", // URL with parameters
                new { controller = "Home", action = "ContactForm" }, // Parameter defaults
                new { id = @"\d+" }
            );
            routes.MapRoute(
                "Contact Form Successful", // Route name
                "contact-form/success", // URL with parameters
                new { controller = "Home", action = "ContactFormSuccess" }
            );
            routes.MapRoute(
                "Scoring Legend", // Route name
                "scoring-legend", // URL with parameters
                new { controller = "Home", action = "ScoringLegend" } // Parameter defaults
            );
            routes.MapRoute(
                "Public Pages - Reports & Login", // Route name
                "{action}", // URL with parameters
                new { controller = "Home" } // Parameter defaults
            );
            #endregion

            #region Miscellaneous
            routes.MapRoute(
                "Scores for a Factor", // Route name
                "scores/{factorTypeId}", // URL with parameters
                new { controller = "Answer", action = "Scores" }, // Parameter defaults
                new { factorTypeId = @"\d+" }
            );
            #endregion



            #region Default Route
            //default route
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },// Parameter defaults
                new string[] { "MontereyBayAquarium.Swat.Web.Controllers" }  
            );
            #endregion
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            RegisterBinders();
        }

        private static void RegisterBinders()
        {
            ModelBinders.Binders.Add(typeof(AuthenticatedUser), new UserBinder());
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User == null) return;
            if (!HttpContext.Current.User.Identity.IsAuthenticated) return;
            if (!(HttpContext.Current.User.Identity is FormsIdentity)) return;

            /*
             * 1. Model Bind Current<User>
             * 2. Chedk Httpcon for a non-null user
             * 3. Look up the user object by the valiue in iden.name
             * 4. create return new current<user>
             */

            if (HttpContext.Current.User.Identity is FormsIdentity)
            {
                FormsIdentity identity = (FormsIdentity)HttpContext.Current.User.Identity;
                FormsAuthenticationTicket ticket = identity.Ticket;

                AuthenticatedUser user = new AuthenticatedUser(new ARTService().LoadUser(Convert.ToInt32(ticket.Name)));

                Context.User = new CustomPrincipal(new CustomIdentity<IAuthenticatedUser>(user));
            }
        }
    }
}