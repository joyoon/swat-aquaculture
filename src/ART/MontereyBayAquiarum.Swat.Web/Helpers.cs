﻿namespace MontereyBayAquarium.Swat.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.IO;
    using System.Configuration;
    using MontereyBayAquarium.Swat.Web.Models.Analysis;
    using ART;
    using MontereyBayAquarium.Swat.Web.Models.Shared;
    using Microsoft.WindowsAzure.Storage.Blob;
    using Microsoft.WindowsAzure;
    using System.Net.Mail;
    using System.Net;
    using System.Text.RegularExpressions;
    using NuGet;

    public static class Helpers
    {

        public static AnswerEntity GetAnswer(int factorId, string score, string scoreDescription, string label, int commentCount,
            string title, string keyInfo, int factorTypeId)
        {
            AnswerEntity answer = new AnswerEntity();
            answer.FactorId = factorId;
            answer.Score = score;
            answer.ScoreDescription = scoreDescription;
            answer.Label = label;
            answer.CommentCount = commentCount;
            answer.Title = title;
            answer.KeyInfo = keyInfo;
            answer.FactorTypeId = factorTypeId;

            return answer;
        }

        public static AnswerEntity GetAnswer(FactorColumnEntity factorColumnEntity, int factorTypeId)
        {
            AnswerEntity answer = new AnswerEntity();
            answer.FactorId = factorColumnEntity.FactorId;
            answer.Score = factorColumnEntity.FactorScore;
            answer.ScoreDescription = factorColumnEntity.FactorScoreDescription;
            answer.Label = factorColumnEntity.FactorLabel;
            answer.CommentCount = factorColumnEntity.FactorCommentCount;
            answer.Title = factorColumnEntity.FactorTitle;
            answer.KeyInfo = factorColumnEntity.FactorKeyInfo;
            answer.FactorTypeId = factorTypeId;
            return answer;
        }

        public static AnswerCellViewModel MakeAnswerCellViewModel(FactorColumnEntity factorColumnEntity, int factorTypeId, string factorTypeName, string species, string location, string bodyOfWater, string method)
        {
            AnswerCellViewModel model = new AnswerCellViewModel()
            {
                Answer = GetAnswer(factorColumnEntity, factorTypeId),
                FactorTypeName = factorTypeName,
                Species = species,
                Location = location,
                BodyOfWater = bodyOfWater,
                Method = method
            };
            return model;
        }

        public static AnswerCellViewModel MakeAnswerCellViewModel(int factorId, string score, string scoreDescription, string label, int commentCount,
            string title, string keyInfo, int factorTypeId, string factorTypeName, string species, string location, string bodyOfWater,
            string method)
        {
            AnswerCellViewModel model = new AnswerCellViewModel()
            {
                Answer = GetAnswer(factorId, score, scoreDescription, label, commentCount, title, keyInfo, factorTypeId),
                FactorTypeName = factorTypeName,
                Species = species,
                Location = location,
                BodyOfWater = bodyOfWater,
                Method = method
            };
            return model;
        }

        public static AnswerCellViewModel MakeAnswerCellViewModel(int factorId, string score, string scoreDescription, string label, int commentCount,
            string title, string keyInfo, int factorTypeId, string factorTypeName, string species, string location, string bodyOfWater,
            string method, bool isEditable)
        {
            AnswerCellViewModel model = MakeAnswerCellViewModel(factorId, score, scoreDescription, label, commentCount, title, keyInfo,
                factorTypeId, factorTypeName, species, location, bodyOfWater, method);
            model.IsEditable = isEditable;
            return model;
        }

        public static string SaveFile(HttpPostedFileBase file, HttpServerUtilityBase Server, string subdirectory)
        {
            //get the resource
            //TODO: Contact form and Reference uses ReportID but Comments use AnswerID so they need to be saved in different places?
            string finalPath = "";
            string uniqueFilename = "";

            Microsoft.WindowsAzure.Storage.CloudStorageAccount storageAccount = Microsoft.WindowsAzure.Storage.CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("sfwart");

            if (file != null)
            {
                var filename = Path.GetFileName(file.FileName);
                //var virtualPath = Path.Combine(ConfigurationManager.AppSettings["ResourcePath"], subdirectory);
                //string subfolder = GetPath(postId);
                //var path = Path.Combine(Server.MapPath(virtualPath), subfolder);
                //if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);
                var nameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
                if (nameWithoutExtension.Length > 100)
                {
                    var extensionName = Path.GetExtension(filename);
                    filename = nameWithoutExtension.Substring(0, 100) + extensionName;
                }

                uniqueFilename = GetUniqueName(subdirectory + filename, container);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(uniqueFilename);

                using (var fileStream = file.InputStream)
                {
                    blockBlob.UploadFromStream(fileStream);
                }
                //file.SaveAs(Path.Combine(path, filename));
            }

            return HttpUtility.UrlEncode(uniqueFilename);
        }

        public static void SendMail(string subject, string body)
        {
            //var fromAddress = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"]);

            var smtp = new SmtpClient
            {
                //Host = "127.0.0.1",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("mbawebteam@gmail.com", "seaotter01")
            };

            var message = new MailMessage
                {
                    From = new MailAddress("mbawebteam@gmail.com"),
                    Subject = subject,
                    Body = body
                };

            var toAddresses = ConfigurationManager.AppSettings["EmailAddress"].Split(new[] { ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var address in toAddresses)
            {
                try
                {
                    message.To.Add(new MailAddress(address.Trim()));
                }
                catch (Exception) { }
            }

            smtp.Send(message);
        }


        public static IList<Criteria2Display> BuildCriteria2List(IList<Criteria2Entity> results)
        {
            IList<Criteria2Display> criteria = new List<Criteria2Display>();

            var species =
                results.Select(
                    s => new { s.TargetCommonName, s.BodyOfWater, s.Method })
                    .OrderBy(s => s.TargetCommonName)
                    .ThenBy(s => s.BodyOfWater)
                    .ThenBy(s => s.Method)
                    .Distinct();

            foreach (var item in species)
            {
                var specie = item;
                var data =
                    results.Where(
                        s =>
                        s.TargetCommonName.Equals(specie.TargetCommonName) && (s.BodyOfWater == null || s.BodyOfWater.Equals(specie.BodyOfWater))
                        && s.Method.Equals(specie.Method));
                var curDisplay = new Criteria2Display();


                curDisplay.OtherStock.AddRange(data.AsEnumerable());
                criteria.Add(curDisplay);
            }

            DefineLimitingStock(criteria);

            return criteria;
        }

        private static void DefineLimitingStock(IEnumerable<Criteria2Display> criteria)
        {
            foreach (var display in criteria)
            {
                display.LimitingStock = display.OtherStock.FirstOrDefault(x => x.IsLowest) ?? display.OtherStock[0];

                display.OtherStock.Remove(display.LimitingStock);
            }
        }

        public static IList<ReferenceEntity> GetAllReferences(string content, int reportId)
        {
            IList<ReferenceEntity> results = new List<ReferenceEntity>();
            if (content == null)
            {
                return results;
            }
            string pattern = "{(?!IMG-[0-9]*)[^}]+}";
            MatchCollection matches;
            Regex referenceRegex = new Regex(pattern);
            matches = referenceRegex.Matches(content);
            ARTService service = new ARTService();
            foreach (Match match in matches)
            {
                ReferenceEntity reference = service.GetReferenceByShortName(reportId, match.Value.Substring(1, match.Value.Length - 2));
                ReferenceComparer comparer = new ReferenceComparer();
                if (reference != null && reference.ReferenceId != 0 && !results.Contains(reference, comparer))
                {
                    results.Add(reference);
                }
            }
            return results;
        }

        public static string ParseWithImages(string content, int reportId)
        {
            if (content == null)
            {
                return "";
            }
            string pattern = "{(IMG-[0-9]*)[^}]+}";
            MatchCollection matches;
            Regex referenceRegex = new Regex(pattern);
            matches = referenceRegex.Matches(content);
            string idPattern = "(?<=IMG-)[0-9]+";
            Regex idRegex = new Regex(idPattern);
            ARTService service = new ARTService();
            foreach (Match match in matches)
            {
                Match imageIdString = idRegex.Match(match.Value);
                int imageId = Convert.ToInt16(imageIdString.Value);
                ImageEntity image = service.ImageGet(reportId, imageId);
                string imageHtml = "<div class='image-container'><img src='/ConstrainImage.ashx?imgUrl=" + image.Path + "&mw=780&mh=500'/><p>" + image.Description + "</p></div>";

                content = content.Replace(match.Value, imageHtml);
            }
            return content;
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (value == null)
            {
                return string.Empty;
            }
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        private static string GetPath(int postId)
        {
            string padded = postId.ToString().PadRight(4, '0');
            char[] chars = padded.ToCharArray();
            Array.Reverse(chars);
            var toProcess = new string(chars);
            string path = toProcess.Insert(4, "/").Insert(2, "/");
            return path;
        }

        private static string GetUniqueName(string filename, CloudBlobContainer container)
        {
            var validatedName = filename; //TODO: folder information not persisting
            var nameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
            var directoryPath = Path.GetDirectoryName(filename);
            var extensionName = Path.GetExtension(filename);
            int tries = 1;
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
            while (blockBlob.Exists())
            {
                validatedName = string.Format("{0}-{2}{1}", directoryPath + "\\" + nameWithoutExtension, extensionName, tries++);
                blockBlob = container.GetBlockBlobReference(validatedName);
            }
            return validatedName;
        }
    }
}