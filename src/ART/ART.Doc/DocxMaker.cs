﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using Vt = DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using M = DocumentFormat.OpenXml.Math;
using Ovml = DocumentFormat.OpenXml.Vml.Office;
using V = DocumentFormat.OpenXml.Vml;
using A = DocumentFormat.OpenXml.Drawing;
using ART.Doc;

namespace ART.Doc
{
	public class DocxReportMaker
	{
		int m_ReportId = 0;

		string clrBlueText = "404F59";
		string clrBlueMid = "DEE7E7";
		string clrBlueLt = "F4F7F7";
		string clrSecondaryLt = "6D808E";
		string clrSecondaryDk = "354857";
		string clrGreyText = "333333";
		string clrHeading = "52636F";
		string clrGreen = "B4E7B4";
		string clrYellow = "FFFFCC";
		string clrRed = "FFD7D7";
		string clrBlack = "FFD7D7";

		Dictionary<string, DataTable> dataSources = new Dictionary<string, DataTable>();
		MainDocumentPart mainDoc;

		//string xml = "<table border=\"1\"><caption>&nbsp;</caption> <tbody> <tr> <td>stock status</td> <td>fishing mortality</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> </tbody> </table>";


		internal List<customSection> GenerateReport(MainDocumentPart mainDocumentPt, int reportId)
		{
			m_ReportId = reportId;
			List<customSection> retval = new List<customSection>();
			init(); //Init data sources
			mainDoc = mainDocumentPt;


			////retval.Add(getText(xml));
			////retval.AddRange(splitXml(xml));


			retval.Add(getHeader1("Final Seafood Recommendation"));
			retval.Add(getOverallRec(dataSources["OverallRec"]));
			retval.AddRange(getHTML("<font style='font-family:Arial; font-size:12pt;'><b>Scoring note</b> – scores range from zero to five where zero indicates very poor performance and five indicates the fishing operations have no significant impact.</font>"));

			retval.Add(getPageBreak());

			retval.Add(getHeader1("Executive Summary"));
			retval.AddRange(getHTML(dataSources["TextBlockExecSummary"].Rows[0][1].ToString()));


			retval.Add(getHeader1("Introduction"));
			retval.Add(getHeader2("Scope of the analysis and ensuing recommendation"));
			retval.AddRange(getHTML(dataSources["TextBlockScope"].Rows[0][1].ToString()));

			retval.Add(getHeader2("Overview of the species and management bodies"));
			retval.AddRange(getHTML(dataSources["TextBlockSpeciesOverview"].Rows[0][1].ToString()));

			retval.Add(getHeader2("Production Statistics"));
			retval.AddRange(getHTML(dataSources["TextBlockProductionStatistics"].Rows[0][1].ToString()));

			retval.Add(getHeader2("Importance to the US/North American market"));
			retval.AddRange(getHTML(dataSources["TextBlockImportance"].Rows[0][1].ToString()));

			retval.Add(getHeader2("Common and market names"));
			retval.AddRange(getHTML(dataSources["TextBlockMarketNames"].Rows[0][1].ToString()));

			retval.Add(getHeader2("Primary product forms"));
			retval.AddRange(getHTML(dataSources["TextBlockProductForms"].Rows[0][1].ToString()));

			retval.Add(getPageBreak());

			retval.Add(getHeader1("Analysis"));
			retval.Add(getHeader2("Scoring Guide"));
			retval.Add(getBullet("All scores result in a zero to five final score for the criterion and the overall final rank. A zero score indicates poor performance, while a score of five indicates high performance."));
			retval.Add(getBullet("The full Seafood Watch Fisheries Criteria that the following scores relate to are available on our website at http://www.seafoodwatch.org"));

			retval.Add(getHeader1("Criterion 1: Stock for which you want a recommendation"));
			retval.Add(getHeader2("Guiding Principles"));
			retval.Add(getBullet("The stock is healthy and abundant. Abundance, size, sex, age and genetic structure should be maintained at levels that do not impair the long-term productivity of the stock or fulfillment of its role in the ecosystem and food web."));

			retval.Add(getBullet("Fishing mortality does not threaten populations or impede the ecological role of any marine life. Fishing mortality should be appropriate given current abundance and inherent resilience to fishing while accounting for scientific uncertainty, management uncertainty, and non-fishery impacts such as habitat degradation."));

			retval.Add(getHeader1("Summary"));
			retval.AddRange(getCriteria1Grids(dataSources["Criteria1Grid"]));

			retval.Add(getHeader1("Justification of Ranking"));
			retval.AddRange(getCriteria12(dataSources["Criteria1"]));


			retval.Add(getHeader1("Criterion 2: Impacts on other retained and bycatch stocks"));
			retval.Add(getHeader2("Guiding Principles"));
			retval.Add(getBullet("The fishery minimizes bycatch. Seafood Watch® defines bycatch as all fisheries-related mortality or injury other than the retained catch. Examples include discards, endangered or threatened species catch, pre-catch mortality and ghost fishing. All discards, including those released alive, are considered bycatch unless there is valid scientific evidence of high post-release survival and there is no documented evidence of negative impacts at the population level."));

			retval.Add(getBullet("Fishing mortality does not threaten populations or impede the ecological role of any marine life. Fishing mortality should be appropriate given each impacted species’ abundance and productivity, accounting for scientific uncertainty, management uncertainty and non-fishery impacts such as habitat degradation."));

			retval.Add(getHeader1("Summary"));
			retval.AddRange(getCriteria2Grids(dataSources["Criteria2Grid"]));

			retval.Add(getHeader1("Justification of Ranking"));
			retval.AddRange(getCriteria12(dataSources["Criteria2"]));

			retval.Add(getHeader1("Criterion 3: Management effectiveness"));
			retval.Add(getHeader2("Guiding Principles"));
			retval.Add(getBullet("The fishery is managed to sustain the long-term productivity of all impacted species. Management should be appropriate for the inherent resilience of affected marine life and should incorporate data sufficient to assess the affected species and manage fishing mortality to ensure little risk of depletion. Measures should be implemented and enforced to ensure that fishery mortality does not threaten the long-term productivity or ecological role of any species in the future."));

			retval.Add(getHeader1("Summary"));
			retval.AddRange(getCriteria3Grid(dataSources["Criteria3Grid"]));

			retval.AddRange(getCriteria31Grid(dataSources["Criteria31Grid"]));
			retval.Add(getHeader1("Justification of Ranking"));
			retval.AddRange(getCriteria34(dataSources["Criteria31"]));

			retval.AddRange(getCriteria32Grid(dataSources["Criteria32Grid"]));
			retval.Add(getHeader1("Justification of Ranking"));
			retval.AddRange(getCriteria34(dataSources["Criteria32"]));

			retval.Add(getHeader1("Criterion 4: Impacts on the habitat and ecosystem"));
			retval.Add(getHeader2("Guiding Principles"));
			retval.Add(getBullet("The fishery is conducted such that impacts on the seafloor are minimized and the ecological and functional roles of seafloor habitats are maintained."));
			retval.Add(getBullet("Fishing activities should not seriously reduce ecosystem services provided by any fished species or result in harmful changes such as trophic cascades, phase shifts or reduction of genetic diversity."));

			retval.Add(getHeader1("Summary"));
			retval.AddRange(getCriteria4Grid(dataSources["Criteria4Grid"]));

			retval.Add(getHeader1("Justification of Ranking"));
			retval.AddRange(getCriteria34(dataSources["Criteria4"]));

			return retval;
		}


		public void init()
		{
			ART.ReportSource rs = new ReportSource();
			ARTService service = new ARTService();


			dataSources.Add("TextBlockExecSummary", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.ExecSummary));
			dataSources.Add("TextBlockScope", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.Scope));
			dataSources.Add("TextBlockSpeciesOverview", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.SpeciesOverview));
			dataSources.Add("TextBlockProductionStatistics", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.ProductionStatistics));
			dataSources.Add("TextBlockImportance", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.ImportanceToMarket));
			dataSources.Add("TextBlockMarketNames", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.CommonMarketNames));
			dataSources.Add("TextBlockProductForms", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.ProductForms));


			dataSources.Add("OverallRec", rs.OverallRec(m_ReportId));

			dataSources.Add("Criteria1Grid", service.Criteria1ReportData(m_ReportId));
			//dataSources.Add("Criteria1Synthesis", service.SynthesisGet(reportId, 1));
			dataSources.Add("Criteria1", rs.Criteria1All(m_ReportId));

			dataSources.Add("Criteria2Grid", service.Criteria2ReportData(m_ReportId));
			//dataSources.Add("Criteria2Synthesis", service.SynthesisGet(reportId, 2));
			dataSources.Add("Criteria2", rs.Criteria2All(m_ReportId));

			dataSources.Add("Criteria3Grid", service.Criteria3ReportData(m_ReportId));
			dataSources.Add("Criteria31Grid", service.Criteria31ReportData(m_ReportId));
			dataSources.Add("Criteria32Grid", service.Criteria32ReportData(m_ReportId));
			//dataSources.Add("Criteria3Synthesis", service.SynthesisGet(reportId, 3));
			//dataSources.Add("Criteria31Synthesis", service.SynthesisGet(reportId, 5));
			//dataSources.Add("Criteria32Synthesis", service.SynthesisGet(reportId, 6));
			dataSources.Add("Criteria31", rs.Criteria31All(m_ReportId));
			dataSources.Add("Criteria32", rs.Criteria32All(m_ReportId));

			dataSources.Add("Criteria4Grid", service.Criteria4ReportData(m_ReportId));
			//dataSources.Add("Criteria4Synthesis", service.SynthesisGet(reportId, 4));
			dataSources.Add("Criteria4", rs.Criteria4All(m_ReportId));

			dataSources.Add("TextBlockAck", service.TextBlockGetDataTable(m_ReportId, ARTService.TextBlock.Acknowledgements));
			dataSources.Add("References", rs.References(m_ReportId));

			dataSources.Add("AppendixAGrid", service.AppendixAReportData(m_ReportId));
		}

		////Splits out image
		//internal List<customSection> splitXml(string xml, List<customSection> sections)
		//{
		//    NotesFor.HtmlToOpenXml.HtmlConverter htmlconv = new NotesFor.HtmlToOpenXml.HtmlConverter(mainDoc);
		//    foreach (OpenXmlCompositeElement element in htmlconv.Parse(xml))
		//    {

		//        int start = xml.IndexOf("{IMG");

		//        if (start > 0)
		//        {
		//            //There's an image.  Start by adding the text before the image
		//            sections.AddRange(getHTML(xml.Substring(0, start)));
		//            //Add the image
		//            string part = xml.Substring(start);
		//            int end = part.IndexOf('}');
		//            imageObj image = new imageObj();
		//            string imagestring = part.Substring(5, end - 5); //gives "#:title" inside the {}
		//            image.Url = "http://mbastorage.blob.core.windows.net/sfwart/" + new ARTService().ImageGet(m_ReportId, Convert.ToInt32(imagestring.Split(':')[0])).Path.Replace(" ", "%20");
		//            image.Link = image.Url;
		//            image.Filename = image.Url;
		//            sections.Add(new customSection(image));

		//            //Now recursively pass the remaining xml to this function again (in case there are additional images to process)
		//            sections = splitXml(xml.Substring(start + end), sections);
		//        }
		//        else
		//        {
		//            sections.AddRange(getHTML(xml));
		//        }
		//    }
		//    return sections;
		//}

		internal customSection getHeader1(string text)
		{
			Paragraph p = new Paragraph(@"<w:p xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"">
  <w:r>
	<w:rPr>
        <w:b />
        <w:u w:val=""single"" />
        <w:sz w:val=""48"" />
        <w:szCs w:val=""48"" />
      </w:rPr>
	<w:t>" + text + @"</w:t>
  </w:r>
</w:p>");
			return new customSection(new List<OpenXmlCompositeElement>() { p });
		}

		internal customSection getHeader2(string text)
		{
			Paragraph p = new Paragraph(@"<w:p xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"">
  <w:r>
	<w:rPr>
        <w:b />
        <w:sz w:val=""24"" />
        <w:szCs w:val=""24"" />
      </w:rPr>
	<w:t>" + text + @"</w:t>
  </w:r>
</w:p>");
			return new customSection(new List<OpenXmlCompositeElement>() { p });
		}

		internal customSection getPageBreak()
		{
			return new customSection(new Break() { Type = BreakValues.Page });
		}

		internal customSection getBullet(string text)
		{
			Paragraph p = new Paragraph(@"<w:p w:rsidR=""008455AA"" w:rsidP=""008455AA"" w:rsidRDefault=""008455AA"" xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"">
  <w:pPr>
    <w:pStyle w:val=""ListParagraph"" />
    <w:numPr>
      <w:ilvl w:val=""0"" />
      <w:numId w:val=""1"" />
    </w:numPr>
  </w:pPr>
  <w:r>
    <w:t>" + text + @"</w:t>
  </w:r>
</w:p>");
			return new customSection(new List<OpenXmlCompositeElement>() {p});
		}

		internal List<customSection> getHTML(string text, List<customSection> sections = null)
		{
			if (sections == null) sections = new List<customSection>();
			NotesFor.HtmlToOpenXml.HtmlConverter htmlconv = new NotesFor.HtmlToOpenXml.HtmlConverter(mainDoc);
			int start = text.ToString().IndexOf("{IMG");

			if (start > 0)
			{
				//There's an image.  Start by adding the text before the image

				sections.Add(new customSection(htmlconv.Parse(text.Substring(0, start))));
				//Add the image
				string part = text.Substring(start);
				int end = part.IndexOf('}');
				imageObj image = new imageObj();
				string imagestring = part.Substring(5, end - 5); //gives "#:title" inside the {}
				image.Url = "http://mbastorage.blob.core.windows.net/sfwart/" + new ARTService().ImageGet(m_ReportId, Convert.ToInt32(imagestring.Split(':')[0])).Path.Replace(" ", "%20");
				image.Link = image.Url;
				image.Filename = image.Url;
				sections.Add(new customSection(image));

				//Now recursively pass the remaining xml to this function again (in case there are additional images to process)
				sections = getHTML(text.Substring(start + end+ 1), sections);
			}
			else
			{
				sections.Add(new customSection(htmlconv.Parse(text)));
			}
			return sections;
		}


		internal customSection getOverallRec(DataTable dt)
		{

			Table table1 = new Table();

			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };

			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			TableGrid tableGrid1 = new TableGrid();
			tableGrid1.Append(new GridColumn() { Width = "3325" });
			tableGrid1.Append(new GridColumn() { Width = "1350" });
			tableGrid1.Append(new GridColumn() { Width = "1350" });
			tableGrid1.Append(new GridColumn() { Width = "1350" });
			tableGrid1.Append(new GridColumn() { Width = "1350" });
			tableGrid1.Append(new GridColumn() { Width = "1890" });

			table1.Append(tableProperties1);
			table1.Append(tableGrid1);

			TableRow tableRow1 = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			tableRow1.Append(getCell("Stock / Fishery", clrBlueMid, "3325", clrBlueText, true));
			tableRow1.Append(getCell("Impacts on \r\nthe Stock", clrBlueMid, "1350", clrBlueText, true));
			tableRow1.Append(getCell("Impacts on \r\nother Spp.", clrBlueMid, "1350", clrBlueText, true));
			tableRow1.Append(getCell("Management", clrBlueMid, "1350", clrBlueText, true));
			tableRow1.Append(getCell("Habitat and \r\nEcosystem", clrBlueMid, "1350", clrBlueText, true));
			tableRow1.Append(getCell("Overall\r\nRecommendation", clrBlueMid, "1890", clrBlueText, true));
			table1.Append(tableRow1);

			for (int r = 0; r < dt.Rows.Count; r++)
			{
				tableRow1 = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dt.Rows[r]["CommonName"].ToString() + "\r\n" + dt.Rows[r]["Region"].ToString() + " - " + dt.Rows[r]["Method"].ToString();
				tableRow1.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col1 = dt.Rows[r]["Color1"].ToString() + " (" + dt.Rows[r]["Score1"].ToString() + ")";
				tableRow1.Append(getCell(col1, getColor(dt.Rows[r]["Color1"].ToString()), "1000"));

				string col2 = dt.Rows[r]["Color2"].ToString() + " (" + dt.Rows[r]["Score2"].ToString() + ")";
				tableRow1.Append(getCell(col2, getColor(dt.Rows[r]["Color2"].ToString()), "1000"));

				string col3 = dt.Rows[r]["Color3"].ToString() + " (" + dt.Rows[r]["Score3"].ToString() + ")";
				tableRow1.Append(getCell(col3, getColor(dt.Rows[r]["Color3"].ToString()), "1000"));

				string col4 = dt.Rows[r]["Color4"].ToString() + " (" + dt.Rows[r]["Score4"].ToString() + ")";
				tableRow1.Append(getCell(col4, getColor(dt.Rows[r]["Color4"].ToString()), "1000"));

				string col5 = dt.Rows[r]["Rec"].ToString() + " (" + dt.Rows[r]["Score"].ToString() + ")";
				tableRow1.Append(getCell(col5, getColor(dt.Rows[r]["Color"].ToString()), "1000", true));

				table1.Append(tableRow1);
			}


			return new customSection(table1);
		}

		private List<customSection> getCriteria1Grids(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3325" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });

			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Region / Method", clrBlueMid, "3325", clrBlueText, true));
			header.Append(getCell("Inherent \r\nVulnerability", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Stock Status", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Fishing Mortality", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Subscore", clrBlueMid, "1890", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			List<customSection> tables = new List<customSection>();

			string commonName = "";
			foreach (DataRow dr in dt.Rows)
			{
				//Run once per species
				if (commonName != dr["CommonName"].ToString())
				{
					//Store what you've already done (skip first pass), and reset variable
					if (commonName != "") tables.Add(new customSection(tableBody));
					commonName = dr["CommonName"].ToString();

					//add Species Header
					tableHeader = new Table();
					TableGrid tmpGrid = (TableGrid)gridHeader.Clone();
					TableRow headerRow = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
					headerRow.Append(getCell(commonName, clrHeading, "8725", clrBlueText, true));
					tmpGrid.Append(headerRow);
					tableHeader.Append(tableProperties1.CloneNode(true));
					tableHeader.Append(tmpGrid);
					tables.Add(new customSection(tableHeader));
					
					//Add "Region/Method" header
					tableBody = new Table();
					tableBody.Append(tableProperties1.CloneNode(true));
					tableBody.Append(gridBody.CloneNode(true));
					tableBody.Append(header.CloneNode(true));
					
				}

				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["Location"].ToString() + " " + dr["BodyOfWater"].ToString() + "\r\n" + dr["Method"].ToString();
				row.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col1 = dr["Factor1Score"].ToString() + ":" + dr["Factor1ScoreDescription"].ToString();
				row.Append(getCell(col1, "FFFFFF", "1000"));

				string col2 = dr["Factor2Score"].ToString() + ":" + dr["Factor2ScoreDescription"].ToString();
				row.Append(getCell(col2, "FFFFFF", "1000"));

				string col3 = dr["Factor3Score"].ToString() + ":" + dr["Factor3ScoreDescription"].ToString();
				row.Append(getCell(col3, "FFFFFF", "1000"));

				string col5 = dr["Color"].ToString() + " (" + dr["Score"].ToString() + ")";
				row.Append(getCell(col5, getColor(dr["Color"].ToString()), "1000", true));

				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria2Grids(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3325" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });

			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Species", clrBlueMid, "3325", clrBlueText, true));
			header.Append(getCell("Inherent \r\nVulnerability", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Stock Status", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Fishing Mortality", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Subscore", clrBlueMid, "1890", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			List<customSection> tables = new List<customSection>();

			string commonName = "";
			foreach (DataRow dr in dt.Rows)
			{
				//Run once per species
				if (commonName != dr["LocationMethod"].ToString())
				{
					//Store what you've already done (skip first pass), and reset variable
					if (commonName != "") tables.Add(new customSection(tableBody));
					commonName = dr["LocationMethod"].ToString();

					//add Species Header
					tableHeader = new Table();
					TableGrid tmpGrid = (TableGrid)gridHeader.Clone();
					TableRow headerRow = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
					headerRow.Append(getCell(commonName, clrHeading, "8725", clrBlueText, true));
					
					//add Scoring Header
					#region Scoring Header
					TableRow headerRow2 = new TableRow(@"<w:tr w:rsidR=""00B702EB"" w:rsidTr=""00284F47"" xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"">
  <w:trPr>
    <w:cnfStyle w:val=""000000100000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""0"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""1"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
    <w:trHeight w:val=""297"" />
  </w:trPr>
  <w:tc>
    <w:tcPr>
      <w:cnfStyle w:val=""001000000000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""0"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
      <w:tcW w:w=""9576"" w:type=""dxa"" />
      <w:tcBorders>
        <w:top w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
        <w:left w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
        <w:bottom w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
        <w:right w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
      </w:tcBorders>
    </w:tcPr>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val=""TableGrid"" />
        <w:tblpPr w:leftFromText=""180"" w:rightFromText=""180"" w:vertAnchor=""text"" w:horzAnchor=""margin"" w:tblpX=""-90"" w:tblpY=""14"" />
        <w:tblOverlap w:val=""never"" />
        <w:tblW w:w=""0"" w:type=""auto"" />
        <w:tblBorders>
          <w:top w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:left w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:bottom w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:right w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:insideH w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:insideV w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
        </w:tblBorders>
        <w:tblLayout w:type=""fixed"" />
        <w:tblLook w:val=""04A0"" w:firstRow=""1"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:noHBand=""0"" w:noVBand=""1"" />
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w=""1170"" />
        <w:gridCol w:w=""630"" />
        <w:gridCol w:w=""720"" />
        <w:gridCol w:w=""1440"" />
        <w:gridCol w:w=""630"" />
        <w:gridCol w:w=""720"" />
        <w:gridCol w:w=""1080"" />
        <w:gridCol w:w=""630"" />
      </w:tblGrid>
      <w:tr w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidTr=""00284F47"">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""1170"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""auto"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00284F47"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr=""00284F47"">
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>Subscore:</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""630"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""DBE5F1"" w:themeFill=""accent1"" w:themeFillTint=""33"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00F56BE2"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>" + dr["HeaderSubScore"].ToString() + @"</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""720"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""auto"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00284F47"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""1440"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""auto"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00284F47"" w:rsidRDefault=""00284F47"">
            <w:pPr>
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>Discard Rate:</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""630"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""DBE5F1"" w:themeFill=""accent1"" w:themeFillTint=""33"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00F56BE2"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>" + dr["HeaderDiscardRate"].ToString() + @"</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""720"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""auto"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00284F47"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""1080"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""auto"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00284F47"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>C2 Rate:</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w=""630"" w:type=""dxa"" />
            <w:shd w:val=""clear"" w:color=""auto"" w:fill=""DBE5F1"" w:themeFill=""accent1"" w:themeFillTint=""33"" />
          </w:tcPr>
          <w:p w:rsidRPr=""00284F47"" w:rsidR=""00284F47"" w:rsidP=""00B702EB"" w:rsidRDefault=""00F56BE2"">
            <w:pPr>
              <w:jc w:val=""center"" />
              <w:rPr>
                <w:b />
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b />
              </w:rPr>
              <w:t>" + dr["HeaderScore"].ToString() + @"</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR=""00284F47"" w:rsidP=""00284F47"" w:rsidRDefault=""00284F47"" />
  </w:tc>
</w:tr>");
					#endregion
					
					//tmpGrid.Append(headerRow);
					//tmpGrid.Append(headerRow2);
					tableHeader.Append(tableProperties1.CloneNode(true));
					tableHeader.Append(tmpGrid);
					tableHeader.Append(headerRow);
					tableHeader.Append(headerRow2);
					tables.Add(new customSection(tableHeader));

					//Add "Region/Method" header
					tableBody = new Table();
					tableBody.Append(tableProperties1.CloneNode(true));
					tableBody.Append(gridBody.CloneNode(true));
					tableBody.Append(header.CloneNode(true));

				}

				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["CommonName"].ToString();
				row.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col1 = dr["Factor1Score"].ToString() + ":" + dr["Factor1ScoreDescription"].ToString();
				row.Append(getCell(col1, "FFFFFF", "1000"));

				string col2 = dr["Factor2Score"].ToString() + ":" + dr["Factor2ScoreDescription"].ToString();
				row.Append(getCell(col2, "FFFFFF", "1000"));

				string col3 = dr["Factor3Score"].ToString() + ":" + dr["Factor3ScoreDescription"].ToString();
				row.Append(getCell(col3, "FFFFFF", "1000"));

				string col5 = dr["SubScore"].ToString();
				row.Append(getCell(col5, "FFFFFF", "1000", true));

				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria3Grid(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3325" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });

			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Region / Method", clrBlueMid, "3325", clrBlueText, true));
			header.Append(getCell("Management of \r\nRetained Species", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Management of \r\nNon-Retained Species", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Overall \r\nRecommendation", clrBlueMid, "1350", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			//Add column headers
			tableBody = new Table();
			tableBody.Append(tableProperties1.CloneNode(true));
			tableBody.Append(gridBody.CloneNode(true));
			tableBody.Append(header.CloneNode(true));

			List<customSection> tables = new List<customSection>();

			//string commonName = "";

			foreach (DataRow dr in dt.Rows)
			{
				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["Location"].ToString() + " " + dr["BodyOfWater"].ToString() + "\r\n" + dr["Method"].ToString();
				row.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col1 = dr["Score1"].ToString();
				row.Append(getCell(col1, "FFFFFF", "1000"));

				string col2 = dr["Score2"].ToString();
				row.Append(getCell(col2, "FFFFFF", "1000"));

				string col5 = dr["Color"].ToString() + "(" + dr["Score"].ToString() + ")";
				row.Append(getCell(col5, getColor(dr["Color"].ToString()), "1000"));

				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria31Grid(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3315" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });
			gridBody.Append(new GridColumn() { Width = "770" });

			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Region / Method", clrBlueMid, "3315", clrBlueText, true));
			header.Append(getCell("Strategy", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Recovery", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Research", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Advice", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Enforce", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Track", clrBlueMid, "770", clrBlueText, true));
			header.Append(getCell("Inclusion", clrBlueMid, "770", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			List<customSection> tables = new List<customSection>();

			//add 3.1 Header
			tableHeader = new Table();
			TableGrid tmpGrid = (TableGrid)gridHeader.Clone();
			TableRow headerRow = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			headerRow.Append(getCell("Factor 3.1: Management of fishing impacts on retained species", clrHeading, "8725", clrBlueText, true));
			tmpGrid.Append(headerRow);
			tableHeader.Append(tableProperties1.CloneNode(true));
			tableHeader.Append(tmpGrid);
			tables.Add(new customSection(tableHeader));

			//Add column headers
			tableBody = new Table();
			tableBody.Append(tableProperties1.CloneNode(true));
			tableBody.Append(gridBody.CloneNode(true));
			tableBody.Append(header.CloneNode(true));

			//string commonName = "";
			foreach (DataRow dr in dt.Rows)
			{
				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["Location"].ToString() + " " + dr["BodyOfWater"].ToString() + "\r\n" + dr["Method"].ToString();
				row.Append(getCell(col, clrBlueLt, "3315", clrGreyText, true));

				string col2 = dr["Factor2ScoreDescription"].ToString();
				row.Append(getCell(col2, "FFFFFF", "700"));

				string col3 = dr["Factor3ScoreDescription"].ToString();
				row.Append(getCell(col3, "FFFFFF", "700"));

				string col4 = dr["Factor4ScoreDescription"].ToString();
				row.Append(getCell(col4, "FFFFFF", "700"));

				string col5 = dr["Factor5ScoreDescription"].ToString();
				row.Append(getCell(col5, "FFFFFF", "700"));

				string col6 = dr["Factor6ScoreDescription"].ToString();
				row.Append(getCell(col6, "FFFFFF", "700"));

				string col7 = dr["Factor7ScoreDescription"].ToString();
				row.Append(getCell(col7, "FFFFFF", "700"));

				string col8 = dr["Factor8ScoreDescription"].ToString();
				row.Append(getCell(col8, "FFFFFF", "700"));

				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria32Grid(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3325" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });


			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Region / Method", clrBlueMid, "3325", clrBlueText, true));
			header.Append(getCell("Strategy", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Research", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Advice", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Enforce", clrBlueMid, "1350", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			List<customSection> tables = new List<customSection>();

			//add 3.2 Header
			tableHeader = new Table();
			TableGrid tmpGrid = (TableGrid)gridHeader.Clone();
			TableRow headerRow = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			headerRow.Append(getCell("Factor 3.2: Management of fishing impacts on bycatch species", clrHeading, "8725", clrBlueText, true));
			tmpGrid.Append(headerRow);
			tableHeader.Append(tableProperties1.CloneNode(true));
			tableHeader.Append(tmpGrid);
			tables.Add(new customSection(tableHeader));

			//Add column headers
			tableBody = new Table();
			tableBody.Append(tableProperties1.CloneNode(true));
			tableBody.Append(gridBody.CloneNode(true));
			tableBody.Append(header.CloneNode(true));

			//string commonName = "";
			foreach (DataRow dr in dt.Rows)
			{
				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["Location"].ToString() + " " + dr["BodyOfWater"].ToString() + "\r\n" + dr["Method"].ToString();
				row.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col2 = dr["Factor2ScoreDescription"].ToString();
				row.Append(getCell(col2, "FFFFFF", "1000"));

				string col3 = dr["Factor3ScoreDescription"].ToString();
				row.Append(getCell(col3, "FFFFFF", "1000"));

				string col4 = dr["Factor4ScoreDescription"].ToString();
				row.Append(getCell(col4, "FFFFFF", "1000"));

				string col5 = dr["Factor5ScoreDescription"].ToString();
				row.Append(getCell(col5, "FFFFFF", "1000"));
				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria4Grid(DataTable dt)
		{
			//Set up TableProperties
			TableProperties tableProperties1 = new TableProperties();
			TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
			TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };
			TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };
			TableLook tableLook1 = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };
			tableProperties1.Append(tableStyle1);
			tableProperties1.Append(tableWidth1);
			tableProperties1.Append(tableIndentation1);
			tableProperties1.Append(tableLook1);

			//Species header (need as separate grid b/c only one column)
			TableGrid gridHeader = new TableGrid();
			gridHeader.Append(new GridColumn() { Width = "8725" });

			//Body
			TableGrid gridBody = new TableGrid();
			gridBody.Append(new GridColumn() { Width = "3325" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });
			gridBody.Append(new GridColumn() { Width = "1350" });

			//Title bar
			TableRow header = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };
			header.Append(getCell("Region / Method", clrBlueMid, "3325", clrBlueText, true));
			header.Append(getCell("Gear Type and \r\nSubstrate", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("Mitigation of \r\nGear Impacts", clrBlueMid, "1350", clrBlueText, true));
			header.Append(getCell("EBFM", clrBlueMid, "1350", clrBlueText,true));
			header.Append(getCell("Overall Recomm.", clrBlueMid, "1890", clrBlueText, true));

			Table tableHeader = new Table();
			Table tableBody = new Table();

			List<customSection> tables = new List<customSection>();

			//Add "Region/Method" header
			tableBody = new Table();
			tableBody.Append(tableProperties1.CloneNode(true));
			tableBody.Append(gridBody.CloneNode(true));
			tableBody.Append(header.CloneNode(true));
			
			foreach (DataRow dr in dt.Rows)
			{
				TableRow row = new TableRow() { RsidTableRowAddition = "003C5001", RsidTableRowProperties = "003C5001" };

				string col = dr["Location"].ToString() + " " + dr["BodyOfWater"].ToString() + "\r\n" + dr["Method"].ToString();
				row.Append(getCell(col, clrBlueLt, "1000", clrGreyText, true));

				string col1 = dr["Factor1Score"].ToString() + ":" + dr["Factor1ScoreDescription"].ToString();
				row.Append(getCell(col1, "FFFFFF", "1000"));

				string col2 = dr["Factor2Score"].ToString() + ":" + dr["Factor2ScoreDescription"].ToString();
				row.Append(getCell(col2, "FFFFFF", "1000"));

				string col3 = dr["Factor3Score"].ToString() + ":" + dr["Factor3ScoreDescription"].ToString();
				row.Append(getCell(col3, "FFFFFF", "1000"));

				string col5 = dr["Color"].ToString() + " (" + dr["Score"].ToString() + ")";
				row.Append(getCell(col5, getColor(dr["Color"].ToString()), "1000", true));

				tableBody.Append(row);
			}
			tables.Add(new customSection(tableBody)); //store last table
			return tables;
		}

		private List<customSection> getCriteria12(DataTable dt)
		{
			List<customSection> tables = new List<customSection>();

			string species = "";
			string ft = "";
			foreach (DataRow dr in dt.Rows)
			{
				//Run once per species
				if (species != dr["Species"].ToString())
				{
					tables.Add(getHeader1(dr["Species"].ToString())); //TODO change to image background text (that blue gradient)
					species = dr["Species"].ToString();
				}

				//Run once per Factor Type
				if (ft != dr["FactorType"].ToString())
				{
					tables.Add(getHeader2(dr["FactorType"].ToString()));
					ft = dr["FactorType"].ToString();
				}

				//Add body
				string body = dr["KeyInfo"].ToString();
				if (dr["Rationale"].ToString().Length > 0) body += "<br/><br/><b>Rationale:</b><br/>" + dr["Rationale"].ToString();

				//tables.AddRange(getHTML(body));
				string result = "";
				//foreach (customSection section in getHTML(body))
				//{
				//    foreach (OpenXmlCompositeElement xml in section.Xml)
				//    {
				//        result += xml.OuterXml;
				//    }
				//}

				tables.Add(getCriteriaSection(dr["LocationMethod"].ToString(), dr["ScoreNum"].ToString(), dr["ScoreDescription"].ToString(), result));
			}					
			return tables;
		}

		private List<customSection> getCriteria34(DataTable dt)
		{
			List<customSection> tables = new List<customSection>();

			string ft = "";
			foreach (DataRow dr in dt.Rows)
			{
				////Run once per factor type
				if (ft != dr["FactorType"].ToString())
				{
					tables.Add(getHeader2(dr["FactorType"].ToString())); //TODO change to image background text (that blue gradient)
					ft = dr["FactorType"].ToString();
				}

				//Add body
				string body = dr["KeyInfo"].ToString();
				if (dr["Rationale"].ToString().Length > 0) body += "<br/><br/><b>Rationale:</b><br/>" + dr["Rationale"].ToString();
				string result = "";
				foreach (customSection section in getHTML(body))
				{
					foreach (OpenXmlCompositeElement xml in section.Xml)
					{
						result += xml.OuterXml;
					}
				}

				tables.Add(getCriteriaSection(dr["LocationMethod"].ToString(), dr["ScoreNum"].ToString(), dr["ScoreDescription"].ToString(), result));
			}
			return tables;
		}

		private customSection getCriteriaSection(string fishery, string scoreNum, string scoreDescription, string body)
		{
			Table t = new Table(@"<w:tbl xmlns:w=""http://schemas.openxmlformats.org/wordprocessingml/2006/main"">
  <w:tblPr>
    <w:tblStyle w:val=""LightList-Accent5"" />
    <w:tblW w:w=""0"" w:type=""auto"" />
    <w:tblBorders>
      <w:top w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
      <w:left w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
      <w:bottom w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
      <w:right w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
    </w:tblBorders>
    <w:tblLook w:val=""04A0"" w:firstRow=""1"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:noHBand=""0"" w:noVBand=""1"" />
  </w:tblPr>
  <w:tblGrid>
    <w:gridCol w:w=""9576"" />
  </w:tblGrid>
  <w:tr w:rsidR=""00B702EB"" w:rsidTr=""00B702EB"">
    <w:trPr>
      <w:cnfStyle w:val=""100000000000"" w:firstRow=""1"" w:lastRow=""0"" w:firstColumn=""0"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""0"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
    </w:trPr>
    <w:tc>
      <w:tcPr>
        <w:cnfStyle w:val=""001000000000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""0"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
        <w:tcW w:w=""9576"" w:type=""dxa"" />
		<w:shd w:val=""clear"" w:color=""auto"" w:fill=""" + clrBlueMid + @""" w:themeFillShade=""80"" />
      </w:tcPr>
      <w:p w:rsidR=""00B702EB"" w:rsidP=""000E5482"" w:rsidRDefault=""00B702EB"">
        <w:r>
		  <w:rPr>
            <w:b />
            <w:color w:val=""" + clrBlueText + @"""/>
          </w:rPr>
          <w:t>" + fishery + @"</w:t>
        </w:r>
      </w:p>
    </w:tc>
  </w:tr>
  <w:tr w:rsidR=""00B702EB"" w:rsidTr=""00B702EB"">
    <w:trPr>
      <w:cnfStyle w:val=""000000100000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""0"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""1"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
      <w:trHeight w:val=""297"" />
    </w:trPr>
    <w:tc>
      <w:tcPr>
        <w:cnfStyle w:val=""001000000000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""0"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
        <w:tcW w:w=""9576"" w:type=""dxa"" />
        <w:tcBorders>
          <w:top w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:left w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:bottom w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
          <w:right w:val=""none"" w:color=""auto"" w:sz=""0"" w:space=""0"" />
        </w:tcBorders>
		<w:shd w:val=""clear"" w:color=""auto"" w:fill=""" + clrBlueLt + @""" w:themeFillShade=""80"" />
      </w:tcPr>
      <w:tbl>
        <w:tblPr>
          <w:tblStyle w:val=""TableGrid"" />
          <w:tblpPr w:leftFromText=""180"" w:rightFromText=""180"" w:vertAnchor=""text"" w:horzAnchor=""margin"" w:tblpY=""14"" />
          <w:tblOverlap w:val=""never"" />
          <w:tblW w:w=""0"" w:type=""auto"" />
          <w:tblLook w:val=""04A0"" w:firstRow=""1"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:noHBand=""0"" w:noVBand=""1"" />
        </w:tblPr>
        <w:tblGrid>
          <w:gridCol w:w=""985"" />
        </w:tblGrid>
        <w:tr w:rsidR=""00B702EB"" w:rsidTr=""00B702EB"">
          <w:tc>
            <w:tcPr>
              <w:tcW w:w=""985"" w:type=""dxa"" />
              <w:shd w:val=""clear"" w:color=""auto"" w:fill=""808080"" w:themeFill=""background1"" w:themeFillShade=""80"" />
            </w:tcPr>
            <w:p w:rsidRPr=""00B702EB"" w:rsidR=""00B702EB"" w:rsidP=""00B702EB"" w:rsidRDefault=""00B702EB"">
              <w:pPr>
                <w:jc w:val=""center"" />
                <w:rPr>
                  <w:b />
                </w:rPr>
              </w:pPr>
              <w:r w:rsidRPr=""00B702EB"">
                <w:rPr>
                  <w:b />
                  <w:color w:val=""FFFFFF"" w:themeColor=""background1"" />
                </w:rPr>
                <w:t>" + scoreNum + @"</w:t>
              </w:r>
            </w:p>
          </w:tc>
        </w:tr>
      </w:tbl>
      <w:p w:rsidR=""00B702EB"" w:rsidP=""000E5482"" w:rsidRDefault=""00B702EB"">
        <w:r>
		  <w:rPr>
            <w:b/>
          </w:rPr>
          <w:t xml:space=""preserve"">" + scoreDescription + @"</w:t>
        </w:r>
      </w:p>
    </w:tc>
  </w:tr>
  <w:tr w:rsidR=""00B702EB"" w:rsidTr=""00B702EB"">
    <w:tc>
      <w:tcPr>
        <w:cnfStyle w:val=""001000000000"" w:firstRow=""0"" w:lastRow=""0"" w:firstColumn=""1"" w:lastColumn=""0"" w:oddVBand=""0"" w:evenVBand=""0"" w:oddHBand=""0"" w:evenHBand=""0"" w:firstRowFirstColumn=""0"" w:firstRowLastColumn=""0"" w:lastRowFirstColumn=""0"" w:lastRowLastColumn=""0"" />
        <w:tcW w:w=""9576"" w:type=""dxa"" />
		<w:shd w:val=""clear"" w:color=""auto"" w:fill=""" + clrBlueLt + @""" w:themeFillShade=""80"" />
      </w:tcPr>
      <w:p w:rsidRPr=""00B702EB"" w:rsidR=""00B702EB"" w:rsidP=""000E5482"" w:rsidRDefault=""00B702EB"">
        <w:pPr>
          <w:rPr>
            <w:b w:val=""0"" />
          </w:rPr>
        </w:pPr>
        <w:r w:rsidRPr=""00B702EB"">
          <w:rPr>
            <w:b w:val=""0"" />
          </w:rPr>
          <w:t xml:space=""preserve"">" + body.ToString() + @"</w:t>
        </w:r>
      </w:p>
      <w:p w:rsidR=""00B702EB"" w:rsidP=""000E5482"" w:rsidRDefault=""00B702EB"" />
    </w:tc>
  </w:tr>
</w:tbl>");
			return new customSection(new List<OpenXmlCompositeElement>() { t });
		}


		private TableCell getCell(string text, string fillClr, string width, bool isBold = false)
		{
			return getCell(text, fillClr, width, "000000", isBold);
		}

		private TableCell getCell(string text, string fillClr, string width, string textColor, bool isBold = false)
		{
			string[] textSplit = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

			TableCell tableCell1 = new TableCell();

			TableCellProperties tableCellProperties1 = new TableCellProperties();
			TableCellWidth tableCellWidth1 = new TableCellWidth() { Width = width, Type = TableWidthUnitValues.Dxa };

			TableCellBorders tableCellBorders1 = new TableCellBorders();
			TopBorder topBorder1 = new TopBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
			LeftBorder leftBorder1 = new LeftBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
			BottomBorder bottomBorder1 = new BottomBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
			RightBorder rightBorder1 = new RightBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

			tableCellBorders1.Append(topBorder1);
			tableCellBorders1.Append(leftBorder1);
			tableCellBorders1.Append(bottomBorder1);
			tableCellBorders1.Append(rightBorder1);
			Shading shading1 = new Shading() { Val = ShadingPatternValues.Clear, Color = "auto", Fill = fillClr };
			HideMark hideMark1 = new HideMark();

			tableCellProperties1.Append(tableCellWidth1);
			tableCellProperties1.Append(tableCellBorders1);
			tableCellProperties1.Append(shading1);
			tableCellProperties1.Append(hideMark1);

			tableCell1.Append(tableCellProperties1);

			for (int i = 0; i < textSplit.Length; i++)
			{
				Paragraph paragraph1 = new Paragraph() { RsidParagraphAddition = "003C5001", RsidRunAdditionDefault = "003C5001" };

				ParagraphProperties paragraphProperties1 = new ParagraphProperties();
				SpacingBetweenLines spacingBetweenLines1 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

				paragraphProperties1.Append(spacingBetweenLines1);

				ParagraphMarkRunProperties paragraphMarkRunProperties1 = new ParagraphMarkRunProperties();
				Color color1 = new Color() { Val = textColor };
				paragraphMarkRunProperties1.Append(color1);

				Run run1 = new Run();

				RunProperties runProperties1 = new RunProperties();
				Color color2 = new Color() { Val = textColor };

				runProperties1.Append(color2);
				if (isBold) runProperties1.Append(new Bold());

				Text text1 = new Text();
				text1.Text = textSplit[i];

				run1.Append(runProperties1);
				run1.Append(text1);

				paragraph1.Append(paragraphProperties1);
				paragraph1.Append(run1);

				tableCell1.Append(paragraph1);
			}

			return tableCell1;
		}

		private string getColor(string name)
		{
			switch (name.ToLower())
			{
				case "green":
					return clrGreen;
				case "yellow":
					return clrYellow;
				case "red":
					return clrRed;
				case "black":
					return clrBlack;
				default:
					return "";
			}
		}

	}
}
