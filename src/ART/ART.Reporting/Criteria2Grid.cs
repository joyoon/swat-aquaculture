using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class Criteria2Grid : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria2Grid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public string Criteria2Synthesis { get; set; }

		private void reportFooter1_Format(object sender, EventArgs e)
		{
			if (Criteria2Synthesis.Length == 0)
			{
				reportFooter1.Visible = false;
			}
			else
			{
				this.Synthesis.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Criteria2Synthesis + "</font>";
			}
		}

		private void detail_Format(object sender, EventArgs e)
		{
			new Utility().ColorFormat(SubScore);
		}
	}
}
