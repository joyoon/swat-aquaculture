using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for NewActiveReport1.
	/// </summary>
	public partial class Criteria34 : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria34()
		{
			InitializeComponent();
		}

		private void grpFactorFooter_Format(object sender, EventArgs e)
		{
			KeyInfo.Html = "<font style='font-family:Arial; font-size:12pt;'>" + KeyInfo.Text;
			Rationale.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Rationale.Text;

			if (Rationale.Text == "")
			{
				Rationale.Visible = false;
				lblRationale.Visible = false;
			}
			else
			{
				Rationale.Visible = true;
				lblRationale.Visible = true;
			}
		}
	}
}
