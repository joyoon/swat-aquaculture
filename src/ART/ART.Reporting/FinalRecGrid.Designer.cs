namespace ART.Reporting
{
	/// <summary>
	/// Summary description for FinalRecGrid.
	/// </summary>
	partial class FinalRecGrid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FinalRecGrid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.CommonName = new DataDynamics.ActiveReports.TextBox();
			this.Crit1Score = new DataDynamics.ActiveReports.TextBox();
			this.Crit2Score = new DataDynamics.ActiveReports.TextBox();
			this.Crit3Score = new DataDynamics.ActiveReports.TextBox();
			this.Crit4Score = new DataDynamics.ActiveReports.TextBox();
			this.Score = new DataDynamics.ActiveReports.TextBox();
			this.textBox1 = new DataDynamics.ActiveReports.TextBox();
			this.Clr = new DataDynamics.ActiveReports.TextBox();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.CommonName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit1Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit2Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit3Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit4Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Clr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.CommonName,
            this.Crit1Score,
            this.Crit2Score,
            this.Crit3Score,
            this.Crit4Score,
            this.Score,
            this.textBox1,
            this.Clr});
			this.detail.Height = 0.5953333F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// CommonName
			// 
			this.CommonName.DataField = "CommonName";
			this.CommonName.Height = 0.253F;
			this.CommonName.Left = 0F;
			this.CommonName.Name = "CommonName";
			this.CommonName.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.CommonName.Text = "CommonName";
			this.CommonName.Top = 0F;
			this.CommonName.Width = 2.499F;
			// 
			// Crit1Score
			// 
			this.Crit1Score.DataField = "=Color1 + \" (\" + Score1 + \")\"";
			this.Crit1Score.Height = 0.506F;
			this.Crit1Score.Left = 2.499F;
			this.Crit1Score.Name = "Crit1Score";
			this.Crit1Score.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Crit1Score.Text = "Crit1Score";
			this.Crit1Score.Top = 0.003F;
			this.Crit1Score.Width = 0.9559989F;
			// 
			// Crit2Score
			// 
			this.Crit2Score.DataField = "=Color2 + \" (\" + Score2 + \")\"";
			this.Crit2Score.Height = 0.506F;
			this.Crit2Score.Left = 3.454999F;
			this.Crit2Score.Name = "Crit2Score";
			this.Crit2Score.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Crit2Score.Text = "Crit2Score";
			this.Crit2Score.Top = 0.002999991F;
			this.Crit2Score.Width = 0.9559989F;
			// 
			// Crit3Score
			// 
			this.Crit3Score.DataField = "=Color3 + \" (\" + Score3 + \")\"";
			this.Crit3Score.Height = 0.506F;
			this.Crit3Score.Left = 4.410998F;
			this.Crit3Score.Name = "Crit3Score";
			this.Crit3Score.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Crit3Score.Text = "Crit3Score";
			this.Crit3Score.Top = 0.002999994F;
			this.Crit3Score.Width = 0.9559989F;
			// 
			// Crit4Score
			// 
			this.Crit4Score.DataField = "=Color4 + \" (\" + Score4 + \")\"";
			this.Crit4Score.Height = 0.506F;
			this.Crit4Score.Left = 5.366999F;
			this.Crit4Score.Name = "Crit4Score";
			this.Crit4Score.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Crit4Score.Text = "Crit4Score";
			this.Crit4Score.Top = 0.003F;
			this.Crit4Score.Width = 0.9159999F;
			// 
			// Score
			// 
			this.Score.DataField = "=Rec + \" (\" + Score + \")\"";
			this.Score.Height = 0.506F;
			this.Score.Left = 6.283F;
			this.Score.Name = "Score";
			this.Score.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Score.Text = "Score";
			this.Score.Top = 0F;
			this.Score.Width = 1.217F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = "=Region + \" - \" + Method";
			this.textBox1.Height = 0.253F;
			this.textBox1.Left = 0F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.textBox1.Text = "RegionAndMethod";
			this.textBox1.Top = 0.256F;
			this.textBox1.Width = 2.499F;
			// 
			// Clr
			// 
			this.Clr.DataField = "Color";
			this.Clr.Height = 0.2145F;
			this.Clr.Left = 5.94F;
			this.Clr.Name = "Clr";
			this.Clr.Style = "color: #666666; font-size: 9.75pt; font-weight: normal; vertical-align: middle; d" +
				"do-char-set: 0";
			this.Clr.Text = "Clr";
			this.Clr.Top = 0.141F;
			this.Clr.Visible = false;
			this.Clr.Width = 0.6230001F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8});
			this.reportHeader1.Height = 0.39F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// label3
			// 
			this.label3.Height = 0.387F;
			this.label3.HyperLink = null;
			this.label3.Left = 2.499F;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label3.Text = "Impacts on the Stock";
			this.label3.Top = 0.003F;
			this.label3.Width = 0.9559989F;
			// 
			// label4
			// 
			this.label4.Height = 0.387F;
			this.label4.HyperLink = null;
			this.label4.Left = 3.455F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label4.Text = "Impacts on other Spp.";
			this.label4.Top = 6.519258E-09F;
			this.label4.Width = 0.9559989F;
			// 
			// label5
			// 
			this.label5.Height = 0.387F;
			this.label5.HyperLink = null;
			this.label5.Left = 4.411F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "Management";
			this.label5.Top = 9.313226E-09F;
			this.label5.Width = 0.9559989F;
			// 
			// label6
			// 
			this.label6.Height = 0.387F;
			this.label6.HyperLink = null;
			this.label6.Left = 6.283F;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label6.Text = "Overall Recommendation";
			this.label6.Top = 0F;
			this.label6.Width = 1.217F;
			// 
			// label7
			// 
			this.label7.Height = 0.387F;
			this.label7.HyperLink = null;
			this.label7.Left = 5.367F;
			this.label7.Name = "label7";
			this.label7.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label7.Text = "Habitat and Ecosystem";
			this.label7.Top = 0F;
			this.label7.Width = 0.9159999F;
			// 
			// label8
			// 
			this.label8.Height = 0.387F;
			this.label8.HyperLink = null;
			this.label8.Left = 0F;
			this.label8.Name = "label8";
			this.label8.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label8.Text = "Stock /  Fishery";
			this.label8.Top = 0.002999991F;
			this.label8.Width = 2.499F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// FinalRecGrid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.CommonName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit1Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit2Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit3Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Crit4Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Clr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.TextBox CommonName;
		private DataDynamics.ActiveReports.TextBox Crit1Score;
		private DataDynamics.ActiveReports.TextBox Crit2Score;
		private DataDynamics.ActiveReports.TextBox Crit3Score;
		private DataDynamics.ActiveReports.TextBox Crit4Score;
		private DataDynamics.ActiveReports.TextBox Score;
		private DataDynamics.ActiveReports.TextBox textBox1;
		private DataDynamics.ActiveReports.TextBox Clr;
	}
}
