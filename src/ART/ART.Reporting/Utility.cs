﻿using System;
using System.Collections.Generic;
using DataDynamics.ActiveReports;
using System.Text;

namespace ART.Reporting
{
	public class Utility
	{
		public void ColorFormat(DataDynamics.ActiveReports.TextBox ctrl)
		{
			ColorFormat(ctrl, ctrl.Text);
		}

		public void ColorFormat(DataDynamics.ActiveReports.TextBox ctrl, string text)
		{
			if (text == null) return;

			if (text.Length > 4 && text.Substring(0, 5) == "Green")
			{
				ctrl.BackColor = System.Drawing.Color.FromArgb(138, 207, 88); //#8ACF58
				ctrl.ForeColor = System.Drawing.Color.Black;
			}
			else if (text.Length > 5 && text.Substring(0, 6) == "Yellow")
			{
				ctrl.BackColor = System.Drawing.Color.FromArgb(255, 255, 204); //#FDFFA4
				ctrl.ForeColor = System.Drawing.Color.FromArgb(156, 124, 0);
			}
			else if (text.Length > 2 && text.Substring(0, 3) == "Red")
			{
				ctrl.BackColor = System.Drawing.Color.FromArgb(255, 215, 215);
				ctrl.ForeColor = System.Drawing.Color.FromArgb(204, 51, 0);
			}
			else if (text.Length > 7 && text.Substring(0, 8) == "Critical")
			{
				ctrl.BackColor = System.Drawing.Color.FromName("Black");
				ctrl.ForeColor = System.Drawing.Color.White;
			}
			else
			{
				ctrl.BackColor = System.Drawing.Color.White;
				ctrl.ForeColor = System.Drawing.Color.Black;
			}
		}
	}
}
