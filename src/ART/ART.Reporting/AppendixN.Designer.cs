namespace ART.Reporting
{
	/// <summary>
	/// Summary description for AppendixAGrid.
	/// </summary>
	partial class AppendixN
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppendixN));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.richTextBox1 = new DataDynamics.ActiveReports.RichTextBox();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.line1 = new DataDynamics.ActiveReports.Line();
			this.Title = new DataDynamics.ActiveReports.TextBox();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			((System.ComponentModel.ISupportInitialize)(this.Title)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.richTextBox1});
			this.detail.Height = 0.2291667F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// richTextBox1
			// 
			this.richTextBox1.AutoReplaceFields = true;
			this.richTextBox1.DataField = "Appendix";
			this.richTextBox1.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox1.Height = 0.219F;
			this.richTextBox1.Left = 0F;
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.RTF = resources.GetString("richTextBox1.RTF");
			this.richTextBox1.Top = 0F;
			this.richTextBox1.Width = 7.5F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.line1,
            this.Title});
			this.reportHeader1.Height = 0.3229167F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// line1
			// 
			this.line1.Height = 0.01499999F;
			this.line1.Left = 0.042F;
			this.line1.LineColor = System.Drawing.Color.LightGray;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 0.26F;
			this.line1.Width = 7.458001F;
			this.line1.X1 = 0.042F;
			this.line1.X2 = 7.5F;
			this.line1.Y1 = 0.275F;
			this.line1.Y2 = 0.26F;
			// 
			// Title
			// 
			this.Title.DataField = "=\"Appendix: \" + Title";
			this.Title.Height = 0.26F;
			this.Title.Left = 0F;
			this.Title.Name = "Title";
			this.Title.Style = "color: #005A74; font-size: 15pt; font-weight: bold";
			this.Title.Text = "Title";
			this.Title.Top = 0F;
			this.Title.Width = 7.5F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// AppendixN
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.488418F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Title)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Line line1;
		private DataDynamics.ActiveReports.RichTextBox richTextBox1;
		private DataDynamics.ActiveReports.TextBox Title;
	}
}
