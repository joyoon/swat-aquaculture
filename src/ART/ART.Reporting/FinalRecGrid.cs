using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for FinalRecGrid.
	/// </summary>
	public partial class FinalRecGrid : DataDynamics.ActiveReports.ActiveReport
	{

		public FinalRecGrid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			new Utility().ColorFormat(Crit1Score);
			new Utility().ColorFormat(Crit2Score);
			new Utility().ColorFormat(Crit3Score);
			new Utility().ColorFormat(Crit4Score);
			new Utility().ColorFormat(Score, this.Clr.Text); //pass separate field as color indicator
			//new Utility().ColorFormat(Color);
		}
	}
}
