namespace ART.Reporting
{
	/// <summary>
	/// Summary description for References.
	/// </summary>
	partial class References
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(References));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.richTextBox1 = new DataDynamics.ActiveReports.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.richTextBox1});
			this.detail.Height = 0.5624999F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// richTextBox1
			// 
			this.richTextBox1.AutoReplaceFields = true;
			this.richTextBox1.DataField = "Reference";
			this.richTextBox1.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox1.Height = 0.271F;
			this.richTextBox1.Left = 0.052F;
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.RTF = resources.GetString("richTextBox1.RTF");
			this.richTextBox1.Top = 0F;
			this.richTextBox1.Width = 7.354F;
			// 
			// References
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.47925F;
			this.Sections.Add(this.detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.RichTextBox richTextBox1;
	}
}
