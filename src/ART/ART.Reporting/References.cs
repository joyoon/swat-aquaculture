using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for References.
	/// </summary>
	public partial class References : DataDynamics.ActiveReports.ActiveReport
	{

		public References()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			richTextBox1.Html = "<font style='font-family:Arial; font-size:12pt;'>" + richTextBox1.Text + "</font>";
			richTextBox1.Width = 7.5f;
			richTextBox1.SelectionIndent = 0.5F;
			richTextBox1.SelectionHangingIndent = 0.5F; //This indents subsequent lines
			
		}
	}
}
