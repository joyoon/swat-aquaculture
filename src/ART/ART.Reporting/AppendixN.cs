using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class AppendixN : DataDynamics.ActiveReports.ActiveReport
	{

		public AppendixN()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			richTextBox1.Html = "<font style='font-family:Arial; font-size:12pt;'>" + richTextBox1.Text + "</font>";
		}
	}
}
