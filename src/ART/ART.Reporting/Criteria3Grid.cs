using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class Criteria3Grid : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria3Grid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private Criteria31Grid rpt31Grid;
		private Criteria32Grid rpt32Grid;
		private Criteria34 rpt31;
		private Criteria34 rpt32;

		private void Criteria3Grid_ReportStart(object sender, EventArgs e)
		{
			rpt31Grid = new Criteria31Grid();
			rpt32Grid = new Criteria32Grid();
			rpt31 = new Criteria34();
			rpt32 = new Criteria34();

			rpt31Grid.DataSource = DataSource31Grid;
			rpt31Grid.Criteria31Synthesis = Criteria31Synthesis;

			rpt32Grid.DataSource = DataSource32Grid;
			rpt32Grid.Criteria32Synthesis = Criteria32Synthesis;
			
			rpt31.DataSource = DataSource31;
			rpt32.DataSource = DataSource32;

			this.Criteria31Grid.Report = rpt31Grid;
			this.Criteria32Grid.Report = rpt32Grid;
			this.Criteria31.Report = rpt31;
			this.Criteria32.Report = rpt32;
		}

		public Object DataSource31Grid { get; set; }
		public Object DataSource32Grid { get; set; }
		public Object DataSource31 { get; set; }
		public Object DataSource32 { get; set; }

		public string Criteria3Synthesis { get; set; }
		public string Criteria31Synthesis { get; set; }
		public string Criteria32Synthesis { get; set; }

		private void reportFooter1_Format(object sender, EventArgs e)
		{
			if (Criteria3Synthesis.Length == 0)
			{
				lblSynthesis.Visible = false;
				Synthesis.Visible = false;
			}
			else
			{
				this.Synthesis.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Criteria3Synthesis + "</font>";
			}
		}

		private void detail_Format(object sender, EventArgs e)
		{
			new Utility().ColorFormat(Score, Color.Text);
		}
	}
}
