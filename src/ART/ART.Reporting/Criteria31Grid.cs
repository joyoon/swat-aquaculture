using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class Criteria31Grid : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria31Grid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public string Criteria31Synthesis { get; set; }

		private void detail_Format(object sender, EventArgs e)
		{
			//new Utility().ColorFormat(Color);
		}

		private void groupFooter1_Format(object sender, EventArgs e)
		{
			this.Synthesis.Html = "<font style='font-family:Arial; font-size:12pt;'/>" + Criteria31Synthesis;
		}
	}
}
