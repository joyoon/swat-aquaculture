using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class Criteria32Grid : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria32Grid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			//new Utility().ColorFormat(this.Color);
		}

		public string Criteria32Synthesis { get; set; }

		private void reportFooter1_Format(object sender, EventArgs e)
		{
			this.Synthesis.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Criteria32Synthesis + "</font>";
		}
	}
}
