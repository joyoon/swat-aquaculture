namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria2Grid.
	/// </summary>
	partial class Criteria3Grid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria3Grid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.Region = new DataDynamics.ActiveReports.TextBox();
			this.Method = new DataDynamics.ActiveReports.TextBox();
			this.Score1 = new DataDynamics.ActiveReports.TextBox();
			this.Score = new DataDynamics.ActiveReports.TextBox();
			this.Score2 = new DataDynamics.ActiveReports.TextBox();
			this.Color = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.Criteria31Grid = new DataDynamics.ActiveReports.SubReport();
			this.Criteria32Grid = new DataDynamics.ActiveReports.SubReport();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			this.picture1 = new DataDynamics.ActiveReports.Picture();
			this.lblSynthesis = new DataDynamics.ActiveReports.Label();
			this.Synthesis = new DataDynamics.ActiveReports.RichTextBox();
			this.label2 = new DataDynamics.ActiveReports.Label();
			this.Criteria32 = new DataDynamics.ActiveReports.SubReport();
			this.Criteria31 = new DataDynamics.ActiveReports.SubReport();
			this.picture2 = new DataDynamics.ActiveReports.Picture();
			this.label3 = new DataDynamics.ActiveReports.Label();
			((System.ComponentModel.ISupportInitialize)(this.Region)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSynthesis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Region,
            this.Method,
            this.Score1,
            this.Score,
            this.Score2,
            this.Color});
			this.detail.Height = 0.4716665F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// Region
			// 
			this.Region.DataField = "=Location + \' \'+BodyOfWater";
			this.Region.Height = 0.231F;
			this.Region.Left = 0F;
			this.Region.Name = "Region";
			this.Region.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Region.Text = "Region";
			this.Region.Top = 2.384186E-07F;
			this.Region.Width = 2.333F;
			// 
			// Method
			// 
			this.Method.Border.BottomColor = System.Drawing.Color.Silver;
			this.Method.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Method.DataField = "Method";
			this.Method.Height = 0.231F;
			this.Method.Left = 0F;
			this.Method.Name = "Method";
			this.Method.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Method.Text = "Method";
			this.Method.Top = 0.231F;
			this.Method.Width = 2.333F;
			// 
			// Score1
			// 
			this.Score1.Border.BottomColor = System.Drawing.Color.Silver;
			this.Score1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Score1.DataField = "Score1";
			this.Score1.Height = 0.4619998F;
			this.Score1.Left = 2.333F;
			this.Score1.Name = "Score1";
			this.Score1.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Score1.Text = "Score1";
			this.Score1.Top = 0.006000238F;
			this.Score1.Width = 1.656F;
			// 
			// Score
			// 
			this.Score.Border.BottomColor = System.Drawing.Color.Silver;
			this.Score.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Score.DataField = "=Color + \" (\" + Score + \")\"";
			this.Score.Height = 0.4619998F;
			this.Score.Left = 5.562F;
			this.Score.Name = "Score";
			this.Score.Style = "color: #666666; font-size: 10pt; font-weight: bold; text-align: left; vertical-al" +
				"ign: middle";
			this.Score.Text = "Score";
			this.Score.Top = 0.006000238F;
			this.Score.Width = 1.219F;
			// 
			// Score2
			// 
			this.Score2.Border.BottomColor = System.Drawing.Color.Silver;
			this.Score2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Score2.DataField = "Score2";
			this.Score2.Height = 0.4619998F;
			this.Score2.Left = 3.989F;
			this.Score2.Name = "Score2";
			this.Score2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Score2.Text = "Score2";
			this.Score2.Top = 0.006000238F;
			this.Score2.Width = 1.573F;
			// 
			// Color
			// 
			this.Color.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Color.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Color.DataField = "Color";
			this.Color.Height = 0.2534998F;
			this.Color.Left = 5.364F;
			this.Color.Name = "Color";
			this.Color.Style = "color: #666666; font-size: 10pt; font-weight: bold; text-align: center; vertical-" +
				"align: middle";
			this.Color.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount;
			this.Color.Text = "Color";
			this.Color.Top = 0.07200003F;
			this.Color.Visible = false;
			this.Color.Width = 0.5310001F;
			// 
			// label1
			// 
			this.label1.Height = 0.4939997F;
			this.label1.HyperLink = null;
			this.label1.Left = 0F;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label1.Text = "Region / Method";
			this.label1.Top = 0F;
			this.label1.Width = 2.333F;
			// 
			// label6
			// 
			this.label6.Height = 0.488F;
			this.label6.HyperLink = null;
			this.label6.Left = 5.573999F;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; te" +
				"xt-align: left; vertical-align: middle";
			this.label6.Text = "Overall Recommendation";
			this.label6.Top = 0.006F;
			this.label6.Width = 1.207001F;
			// 
			// label5
			// 
			this.label5.Height = 0.4909998F;
			this.label5.HyperLink = null;
			this.label5.Left = 2.345F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "Management of Retained Species";
			this.label5.Top = 0.006F;
			this.label5.Width = 1.656F;
			// 
			// label8
			// 
			this.label8.Height = 0.488F;
			this.label8.HyperLink = null;
			this.label8.Left = 4.000999F;
			this.label8.Name = "label8";
			this.label8.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label8.Text = "Management of Non-Retained Species";
			this.label8.Top = 0.006F;
			this.label8.Width = 1.573F;
			// 
			// Criteria31Grid
			// 
			this.Criteria31Grid.CloseBorder = false;
			this.Criteria31Grid.Height = 0.562F;
			this.Criteria31Grid.Left = -4.440892E-16F;
			this.Criteria31Grid.Name = "Criteria31Grid";
			this.Criteria31Grid.Report = null;
			this.Criteria31Grid.ReportName = "Criteria31Grid";
			this.Criteria31Grid.Top = 1.545F;
			this.Criteria31Grid.Width = 7.511F;
			// 
			// Criteria32Grid
			// 
			this.Criteria32Grid.CloseBorder = false;
			this.Criteria32Grid.Height = 0.5730002F;
			this.Criteria32Grid.Left = 0F;
			this.Criteria32Grid.Name = "Criteria32Grid";
			this.Criteria32Grid.Report = null;
			this.Criteria32Grid.ReportName = "Criteria32Grid";
			this.Criteria32Grid.Top = 3.408F;
			this.Criteria32Grid.Width = 7.511F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label5,
            this.label6,
            this.label8,
            this.label1});
			this.reportHeader1.Height = 0.5F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picture1,
            this.lblSynthesis,
            this.Synthesis,
            this.Criteria31Grid,
            this.label2,
            this.Criteria32Grid,
            this.Criteria32,
            this.Criteria31,
            this.picture2,
            this.label3});
			this.reportFooter1.Height = 4.718334F;
			this.reportFooter1.Name = "reportFooter1";
			this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
			// 
			// picture1
			// 
			this.picture1.Height = 0.5190001F;
			this.picture1.HyperLink = null;
			this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
			this.picture1.Left = 0F;
			this.picture1.Name = "picture1";
			this.picture1.Top = 0.937F;
			this.picture1.Width = 7.5F;
			// 
			// lblSynthesis
			// 
			this.lblSynthesis.Height = 0.2703333F;
			this.lblSynthesis.HyperLink = null;
			this.lblSynthesis.Left = 0F;
			this.lblSynthesis.Name = "lblSynthesis";
			this.lblSynthesis.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 0";
			this.lblSynthesis.Text = "Synthesis";
			this.lblSynthesis.Top = 0.26F;
			this.lblSynthesis.Width = 2.871F;
			// 
			// Synthesis
			// 
			this.Synthesis.AutoReplaceFields = true;
			this.Synthesis.Font = new System.Drawing.Font("Arial", 10F);
			this.Synthesis.Height = 0.24F;
			this.Synthesis.Left = 0F;
			this.Synthesis.Name = "Synthesis";
			this.Synthesis.RTF = resources.GetString("Synthesis.RTF");
			this.Synthesis.Top = 0.53F;
			this.Synthesis.Width = 7.428F;
			// 
			// label2
			// 
			this.label2.Height = 0.3980002F;
			this.label2.HyperLink = null;
			this.label2.Left = 0F;
			this.label2.Name = "label2";
			this.label2.Style = "font-weight: bold; vertical-align: middle";
			this.label2.Text = "Factor 3.1:  Management of fishing impacts on retained species";
			this.label2.Top = 1F;
			this.label2.Width = 7.373F;
			// 
			// Criteria32
			// 
			this.Criteria32.CloseBorder = false;
			this.Criteria32.Height = 0.625F;
			this.Criteria32.Left = 0F;
			this.Criteria32.Name = "Criteria32";
			this.Criteria32.Report = null;
			this.Criteria32.ReportName = "Criteria3";
			this.Criteria32.Top = 4.062F;
			this.Criteria32.Width = 7.479F;
			// 
			// Criteria31
			// 
			this.Criteria31.CloseBorder = false;
			this.Criteria31.Height = 0.625F;
			this.Criteria31.Left = 0F;
			this.Criteria31.Name = "Criteria31";
			this.Criteria31.Report = null;
			this.Criteria31.ReportName = "Criteria3";
			this.Criteria31.Top = 2.166F;
			this.Criteria31.Width = 7.479F;
			// 
			// picture2
			// 
			this.picture2.Height = 0.5190001F;
			this.picture2.HyperLink = null;
			this.picture2.ImageData = ((System.IO.Stream)(resources.GetObject("picture2.ImageData")));
			this.picture2.Left = 0F;
			this.picture2.Name = "picture2";
			this.picture2.Top = 2.889F;
			this.picture2.Width = 7.5F;
			// 
			// label3
			// 
			this.label3.Height = 0.398F;
			this.label3.HyperLink = null;
			this.label3.Left = 0F;
			this.label3.Name = "label3";
			this.label3.Style = "font-weight: bold; vertical-align: middle";
			this.label3.Text = "Factor 3.2:  Management of fishing impacts on bycatch species";
			this.label3.Top = 2.952F;
			this.label3.Width = 7.373F;
			// 
			// Criteria3Grid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.500583F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			this.ReportStart += new System.EventHandler(this.Criteria3Grid_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Region)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSynthesis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox Region;
		private DataDynamics.ActiveReports.TextBox Method;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.TextBox Score;
		private DataDynamics.ActiveReports.TextBox Score1;
		private DataDynamics.ActiveReports.TextBox Score2;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.SubReport Criteria31Grid;
		private DataDynamics.ActiveReports.SubReport Criteria32Grid;
		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Label lblSynthesis;
		private DataDynamics.ActiveReports.RichTextBox Synthesis;
		private DataDynamics.ActiveReports.Picture picture1;
		private DataDynamics.ActiveReports.Label label2;
		private DataDynamics.ActiveReports.SubReport Criteria32;
		private DataDynamics.ActiveReports.SubReport Criteria31;
		private DataDynamics.ActiveReports.Picture picture2;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.TextBox Color;
	}
}
