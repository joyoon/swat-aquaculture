namespace ART.Reporting
{
	/// <summary>
	/// Summary description for NewActiveReport1.
	/// </summary>
	partial class Criteria1
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria1));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.ScoreDescription = new DataDynamics.ActiveReports.TextBox();
			this.lblRationale = new DataDynamics.ActiveReports.Label();
			this.KeyInfo = new DataDynamics.ActiveReports.RichTextBox();
			this.Rationale = new DataDynamics.ActiveReports.RichTextBox();
			this.ScoreNum = new DataDynamics.ActiveReports.TextBox();
			this.grpFactorType = new DataDynamics.ActiveReports.GroupHeader();
			this.FactorType = new DataDynamics.ActiveReports.TextBox();
			this.grpFactorTypeFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.LocationMethod = new DataDynamics.ActiveReports.TextBox();
			this.grpLocationMethod = new DataDynamics.ActiveReports.GroupHeader();
			this.grpLocationMethodFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.grpSpecies = new DataDynamics.ActiveReports.GroupHeader();
			this.picture1 = new DataDynamics.ActiveReports.Picture();
			this.Species = new DataDynamics.ActiveReports.TextBox();
			this.grpSpeciesFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.header = new DataDynamics.ActiveReports.ReportHeader();
			this.lblJustification = new DataDynamics.ActiveReports.Label();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			this.grpFactorId = new DataDynamics.ActiveReports.GroupHeader();
			this.grpFactorIdFooter = new DataDynamics.ActiveReports.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Species)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
			this.detail.ColumnSpacing = 0F;
			this.detail.Height = 0F;
			this.detail.Name = "detail";
			// 
			// ScoreDescription
			// 
			this.ScoreDescription.DataField = "ScoreDescription";
			this.ScoreDescription.Height = 0.291F;
			this.ScoreDescription.Left = 0.788F;
			this.ScoreDescription.Name = "ScoreDescription";
			this.ScoreDescription.Style = "color: #333333; font-size: 11pt; font-weight: bold; vertical-align: middle; white" +
				"-space: nowrap; ddo-char-set: 1";
			this.ScoreDescription.Text = "ScoreDescription";
			this.ScoreDescription.Top = 0.125F;
			this.ScoreDescription.Width = 6.629F;
			// 
			// lblRationale
			// 
			this.lblRationale.Height = 0.1875F;
			this.lblRationale.HyperLink = null;
			this.lblRationale.Left = 0.09000014F;
			this.lblRationale.Name = "lblRationale";
			this.lblRationale.Style = "font-family: Arial; font-size: 11pt; font-weight: bold; ddo-char-set: 1";
			this.lblRationale.Text = "Detail Rationale:";
			this.lblRationale.Top = 0.9276668F;
			this.lblRationale.Width = 1.76F;
			// 
			// KeyInfo
			// 
			this.KeyInfo.AutoReplaceFields = true;
			this.KeyInfo.CanShrink = true;
			this.KeyInfo.DataField = "KeyInfo";
			this.KeyInfo.Font = new System.Drawing.Font("Arial", 10F);
			this.KeyInfo.Height = 0.2290001F;
			this.KeyInfo.Left = 0.06900001F;
			this.KeyInfo.Name = "KeyInfo";
			this.KeyInfo.RTF = resources.GetString("KeyInfo.RTF");
			this.KeyInfo.Top = 0.536F;
			this.KeyInfo.Width = 7.389F;
			// 
			// Rationale
			// 
			this.Rationale.AutoReplaceFields = true;
			this.Rationale.DataField = "Rationale";
			this.Rationale.Font = new System.Drawing.Font("Arial", 10F);
			this.Rationale.Height = 0.2290001F;
			this.Rationale.Left = 0.06900001F;
			this.Rationale.Name = "Rationale";
			this.Rationale.RTF = resources.GetString("Rationale.RTF");
			this.Rationale.Top = 1.162F;
			this.Rationale.Width = 7.389F;
			// 
			// ScoreNum
			// 
			this.ScoreNum.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.DataField = "ScoreNum";
			this.ScoreNum.Height = 0.291F;
			this.ScoreNum.Left = 0.09F;
			this.ScoreNum.Name = "ScoreNum";
			this.ScoreNum.Style = "background-color: #606060; color: White; font-size: 11pt; font-weight: bold; text" +
				"-align: center; vertical-align: middle; white-space: nowrap; ddo-char-set: 1";
			this.ScoreNum.Text = "ScoreNumber";
			this.ScoreNum.Top = 0.125F;
			this.ScoreNum.Width = 0.7F;
			// 
			// grpFactorType
			// 
			this.grpFactorType.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.FactorType});
			this.grpFactorType.DataField = "FactorType";
			this.grpFactorType.Height = 0.3126668F;
			this.grpFactorType.Name = "grpFactorType";
			this.grpFactorType.Format += new System.EventHandler(this.grpFactorType_Format);
			// 
			// FactorType
			// 
			this.FactorType.DataField = "FactorType";
			this.FactorType.Height = 0.2291667F;
			this.FactorType.Left = 0F;
			this.FactorType.Name = "FactorType";
			this.FactorType.Style = "color: #333333; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-c" +
				"har-set: 1";
			this.FactorType.Text = "FactorType";
			this.FactorType.Top = 0F;
			this.FactorType.Width = 7.479001F;
			// 
			// grpFactorTypeFooter
			// 
			this.grpFactorTypeFooter.BackColor = System.Drawing.Color.White;
			this.grpFactorTypeFooter.Height = 0F;
			this.grpFactorTypeFooter.Name = "grpFactorTypeFooter";
			// 
			// LocationMethod
			// 
			this.LocationMethod.DataField = "LocationMethod";
			this.LocationMethod.Height = 0.23F;
			this.LocationMethod.Left = 0.09F;
			this.LocationMethod.Name = "LocationMethod";
			this.LocationMethod.Style = "color: #6D808E; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-c" +
				"har-set: 1";
			this.LocationMethod.Text = "LocationMethod";
			this.LocationMethod.Top = 0F;
			this.LocationMethod.Width = 7.327F;
			// 
			// grpLocationMethod
			// 
			this.grpLocationMethod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
			this.grpLocationMethod.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.LocationMethod});
			this.grpLocationMethod.DataField = "LocationMethod";
			this.grpLocationMethod.Height = 0.2296667F;
			this.grpLocationMethod.Name = "grpLocationMethod";
			// 
			// grpLocationMethodFooter
			// 
			this.grpLocationMethodFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
			this.grpLocationMethodFooter.Height = 0F;
			this.grpLocationMethodFooter.Name = "grpLocationMethodFooter";
			// 
			// grpSpecies
			// 
			this.grpSpecies.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picture1,
            this.Species});
			this.grpSpecies.DataField = "Species";
			this.grpSpecies.Height = 0.4895833F;
			this.grpSpecies.Name = "grpSpecies";
			this.grpSpecies.Format += new System.EventHandler(this.grpSpecies_Format);
			// 
			// picture1
			// 
			this.picture1.Height = 0.489F;
			this.picture1.HyperLink = null;
			this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
			this.picture1.Left = 0F;
			this.picture1.Name = "picture1";
			this.picture1.Top = 0F;
			this.picture1.Width = 7.5F;
			// 
			// Species
			// 
			this.Species.DataField = "Species";
			this.Species.Height = 0.437F;
			this.Species.Left = 0F;
			this.Species.Name = "Species";
			this.Species.Style = "color: Black; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-cha" +
				"r-set: 1";
			this.Species.Text = "Species";
			this.Species.Top = 0F;
			this.Species.Width = 7.479001F;
			// 
			// grpSpeciesFooter
			// 
			this.grpSpeciesFooter.BackColor = System.Drawing.Color.White;
			this.grpSpeciesFooter.CanShrink = true;
			this.grpSpeciesFooter.Height = 0F;
			this.grpSpeciesFooter.Name = "grpSpeciesFooter";
			// 
			// header
			// 
			this.header.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblJustification});
			this.header.Height = 0.3123333F;
			this.header.Name = "header";
			this.header.Visible = false;
			// 
			// lblJustification
			// 
			this.lblJustification.Height = 0.2703333F;
			this.lblJustification.HyperLink = null;
			this.lblJustification.Left = 0F;
			this.lblJustification.Name = "lblJustification";
			this.lblJustification.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 0";
			this.lblJustification.Text = "Justification of Ranking";
			this.lblJustification.Top = 0.042F;
			this.lblJustification.Width = 2.871F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// grpFactorId
			// 
			this.grpFactorId.DataField = "FactorId";
			this.grpFactorId.Height = 0F;
			this.grpFactorId.Name = "grpFactorId";
			// 
			// grpFactorIdFooter
			// 
			this.grpFactorIdFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
			this.grpFactorIdFooter.CanShrink = true;
			this.grpFactorIdFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.ScoreDescription,
            this.lblRationale,
            this.KeyInfo,
            this.Rationale,
            this.ScoreNum});
			this.grpFactorIdFooter.Height = 1.494666F;
			this.grpFactorIdFooter.Name = "grpFactorIdFooter";
			this.grpFactorIdFooter.Format += new System.EventHandler(this.grpFactorIdFooter_Format);
			// 
			// Criteria1
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489584F;
			this.Sections.Add(this.header);
			this.Sections.Add(this.grpSpecies);
			this.Sections.Add(this.grpFactorType);
			this.Sections.Add(this.grpFactorId);
			this.Sections.Add(this.grpLocationMethod);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.grpLocationMethodFooter);
			this.Sections.Add(this.grpFactorIdFooter);
			this.Sections.Add(this.grpFactorTypeFooter);
			this.Sections.Add(this.grpSpeciesFooter);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Species)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.GroupHeader grpFactorType;
		private DataDynamics.ActiveReports.GroupFooter grpFactorTypeFooter;
		private DataDynamics.ActiveReports.TextBox FactorType;
		private DataDynamics.ActiveReports.TextBox LocationMethod;
		private DataDynamics.ActiveReports.GroupHeader grpLocationMethod;
		private DataDynamics.ActiveReports.GroupFooter grpLocationMethodFooter;
		private DataDynamics.ActiveReports.TextBox ScoreDescription;
		private DataDynamics.ActiveReports.TextBox ScoreNum;
		private DataDynamics.ActiveReports.Label lblRationale;
		private DataDynamics.ActiveReports.GroupHeader grpSpecies;
		private DataDynamics.ActiveReports.TextBox Species;
		private DataDynamics.ActiveReports.GroupFooter grpSpeciesFooter;
		private DataDynamics.ActiveReports.RichTextBox KeyInfo;
		private DataDynamics.ActiveReports.RichTextBox Rationale;
		private DataDynamics.ActiveReports.ReportHeader header;
		private DataDynamics.ActiveReports.Label lblJustification;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.GroupHeader grpFactorId;
		private DataDynamics.ActiveReports.GroupFooter grpFactorIdFooter;
		private DataDynamics.ActiveReports.Picture picture1;

	}
}
