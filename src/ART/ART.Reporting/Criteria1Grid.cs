using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class Criteria1Grid : DataDynamics.ActiveReports.ActiveReport
	{

		public Criteria1Grid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public string Criteria1Synthesis { get; set; }

		private void reportFooter1_Format(object sender, EventArgs e)
		{
			if (Criteria1Synthesis.Length == 0)
				reportFooter1.Visible = false;
			else
				this.Synthesis.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Criteria1Synthesis + "</font>";
		}

		private void detail_Format(object sender, EventArgs e)
		{
			new Utility().ColorFormat(Score, Color.Text);
		}
	}
}
