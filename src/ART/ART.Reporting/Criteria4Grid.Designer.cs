namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria2Grid.
	/// </summary>
	partial class Criteria4Grid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria4Grid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.Region = new DataDynamics.ActiveReports.TextBox();
			this.Method = new DataDynamics.ActiveReports.TextBox();
			this.Factor1 = new DataDynamics.ActiveReports.TextBox();
			this.Factor2 = new DataDynamics.ActiveReports.TextBox();
			this.Factor3 = new DataDynamics.ActiveReports.TextBox();
			this.Score = new DataDynamics.ActiveReports.TextBox();
			this.Color = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			this.lblJustification = new DataDynamics.ActiveReports.Label();
			this.Synthesis = new DataDynamics.ActiveReports.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.Region)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Region,
            this.Method,
            this.Factor1,
            this.Factor2,
            this.Factor3,
            this.Score,
            this.Color});
			this.detail.Height = 0.4724167F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// Region
			// 
			this.Region.DataField = "=Location + \' \' + BodyOfWater";
			this.Region.Height = 0.231F;
			this.Region.Left = 0F;
			this.Region.Name = "Region";
			this.Region.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Region.Text = "Region";
			this.Region.Top = 0F;
			this.Region.Width = 2.333F;
			// 
			// Method
			// 
			this.Method.Border.BottomColor = System.Drawing.Color.Silver;
			this.Method.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Method.DataField = "Method";
			this.Method.Height = 0.231F;
			this.Method.Left = 0F;
			this.Method.Name = "Method";
			this.Method.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Method.Text = "Method";
			this.Method.Top = 0.231F;
			this.Method.Width = 2.333F;
			// 
			// Factor1
			// 
			this.Factor1.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor1.DataField = "=Factor1Score + \":\" + Factor1ScoreDescription";
			this.Factor1.Height = 0.462F;
			this.Factor1.Left = 2.333001F;
			this.Factor1.Name = "Factor1";
			this.Factor1.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor1.Text = "Factor1";
			this.Factor1.Top = 4.470348E-08F;
			this.Factor1.Width = 1.384001F;
			// 
			// Factor2
			// 
			this.Factor2.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor2.DataField = "=Factor2Score + \":\" + Factor2ScoreDescription";
			this.Factor2.Height = 0.462F;
			this.Factor2.Left = 3.717F;
			this.Factor2.Name = "Factor2";
			this.Factor2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor2.Text = "Factor2";
			this.Factor2.Top = 0F;
			this.Factor2.Width = 1.384001F;
			// 
			// Factor3
			// 
			this.Factor3.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor3.DataField = "=Factor3Score + \":\" + Factor3ScoreDescription";
			this.Factor3.Height = 0.462F;
			this.Factor3.Left = 5.101F;
			this.Factor3.Name = "Factor3";
			this.Factor3.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor3.Text = "Factor3";
			this.Factor3.Top = 0F;
			this.Factor3.Width = 1.384001F;
			// 
			// Score
			// 
			this.Score.Border.BottomColor = System.Drawing.Color.Silver;
			this.Score.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Score.DataField = "=Color + \" (\" + Score + \")\"";
			this.Score.Height = 0.462F;
			this.Score.Left = 6.485F;
			this.Score.Name = "Score";
			this.Score.Style = "color: #666666; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Score.Text = "Score";
			this.Score.Top = 0F;
			this.Score.Width = 1.003F;
			// 
			// Color
			// 
			this.Color.Border.BottomColor = System.Drawing.Color.Silver;
			this.Color.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Color.DataField = "Color";
			this.Color.Height = 0.306F;
			this.Color.Left = 5.831F;
			this.Color.Name = "Color";
			this.Color.Style = "background-color: CornflowerBlue; color: #666666; font-size: 10pt; font-weight: b" +
				"old; vertical-align: middle";
			this.Color.Text = "Color";
			this.Color.Top = 0.052F;
			this.Color.Visible = false;
			this.Color.Width = 0.4689999F;
			// 
			// label1
			// 
			this.label1.Height = 0.387F;
			this.label1.HyperLink = null;
			this.label1.Left = 0F;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label1.Text = "Region / Method";
			this.label1.Top = 0.003000006F;
			this.label1.Width = 2.333F;
			// 
			// label3
			// 
			this.label3.Height = 0.387F;
			this.label3.HyperLink = null;
			this.label3.Left = 2.333F;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label3.Text = "Gear Type and Substrate";
			this.label3.Top = 0F;
			this.label3.Width = 1.436F;
			// 
			// label4
			// 
			this.label4.Height = 0.387F;
			this.label4.HyperLink = null;
			this.label4.Left = 3.717F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label4.Text = "Mitigation of Gear Impacts";
			this.label4.Top = 0.003000006F;
			this.label4.Width = 1.436F;
			// 
			// label5
			// 
			this.label5.Height = 0.387F;
			this.label5.HyperLink = null;
			this.label5.Left = 5.101F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "EBFM";
			this.label5.Top = 0.003000006F;
			this.label5.Width = 1.436F;
			// 
			// label6
			// 
			this.label6.Height = 0.387F;
			this.label6.HyperLink = null;
			this.label6.Left = 6.485F;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label6.Text = "Overall Recomm.";
			this.label6.Top = 0F;
			this.label6.Width = 1.003F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label1,
            this.label3,
            this.label4,
            this.label5,
            this.label6});
			this.reportHeader1.Height = 0.39F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblJustification,
            this.Synthesis});
			this.reportFooter1.Height = 0.68725F;
			this.reportFooter1.Name = "reportFooter1";
			this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
			// 
			// lblJustification
			// 
			this.lblJustification.Height = 0.2703333F;
			this.lblJustification.HyperLink = null;
			this.lblJustification.Left = 0F;
			this.lblJustification.Name = "lblJustification";
			this.lblJustification.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 0";
			this.lblJustification.Text = "Synthesis";
			this.lblJustification.Top = 0.156F;
			this.lblJustification.Width = 2.871F;
			// 
			// Synthesis
			// 
			this.Synthesis.AutoReplaceFields = true;
			this.Synthesis.Font = new System.Drawing.Font("Arial", 10F);
			this.Synthesis.Height = 0.24F;
			this.Synthesis.Left = 0F;
			this.Synthesis.Name = "Synthesis";
			this.Synthesis.RTF = resources.GetString("Synthesis.RTF");
			this.Synthesis.Top = 0.426F;
			this.Synthesis.Width = 7.445F;
			// 
			// Criteria4Grid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.487501F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Region)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox Region;
		private DataDynamics.ActiveReports.TextBox Method;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.TextBox Factor1;
		private DataDynamics.ActiveReports.TextBox Factor2;
		private DataDynamics.ActiveReports.TextBox Factor3;
		private DataDynamics.ActiveReports.TextBox Score;
		private DataDynamics.ActiveReports.TextBox Color;
		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Label lblJustification;
		private DataDynamics.ActiveReports.RichTextBox Synthesis;
	}
}
