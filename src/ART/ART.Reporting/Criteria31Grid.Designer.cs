namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria2Grid.
	/// </summary>
	partial class Criteria31Grid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria31Grid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.Region = new DataDynamics.ActiveReports.TextBox();
			this.Method = new DataDynamics.ActiveReports.TextBox();
			this.Factor2 = new DataDynamics.ActiveReports.TextBox();
			this.Factor3 = new DataDynamics.ActiveReports.TextBox();
			this.textBox2 = new DataDynamics.ActiveReports.TextBox();
			this.textBox1 = new DataDynamics.ActiveReports.TextBox();
			this.textBox4 = new DataDynamics.ActiveReports.TextBox();
			this.textBox5 = new DataDynamics.ActiveReports.TextBox();
			this.textBox6 = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.label11 = new DataDynamics.ActiveReports.Label();
			this.label10 = new DataDynamics.ActiveReports.Label();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.label12 = new DataDynamics.ActiveReports.Label();
			this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
			this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
			this.lblJustification = new DataDynamics.ActiveReports.Label();
			this.Synthesis = new DataDynamics.ActiveReports.RichTextBox();
			((System.ComponentModel.ISupportInitialize)(this.Region)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Region,
            this.Method,
            this.Factor2,
            this.Factor3,
            this.textBox2,
            this.textBox1,
            this.textBox4,
            this.textBox5,
            this.textBox6});
			this.detail.Height = 0.4631667F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// Region
			// 
			this.Region.DataField = "=Location + \' \' + BodyOfWater";
			this.Region.Height = 0.231F;
			this.Region.Left = 0F;
			this.Region.Name = "Region";
			this.Region.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Region.Text = "Region";
			this.Region.Top = 0F;
			this.Region.Width = 2.398F;
			// 
			// Method
			// 
			this.Method.Border.BottomColor = System.Drawing.Color.Silver;
			this.Method.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Method.DataField = "Method";
			this.Method.Height = 0.231F;
			this.Method.Left = 0F;
			this.Method.Name = "Method";
			this.Method.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.Method.Text = "Method";
			this.Method.Top = 0.231F;
			this.Method.Width = 2.398F;
			// 
			// Factor2
			// 
			this.Factor2.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor2.DataField = "Factor2ScoreDescription";
			this.Factor2.Height = 0.459F;
			this.Factor2.Left = 2.408F;
			this.Factor2.Name = "Factor2";
			this.Factor2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor2.Text = "Factor2";
			this.Factor2.Top = 0F;
			this.Factor2.Width = 0.7820005F;
			// 
			// Factor3
			// 
			this.Factor3.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor3.DataField = "Factor3ScoreDescription";
			this.Factor3.Height = 0.459F;
			this.Factor3.Left = 3.2F;
			this.Factor3.Name = "Factor3";
			this.Factor3.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor3.Text = "Factor3";
			this.Factor3.Top = 0F;
			this.Factor3.Width = 0.7820005F;
			// 
			// textBox2
			// 
			this.textBox2.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox2.DataField = "Factor4ScoreDescription";
			this.textBox2.Height = 0.459F;
			this.textBox2.Left = 3.992001F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox2.Text = "Factor4";
			this.textBox2.Top = 0F;
			this.textBox2.Width = 0.6990008F;
			// 
			// textBox1
			// 
			this.textBox1.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox1.DataField = "Factor5ScoreDescription";
			this.textBox1.Height = 0.459F;
			this.textBox1.Left = 4.701001F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox1.Text = "Factor5";
			this.textBox1.Top = 0.003000021F;
			this.textBox1.Width = 0.6880002F;
			// 
			// textBox4
			// 
			this.textBox4.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox4.DataField = "Factor6ScoreDescription";
			this.textBox4.Height = 0.459F;
			this.textBox4.Left = 5.399003F;
			this.textBox4.Name = "textBox4";
			this.textBox4.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox4.Text = "Factor6";
			this.textBox4.Top = 0.003000021F;
			this.textBox4.Width = 0.678F;
			// 
			// textBox5
			// 
			this.textBox5.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox5.DataField = "Factor7ScoreDescription";
			this.textBox5.Height = 0.459F;
			this.textBox5.Left = 6.087001F;
			this.textBox5.Name = "textBox5";
			this.textBox5.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox5.Text = "Factor7";
			this.textBox5.Top = 0.003000021F;
			this.textBox5.Width = 0.6990005F;
			// 
			// textBox6
			// 
			this.textBox6.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox6.DataField = "Factor8ScoreDescription";
			this.textBox6.Height = 0.459F;
			this.textBox6.Left = 6.796F;
			this.textBox6.Name = "textBox6";
			this.textBox6.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox6.Text = "Factor8";
			this.textBox6.Top = 0F;
			this.textBox6.Width = 0.6990005F;
			// 
			// label1
			// 
			this.label1.Height = 0.262F;
			this.label1.HyperLink = null;
			this.label1.Left = 0F;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label1.Text = "Region / Method";
			this.label1.Top = 0.003000006F;
			this.label1.Width = 2.398F;
			// 
			// label4
			// 
			this.label4.Height = 0.262F;
			this.label4.HyperLink = null;
			this.label4.Left = 2.398F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label4.Text = "Strategy";
			this.label4.Top = 0.003000006F;
			this.label4.Width = 0.7820005F;
			// 
			// label5
			// 
			this.label5.Height = 0.259F;
			this.label5.HyperLink = null;
			this.label5.Left = 3.19F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "Recovery";
			this.label5.Top = 0.003000006F;
			this.label5.Width = 0.7920001F;
			// 
			// label8
			// 
			this.label8.Height = 0.259F;
			this.label8.HyperLink = null;
			this.label8.Left = 3.982F;
			this.label8.Name = "label8";
			this.label8.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label8.Text = "Research";
			this.label8.Top = 0F;
			this.label8.Width = 0.8250008F;
			// 
			// label11
			// 
			this.label11.Height = 0.259F;
			this.label11.HyperLink = null;
			this.label11.Left = 6.077001F;
			this.label11.Name = "label11";
			this.label11.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label11.Text = "Track";
			this.label11.Top = 0.003F;
			this.label11.Width = 0.6990005F;
			// 
			// label10
			// 
			this.label10.Height = 0.259F;
			this.label10.HyperLink = null;
			this.label10.Left = 5.388999F;
			this.label10.Name = "label10";
			this.label10.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label10.Text = "Enforce";
			this.label10.Top = 1.396984E-09F;
			this.label10.Width = 0.678F;
			// 
			// label7
			// 
			this.label7.Height = 0.262F;
			this.label7.HyperLink = null;
			this.label7.Left = 4.691F;
			this.label7.Name = "label7";
			this.label7.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label7.Text = "Advice";
			this.label7.Top = 0F;
			this.label7.Width = 0.6880002F;
			// 
			// label12
			// 
			this.label12.Height = 0.259F;
			this.label12.HyperLink = null;
			this.label12.Left = 6.786001F;
			this.label12.Name = "label12";
			this.label12.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label12.Text = "Inclusion";
			this.label12.Top = 0F;
			this.label12.Width = 0.6990005F;
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label1,
            this.label4,
            this.label5,
            this.label8,
            this.label11,
            this.label10,
            this.label7,
            this.label12});
			this.groupHeader1.Height = 0.2648333F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// groupFooter1
			// 
			this.groupFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblJustification,
            this.Synthesis});
			this.groupFooter1.Height = 0.7916667F;
			this.groupFooter1.Name = "groupFooter1";
			this.groupFooter1.Format += new System.EventHandler(this.groupFooter1_Format);
			// 
			// lblJustification
			// 
			this.lblJustification.Height = 0.2703333F;
			this.lblJustification.HyperLink = null;
			this.lblJustification.Left = 0F;
			this.lblJustification.Name = "lblJustification";
			this.lblJustification.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 0";
			this.lblJustification.Text = "Synthesis";
			this.lblJustification.Top = 0.208F;
			this.lblJustification.Width = 2.871F;
			// 
			// Synthesis
			// 
			this.Synthesis.AutoReplaceFields = true;
			this.Synthesis.Font = new System.Drawing.Font("Arial", 10F);
			this.Synthesis.Height = 0.24F;
			this.Synthesis.Left = 0F;
			this.Synthesis.Name = "Synthesis";
			this.Synthesis.RTF = resources.GetString("Synthesis.RTF");
			this.Synthesis.Top = 0.478F;
			this.Synthesis.Width = 7.511F;
			// 
			// Criteria31Grid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.48125F;
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Region)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Method)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblJustification)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox Region;
		private DataDynamics.ActiveReports.TextBox Method;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.TextBox Factor2;
		private DataDynamics.ActiveReports.TextBox Factor3;
		private DataDynamics.ActiveReports.TextBox textBox2;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.TextBox textBox1;
		private DataDynamics.ActiveReports.TextBox textBox4;
		private DataDynamics.ActiveReports.TextBox textBox5;
		private DataDynamics.ActiveReports.TextBox textBox6;
		private DataDynamics.ActiveReports.Label label11;
		private DataDynamics.ActiveReports.Label label10;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.Label label12;
		private DataDynamics.ActiveReports.GroupHeader groupHeader1;
		private DataDynamics.ActiveReports.GroupFooter groupFooter1;
		private DataDynamics.ActiveReports.Label lblJustification;
		private DataDynamics.ActiveReports.RichTextBox Synthesis;
	}
}
