namespace ART.Reporting
{
	/// <summary>
	/// Summary description for OverallRec.
	/// </summary>
	partial class OverallRec
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(OverallRec));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.Recommendation = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.label2 = new DataDynamics.ActiveReports.Label();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.Score1 = new DataDynamics.ActiveReports.TextBox();
			this.Color1 = new DataDynamics.ActiveReports.TextBox();
			this.Score2 = new DataDynamics.ActiveReports.TextBox();
			this.Color2 = new DataDynamics.ActiveReports.TextBox();
			this.Score3 = new DataDynamics.ActiveReports.TextBox();
			this.Color3 = new DataDynamics.ActiveReports.TextBox();
			this.Score4 = new DataDynamics.ActiveReports.TextBox();
			this.Color4 = new DataDynamics.ActiveReports.TextBox();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.label9 = new DataDynamics.ActiveReports.Label();
			this.Score = new DataDynamics.ActiveReports.TextBox();
			this.Criticals = new DataDynamics.ActiveReports.TextBox();
			this.RedCount = new DataDynamics.ActiveReports.TextBox();
			this.Color = new DataDynamics.ActiveReports.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Recommendation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Criticals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RedCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Recommendation,
            this.label1,
            this.label2,
            this.label3,
            this.label5,
            this.label6,
            this.Score1,
            this.Color1,
            this.Score2,
            this.Color2,
            this.Score3,
            this.Color3,
            this.Score4,
            this.Color4,
            this.label7,
            this.label8,
            this.label9,
            this.Score,
            this.Criticals,
            this.RedCount,
            this.Color});
			this.detail.Height = 2F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// Recommendation
			// 
			this.Recommendation.DataField = "Recommendation";
			this.Recommendation.Height = 0.325F;
			this.Recommendation.Left = 0.208F;
			this.Recommendation.Name = "Recommendation";
			this.Recommendation.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 1";
			this.Recommendation.Text = "Recommendation";
			this.Recommendation.Top = 0.04F;
			this.Recommendation.Width = 7.292F;
			// 
			// label1
			// 
			this.label1.Height = 0.2F;
			this.label1.HyperLink = null;
			this.label1.Left = 0.208F;
			this.label1.Name = "label1";
			this.label1.Style = "font-weight: bold";
			this.label1.Text = "Criterion";
			this.label1.Top = 0.425F;
			this.label1.Width = 0.6350001F;
			// 
			// label2
			// 
			this.label2.Height = 0.2F;
			this.label2.HyperLink = null;
			this.label2.Left = 0.208F;
			this.label2.Name = "label2";
			this.label2.Style = "font-weight: normal";
			this.label2.Text = "Impacts on the stock";
			this.label2.Top = 0.688F;
			this.label2.Width = 1.571F;
			// 
			// label3
			// 
			this.label3.Height = 0.2F;
			this.label3.HyperLink = null;
			this.label3.Left = 0.208F;
			this.label3.Name = "label3";
			this.label3.Style = "font-weight: normal";
			this.label3.Text = "Impacts on other stocks";
			this.label3.Top = 0.91F;
			this.label3.Width = 1.571F;
			// 
			// label5
			// 
			this.label5.Height = 0.2F;
			this.label5.HyperLink = null;
			this.label5.Left = 0.208F;
			this.label5.Name = "label5";
			this.label5.Style = "font-weight: normal";
			this.label5.Text = "Management";
			this.label5.Top = 1.135F;
			this.label5.Width = 1.571F;
			// 
			// label6
			// 
			this.label6.Height = 0.2F;
			this.label6.HyperLink = null;
			this.label6.Left = 0.208F;
			this.label6.Name = "label6";
			this.label6.Style = "font-weight: normal";
			this.label6.Text = "Habitat & Ecosystem";
			this.label6.Top = 1.367F;
			this.label6.Width = 1.571F;
			// 
			// Score1
			// 
			this.Score1.DataField = "Score1";
			this.Score1.Height = 0.1979167F;
			this.Score1.Left = 1.885F;
			this.Score1.Name = "Score1";
			this.Score1.Text = "Score1";
			this.Score1.Top = 0.688F;
			this.Score1.Width = 0.4895833F;
			// 
			// Color1
			// 
			this.Color1.DataField = "Color1";
			this.Color1.Height = 0.1979167F;
			this.Color1.Left = 2.479F;
			this.Color1.Name = "Color1";
			this.Color1.Text = "Color1";
			this.Color1.Top = 0.688F;
			this.Color1.Width = 0.833F;
			// 
			// Score2
			// 
			this.Score2.DataField = "Score2";
			this.Score2.Height = 0.1979167F;
			this.Score2.Left = 1.885F;
			this.Score2.Name = "Score2";
			this.Score2.Text = "Score2";
			this.Score2.Top = 0.91F;
			this.Score2.Width = 0.4895833F;
			// 
			// Color2
			// 
			this.Color2.DataField = "Color2";
			this.Color2.Height = 0.1979167F;
			this.Color2.Left = 2.479F;
			this.Color2.Name = "Color2";
			this.Color2.Text = "Color2";
			this.Color2.Top = 0.91F;
			this.Color2.Width = 0.833F;
			// 
			// Score3
			// 
			this.Score3.DataField = "Score3";
			this.Score3.Height = 0.1979167F;
			this.Score3.Left = 1.885F;
			this.Score3.Name = "Score3";
			this.Score3.Text = "Score3";
			this.Score3.Top = 1.14F;
			this.Score3.Width = 0.4895833F;
			// 
			// Color3
			// 
			this.Color3.DataField = "Color3";
			this.Color3.Height = 0.1979167F;
			this.Color3.Left = 2.479F;
			this.Color3.Name = "Color3";
			this.Color3.Text = "Color3";
			this.Color3.Top = 1.14F;
			this.Color3.Width = 0.833F;
			// 
			// Score4
			// 
			this.Score4.DataField = "Score4";
			this.Score4.Height = 0.1979167F;
			this.Score4.Left = 1.885F;
			this.Score4.Name = "Score4";
			this.Score4.Text = "Score4";
			this.Score4.Top = 1.362F;
			this.Score4.Width = 0.4895833F;
			// 
			// Color4
			// 
			this.Color4.DataField = "Color4";
			this.Color4.Height = 0.1979167F;
			this.Color4.Left = 2.479F;
			this.Color4.Name = "Color4";
			this.Color4.Text = "Color4";
			this.Color4.Top = 1.362F;
			this.Color4.Width = 0.833F;
			// 
			// label7
			// 
			this.label7.Height = 0.2F;
			this.label7.HyperLink = null;
			this.label7.Left = 3.51F;
			this.label7.Name = "label7";
			this.label7.Style = "font-weight: normal";
			this.label7.Text = "Overall Score";
			this.label7.Top = 0.683F;
			this.label7.Width = 1.571F;
			// 
			// label8
			// 
			this.label8.Height = 0.2F;
			this.label8.HyperLink = null;
			this.label8.Left = 3.51F;
			this.label8.Name = "label8";
			this.label8.Style = "font-weight: normal";
			this.label8.Text = "Any Criticals?";
			this.label8.Top = 0.905F;
			this.label8.Width = 1.571F;
			// 
			// label9
			// 
			this.label9.Height = 0.2F;
			this.label9.HyperLink = null;
			this.label9.Left = 3.51F;
			this.label9.Name = "label9";
			this.label9.Style = "font-weight: normal";
			this.label9.Text = "Number of Reds";
			this.label9.Top = 1.138F;
			this.label9.Width = 1.571F;
			// 
			// Score
			// 
			this.Score.DataField = "Score";
			this.Score.Height = 0.1979167F;
			this.Score.Left = 5.187F;
			this.Score.Name = "Score";
			this.Score.Text = "Score";
			this.Score.Top = 0.683F;
			this.Score.Width = 0.4895833F;
			// 
			// Criticals
			// 
			this.Criticals.DataField = "Criticals";
			this.Criticals.Height = 0.1979167F;
			this.Criticals.Left = 5.187F;
			this.Criticals.Name = "Criticals";
			this.Criticals.Text = "Criticals";
			this.Criticals.Top = 0.905F;
			this.Criticals.Width = 0.4895833F;
			// 
			// RedCount
			// 
			this.RedCount.DataField = "RedCount";
			this.RedCount.Height = 0.1979167F;
			this.RedCount.Left = 5.187F;
			this.RedCount.Name = "RedCount";
			this.RedCount.Text = "RedCount";
			this.RedCount.Top = 1.135F;
			this.RedCount.Width = 0.4895833F;
			// 
			// Color
			// 
			this.Color.DataField = "Color";
			this.Color.Height = 0.563F;
			this.Color.Left = 5.78F;
			this.Color.Name = "Color";
			this.Color.Style = "text-align: center; vertical-align: middle";
			this.Color.Text = "Color";
			this.Color.Top = 0.688F;
			this.Color.Width = 1.72F;
			// 
			// OverallRec
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.Sections.Add(this.detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Recommendation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Criticals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RedCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox Recommendation;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.Label label2;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.TextBox Score1;
		private DataDynamics.ActiveReports.TextBox Color1;
		private DataDynamics.ActiveReports.TextBox Score2;
		private DataDynamics.ActiveReports.TextBox Color2;
		private DataDynamics.ActiveReports.TextBox Score3;
		private DataDynamics.ActiveReports.TextBox Color3;
		private DataDynamics.ActiveReports.TextBox Score4;
		private DataDynamics.ActiveReports.TextBox Color4;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.Label label9;
		private DataDynamics.ActiveReports.TextBox Score;
		private DataDynamics.ActiveReports.TextBox Criticals;
		private DataDynamics.ActiveReports.TextBox RedCount;
		private DataDynamics.ActiveReports.TextBox Color;
	}
}
