namespace ART.Reporting
{
	/// <summary>
	/// Summary description for AppendixAGrid.
	/// </summary>
	partial class AppendixAGrid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AppendixAGrid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.Factor1 = new DataDynamics.ActiveReports.TextBox();
			this.Factor2 = new DataDynamics.ActiveReports.TextBox();
			this.Factor3 = new DataDynamics.ActiveReports.TextBox();
			this.Score = new DataDynamics.ActiveReports.TextBox();
			this.textBox1 = new DataDynamics.ActiveReports.TextBox();
			this.textBox2 = new DataDynamics.ActiveReports.TextBox();
			this.Color = new DataDynamics.ActiveReports.TextBox();
			this.RegionMethod = new DataDynamics.ActiveReports.TextBox();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.label9 = new DataDynamics.ActiveReports.Label();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.label10 = new DataDynamics.ActiveReports.Label();
			this.line1 = new DataDynamics.ActiveReports.Line();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
			this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RegionMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Factor1,
            this.Factor2,
            this.Factor3,
            this.Score,
            this.textBox1,
            this.textBox2,
            this.Color});
			this.detail.Height = 0.481F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// Factor1
			// 
			this.Factor1.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor1.DataField = "=Factor1Score + \":\" + Factor1ScoreDescription";
			this.Factor1.Height = 0.471F;
			this.Factor1.Left = 1.665F;
			this.Factor1.Name = "Factor1";
			this.Factor1.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor1.Text = "Factor1";
			this.Factor1.Top = 0.01F;
			this.Factor1.Width = 1.260999F;
			// 
			// Factor2
			// 
			this.Factor2.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor2.DataField = "=Factor2Score + \":\" + Factor2ScoreDescription";
			this.Factor2.Height = 0.471F;
			this.Factor2.Left = 2.927F;
			this.Factor2.Name = "Factor2";
			this.Factor2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor2.Text = "Factor2";
			this.Factor2.Top = 0.009999956F;
			this.Factor2.Width = 1.280999F;
			// 
			// Factor3
			// 
			this.Factor3.Border.BottomColor = System.Drawing.Color.Silver;
			this.Factor3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Factor3.DataField = "=Factor3Score + \":\" + Factor3ScoreDescription";
			this.Factor3.Height = 0.471F;
			this.Factor3.Left = 4.208001F;
			this.Factor3.Name = "Factor3";
			this.Factor3.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor3.Text = "Factor3";
			this.Factor3.Top = 0.01F;
			this.Factor3.Width = 1.249999F;
			// 
			// Score
			// 
			this.Score.Border.BottomColor = System.Drawing.Color.Silver;
			this.Score.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Score.DataField = "Score";
			this.Score.Height = 0.471F;
			this.Score.Left = 6.416F;
			this.Score.Name = "Score";
			this.Score.Style = "color: #666666; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.Score.Text = "Score";
			this.Score.Top = 0.01000002F;
			this.Score.Width = 0.5309997F;
			// 
			// textBox1
			// 
			this.textBox1.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox1.DataField = "CommonName";
			this.textBox1.Height = 0.471F;
			this.textBox1.Left = 0F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: normal; " +
				"vertical-align: middle; white-space: inherit";
			this.textBox1.Text = "CommonName";
			this.textBox1.Top = 0.009999956F;
			this.textBox1.Width = 1.665F;
			// 
			// textBox2
			// 
			this.textBox2.Border.BottomColor = System.Drawing.Color.Silver;
			this.textBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.textBox2.DataField = "=Factor4Score + \":\" + Factor4ScoreDescription";
			this.textBox2.Height = 0.471F;
			this.textBox2.Left = 5.458F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.textBox2.Text = "Factor4";
			this.textBox2.Top = 0.00999999F;
			this.textBox2.Width = 0.9580002F;
			// 
			// Color
			// 
			this.Color.Border.BottomColor = System.Drawing.Color.Silver;
			this.Color.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.Color.DataField = "Color";
			this.Color.Height = 0.471F;
			this.Color.Left = 6.947001F;
			this.Color.Name = "Color";
			this.Color.Style = "background-color: CornflowerBlue; color: #666666; font-size: 10pt; font-weight: b" +
				"old; vertical-align: middle";
			this.Color.Text = "Color";
			this.Color.Top = 0.01000002F;
			this.Color.Width = 0.5629989F;
			// 
			// RegionMethod
			// 
			this.RegionMethod.DataField = "=Location + \' \' + BodyOfWater + \", \"  + Method";
			this.RegionMethod.Height = 0.231F;
			this.RegionMethod.Left = 0F;
			this.RegionMethod.Name = "RegionMethod";
			this.RegionMethod.Style = "font-size: 10pt; font-style: italic; font-weight: bold; vertical-align: middle";
			this.RegionMethod.Text = "RegionMethod";
			this.RegionMethod.Top = 0F;
			this.RegionMethod.Width = 5.562F;
			// 
			// label3
			// 
			this.label3.Height = 0.387F;
			this.label3.HyperLink = null;
			this.label3.Left = 1.665F;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label3.Text = "Inherent Vulnerability";
			this.label3.Top = 0.281F;
			this.label3.Width = 1.260999F;
			// 
			// label4
			// 
			this.label4.Height = 0.387F;
			this.label4.HyperLink = null;
			this.label4.Left = 2.927F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label4.Text = "Stock Status";
			this.label4.Top = 0.284F;
			this.label4.Width = 1.280999F;
			// 
			// label5
			// 
			this.label5.Height = 0.387F;
			this.label5.HyperLink = null;
			this.label5.Left = 4.208F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "Fishing Mortality";
			this.label5.Top = 0.284F;
			this.label5.Width = 1.249999F;
			// 
			// label6
			// 
			this.label6.Height = 0.387F;
			this.label6.HyperLink = null;
			this.label6.Left = 6.416F;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label6.Text = "Score";
			this.label6.Top = 0.281F;
			this.label6.Width = 0.5309992F;
			// 
			// label7
			// 
			this.label7.Height = 0.387F;
			this.label7.HyperLink = null;
			this.label7.Left = 0F;
			this.label7.Name = "label7";
			this.label7.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label7.Text = "Stock";
			this.label7.Top = 0.284F;
			this.label7.Width = 1.665F;
			// 
			// label8
			// 
			this.label8.Height = 0.387F;
			this.label8.HyperLink = null;
			this.label8.Left = 5.458F;
			this.label8.Name = "label8";
			this.label8.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label8.Text = "Discard Rate";
			this.label8.Top = 0.281F;
			this.label8.Width = 0.9580002F;
			// 
			// label9
			// 
			this.label9.Height = 0.387F;
			this.label9.HyperLink = null;
			this.label9.Left = 6.947001F;
			this.label9.Name = "label9";
			this.label9.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label9.Text = "Rank";
			this.label9.Top = 0.284F;
			this.label9.Width = 0.5629998F;
			// 
			// reportHeader1
			// 
			this.reportHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label10,
            this.line1});
			this.reportHeader1.Height = 0.2858333F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// label10
			// 
			this.label10.Height = 0.25F;
			this.label10.HyperLink = null;
			this.label10.Left = 0.04166667F;
			this.label10.Name = "label10";
			this.label10.Style = "color: #005A74; font-size: 15pt; font-weight: bold; text-decoration: none";
			this.label10.Text = "Appendix: All Species Evaluated under Criterion 2";
			this.label10.Top = 0F;
			this.label10.Width = 7.385417F;
			// 
			// line1
			// 
			this.line1.Height = 0.004000008F;
			this.line1.Left = 0.042F;
			this.line1.LineColor = System.Drawing.Color.LightGray;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 0.274F;
			this.line1.Width = 7.385001F;
			this.line1.X1 = 0.042F;
			this.line1.X2 = 7.427001F;
			this.line1.Y1 = 0.278F;
			this.line1.Y2 = 0.274F;
			// 
			// reportFooter1
			// 
			this.reportFooter1.Height = 0F;
			this.reportFooter1.Name = "reportFooter1";
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.RegionMethod,
            this.label7,
            this.label4,
            this.label5,
            this.label6,
            this.label3,
            this.label8,
            this.label9});
			this.groupHeader1.DataField = "=Location+BodyOfWater+Method";
			this.groupHeader1.Height = 0.671F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// groupFooter1
			// 
			this.groupFooter1.Height = 0.07291666F;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// AppendixAGrid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.446751F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Score)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Color)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RegionMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox RegionMethod;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.TextBox Factor1;
		private DataDynamics.ActiveReports.TextBox Factor2;
		private DataDynamics.ActiveReports.TextBox Factor3;
		private DataDynamics.ActiveReports.TextBox Score;
		private DataDynamics.ActiveReports.TextBox textBox1;
		private DataDynamics.ActiveReports.TextBox textBox2;
		private DataDynamics.ActiveReports.TextBox Color;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.Label label9;
		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Label label10;
		private DataDynamics.ActiveReports.Line line1;
		private DataDynamics.ActiveReports.GroupHeader groupHeader1;
		private DataDynamics.ActiveReports.GroupFooter groupFooter1;
	}
}
