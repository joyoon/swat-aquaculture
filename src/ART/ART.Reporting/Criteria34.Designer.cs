namespace ART.Reporting
{
	/// <summary>
	/// Summary description for NewActiveReport1.
	/// </summary>
	partial class Criteria34
	{
		private DataDynamics.ActiveReports.Detail detail;


		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Criteria34));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.ScoreDescription = new DataDynamics.ActiveReports.TextBox();
			this.ScoreNum = new DataDynamics.ActiveReports.TextBox();
			this.lblRationale = new DataDynamics.ActiveReports.Label();
			this.KeyInfo = new DataDynamics.ActiveReports.RichTextBox();
			this.Rationale = new DataDynamics.ActiveReports.RichTextBox();
			this.LocationMethod = new DataDynamics.ActiveReports.TextBox();
			this.grpLocationMethod = new DataDynamics.ActiveReports.GroupHeader();
			this.grpLocationMethodFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.grpFactorType = new DataDynamics.ActiveReports.GroupHeader();
			this.FactorType = new DataDynamics.ActiveReports.TextBox();
			this.grpFactorTypeFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.grpFactor = new DataDynamics.ActiveReports.GroupHeader();
			this.grpFactorFooter = new DataDynamics.ActiveReports.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.BackColor = System.Drawing.Color.Empty;
			this.detail.ColumnSpacing = 0F;
			this.detail.Height = 0F;
			this.detail.Name = "detail";
			// 
			// ScoreDescription
			// 
			this.ScoreDescription.DataField = "ScoreDescription";
			this.ScoreDescription.Height = 0.292F;
			this.ScoreDescription.Left = 0.79F;
			this.ScoreDescription.Name = "ScoreDescription";
			this.ScoreDescription.Style = "color: #333333; font-size: 11pt; font-weight: bold; vertical-align: middle; white" +
				"-space: nowrap; ddo-char-set: 1";
			this.ScoreDescription.Text = "ScoreDescription";
			this.ScoreDescription.Top = 0F;
			this.ScoreDescription.Width = 6.606F;
			// 
			// ScoreNum
			// 
			this.ScoreNum.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.DataField = "ScoreNum";
			this.ScoreNum.Height = 0.291F;
			this.ScoreNum.Left = 0.09F;
			this.ScoreNum.Name = "ScoreNum";
			this.ScoreNum.Style = "background-color: #606060; color: White; font-size: 11pt; font-weight: bold; text" +
				"-align: center; vertical-align: middle; white-space: nowrap; ddo-char-set: 1";
			this.ScoreNum.Text = "ScoreNumber";
			this.ScoreNum.Top = 0F;
			this.ScoreNum.Width = 0.7F;
			// 
			// lblRationale
			// 
			this.lblRationale.Height = 0.1875F;
			this.lblRationale.HyperLink = null;
			this.lblRationale.Left = 0.07900006F;
			this.lblRationale.Name = "lblRationale";
			this.lblRationale.Style = "font-family: Arial; font-size: 11pt; font-weight: bold; ddo-char-set: 1";
			this.lblRationale.Text = "Detail Rationale:";
			this.lblRationale.Top = 0.7916671F;
			this.lblRationale.Width = 1.76F;
			// 
			// KeyInfo
			// 
			this.KeyInfo.AutoReplaceFields = true;
			this.KeyInfo.DataField = "KeyInfo";
			this.KeyInfo.Font = new System.Drawing.Font("Arial", 10F);
			this.KeyInfo.Height = 0.2290001F;
			this.KeyInfo.Left = 0.07899992F;
			this.KeyInfo.Name = "KeyInfo";
			this.KeyInfo.RTF = resources.GetString("KeyInfo.RTF");
			this.KeyInfo.Top = 0.4000003F;
			this.KeyInfo.Width = 7.337834F;
			// 
			// Rationale
			// 
			this.Rationale.AutoReplaceFields = true;
			this.Rationale.DataField = "Rationale";
			this.Rationale.Font = new System.Drawing.Font("Arial", 10F);
			this.Rationale.Height = 0.2290001F;
			this.Rationale.Left = 0.079F;
			this.Rationale.Name = "Rationale";
			this.Rationale.RTF = resources.GetString("Rationale.RTF");
			this.Rationale.Top = 1.066F;
			this.Rationale.Width = 7.337834F;
			// 
			// LocationMethod
			// 
			this.LocationMethod.DataField = "LocationMethod";
			this.LocationMethod.Height = 0.23F;
			this.LocationMethod.Left = 0.09F;
			this.LocationMethod.Name = "LocationMethod";
			this.LocationMethod.Style = "color: #6D808E; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-c" +
				"har-set: 1";
			this.LocationMethod.Text = "LocationMethod";
			this.LocationMethod.Top = 0F;
			this.LocationMethod.Width = 7.327F;
			// 
			// grpLocationMethod
			// 
			this.grpLocationMethod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
			this.grpLocationMethod.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.LocationMethod});
			this.grpLocationMethod.DataField = "LocationMethod";
			this.grpLocationMethod.Height = 0.2505001F;
			this.grpLocationMethod.KeepTogether = true;
			this.grpLocationMethod.Name = "grpLocationMethod";
			// 
			// grpLocationMethodFooter
			// 
			this.grpLocationMethodFooter.BackColor = System.Drawing.Color.White;
			this.grpLocationMethodFooter.Height = 0F;
			this.grpLocationMethodFooter.Name = "grpLocationMethodFooter";
			// 
			// grpFactorType
			// 
			this.grpFactorType.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.FactorType});
			this.grpFactorType.DataField = "FactorType";
			this.grpFactorType.Height = 0.3645833F;
			this.grpFactorType.Name = "grpFactorType";
			// 
			// FactorType
			// 
			this.FactorType.DataField = "FactorType";
			this.FactorType.Height = 0.2291667F;
			this.FactorType.Left = 0F;
			this.FactorType.Name = "FactorType";
			this.FactorType.Style = "background-color: White; color: Black; font-size: 11pt; font-weight: bold; vertic" +
				"al-align: middle; ddo-char-set: 1";
			this.FactorType.Text = "FactorType";
			this.FactorType.Top = 0F;
			this.FactorType.Width = 7.417F;
			// 
			// grpFactorTypeFooter
			// 
			this.grpFactorTypeFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
			this.grpFactorTypeFooter.Height = 0F;
			this.grpFactorTypeFooter.Name = "grpFactorTypeFooter";
			// 
			// grpFactor
			// 
			this.grpFactor.DataField = "FactorId";
			this.grpFactor.Height = 0F;
			this.grpFactor.Name = "grpFactor";
			// 
			// grpFactorFooter
			// 
			this.grpFactorFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
			this.grpFactorFooter.CanShrink = true;
			this.grpFactorFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.ScoreDescription,
            this.ScoreNum,
            this.lblRationale,
            this.KeyInfo,
            this.Rationale});
			this.grpFactorFooter.Height = 1.295F;
			this.grpFactorFooter.Name = "grpFactorFooter";
			this.grpFactorFooter.Format += new System.EventHandler(this.grpFactorFooter_Format);
			// 
			// Criteria34
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.437834F;
			this.Script = "public void ActiveReport_ReportStart()\r\n{\r\n\t\r\n}\r\n\r\n\r\n";
			this.Sections.Add(this.grpFactorType);
			this.Sections.Add(this.grpFactor);
			this.Sections.Add(this.grpLocationMethod);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.grpLocationMethodFooter);
			this.Sections.Add(this.grpFactorFooter);
			this.Sections.Add(this.grpFactorTypeFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox LocationMethod;
		private DataDynamics.ActiveReports.GroupHeader grpLocationMethod;
		private DataDynamics.ActiveReports.GroupFooter grpLocationMethodFooter;
		private DataDynamics.ActiveReports.TextBox ScoreDescription;
		private DataDynamics.ActiveReports.TextBox ScoreNum;
		private DataDynamics.ActiveReports.GroupHeader grpFactorType;
		private DataDynamics.ActiveReports.TextBox FactorType;
		private DataDynamics.ActiveReports.GroupFooter grpFactorTypeFooter;
		private DataDynamics.ActiveReports.Label lblRationale;
		private DataDynamics.ActiveReports.RichTextBox KeyInfo;
		private DataDynamics.ActiveReports.RichTextBox Rationale;
		private DataDynamics.ActiveReports.GroupHeader grpFactor;
		private DataDynamics.ActiveReports.GroupFooter grpFactorFooter;

	}
}
