namespace ART.Reporting
{
	/// <summary>
	/// Summary description for FullReport.
	/// </summary>
	partial class FullReport
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FullReport));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.TextBlockScope = new DataDynamics.ActiveReports.SubReport();
			this.Criteria1Grid = new DataDynamics.ActiveReports.SubReport();
			this.Criteria2Grid = new DataDynamics.ActiveReports.SubReport();
			this.Criteria3Grid = new DataDynamics.ActiveReports.SubReport();
			this.Criteria4Grid = new DataDynamics.ActiveReports.SubReport();
			this.FinalRecGrid = new DataDynamics.ActiveReports.SubReport();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.richTextBox1 = new DataDynamics.ActiveReports.RichTextBox();
			this.pageBreak3 = new DataDynamics.ActiveReports.PageBreak();
			this.TextBlockExecSummary = new DataDynamics.ActiveReports.SubReport();
			this.pageBreak4 = new DataDynamics.ActiveReports.PageBreak();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.richTextBox2 = new DataDynamics.ActiveReports.RichTextBox();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.richTextBox3 = new DataDynamics.ActiveReports.RichTextBox();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.Criteria1 = new DataDynamics.ActiveReports.SubReport();
			this.Criteria2 = new DataDynamics.ActiveReports.SubReport();
			this.Criteria4 = new DataDynamics.ActiveReports.SubReport();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.label9 = new DataDynamics.ActiveReports.Label();
			this.richTextBox4 = new DataDynamics.ActiveReports.RichTextBox();
			this.label10 = new DataDynamics.ActiveReports.Label();
			this.label11 = new DataDynamics.ActiveReports.Label();
			this.label12 = new DataDynamics.ActiveReports.Label();
			this.richTextBox5 = new DataDynamics.ActiveReports.RichTextBox();
			this.label14 = new DataDynamics.ActiveReports.Label();
			this.label15 = new DataDynamics.ActiveReports.Label();
			this.richTextBox6 = new DataDynamics.ActiveReports.RichTextBox();
			this.label16 = new DataDynamics.ActiveReports.Label();
			this.label17 = new DataDynamics.ActiveReports.Label();
			this.richTextBox7 = new DataDynamics.ActiveReports.RichTextBox();
			this.label18 = new DataDynamics.ActiveReports.Label();
			this.label19 = new DataDynamics.ActiveReports.Label();
			this.pageBreak5 = new DataDynamics.ActiveReports.PageBreak();
			this.label21 = new DataDynamics.ActiveReports.Label();
			this.AppendixAGrid = new DataDynamics.ActiveReports.SubReport();
			this.pageBreak6 = new DataDynamics.ActiveReports.PageBreak();
			this.AppendixN1 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN2 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN3 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN4 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN5 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN6 = new DataDynamics.ActiveReports.SubReport();
			this.AppendixN7 = new DataDynamics.ActiveReports.SubReport();
			this.label22 = new DataDynamics.ActiveReports.Label();
			this.pageBreak7 = new DataDynamics.ActiveReports.PageBreak();
			this.label23 = new DataDynamics.ActiveReports.Label();
			this.pageBreak8 = new DataDynamics.ActiveReports.PageBreak();
			this.richTextBox8 = new DataDynamics.ActiveReports.RichTextBox();
			this.richTextBox9 = new DataDynamics.ActiveReports.RichTextBox();
			this.richTextBox10 = new DataDynamics.ActiveReports.RichTextBox();
			this.pageBreak9 = new DataDynamics.ActiveReports.PageBreak();
			this.TextBlockAck = new DataDynamics.ActiveReports.SubReport();
			this.References = new DataDynamics.ActiveReports.SubReport();
			this.label24 = new DataDynamics.ActiveReports.Label();
			this.label25 = new DataDynamics.ActiveReports.Label();
			this.label13 = new DataDynamics.ActiveReports.Label();
			this.label26 = new DataDynamics.ActiveReports.Label();
			this.label27 = new DataDynamics.ActiveReports.Label();
			this.label2 = new DataDynamics.ActiveReports.Label();
			this.label28 = new DataDynamics.ActiveReports.Label();
			this.label29 = new DataDynamics.ActiveReports.Label();
			this.TextBlockOverview = new DataDynamics.ActiveReports.SubReport();
			this.label30 = new DataDynamics.ActiveReports.Label();
			this.TextBlockProductionStatistics = new DataDynamics.ActiveReports.SubReport();
			this.label31 = new DataDynamics.ActiveReports.Label();
			this.TextBlockImportance = new DataDynamics.ActiveReports.SubReport();
			this.label32 = new DataDynamics.ActiveReports.Label();
			this.TextBlockMarketNames = new DataDynamics.ActiveReports.SubReport();
			this.label33 = new DataDynamics.ActiveReports.Label();
			this.pageBreak1 = new DataDynamics.ActiveReports.PageBreak();
			this.pageBreak2 = new DataDynamics.ActiveReports.PageBreak();
			this.pageBreak10 = new DataDynamics.ActiveReports.PageBreak();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.TextBlockScope,
            this.Criteria1Grid,
            this.Criteria2Grid,
            this.Criteria3Grid,
            this.Criteria4Grid,
            this.FinalRecGrid,
            this.label1,
            this.richTextBox1,
            this.pageBreak3,
            this.TextBlockExecSummary,
            this.pageBreak4,
            this.label3,
            this.label4,
            this.richTextBox2,
            this.label5,
            this.label6,
            this.richTextBox3,
            this.label7,
            this.Criteria1,
            this.Criteria2,
            this.Criteria4,
            this.label8,
            this.label9,
            this.richTextBox4,
            this.label10,
            this.label11,
            this.label12,
            this.richTextBox5,
            this.label14,
            this.label15,
            this.richTextBox6,
            this.label16,
            this.label17,
            this.richTextBox7,
            this.label18,
            this.label19,
            this.pageBreak5,
            this.label21,
            this.AppendixAGrid,
            this.pageBreak6,
            this.AppendixN1,
            this.AppendixN2,
            this.AppendixN3,
            this.AppendixN4,
            this.AppendixN5,
            this.AppendixN6,
            this.AppendixN7,
            this.label22,
            this.pageBreak7,
            this.label23,
            this.pageBreak8,
            this.richTextBox8,
            this.richTextBox9,
            this.richTextBox10,
            this.pageBreak9,
            this.TextBlockAck,
            this.References,
            this.label24,
            this.label25,
            this.label13,
            this.label26,
            this.label27,
            this.label2,
            this.label28,
            this.label29,
            this.TextBlockOverview,
            this.label30,
            this.TextBlockProductionStatistics,
            this.label31,
            this.TextBlockImportance,
            this.label32,
            this.TextBlockMarketNames,
            this.label33,
            this.pageBreak1,
            this.pageBreak2,
            this.pageBreak10});
			this.detail.Height = 41.06052F;
			this.detail.Name = "detail";
			// 
			// TextBlockScope
			// 
			this.TextBlockScope.CloseBorder = false;
			this.TextBlockScope.Height = 0.625F;
			this.TextBlockScope.Left = 0F;
			this.TextBlockScope.Name = "TextBlockScope";
			this.TextBlockScope.Report = null;
			this.TextBlockScope.ReportName = "TextBlockScope";
			this.TextBlockScope.Top = 3.111F;
			this.TextBlockScope.Width = 7.479001F;
			// 
			// Criteria1Grid
			// 
			this.Criteria1Grid.CloseBorder = false;
			this.Criteria1Grid.Height = 0.625F;
			this.Criteria1Grid.Left = 0F;
			this.Criteria1Grid.Name = "Criteria1Grid";
			this.Criteria1Grid.Report = null;
			this.Criteria1Grid.ReportName = "Criteria1Grid";
			this.Criteria1Grid.Top = 12.576F;
			this.Criteria1Grid.Width = 7.479F;
			// 
			// Criteria2Grid
			// 
			this.Criteria2Grid.CloseBorder = false;
			this.Criteria2Grid.Height = 0.625F;
			this.Criteria2Grid.Left = 0.03F;
			this.Criteria2Grid.Name = "Criteria2Grid";
			this.Criteria2Grid.Report = null;
			this.Criteria2Grid.ReportName = "Criteria2Grid";
			this.Criteria2Grid.Top = 15.931F;
			this.Criteria2Grid.Width = 7.479F;
			// 
			// Criteria3Grid
			// 
			this.Criteria3Grid.CloseBorder = false;
			this.Criteria3Grid.Height = 0.625F;
			this.Criteria3Grid.Left = 0.009F;
			this.Criteria3Grid.Name = "Criteria3Grid";
			this.Criteria3Grid.Report = null;
			this.Criteria3Grid.ReportName = "Criteria3Grid";
			this.Criteria3Grid.Top = 19.39F;
			this.Criteria3Grid.Width = 7.479F;
			// 
			// Criteria4Grid
			// 
			this.Criteria4Grid.CloseBorder = false;
			this.Criteria4Grid.Height = 0.625F;
			this.Criteria4Grid.Left = 3.72529E-09F;
			this.Criteria4Grid.Name = "Criteria4Grid";
			this.Criteria4Grid.Report = null;
			this.Criteria4Grid.ReportName = "Criteria4Grid";
			this.Criteria4Grid.Top = 21.53601F;
			this.Criteria4Grid.Width = 7.5F;
			// 
			// FinalRecGrid
			// 
			this.FinalRecGrid.CloseBorder = false;
			this.FinalRecGrid.Height = 0.625F;
			this.FinalRecGrid.Left = 0F;
			this.FinalRecGrid.Name = "FinalRecGrid";
			this.FinalRecGrid.Report = null;
			this.FinalRecGrid.ReportName = "FinalRecGrid";
			this.FinalRecGrid.Top = 0.487F;
			this.FinalRecGrid.Width = 7.479F;
			// 
			// label1
			// 
			this.label1.Height = 0.344F;
			this.label1.HyperLink = null;
			this.label1.Left = 0F;
			this.label1.Name = "label1";
			this.label1.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label1.Text = "FINAL SEAFOOD RECOMMENDATION";
			this.label1.Top = 0.09F;
			this.label1.Width = 5.75F;
			// 
			// richTextBox1
			// 
			this.richTextBox1.AutoReplaceFields = true;
			this.richTextBox1.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox1.Height = 0.4480001F;
			this.richTextBox1.Left = 0F;
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.RTF = resources.GetString("richTextBox1.RTF");
			this.richTextBox1.Top = 1.205F;
			this.richTextBox1.Width = 7.479001F;
			// 
			// pageBreak3
			// 
			this.pageBreak3.Height = 0.01F;
			this.pageBreak3.Left = 0F;
			this.pageBreak3.Name = "pageBreak3";
			this.pageBreak3.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak3.Top = 1.716F;
			this.pageBreak3.Width = 6.5F;
			// 
			// TextBlockExecSummary
			// 
			this.TextBlockExecSummary.CloseBorder = false;
			this.TextBlockExecSummary.Height = 0.302F;
			this.TextBlockExecSummary.Left = 0.009000001F;
			this.TextBlockExecSummary.Name = "TextBlockExecSummary";
			this.TextBlockExecSummary.Report = null;
			this.TextBlockExecSummary.ReportName = "TextBlockExecSummary";
			this.TextBlockExecSummary.Top = 2.111F;
			this.TextBlockExecSummary.Width = 7.479F;
			// 
			// pageBreak4
			// 
			this.pageBreak4.Height = 0.01F;
			this.pageBreak4.Left = 0F;
			this.pageBreak4.Name = "pageBreak4";
			this.pageBreak4.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak4.Top = 9.684004F;
			this.pageBreak4.Width = 6.5F;
			// 
			// label3
			// 
			this.label3.Height = 0.3125F;
			this.label3.HyperLink = null;
			this.label3.Left = 0F;
			this.label3.Name = "label3";
			this.label3.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label3.Text = "ANALYSIS";
			this.label3.Top = 9.734004F;
			this.label3.Width = 4.509999F;
			// 
			// label4
			// 
			this.label4.Height = 0.3125F;
			this.label4.HyperLink = null;
			this.label4.Left = 0F;
			this.label4.Name = "label4";
			this.label4.Style = "font-size: 18pt; font-weight: bold; text-decoration: none";
			this.label4.Text = "Scoring Guide";
			this.label4.Top = 10.129F;
			this.label4.Width = 4.509999F;
			// 
			// richTextBox2
			// 
			this.richTextBox2.AutoReplaceFields = true;
			this.richTextBox2.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox2.Height = 0.4790001F;
			this.richTextBox2.Left = 0F;
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.RTF = resources.GetString("richTextBox2.RTF");
			this.richTextBox2.Top = 10.441F;
			this.richTextBox2.Width = 7.458001F;
			// 
			// label5
			// 
			this.label5.Height = 0.3125F;
			this.label5.HyperLink = null;
			this.label5.Left = 0F;
			this.label5.Name = "label5";
			this.label5.Style = "font-size: 18pt; font-weight: bold; text-decoration: underline";
			this.label5.Text = "Criterion 1: Stock for which you want a recommendation";
			this.label5.Top = 11.119F;
			this.label5.Width = 7.479001F;
			// 
			// label6
			// 
			this.label6.Height = 0.2080002F;
			this.label6.HyperLink = null;
			this.label6.Left = 0F;
			this.label6.Name = "label6";
			this.label6.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label6.Text = "Guiding Principles";
			this.label6.Top = 11.514F;
			this.label6.Width = 1.781F;
			// 
			// richTextBox3
			// 
			this.richTextBox3.AutoReplaceFields = true;
			this.richTextBox3.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox3.Height = 0.4790001F;
			this.richTextBox3.Left = 0F;
			this.richTextBox3.Name = "richTextBox3";
			this.richTextBox3.RTF = resources.GetString("richTextBox3.RTF");
			this.richTextBox3.Top = 11.722F;
			this.richTextBox3.Width = 7.458F;
			// 
			// label7
			// 
			this.label7.Height = 0.2710004F;
			this.label7.HyperLink = null;
			this.label7.Left = 0F;
			this.label7.Name = "label7";
			this.label7.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; text-decoration: none";
			this.label7.Text = "Summary";
			this.label7.Top = 12.253F;
			this.label7.Width = 1.781F;
			// 
			// Criteria1
			// 
			this.Criteria1.CloseBorder = false;
			this.Criteria1.Height = 0.625F;
			this.Criteria1.Left = 0.03F;
			this.Criteria1.Name = "Criteria1";
			this.Criteria1.Report = null;
			this.Criteria1.ReportName = "Criteria1";
			this.Criteria1.Top = 13.784F;
			this.Criteria1.Width = 7.479F;
			// 
			// Criteria2
			// 
			this.Criteria2.CloseBorder = false;
			this.Criteria2.Height = 0.625F;
			this.Criteria2.Left = 0.009F;
			this.Criteria2.Name = "Criteria2";
			this.Criteria2.Report = null;
			this.Criteria2.ReportName = "Criteria1";
			this.Criteria2.Top = 17.097F;
			this.Criteria2.Width = 7.479F;
			// 
			// Criteria4
			// 
			this.Criteria4.CloseBorder = false;
			this.Criteria4.Height = 0.625F;
			this.Criteria4.Left = 7.450581E-09F;
			this.Criteria4.Name = "Criteria4";
			this.Criteria4.Report = null;
			this.Criteria4.ReportName = "Criteria4";
			this.Criteria4.Top = 22.6405F;
			this.Criteria4.Width = 7.479F;
			// 
			// label8
			// 
			this.label8.Height = 0.3125F;
			this.label8.HyperLink = null;
			this.label8.Left = 0.03F;
			this.label8.Name = "label8";
			this.label8.Style = "font-size: 18pt; font-weight: bold; text-decoration: underline";
			this.label8.Text = "Criterion 2: Impacts on other retained and bycatch stocks";
			this.label8.Top = 14.535F;
			this.label8.Width = 7.479F;
			// 
			// label9
			// 
			this.label9.Height = 0.2080002F;
			this.label9.HyperLink = null;
			this.label9.Left = 0.03F;
			this.label9.Name = "label9";
			this.label9.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label9.Text = "Guiding Principles";
			this.label9.Top = 14.93001F;
			this.label9.Width = 1.781F;
			// 
			// richTextBox4
			// 
			this.richTextBox4.AutoReplaceFields = true;
			this.richTextBox4.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox4.Height = 0.4790001F;
			this.richTextBox4.Left = 0.03F;
			this.richTextBox4.Name = "richTextBox4";
			this.richTextBox4.RTF = resources.GetString("richTextBox4.RTF");
			this.richTextBox4.Top = 15.13801F;
			this.richTextBox4.Width = 7.458F;
			// 
			// label10
			// 
			this.label10.Height = 0.2080002F;
			this.label10.HyperLink = null;
			this.label10.Left = 0.03F;
			this.label10.Name = "label10";
			this.label10.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label10.Text = "Summary";
			this.label10.Top = 15.66901F;
			this.label10.Width = 1.781F;
			// 
			// label11
			// 
			this.label11.Height = 0.3125F;
			this.label11.HyperLink = null;
			this.label11.Left = 0.009F;
			this.label11.Name = "label11";
			this.label11.Style = "font-size: 18pt; font-weight: bold; text-decoration: underline";
			this.label11.Text = "Criterion 3: Management effectiveness";
			this.label11.Top = 17.784F;
			this.label11.Width = 7.479F;
			// 
			// label12
			// 
			this.label12.Height = 0.2080002F;
			this.label12.HyperLink = null;
			this.label12.Left = 0.009F;
			this.label12.Name = "label12";
			this.label12.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label12.Text = "Guiding Principles";
			this.label12.Top = 18.179F;
			this.label12.Width = 1.781F;
			// 
			// richTextBox5
			// 
			this.richTextBox5.AutoReplaceFields = true;
			this.richTextBox5.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox5.Height = 0.4790001F;
			this.richTextBox5.Left = 0.009F;
			this.richTextBox5.Name = "richTextBox5";
			this.richTextBox5.RTF = resources.GetString("richTextBox5.RTF");
			this.richTextBox5.Top = 18.387F;
			this.richTextBox5.Width = 7.458F;
			// 
			// label14
			// 
			this.label14.Height = 0.3125F;
			this.label14.HyperLink = null;
			this.label14.Left = -2.220446E-16F;
			this.label14.Name = "label14";
			this.label14.Style = "font-size: 18pt; font-weight: bold; text-decoration: underline";
			this.label14.Text = "Criterion 4: Impacts on the habitat and ecosystem";
			this.label14.Top = 20.138F;
			this.label14.Width = 7.479F;
			// 
			// label15
			// 
			this.label15.Height = 0.2080002F;
			this.label15.HyperLink = null;
			this.label15.Left = -2.220446E-16F;
			this.label15.Name = "label15";
			this.label15.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label15.Text = "Guiding Principles";
			this.label15.Top = 20.533F;
			this.label15.Width = 1.781F;
			// 
			// richTextBox6
			// 
			this.richTextBox6.AutoReplaceFields = true;
			this.richTextBox6.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox6.Height = 0.4790001F;
			this.richTextBox6.Left = -2.220446E-16F;
			this.richTextBox6.Name = "richTextBox6";
			this.richTextBox6.RTF = resources.GetString("richTextBox6.RTF");
			this.richTextBox6.Top = 20.741F;
			this.richTextBox6.Width = 7.458F;
			// 
			// label16
			// 
			this.label16.Height = 0.2080002F;
			this.label16.HyperLink = null;
			this.label16.Left = -2.220446E-16F;
			this.label16.Name = "label16";
			this.label16.Style = "font-size: 12pt; font-weight: bold; text-decoration: none";
			this.label16.Text = "Summary";
			this.label16.Top = 21.27201F;
			this.label16.Width = 1.781F;
			// 
			// label17
			// 
			this.label17.Height = 0.3540001F;
			this.label17.HyperLink = null;
			this.label17.Left = 0F;
			this.label17.Name = "label17";
			this.label17.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label17.Text = "OVERALL RECOMMENDATION";
			this.label17.Top = 23.37601F;
			this.label17.Width = 7.479F;
			// 
			// richTextBox7
			// 
			this.richTextBox7.AutoReplaceFields = true;
			this.richTextBox7.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox7.Height = 1.415993F;
			this.richTextBox7.Left = 0F;
			this.richTextBox7.Name = "richTextBox7";
			this.richTextBox7.RTF = resources.GetString("richTextBox7.RTF");
			this.richTextBox7.Top = 23.73001F;
			this.richTextBox7.Width = 7.5F;
			// 
			// label18
			// 
			this.label18.Height = 0.3540001F;
			this.label18.HyperLink = null;
			this.label18.Left = 0F;
			this.label18.Name = "label18";
			this.label18.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label18.Text = "ACKNOWLEDGEMENTS";
			this.label18.Top = 25.29201F;
			this.label18.Width = 7.479F;
			// 
			// label19
			// 
			this.label19.Height = 0.4080002F;
			this.label19.HyperLink = null;
			this.label19.Left = 0F;
			this.label19.Name = "label19";
			this.label19.Style = "font-style: italic";
			this.label19.Text = resources.GetString("label19.Text");
			this.label19.Top = 25.64601F;
			this.label19.Width = 7.5F;
			// 
			// pageBreak5
			// 
			this.pageBreak5.Height = 0.01F;
			this.pageBreak5.Left = 0F;
			this.pageBreak5.Name = "pageBreak5";
			this.pageBreak5.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak5.Top = 28.19501F;
			this.pageBreak5.Width = 6.5F;
			// 
			// label21
			// 
			this.label21.Height = 0.3540001F;
			this.label21.HyperLink = null;
			this.label21.Left = 0F;
			this.label21.Name = "label21";
			this.label21.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label21.Text = "REFERENCES";
			this.label21.Top = 28.35501F;
			this.label21.Width = 7.479F;
			// 
			// AppendixAGrid
			// 
			this.AppendixAGrid.CloseBorder = false;
			this.AppendixAGrid.Height = 0.625F;
			this.AppendixAGrid.Left = 0F;
			this.AppendixAGrid.Name = "AppendixAGrid";
			this.AppendixAGrid.Report = null;
			this.AppendixAGrid.ReportName = "AppendixAGrid";
			this.AppendixAGrid.Top = 30.37F;
			this.AppendixAGrid.Width = 7.479F;
			// 
			// pageBreak6
			// 
			this.pageBreak6.Height = 0.01F;
			this.pageBreak6.Left = 0F;
			this.pageBreak6.Name = "pageBreak6";
			this.pageBreak6.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak6.Top = 29.823F;
			this.pageBreak6.Width = 6.5F;
			// 
			// AppendixN1
			// 
			this.AppendixN1.CloseBorder = false;
			this.AppendixN1.Height = 0.625F;
			this.AppendixN1.Left = 0F;
			this.AppendixN1.Name = "AppendixN1";
			this.AppendixN1.Report = null;
			this.AppendixN1.ReportName = "AppendixN";
			this.AppendixN1.Top = 31.07001F;
			this.AppendixN1.Visible = false;
			this.AppendixN1.Width = 7.479F;
			// 
			// AppendixN2
			// 
			this.AppendixN2.CloseBorder = false;
			this.AppendixN2.Height = 0.625F;
			this.AppendixN2.Left = 0F;
			this.AppendixN2.Name = "AppendixN2";
			this.AppendixN2.Report = null;
			this.AppendixN2.ReportName = "AppendixN";
			this.AppendixN2.Top = 31.75751F;
			this.AppendixN2.Visible = false;
			this.AppendixN2.Width = 7.479F;
			// 
			// AppendixN3
			// 
			this.AppendixN3.CloseBorder = false;
			this.AppendixN3.Height = 0.625F;
			this.AppendixN3.Left = 0F;
			this.AppendixN3.Name = "AppendixN3";
			this.AppendixN3.Report = null;
			this.AppendixN3.ReportName = "AppendixN";
			this.AppendixN3.Top = 32.44501F;
			this.AppendixN3.Visible = false;
			this.AppendixN3.Width = 7.479F;
			// 
			// AppendixN4
			// 
			this.AppendixN4.CloseBorder = false;
			this.AppendixN4.Height = 0.625F;
			this.AppendixN4.Left = 0F;
			this.AppendixN4.Name = "AppendixN4";
			this.AppendixN4.Report = null;
			this.AppendixN4.ReportName = "AppendixN";
			this.AppendixN4.Top = 33.133F;
			this.AppendixN4.Visible = false;
			this.AppendixN4.Width = 7.479F;
			// 
			// AppendixN5
			// 
			this.AppendixN5.CloseBorder = false;
			this.AppendixN5.Height = 0.625F;
			this.AppendixN5.Left = 0F;
			this.AppendixN5.Name = "AppendixN5";
			this.AppendixN5.Report = null;
			this.AppendixN5.ReportName = "AppendixN";
			this.AppendixN5.Top = 33.86201F;
			this.AppendixN5.Visible = false;
			this.AppendixN5.Width = 7.479F;
			// 
			// AppendixN6
			// 
			this.AppendixN6.CloseBorder = false;
			this.AppendixN6.Height = 0.625F;
			this.AppendixN6.Left = 0F;
			this.AppendixN6.Name = "AppendixN6";
			this.AppendixN6.Report = null;
			this.AppendixN6.ReportName = "AppendixN";
			this.AppendixN6.Top = 34.54951F;
			this.AppendixN6.Visible = false;
			this.AppendixN6.Width = 7.479F;
			// 
			// AppendixN7
			// 
			this.AppendixN7.CloseBorder = false;
			this.AppendixN7.Height = 0.625F;
			this.AppendixN7.Left = 0F;
			this.AppendixN7.Name = "AppendixN7";
			this.AppendixN7.Report = null;
			this.AppendixN7.ReportName = "AppendixN";
			this.AppendixN7.Top = 35.23701F;
			this.AppendixN7.Visible = false;
			this.AppendixN7.Width = 7.479F;
			// 
			// label22
			// 
			this.label22.Height = 0.3540001F;
			this.label22.HyperLink = null;
			this.label22.Left = 0F;
			this.label22.Name = "label22";
			this.label22.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label22.Text = "About Seafood Watch�  ";
			this.label22.Top = 36.05F;
			this.label22.Width = 7.479F;
			// 
			// pageBreak7
			// 
			this.pageBreak7.Height = 0.01F;
			this.pageBreak7.Left = 0F;
			this.pageBreak7.Name = "pageBreak7";
			this.pageBreak7.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak7.Top = 37.2535F;
			this.pageBreak7.Width = 6.5F;
			// 
			// label23
			// 
			this.label23.Height = 0.3540001F;
			this.label23.HyperLink = null;
			this.label23.Left = 0F;
			this.label23.Name = "label23";
			this.label23.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label23.Text = "Guiding Principles";
			this.label23.Top = 37.3615F;
			this.label23.Width = 7.479F;
			// 
			// pageBreak8
			// 
			this.pageBreak8.Height = 0.01F;
			this.pageBreak8.Left = 0F;
			this.pageBreak8.Name = "pageBreak8";
			this.pageBreak8.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak8.Top = 35.956F;
			this.pageBreak8.Width = 6.5F;
			// 
			// richTextBox8
			// 
			this.richTextBox8.AutoReplaceFields = true;
			this.richTextBox8.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox8.Height = 0.698F;
			this.richTextBox8.Left = 0F;
			this.richTextBox8.Name = "richTextBox8";
			this.richTextBox8.RTF = resources.GetString("richTextBox8.RTF");
			this.richTextBox8.Top = 36.467F;
			this.richTextBox8.Width = 7.479001F;
			// 
			// richTextBox9
			// 
			this.richTextBox9.AutoReplaceFields = true;
			this.richTextBox9.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox9.Height = 1.5F;
			this.richTextBox9.Left = 0F;
			this.richTextBox9.Name = "richTextBox9";
			this.richTextBox9.RTF = resources.GetString("richTextBox9.RTF");
			this.richTextBox9.Top = 37.7785F;
			this.richTextBox9.Width = 7.458F;
			// 
			// richTextBox10
			// 
			this.richTextBox10.AutoReplaceFields = true;
			this.richTextBox10.Font = new System.Drawing.Font("Arial", 10F);
			this.richTextBox10.Height = 1.5F;
			this.richTextBox10.Left = 0F;
			this.richTextBox10.Name = "richTextBox10";
			this.richTextBox10.RTF = resources.GetString("richTextBox10.RTF");
			this.richTextBox10.Top = 39.56052F;
			this.richTextBox10.Width = 7.458F;
			// 
			// pageBreak9
			// 
			this.pageBreak9.Height = 0.01F;
			this.pageBreak9.Left = 0F;
			this.pageBreak9.Name = "pageBreak9";
			this.pageBreak9.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak9.Top = 39.41651F;
			this.pageBreak9.Width = 6.5F;
			// 
			// TextBlockAck
			// 
			this.TextBlockAck.CloseBorder = false;
			this.TextBlockAck.Height = 0.625F;
			this.TextBlockAck.Left = 0F;
			this.TextBlockAck.Name = "TextBlockAck";
			this.TextBlockAck.Report = null;
			this.TextBlockAck.ReportName = "TextBlockAck";
			this.TextBlockAck.Top = 26.1165F;
			this.TextBlockAck.Width = 7.479F;
			// 
			// References
			// 
			this.References.CloseBorder = false;
			this.References.Height = 0.625F;
			this.References.Left = 0F;
			this.References.Name = "References";
			this.References.Report = null;
			this.References.ReportName = "References";
			this.References.Top = 28.787F;
			this.References.Width = 7.479001F;
			// 
			// label24
			// 
			this.label24.Height = 0.2710004F;
			this.label24.HyperLink = null;
			this.label24.Left = 0F;
			this.label24.Name = "label24";
			this.label24.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; text-decoration: none";
			this.label24.Text = "Justification of Ranking";
			this.label24.Top = 13.43001F;
			this.label24.Width = 3.333F;
			// 
			// label25
			// 
			this.label25.Height = 0.2710004F;
			this.label25.HyperLink = null;
			this.label25.Left = 0.009F;
			this.label25.Name = "label25";
			this.label25.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; text-decoration: none";
			this.label25.Text = "Justification of Ranking";
			this.label25.Top = 16.764F;
			this.label25.Width = 3.333001F;
			// 
			// label13
			// 
			this.label13.Height = 0.2710004F;
			this.label13.HyperLink = null;
			this.label13.Left = 0.009000001F;
			this.label13.Name = "label13";
			this.label13.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; text-decoration: none";
			this.label13.Text = "Summary";
			this.label13.Top = 19.064F;
			this.label13.Width = 3.333001F;
			// 
			// label26
			// 
			this.label26.Height = 0.2710004F;
			this.label26.HyperLink = null;
			this.label26.Left = 0F;
			this.label26.Name = "label26";
			this.label26.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; text-decoration: none";
			this.label26.Text = "Justification of Ranking";
			this.label26.Top = 22.307F;
			this.label26.Width = 3.333001F;
			// 
			// label27
			// 
			this.label27.Height = 0.3540001F;
			this.label27.HyperLink = null;
			this.label27.Left = 0F;
			this.label27.Name = "label27";
			this.label27.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label27.Text = "APPENDICES";
			this.label27.Top = 29.95901F;
			this.label27.Width = 7.479F;
			// 
			// label2
			// 
			this.label2.Height = 0.344F;
			this.label2.HyperLink = null;
			this.label2.Left = 0F;
			this.label2.Name = "label2";
			this.label2.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label2.Text = "Executive Summary";
			this.label2.Top = 1.767F;
			this.label2.Width = 5.75F;
			// 
			// label28
			// 
			this.label28.Height = 0.344F;
			this.label28.HyperLink = null;
			this.label28.Left = 0F;
			this.label28.Name = "label28";
			this.label28.Style = "font-size: 20pt; font-weight: bold; text-decoration: underline";
			this.label28.Text = "Introduction";
			this.label28.Top = 2.413F;
			this.label28.Width = 5.75F;
			// 
			// label29
			// 
			this.label29.Height = 0.2295004F;
			this.label29.HyperLink = null;
			this.label29.Left = 0F;
			this.label29.Name = "label29";
			this.label29.Style = "font-size: 11pt; font-style: italic; font-weight: normal; text-decoration: none; " +
				"ddo-char-set: 0";
			this.label29.Text = "Scope of the analysis and ensuing recommendation";
			this.label29.Top = 2.8195F;
			this.label29.Width = 5.75F;
			// 
			// TextBlockOverview
			// 
			this.TextBlockOverview.CloseBorder = false;
			this.TextBlockOverview.Height = 0.625F;
			this.TextBlockOverview.Left = 0F;
			this.TextBlockOverview.Name = "TextBlockOverview";
			this.TextBlockOverview.Report = null;
			this.TextBlockOverview.ReportName = "TextBlockOverview";
			this.TextBlockOverview.Top = 4.3295F;
			this.TextBlockOverview.Width = 7.479F;
			// 
			// label30
			// 
			this.label30.Height = 0.2295004F;
			this.label30.HyperLink = null;
			this.label30.Left = 0F;
			this.label30.Name = "label30";
			this.label30.Style = "font-size: 11pt; font-style: italic; font-weight: normal; text-decoration: none; " +
				"ddo-char-set: 0";
			this.label30.Text = "Overview of the species and management bodies";
			this.label30.Top = 4.038F;
			this.label30.Width = 5.75F;
			// 
			// TextBlockProductionStatistics
			// 
			this.TextBlockProductionStatistics.CloseBorder = false;
			this.TextBlockProductionStatistics.Height = 0.625F;
			this.TextBlockProductionStatistics.Left = 0.009000001F;
			this.TextBlockProductionStatistics.Name = "TextBlockProductionStatistics";
			this.TextBlockProductionStatistics.Report = null;
			this.TextBlockProductionStatistics.ReportName = "TextBlockProductionStatistics";
			this.TextBlockProductionStatistics.Top = 5.621F;
			this.TextBlockProductionStatistics.Width = 7.479F;
			// 
			// label31
			// 
			this.label31.Height = 0.2295004F;
			this.label31.HyperLink = null;
			this.label31.Left = 0.009000001F;
			this.label31.Name = "label31";
			this.label31.Style = "font-size: 11pt; font-style: italic; font-weight: normal; text-decoration: none; " +
				"ddo-char-set: 0";
			this.label31.Text = "Production Statistics";
			this.label31.Top = 5.3295F;
			this.label31.Width = 5.75F;
			// 
			// TextBlockImportance
			// 
			this.TextBlockImportance.CloseBorder = false;
			this.TextBlockImportance.Height = 0.625F;
			this.TextBlockImportance.Left = 0F;
			this.TextBlockImportance.Name = "TextBlockImportance";
			this.TextBlockImportance.Report = null;
			this.TextBlockImportance.ReportName = "TextBlockImportance";
			this.TextBlockImportance.Top = 6.9855F;
			this.TextBlockImportance.Width = 7.479F;
			// 
			// label32
			// 
			this.label32.Height = 0.2295004F;
			this.label32.HyperLink = null;
			this.label32.Left = 0F;
			this.label32.Name = "label32";
			this.label32.Style = "font-size: 11pt; font-style: italic; font-weight: normal; text-decoration: none; " +
				"ddo-char-set: 0";
			this.label32.Text = "Importance to the US/North American market";
			this.label32.Top = 6.694F;
			this.label32.Width = 5.75F;
			// 
			// TextBlockMarketNames
			// 
			this.TextBlockMarketNames.CloseBorder = false;
			this.TextBlockMarketNames.Height = 0.625F;
			this.TextBlockMarketNames.Left = 0.008999999F;
			this.TextBlockMarketNames.Name = "TextBlockMarketNames";
			this.TextBlockMarketNames.Report = null;
			this.TextBlockMarketNames.ReportName = "TextBlockMarketNames";
			this.TextBlockMarketNames.Top = 8.277F;
			this.TextBlockMarketNames.Width = 7.479F;
			// 
			// label33
			// 
			this.label33.Height = 0.2295004F;
			this.label33.HyperLink = null;
			this.label33.Left = 0.008999999F;
			this.label33.Name = "label33";
			this.label33.Style = "font-size: 11pt; font-style: italic; font-weight: normal; text-decoration: none; " +
				"ddo-char-set: 0";
			this.label33.Text = "Common and market names";
			this.label33.Top = 7.9855F;
			this.label33.Width = 5.75F;
			// 
			// pageBreak1
			// 
			this.pageBreak1.Height = 0.01F;
			this.pageBreak1.Left = 0F;
			this.pageBreak1.Name = "pageBreak1";
			this.pageBreak1.Size = new System.Drawing.SizeF(6.5F, 0.01F);
			this.pageBreak1.Top = 20.076F;
			this.pageBreak1.Width = 6.5F;
			// 
			// pageBreak2
			// 
			this.pageBreak2.Height = 0.05555556F;
			this.pageBreak2.Left = 0F;
			this.pageBreak2.Name = "pageBreak2";
			this.pageBreak2.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
			this.pageBreak2.Top = 17.7845F;
			this.pageBreak2.Width = 6.5F;
			// 
			// pageBreak10
			// 
			this.pageBreak10.Height = 0.05555556F;
			this.pageBreak10.Left = 0F;
			this.pageBreak10.Name = "pageBreak10";
			this.pageBreak10.Size = new System.Drawing.SizeF(6.5F, 0.05555556F);
			this.pageBreak10.Top = 14.4715F;
			this.pageBreak10.Width = 6.5F;
			// 
			// FullReport
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.509F;
			this.Sections.Add(this.detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			this.ReportStart += new System.EventHandler(this.FullReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.SubReport TextBlockScope;
		private DataDynamics.ActiveReports.SubReport Criteria1Grid;
		private DataDynamics.ActiveReports.SubReport Criteria2Grid;
		private DataDynamics.ActiveReports.SubReport Criteria3Grid;
		private DataDynamics.ActiveReports.SubReport Criteria4Grid;
		private DataDynamics.ActiveReports.SubReport FinalRecGrid;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.RichTextBox richTextBox1;
		private DataDynamics.ActiveReports.PageBreak pageBreak3;
		private DataDynamics.ActiveReports.SubReport TextBlockExecSummary;
		private DataDynamics.ActiveReports.PageBreak pageBreak4;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.RichTextBox richTextBox2;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.RichTextBox richTextBox3;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.SubReport Criteria1;
		private DataDynamics.ActiveReports.SubReport Criteria2;
		private DataDynamics.ActiveReports.SubReport Criteria4;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.Label label9;
		private DataDynamics.ActiveReports.RichTextBox richTextBox4;
		private DataDynamics.ActiveReports.Label label10;
		private DataDynamics.ActiveReports.Label label11;
		private DataDynamics.ActiveReports.Label label12;
		private DataDynamics.ActiveReports.RichTextBox richTextBox5;
		private DataDynamics.ActiveReports.Label label14;
		private DataDynamics.ActiveReports.Label label15;
		private DataDynamics.ActiveReports.RichTextBox richTextBox6;
		private DataDynamics.ActiveReports.Label label16;
		private DataDynamics.ActiveReports.Label label17;
		private DataDynamics.ActiveReports.RichTextBox richTextBox7;
		private DataDynamics.ActiveReports.SubReport subReport1;
		private DataDynamics.ActiveReports.Label label18;
		private DataDynamics.ActiveReports.Label label19;
		private DataDynamics.ActiveReports.PageBreak pageBreak5;
		private DataDynamics.ActiveReports.Label label21;
		private DataDynamics.ActiveReports.SubReport AppendixAGrid;
		private DataDynamics.ActiveReports.PageBreak pageBreak6;
		private DataDynamics.ActiveReports.SubReport AppendixN1;
		private DataDynamics.ActiveReports.SubReport AppendixN2;
		private DataDynamics.ActiveReports.SubReport AppendixN3;
		private DataDynamics.ActiveReports.SubReport AppendixN4;
		private DataDynamics.ActiveReports.SubReport AppendixN5;
		private DataDynamics.ActiveReports.SubReport AppendixN6;
		private DataDynamics.ActiveReports.SubReport AppendixN7;
		private DataDynamics.ActiveReports.Label label22;
		private DataDynamics.ActiveReports.PageBreak pageBreak7;
		private DataDynamics.ActiveReports.Label label23;
		private DataDynamics.ActiveReports.PageBreak pageBreak8;
		private DataDynamics.ActiveReports.RichTextBox richTextBox8;
		private DataDynamics.ActiveReports.RichTextBox richTextBox9;
		private DataDynamics.ActiveReports.RichTextBox richTextBox10;
		private DataDynamics.ActiveReports.PageBreak pageBreak9;
		private DataDynamics.ActiveReports.SubReport subReport2;
		private DataDynamics.ActiveReports.SubReport TextBlockAck;
		private DataDynamics.ActiveReports.SubReport References;
		private DataDynamics.ActiveReports.Label label24;
		private DataDynamics.ActiveReports.Label label25;
		private DataDynamics.ActiveReports.Label label13;
		private DataDynamics.ActiveReports.Label label26;
		private DataDynamics.ActiveReports.Label label27;
		private DataDynamics.ActiveReports.Label label2;
		private DataDynamics.ActiveReports.Label label28;
		private DataDynamics.ActiveReports.Label label29;
		private DataDynamics.ActiveReports.SubReport TextBlockOverview;
		private DataDynamics.ActiveReports.Label label30;
		private DataDynamics.ActiveReports.SubReport TextBlockProductionStatistics;
		private DataDynamics.ActiveReports.Label label31;
		private DataDynamics.ActiveReports.SubReport TextBlockImportance;
		private DataDynamics.ActiveReports.Label label32;
		private DataDynamics.ActiveReports.SubReport TextBlockMarketNames;
		private DataDynamics.ActiveReports.Label label33;
		private DataDynamics.ActiveReports.PageBreak pageBreak1;
		private DataDynamics.ActiveReports.PageBreak pageBreak2;
		private DataDynamics.ActiveReports.PageBreak pageBreak10;
	}
}
