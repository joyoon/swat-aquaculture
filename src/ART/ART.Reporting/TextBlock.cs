using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for TextBlock.
	/// </summary>
	public partial class TextBlock : DataDynamics.ActiveReports.ActiveReport
	{

		public TextBlock()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			this.Body.Html = "<font style='font-family:Arial; font-size:12pt;'>" + Body.Text + "</font>";
		}
	}
}
