using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for FullReport.
	/// </summary>
	public partial class FullReport : DataDynamics.ActiveReports.ActiveReport
	{

		public FullReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.PageSettings.Margins.Left = 0.5f;
			this.PageSettings.Margins.Right = 0.5f;
			this.PageSettings.Margins.Top = 0.2f; //less so that the Page header looks to be a header
			this.PageSettings.Margins.Bottom = 0.5f;
		}

		public Dictionary<string, Object> DataSources { get; set; }

		private FinalRecGrid finalRecGrid;

		private TextBlock textBlockExecSummary;
		private TextBlock textBlockScope;
		private TextBlock textBlockOverview;
		private TextBlock textBlockProductionStatistics;
		private TextBlock textBlockImportance;
		private TextBlock textBlockMarketNames;


		private Criteria1Grid criteria1Grid;
		private Criteria1 criteria1;
		private Criteria2Grid criteria2Grid;
		private Criteria1 criteria2; //Note that I'm binding 2 to a type 1
		private Criteria3Grid criteria3Grid;
		//private Criteria34 criteria31;
		//private Criteria34 criteria32;
		private Criteria4Grid criteria4Grid;
		private Criteria34 criteria4;
		private OverallRec overallRec;
		private TextBlock textBlockAck;
		private References references;
		private AppendixAGrid appendixAGrid;
		private AppendixN appendix1;
		private AppendixN appendix2;
		private AppendixN appendix3;
		private AppendixN appendix4;
		private AppendixN appendix5;
		private AppendixN appendix6;
		private AppendixN appendix7;

		private void FullReport_ReportStart(object sender, EventArgs e)
		{
			finalRecGrid = new FinalRecGrid();
			finalRecGrid.DataSource = DataSources["OverallRec"];
			this.FinalRecGrid.Report = finalRecGrid;

			this.richTextBox1.Html = "<font style='font-family:Arial; font-size:12pt;'><b>Scoring note</b> � scores range from zero to five where zero indicates very poor performance and five indicates the fishing operations have no significant impact.</font>";

			textBlockExecSummary = new TextBlock();
			textBlockExecSummary.DataSource = DataSources["TextBlockExecSummary"];
			this.TextBlockExecSummary.Report = textBlockExecSummary;

			textBlockScope = new TextBlock();
			textBlockScope.DataSource = DataSources["TextBlockScope"];
			this.TextBlockScope.Report = textBlockScope;

			textBlockOverview = new TextBlock();
			textBlockOverview.DataSource = DataSources["TextBlockSpeciesOverview"];
			this.TextBlockOverview.Report = textBlockOverview;

			textBlockProductionStatistics = new TextBlock();
			textBlockProductionStatistics.DataSource = DataSources["TextBlockProductionStatistics"];
			this.TextBlockProductionStatistics.Report = textBlockProductionStatistics;

			textBlockImportance = new TextBlock();
			textBlockImportance.DataSource = DataSources["TextBlockImportance"];
			this.TextBlockImportance.Report = textBlockImportance;

			textBlockMarketNames = new TextBlock();
			textBlockMarketNames.DataSource = DataSources["TextBlockMarketNames"];
			this.TextBlockMarketNames.Report = textBlockMarketNames;

			this.richTextBox2.Html = @"<font style='font-family:Arial; font-size:12pt;'><ul><li>All scores result in a zero to five final score for the criterion and the overall final rank. A zero score indicates poor performance, while a score of five indicates high performance.</li><li>The full Seafood Watch Fisheries Criteria that the following scores relate to are available on our website at <a href='http://www.seafoodwatch.org'>http://www.seafoodwatch.org</a>.</li></ul>";

			//overallRec = new OverallRec();
			//overallRec.DataSource = DataSources["OverallRec"];
			//this.OverallRec.Report = overallRec;


			//Crit 1 guiding Principles
			this.richTextBox3.Html = "<font style='font-family:Arial; font-size:12pt;'><ul><li>The stock is healthy and abundant.  Abundance, size, sex, age and genetic structure should be maintained at levels that do not impair the long-term productivity of the stock or fulfillment of its role in the ecosystem and food web.</li><li>Fishing mortality does not threaten populations or impede the ecological role of any marine life. Fishing mortality should be appropriate given current abundance and inherent resilience to fishing while accounting for scientific uncertainty, management uncertainty, and non-fishery impacts such as habitat degradation.</li></ul>";

			criteria1Grid = new Criteria1Grid();
			criteria1Grid.DataSource = DataSources["Criteria1Grid"];
			criteria1Grid.Criteria1Synthesis = DataSources["Criteria1Synthesis"].ToString();
			this.Criteria1Grid.Report = criteria1Grid;

			criteria1 = new Criteria1();
			criteria1.DataSource = DataSources["Criteria1"];
			this.Criteria1.Report = criteria1;

			//Crit 2 guiding Principles
			this.richTextBox4.Html = "<font style='font-family:Arial; font-size:12pt;'><ul><li>The fishery minimizes bycatch. Seafood Watch� defines bycatch as all fisheries-related mortality or injury other than the retained catch.  Examples include discards, endangered or threatened species catch, pre-catch mortality and ghost fishing. All discards, including those released alive, are considered bycatch unless there is valid scientific evidence of high post-release survival and there is no documented evidence of negative impacts at the population level.</li><li>Fishing mortality does not threaten populations or impede the ecological role of any marine life.  Fishing mortality should be appropriate given each impacted species� abundance and productivity, accounting for scientific uncertainty, management uncertainty and non-fishery impacts such as habitat degradation.</li></ul>";

			criteria2Grid = new Criteria2Grid();
			criteria2Grid.DataSource = DataSources["Criteria2Grid"];
			criteria2Grid.Criteria2Synthesis = DataSources["Criteria2Synthesis"].ToString();
			this.Criteria2Grid.Report = criteria2Grid;

			criteria2 = new Criteria1();
			criteria2.DataSource = DataSources["Criteria2"];
			this.Criteria2.Report = criteria2;

			//Crit 3 Guiding Principles
			this.richTextBox5.Html = "<font style='font-family:Arial; font-size:12pt;'><ul><li>The fishery is managed to sustain the long-term productivity of all impacted species. Management should be appropriate for the inherent resilience of affected marine life and should incorporate data sufficient to assess the affected species and manage fishing mortality to ensure little risk of depletion. Measures should be implemented and enforced to ensure that fishery mortality does not threaten the long-term productivity or ecological role of any species in the future.</li></ul>";

			criteria3Grid = new Criteria3Grid();
			criteria3Grid.DataSource = DataSources["Criteria3Grid"];
			criteria3Grid.DataSource31Grid = DataSources["Criteria31Grid"];
			criteria3Grid.DataSource32Grid = DataSources["Criteria32Grid"];
			criteria3Grid.DataSource31 = DataSources["Criteria31"];
			criteria3Grid.DataSource32 = DataSources["Criteria32"];
			criteria3Grid.Criteria3Synthesis = DataSources["Criteria3Synthesis"].ToString();
			criteria3Grid.Criteria31Synthesis = DataSources["Criteria31Synthesis"].ToString();
			criteria3Grid.Criteria32Synthesis = DataSources["Criteria32Synthesis"].ToString();
			this.Criteria3Grid.Report = criteria3Grid;

			//criteria31 = new Criteria34();
			//
			//this.Criteria31.Report = criteria31;

			//criteria32 = new Criteria34();
			//criteria32.DataSource = DataSources["Criteria32"];
			//this.Criteria32.Report = criteria32;

			this.richTextBox6.Html = "<font style='font-family:Arial; font-size:12pt;'><ul><li>The fishery is conducted such that impacts on the seafloor are minimized and the ecological and functional roles of seafloor habitats are maintained.</li><li>Fishing activities should not seriously reduce ecosystem services provided by any fished species or result in harmful changes such as trophic cascades, phase shifts or reduction of genetic diversity.</li></ul>";

			criteria4Grid = new Criteria4Grid();
			criteria4Grid.DataSource = DataSources["Criteria4Grid"];
			criteria4Grid.Criteria4Synthesis = DataSources["Criteria4Synthesis"].ToString();
			this.Criteria4Grid.Report = criteria4Grid;

			criteria4 = new Criteria34();
			criteria4.DataSource = DataSources["Criteria4"];
			this.Criteria4.Report = criteria4;

			textBlockAck = new TextBlock();
			textBlockAck.DataSource = DataSources["TextBlockAck"];
			this.TextBlockAck.Report = textBlockAck;

			references = new References();
			references.DataSource = DataSources["References"];
			this.References.Report = references;

			appendixAGrid = new AppendixAGrid();
			appendixAGrid.DataSource = DataSources["AppendixAGrid"];
			this.AppendixAGrid.Report = appendixAGrid;

			if (DataSources.ContainsKey("Appendix1"))
			{
				appendix1 = new AppendixN();
				appendix1.DataSource = DataSources["Appendix1"];
				this.AppendixN1.Visible = true;
				this.AppendixN1.Report = appendix1;
			}

			if (DataSources.ContainsKey("Appendix2"))
			{
			    appendix2 = new AppendixN();
			    appendix2.DataSource = DataSources["Appendix2"];
			    this.AppendixN2.Visible = true;
			    this.AppendixN2.Report = appendix2;
			}

			if (DataSources.ContainsKey("Appendix3"))
			{
			    appendix3 = new AppendixN();
			    appendix3.DataSource = DataSources["Appendix3"];
			    this.AppendixN3.Visible = true;
			    this.AppendixN3.Report = appendix3;
			}

			if (DataSources.ContainsKey("Appendix4"))
			{
			    appendix4 = new AppendixN();
			    appendix4.DataSource = DataSources["Appendix4"];
			    this.AppendixN4.Visible = true;
			    this.AppendixN4.Report = appendix4;
			}

			if (DataSources.ContainsKey("Appendix5"))
			{
			    appendix5 = new AppendixN();
			    appendix5.DataSource = DataSources["Appendix5"];
			    this.AppendixN5.Visible = true;
			    this.AppendixN5.Report = appendix5;
			}

			if (DataSources.ContainsKey("Appendix6"))
			{
			    appendix6 = new AppendixN();
			    appendix6.DataSource = DataSources["Appendix6"];
			    this.AppendixN6.Visible = true;
			    this.AppendixN6.Report = appendix6;
			}

			if (DataSources.ContainsKey("Appendix7"))
			{
			    appendix7 = new AppendixN();
			    appendix7.DataSource = DataSources["Appendix7"];
			    this.AppendixN7.Visible = true;
			    this.AppendixN7.Report = appendix7;
			}

			//textBlockSpeciesOverview = new TextBlock();
			//textBlockSpeciesOverview.DataSource = DataSources["TextBlockSpeciesOverview"];
			//this.TextBlockSpeciesOverview.Report = textBlockSpeciesOverview;
		}


		/*
		 * 		private Criteria31Grid rpt31;
		private Criteria32Grid rpt32;

		private void Criteria3Grid_ReportStart(object sender, EventArgs e)
		{
			rpt31 = new Criteria31Grid();
			rpt32 = new Criteria32Grid();

			rpt31.DataSource = DataSource31;
			rpt32.DataSource = DataSource32;

			this.Criteria31Grid.Report = rpt31;
			this.Criteria32Grid.Report = rpt32;
		}

		public Object DataSource31 { get; set; }
		public Object DataSource32 { get; set; }*/
	}
}
