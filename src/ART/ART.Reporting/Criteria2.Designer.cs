namespace ART.Reporting
{
	/// <summary>
	/// Summary description for NewActiveReport1.
	/// </summary>
	partial class Criteria2
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria2));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.textBox2 = new DataDynamics.ActiveReports.TextBox();
			this.textBox3 = new DataDynamics.ActiveReports.TextBox();
			this.ScoreDescription = new DataDynamics.ActiveReports.TextBox();
			this.ScoreNum = new DataDynamics.ActiveReports.TextBox();
			this.grpReference = new DataDynamics.ActiveReports.GroupHeader();
			this.line1 = new DataDynamics.ActiveReports.Line();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.grpReferenceFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.grpFactorType = new DataDynamics.ActiveReports.GroupHeader();
			this.picture1 = new DataDynamics.ActiveReports.Picture();
			this.FactorType = new DataDynamics.ActiveReports.TextBox();
			this.grpFactorTypeFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.lblRationale = new DataDynamics.ActiveReports.Label();
			this.KeyInfo = new DataDynamics.ActiveReports.RichTextBox();
			this.Rationale = new DataDynamics.ActiveReports.RichTextBox();
			this.grpSpecies = new DataDynamics.ActiveReports.GroupHeader();
			this.SpeciesLocationMethod = new DataDynamics.ActiveReports.TextBox();
			this.grpSpeciesFooter = new DataDynamics.ActiveReports.GroupFooter();
			this.grpFactorId = new DataDynamics.ActiveReports.GroupHeader();
			this.grpFactorIdFooter = new DataDynamics.ActiveReports.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SpeciesLocationMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.textBox2,
            this.textBox3});
			this.detail.Height = 0.25F;
			this.detail.Name = "detail";
			this.detail.Visible = false;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "ShortName";
			this.textBox2.Height = 0.208F;
			this.textBox2.Left = 0.09F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "font-weight: bold";
			this.textBox2.Text = "ShortName";
			this.textBox2.Top = 0F;
			this.textBox2.Width = 1.192F;
			// 
			// textBox3
			// 
			this.textBox3.DataField = "Bibliography";
			this.textBox3.Height = 0.21875F;
			this.textBox3.Left = 1.35F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Text = "Bibliography";
			this.textBox3.Top = 0F;
			this.textBox3.Width = 6.15F;
			// 
			// ScoreDescription
			// 
			this.ScoreDescription.DataField = "ScoreDescription";
			this.ScoreDescription.Height = 0.292F;
			this.ScoreDescription.Left = 0.791F;
			this.ScoreDescription.Name = "ScoreDescription";
			this.ScoreDescription.Style = "color: #333333; font-size: 11pt; font-weight: bold; vertical-align: middle; white" +
				"-space: nowrap; ddo-char-set: 1";
			this.ScoreDescription.Text = "ScoreDescription";
			this.ScoreDescription.Top = 0F;
			this.ScoreDescription.Width = 6.511001F;
			// 
			// ScoreNum
			// 
			this.ScoreNum.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
			this.ScoreNum.DataField = "ScoreNum";
			this.ScoreNum.Height = 0.291F;
			this.ScoreNum.Left = 0.09100007F;
			this.ScoreNum.Name = "ScoreNum";
			this.ScoreNum.Style = "background-color: #606060; color: White; font-size: 11pt; font-weight: bold; text" +
				"-align: center; vertical-align: middle; white-space: nowrap; ddo-char-set: 1";
			this.ScoreNum.Text = "ScoreNumber";
			this.ScoreNum.Top = 0F;
			this.ScoreNum.Width = 0.7F;
			// 
			// grpReference
			// 
			this.grpReference.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.grpReference.ColumnGroupKeepTogether = true;
			this.grpReference.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.line1,
            this.label3});
			this.grpReference.Height = 0.34375F;
			this.grpReference.Name = "grpReference";
			this.grpReference.Visible = false;
			// 
			// line1
			// 
			this.line1.Height = 0.001000002F;
			this.line1.Left = 0.06900001F;
			this.line1.LineColor = System.Drawing.Color.DarkGray;
			this.line1.LineWeight = 1F;
			this.line1.Name = "line1";
			this.line1.Top = 0.062F;
			this.line1.Width = 7.233001F;
			this.line1.X1 = 0.06900001F;
			this.line1.X2 = 7.302001F;
			this.line1.Y1 = 0.062F;
			this.line1.Y2 = 0.063F;
			// 
			// label3
			// 
			this.label3.Height = 0.1875F;
			this.label3.HyperLink = null;
			this.label3.Left = 0.06900001F;
			this.label3.Name = "label3";
			this.label3.Style = "font-size: 9.75pt; font-weight: bold; ddo-char-set: 0";
			this.label3.Text = "References:";
			this.label3.Top = 0.115F;
			this.label3.Width = 1.40625F;
			// 
			// grpReferenceFooter
			// 
			this.grpReferenceFooter.Height = 0F;
			this.grpReferenceFooter.Name = "grpReferenceFooter";
			// 
			// grpFactorType
			// 
			this.grpFactorType.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picture1,
            this.FactorType});
			this.grpFactorType.DataField = "FactorType";
			this.grpFactorType.Height = 0.5520833F;
			this.grpFactorType.Name = "grpFactorType";
			// 
			// picture1
			// 
			this.picture1.Height = 0.489F;
			this.picture1.HyperLink = null;
			this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
			this.picture1.Left = 0F;
			this.picture1.Name = "picture1";
			this.picture1.Top = 0F;
			this.picture1.Width = 7.5F;
			// 
			// FactorType
			// 
			this.FactorType.DataField = "Species";
			this.FactorType.Height = 0.5420001F;
			this.FactorType.Left = 0F;
			this.FactorType.Name = "FactorType";
			this.FactorType.Style = "color: Black; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-cha" +
				"r-set: 1";
			this.FactorType.Text = "FactorType";
			this.FactorType.Top = 0F;
			this.FactorType.Width = 7.5F;
			// 
			// grpFactorTypeFooter
			// 
			this.grpFactorTypeFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.grpFactorTypeFooter.Height = 0F;
			this.grpFactorTypeFooter.Name = "grpFactorTypeFooter";
			// 
			// lblRationale
			// 
			this.lblRationale.Height = 0.1875F;
			this.lblRationale.HyperLink = null;
			this.lblRationale.Left = 0.06900014F;
			this.lblRationale.Name = "lblRationale";
			this.lblRationale.Style = "font-family: Arial; font-size: 11pt; font-weight: bold; ddo-char-set: 1";
			this.lblRationale.Text = "Detail Rationale:";
			this.lblRationale.Top = 0.796667F;
			this.lblRationale.Width = 1.76F;
			// 
			// KeyInfo
			// 
			this.KeyInfo.AutoReplaceFields = true;
			this.KeyInfo.DataField = "KeyInfo";
			this.KeyInfo.Font = new System.Drawing.Font("Arial", 10F);
			this.KeyInfo.Height = 0.2290001F;
			this.KeyInfo.Left = 0.06900001F;
			this.KeyInfo.Name = "KeyInfo";
			this.KeyInfo.RTF = resources.GetString("KeyInfo.RTF");
			this.KeyInfo.Top = 0.405F;
			this.KeyInfo.Width = 7.431F;
			// 
			// Rationale
			// 
			this.Rationale.AutoReplaceFields = true;
			this.Rationale.DataField = "Rationale";
			this.Rationale.Font = new System.Drawing.Font("Arial", 10F);
			this.Rationale.Height = 0.2290001F;
			this.Rationale.Left = 0.06900008F;
			this.Rationale.Name = "Rationale";
			this.Rationale.RTF = resources.GetString("Rationale.RTF");
			this.Rationale.Top = 1.071F;
			this.Rationale.Width = 7.431F;
			// 
			// grpSpecies
			// 
			this.grpSpecies.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.grpSpecies.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.SpeciesLocationMethod});
			this.grpSpecies.DataField = "Species";
			this.grpSpecies.Height = 0.2296667F;
			this.grpSpecies.Name = "grpSpecies";
			// 
			// SpeciesLocationMethod
			// 
			this.SpeciesLocationMethod.DataField = "=Species + \" - \" + LocationMethod";
			this.SpeciesLocationMethod.Height = 0.23F;
			this.SpeciesLocationMethod.Left = 0.09F;
			this.SpeciesLocationMethod.Name = "SpeciesLocationMethod";
			this.SpeciesLocationMethod.Style = "color: #606060; font-size: 11pt; font-weight: bold; vertical-align: middle; ddo-c" +
				"har-set: 1";
			this.SpeciesLocationMethod.Text = "SpeciesLocationMethod";
			this.SpeciesLocationMethod.Top = 0F;
			this.SpeciesLocationMethod.Width = 7.327F;
			// 
			// grpSpeciesFooter
			// 
			this.grpSpeciesFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.grpSpeciesFooter.Height = 0F;
			this.grpSpeciesFooter.Name = "grpSpeciesFooter";
			// 
			// grpFactorId
			// 
			this.grpFactorId.Height = 0F;
			this.grpFactorId.Name = "grpFactorId";
			// 
			// grpFactorIdFooter
			// 
			this.grpFactorIdFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.grpFactorIdFooter.CanShrink = true;
			this.grpFactorIdFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.ScoreDescription,
            this.ScoreNum,
            this.lblRationale,
            this.KeyInfo,
            this.Rationale});
			this.grpFactorIdFooter.Height = 1.38375F;
			this.grpFactorIdFooter.Name = "grpFactorIdFooter";
			this.grpFactorIdFooter.Format += new System.EventHandler(this.grpFactorIdFooter_Format);
			// 
			// Criteria2
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.Sections.Add(this.grpFactorType);
			this.Sections.Add(this.grpSpecies);
			this.Sections.Add(this.grpFactorId);
			this.Sections.Add(this.grpReference);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.grpReferenceFooter);
			this.Sections.Add(this.grpFactorIdFooter);
			this.Sections.Add(this.grpSpeciesFooter);
			this.Sections.Add(this.grpFactorTypeFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRationale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SpeciesLocationMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox textBox2;
		private DataDynamics.ActiveReports.TextBox textBox3;
		private DataDynamics.ActiveReports.GroupHeader grpReference;
		private DataDynamics.ActiveReports.Line line1;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.GroupFooter grpReferenceFooter;
		private DataDynamics.ActiveReports.TextBox ScoreDescription;
		private DataDynamics.ActiveReports.TextBox ScoreNum;
		private DataDynamics.ActiveReports.GroupHeader grpFactorType;
		private DataDynamics.ActiveReports.TextBox FactorType;
		private DataDynamics.ActiveReports.GroupFooter grpFactorTypeFooter;
		private DataDynamics.ActiveReports.Label lblRationale;
		private DataDynamics.ActiveReports.RichTextBox KeyInfo;
		private DataDynamics.ActiveReports.RichTextBox Rationale;
		private DataDynamics.ActiveReports.GroupHeader grpSpecies;
		private DataDynamics.ActiveReports.TextBox SpeciesLocationMethod;
		private DataDynamics.ActiveReports.GroupFooter grpSpeciesFooter;
		private DataDynamics.ActiveReports.GroupHeader grpFactorId;
		private DataDynamics.ActiveReports.GroupFooter grpFactorIdFooter;
		private DataDynamics.ActiveReports.Picture picture1;

	}
}
