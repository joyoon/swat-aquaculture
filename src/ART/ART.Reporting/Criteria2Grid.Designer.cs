namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	partial class Criteria2Grid
	{
		private DataDynamics.ActiveReports.Detail detail;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criteria2Grid));
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.CommonName = new DataDynamics.ActiveReports.TextBox();
			this.Factor1 = new DataDynamics.ActiveReports.TextBox();
			this.Factor2 = new DataDynamics.ActiveReports.TextBox();
			this.Factor3 = new DataDynamics.ActiveReports.TextBox();
			this.SubScore = new DataDynamics.ActiveReports.TextBox();
			this.grpSpecies = new DataDynamics.ActiveReports.GroupHeader();
			this.picture1 = new DataDynamics.ActiveReports.Picture();
			this.LocationMethod = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.label3 = new DataDynamics.ActiveReports.Label();
			this.label4 = new DataDynamics.ActiveReports.Label();
			this.label5 = new DataDynamics.ActiveReports.Label();
			this.label6 = new DataDynamics.ActiveReports.Label();
			this.label2 = new DataDynamics.ActiveReports.Label();
			this.label7 = new DataDynamics.ActiveReports.Label();
			this.label8 = new DataDynamics.ActiveReports.Label();
			this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
			this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
			this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
			this.lblSynthesis = new DataDynamics.ActiveReports.Label();
			this.Synthesis = new DataDynamics.ActiveReports.RichTextBox();
			this.textBox1 = new DataDynamics.ActiveReports.TextBox();
			this.textBox2 = new DataDynamics.ActiveReports.TextBox();
			this.textBox3 = new DataDynamics.ActiveReports.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.CommonName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubScore)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSynthesis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.CommonName,
            this.Factor1,
            this.Factor2,
            this.Factor3,
            this.SubScore});
			this.detail.Height = 0.45525F;
			this.detail.Name = "detail";
			this.detail.Format += new System.EventHandler(this.detail_Format);
			// 
			// CommonName
			// 
			this.CommonName.DataField = "CommonName";
			this.CommonName.Height = 0.462F;
			this.CommonName.Left = -1.776357E-15F;
			this.CommonName.Name = "CommonName";
			this.CommonName.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.CommonName.Text = "CommonName";
			this.CommonName.Top = 0F;
			this.CommonName.Width = 2.935F;
			// 
			// Factor1
			// 
			this.Factor1.DataField = "=Factor1Score + \":\" + Factor1ScoreDescription";
			this.Factor1.Height = 0.462F;
			this.Factor1.Left = 2.935F;
			this.Factor1.Name = "Factor1";
			this.Factor1.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor1.Text = "Factor1";
			this.Factor1.Top = 0F;
			this.Factor1.Width = 1.165001F;
			// 
			// Factor2
			// 
			this.Factor2.DataField = "=Factor2Score + \":\" + Factor2ScoreDescription";
			this.Factor2.Height = 0.462F;
			this.Factor2.Left = 4.1F;
			this.Factor2.Name = "Factor2";
			this.Factor2.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor2.Text = "Factor2";
			this.Factor2.Top = 0F;
			this.Factor2.Width = 1.165F;
			// 
			// Factor3
			// 
			this.Factor3.DataField = "=Factor3Score + \":\" + Factor3ScoreDescription";
			this.Factor3.Height = 0.462F;
			this.Factor3.Left = 5.265F;
			this.Factor3.Name = "Factor3";
			this.Factor3.Style = "color: #666666; font-size: 10pt; font-weight: normal; vertical-align: middle";
			this.Factor3.Text = "Factor3";
			this.Factor3.Top = 0F;
			this.Factor3.Width = 1.165F;
			// 
			// SubScore
			// 
			this.SubScore.DataField = "SubScore";
			this.SubScore.Height = 0.462F;
			this.SubScore.Left = 6.43F;
			this.SubScore.Name = "SubScore";
			this.SubScore.Style = "color: #666666; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.SubScore.Text = "SubScore";
			this.SubScore.Top = 0F;
			this.SubScore.Width = 1.034F;
			// 
			// grpSpecies
			// 
			this.grpSpecies.BackColor = System.Drawing.Color.Empty;
			this.grpSpecies.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picture1,
            this.LocationMethod,
            this.label1,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label2,
            this.label7,
            this.label8,
            this.textBox1,
            this.textBox2,
            this.textBox3});
			this.grpSpecies.DataField = "LocationMethod";
			this.grpSpecies.Height = 1.207583F;
			this.grpSpecies.Name = "grpSpecies";
			// 
			// picture1
			// 
			this.picture1.Height = 0.489F;
			this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
			this.picture1.Left = 0F;
			this.picture1.Name = "picture1";
			this.picture1.Top = 0F;
			this.picture1.Width = 7.5F;
			// 
			// LocationMethod
			// 
			this.LocationMethod.DataField = "LocationMethod";
			this.LocationMethod.Height = 0.304F;
			this.LocationMethod.Left = 0.02F;
			this.LocationMethod.MultiLine = false;
			this.LocationMethod.Name = "LocationMethod";
			this.LocationMethod.Style = "font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.LocationMethod.Text = "LocationMethod";
			this.LocationMethod.Top = 0.09200001F;
			this.LocationMethod.Width = 7.373F;
			// 
			// label1
			// 
			this.label1.Height = 0.3239999F;
			this.label1.HyperLink = null;
			this.label1.Left = 0F;
			this.label1.Name = "label1";
			this.label1.Style = "background-color: #DFE8E7; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label1.Text = "Species";
			this.label1.Top = 0.8940001F;
			this.label1.Width = 2.935F;
			// 
			// label3
			// 
			this.label3.Height = 0.3239999F;
			this.label3.HyperLink = null;
			this.label3.Left = 2.935F;
			this.label3.Name = "label3";
			this.label3.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label3.Text = "Inherent Vulnerability";
			this.label3.Top = 0.895F;
			this.label3.Width = 1.165F;
			// 
			// label4
			// 
			this.label4.Height = 0.3239999F;
			this.label4.HyperLink = null;
			this.label4.Left = 4.1F;
			this.label4.Name = "label4";
			this.label4.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label4.Text = "Stock Status";
			this.label4.Top = 0.895F;
			this.label4.Width = 1.165F;
			// 
			// label5
			// 
			this.label5.Height = 0.3239999F;
			this.label5.HyperLink = null;
			this.label5.Left = 5.265F;
			this.label5.Name = "label5";
			this.label5.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label5.Text = "Fishing Mortality";
			this.label5.Top = 0.895F;
			this.label5.Width = 1.165F;
			// 
			// label6
			// 
			this.label6.Height = 0.3239999F;
			this.label6.HyperLink = null;
			this.label6.Left = 6.430001F;
			this.label6.Name = "label6";
			this.label6.Style = "background-color: #E9EFEF; color: #6D808E; font-size: 10pt; font-weight: bold; ve" +
				"rtical-align: middle";
			this.label6.Text = "Subscore";
			this.label6.Top = 0.895F;
			this.label6.Width = 1.034F;
			// 
			// label2
			// 
			this.label2.Height = 0.3239999F;
			this.label2.HyperLink = null;
			this.label2.Left = 0.02F;
			this.label2.Name = "label2";
			this.label2.Style = "color: #6D808E; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.label2.Text = "Subscore:";
			this.label2.Top = 0.5700001F;
			this.label2.Width = 0.779F;
			// 
			// label7
			// 
			this.label7.Height = 0.3239999F;
			this.label7.HyperLink = null;
			this.label7.Left = 1.666F;
			this.label7.Name = "label7";
			this.label7.Style = "color: #6D808E; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.label7.Text = "Discard Rate:";
			this.label7.Top = 0.5700001F;
			this.label7.Width = 0.9460001F;
			// 
			// label8
			// 
			this.label8.Height = 0.3239999F;
			this.label8.HyperLink = null;
			this.label8.Left = 3.54F;
			this.label8.Name = "label8";
			this.label8.Style = "color: #6D808E; font-size: 10pt; font-weight: bold; vertical-align: middle";
			this.label8.Text = "C2 Score:";
			this.label8.Top = 0.5700001F;
			this.label8.Width = 0.7479999F;
			// 
			// groupFooter1
			// 
			this.groupFooter1.Height = 0.1354167F;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// reportHeader1
			// 
			this.reportHeader1.Height = 0F;
			this.reportHeader1.Name = "reportHeader1";
			// 
			// reportFooter1
			// 
			this.reportFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblSynthesis,
            this.Synthesis});
			this.reportFooter1.Height = 0.6785001F;
			this.reportFooter1.Name = "reportFooter1";
			this.reportFooter1.Format += new System.EventHandler(this.reportFooter1_Format);
			// 
			// lblSynthesis
			// 
			this.lblSynthesis.Height = 0.2703333F;
			this.lblSynthesis.HyperLink = null;
			this.lblSynthesis.Left = 0F;
			this.lblSynthesis.Name = "lblSynthesis";
			this.lblSynthesis.Style = "color: #005A74; font-size: 15.75pt; font-weight: bold; ddo-char-set: 0";
			this.lblSynthesis.Text = "Synthesis";
			this.lblSynthesis.Top = 0.148F;
			this.lblSynthesis.Width = 2.871F;
			// 
			// Synthesis
			// 
			this.Synthesis.AutoReplaceFields = true;
			this.Synthesis.Font = new System.Drawing.Font("Arial", 10F);
			this.Synthesis.Height = 0.24F;
			this.Synthesis.Left = 0F;
			this.Synthesis.Name = "Synthesis";
			this.Synthesis.RTF = resources.GetString("Synthesis.RTF");
			this.Synthesis.Top = 0.418F;
			this.Synthesis.Width = 7.448F;
			// 
			// textBox1
			// 
			this.textBox1.DataField = "HeaderSubscore";
			this.textBox1.Height = 0.21F;
			this.textBox1.Left = 0.799F;
			this.textBox1.Name = "textBox1";
			this.textBox1.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; te" +
				"xt-align: center; text-justify: auto; vertical-align: middle";
			this.textBox1.Text = "HeaderSubscore";
			this.textBox1.Top = 0.622F;
			this.textBox1.Width = 0.5090003F;
			// 
			// textBox2
			// 
			this.textBox2.DataField = "HeaderDiscardRate";
			this.textBox2.Height = 0.21F;
			this.textBox2.Left = 2.612F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; te" +
				"xt-align: center; text-justify: auto; vertical-align: middle";
			this.textBox2.Text = "HeaderDiscardRate";
			this.textBox2.Top = 0.623F;
			this.textBox2.Width = 0.5090003F;
			// 
			// textBox3
			// 
			this.textBox3.DataField = "HeaderScore";
			this.textBox3.Height = 0.21F;
			this.textBox3.Left = 4.288F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "background-color: #F5F6F8; color: #666666; font-size: 10pt; font-weight: bold; te" +
				"xt-align: center; text-justify: auto; vertical-align: middle";
			this.textBox3.Text = "HeaderScore";
			this.textBox3.Top = 0.622F;
			this.textBox3.Width = 0.5090003F;
			// 
			// Criteria2Grid
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.511F;
			this.Sections.Add(this.reportHeader1);
			this.Sections.Add(this.grpSpecies);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.reportFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
						"l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
						"lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.CommonName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Factor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubScore)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.LocationMethod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSynthesis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.TextBox CommonName;
		private DataDynamics.ActiveReports.GroupHeader grpSpecies;
		private DataDynamics.ActiveReports.TextBox LocationMethod;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.Label label4;
		private DataDynamics.ActiveReports.Label label5;
		private DataDynamics.ActiveReports.Label label6;
		private DataDynamics.ActiveReports.GroupFooter groupFooter1;
		private DataDynamics.ActiveReports.TextBox Factor1;
		private DataDynamics.ActiveReports.TextBox Factor2;
		private DataDynamics.ActiveReports.TextBox Factor3;
		private DataDynamics.ActiveReports.TextBox SubScore;
		private DataDynamics.ActiveReports.ReportHeader reportHeader1;
		private DataDynamics.ActiveReports.ReportFooter reportFooter1;
		private DataDynamics.ActiveReports.Label lblSynthesis;
		private DataDynamics.ActiveReports.RichTextBox Synthesis;
		private DataDynamics.ActiveReports.Picture picture1;
		private DataDynamics.ActiveReports.Label label3;
		private DataDynamics.ActiveReports.Label label2;
		private DataDynamics.ActiveReports.Label label7;
		private DataDynamics.ActiveReports.Label label8;
		private DataDynamics.ActiveReports.TextBox textBox1;
		private DataDynamics.ActiveReports.TextBox textBox2;
		private DataDynamics.ActiveReports.TextBox textBox3;
	}
}
