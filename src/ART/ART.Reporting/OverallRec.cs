using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for OverallRec.
	/// </summary>
	public partial class OverallRec : DataDynamics.ActiveReports.ActiveReport
	{

		public OverallRec()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			Utility u = new Utility();
			u.ColorFormat(Color1);
			u.ColorFormat(Color2);
			u.ColorFormat(Color3);
			u.ColorFormat(Color4);
			u.ColorFormat(Color);
		}
	}
}
