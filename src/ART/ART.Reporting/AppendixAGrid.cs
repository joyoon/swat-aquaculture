using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace ART.Reporting
{
	/// <summary>
	/// Summary description for Criteria1Grid.
	/// </summary>
	public partial class AppendixAGrid : DataDynamics.ActiveReports.ActiveReport
	{

		public AppendixAGrid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void detail_Format(object sender, EventArgs e)
		{
			new Utility().ColorFormat(Color);
		}
	}
}
