namespace SFWR.Reporting
{
	/// <summary>
	/// Summary description for NewActiveReport1.
	/// </summary>
	partial class NewActiveReport1
	{
		private DataDynamics.ActiveReports.PageHeader pageHeader;
		private DataDynamics.ActiveReports.Detail detail;
		private DataDynamics.ActiveReports.PageFooter pageFooter;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(NewActiveReport1));
			this.pageHeader = new DataDynamics.ActiveReports.PageHeader();
			this.detail = new DataDynamics.ActiveReports.Detail();
			this.pageFooter = new DataDynamics.ActiveReports.PageFooter();
			this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
			this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
			this.Factor = new DataDynamics.ActiveReports.TextBox();
			this.groupHeader2 = new DataDynamics.ActiveReports.GroupHeader();
			this.groupFooter2 = new DataDynamics.ActiveReports.GroupFooter();
			this.FactorType = new DataDynamics.ActiveReports.TextBox();
			this.groupHeader3 = new DataDynamics.ActiveReports.GroupHeader();
			this.groupFooter3 = new DataDynamics.ActiveReports.GroupFooter();
			this.SpeciesAndLocation = new DataDynamics.ActiveReports.TextBox();
			this.ScoreNumber = new DataDynamics.ActiveReports.TextBox();
			this.ScoreDescription = new DataDynamics.ActiveReports.TextBox();
			this.label1 = new DataDynamics.ActiveReports.Label();
			this.KeyInfo = new DataDynamics.ActiveReports.TextBox();
			this.label2 = new DataDynamics.ActiveReports.Label();
			this.Rationale = new DataDynamics.ActiveReports.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Factor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SpeciesAndLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.KeyInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Rationale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeader
			// 
			this.pageHeader.Height = 0F;
			this.pageHeader.Name = "pageHeader";
			// 
			// detail
			// 
			this.detail.ColumnSpacing = 0F;
			this.detail.Height = 0F;
			this.detail.Name = "detail";
			// 
			// pageFooter
			// 
			this.pageFooter.Height = 0F;
			this.pageFooter.Name = "pageFooter";
			// 
			// groupHeader1
			// 
			this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Factor});
			this.groupHeader1.Height = 0.2605833F;
			this.groupHeader1.Name = "groupHeader1";
			// 
			// groupFooter1
			// 
			this.groupFooter1.Height = 0F;
			this.groupFooter1.Name = "groupFooter1";
			// 
			// Factor
			// 
			this.Factor.Height = 0.2291667F;
			this.Factor.Left = 0F;
			this.Factor.Name = "Factor";
			this.Factor.Text = "Factor";
			this.Factor.Top = 0.021F;
			this.Factor.Width = 5.124F;
			// 
			// groupHeader2
			// 
			this.groupHeader2.BackColor = System.Drawing.Color.DarkGray;
			this.groupHeader2.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.FactorType});
			this.groupHeader2.Height = 0.2291667F;
			this.groupHeader2.Name = "groupHeader2";
			// 
			// groupFooter2
			// 
			this.groupFooter2.Height = 0F;
			this.groupFooter2.Name = "groupFooter2";
			// 
			// FactorType
			// 
			this.FactorType.Height = 0.2291667F;
			this.FactorType.Left = 0.25F;
			this.FactorType.Name = "FactorType";
			this.FactorType.Style = "color: White; font-size: 9.75pt; font-weight: bold; ddo-char-set: 0";
			this.FactorType.Text = "FactorType";
			this.FactorType.Top = 0F;
			this.FactorType.Width = 3.885F;
			// 
			// groupHeader3
			// 
			this.groupHeader3.BackColor = System.Drawing.Color.Gainsboro;
			this.groupHeader3.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.SpeciesAndLocation,
            this.ScoreNumber,
            this.ScoreDescription,
            this.label1,
            this.KeyInfo,
            this.label2,
            this.Rationale});
			this.groupHeader3.Height = 1.604167F;
			this.groupHeader3.Name = "groupHeader3";
			// 
			// groupFooter3
			// 
			this.groupFooter3.Height = 0F;
			this.groupFooter3.Name = "groupFooter3";
			// 
			// SpeciesAndLocation
			// 
			this.SpeciesAndLocation.Height = 0.2291667F;
			this.SpeciesAndLocation.Left = 0.302F;
			this.SpeciesAndLocation.Name = "SpeciesAndLocation";
			this.SpeciesAndLocation.Text = "SpeciesAndLocation";
			this.SpeciesAndLocation.Top = 0F;
			this.SpeciesAndLocation.Width = 5.916F;
			// 
			// ScoreNumber
			// 
			this.ScoreNumber.Height = 0.2395833F;
			this.ScoreNumber.Left = 0.375F;
			this.ScoreNumber.Name = "ScoreNumber";
			this.ScoreNumber.Style = "background-color: DimGray; color: White";
			this.ScoreNumber.Text = "ScoreNumber";
			this.ScoreNumber.Top = 0.2916667F;
			this.ScoreNumber.Width = 0.302F;
			// 
			// ScoreDescription
			// 
			this.ScoreDescription.Height = 0.2395833F;
			this.ScoreDescription.Left = 0.677F;
			this.ScoreDescription.Name = "ScoreDescription";
			this.ScoreDescription.Style = "background-color: White; font-size: 9.75pt; font-weight: bold; ddo-char-set: 0";
			this.ScoreDescription.Text = "ScoreDescription";
			this.ScoreDescription.Top = 0.292F;
			this.ScoreDescription.Width = 1.125F;
			// 
			// label1
			// 
			this.label1.Height = 0.1875F;
			this.label1.HyperLink = null;
			this.label1.Left = 0.3958333F;
			this.label1.Name = "label1";
			this.label1.Style = "font-size: 9.75pt; font-weight: bold; ddo-char-set: 0";
			this.label1.Text = "Key Relevant Info:";
			this.label1.Top = 0.59375F;
			this.label1.Width = 1.40625F;
			// 
			// KeyInfo
			// 
			this.KeyInfo.Height = 0.21875F;
			this.KeyInfo.Left = 0.3854167F;
			this.KeyInfo.Name = "KeyInfo";
			this.KeyInfo.Text = "KeyInfo";
			this.KeyInfo.Top = 0.84375F;
			this.KeyInfo.Width = 6.114584F;
			// 
			// label2
			// 
			this.label2.Height = 0.1875F;
			this.label2.HyperLink = null;
			this.label2.Left = 0.3958333F;
			this.label2.Name = "label2";
			this.label2.Style = "font-size: 9.75pt; font-weight: bold; ddo-char-set: 0";
			this.label2.Text = "Detail Rationale:";
			this.label2.Top = 1.125F;
			this.label2.Width = 1.40625F;
			// 
			// Rationale
			// 
			this.Rationale.Height = 0.21875F;
			this.Rationale.Left = 0.3854167F;
			this.Rationale.Name = "Rationale";
			this.Rationale.Text = "Rationale";
			this.Rationale.Top = 1.375F;
			this.Rationale.Width = 6.114584F;
			// 
			// NewActiveReport1
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.Sections.Add(this.pageHeader);
			this.Sections.Add(this.groupHeader1);
			this.Sections.Add(this.groupHeader2);
			this.Sections.Add(this.groupHeader3);
			this.Sections.Add(this.detail);
			this.Sections.Add(this.groupFooter3);
			this.Sections.Add(this.groupFooter2);
			this.Sections.Add(this.groupFooter1);
			this.Sections.Add(this.pageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Factor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FactorType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SpeciesAndLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ScoreDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.KeyInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Rationale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private DataDynamics.ActiveReports.GroupHeader groupHeader1;
		private DataDynamics.ActiveReports.TextBox Factor;
		private DataDynamics.ActiveReports.GroupFooter groupFooter1;
		private DataDynamics.ActiveReports.GroupHeader groupHeader2;
		private DataDynamics.ActiveReports.TextBox FactorType;
		private DataDynamics.ActiveReports.GroupFooter groupFooter2;
		private DataDynamics.ActiveReports.GroupHeader groupHeader3;
		private DataDynamics.ActiveReports.TextBox SpeciesAndLocation;
		private DataDynamics.ActiveReports.TextBox ScoreNumber;
		private DataDynamics.ActiveReports.TextBox ScoreDescription;
		private DataDynamics.ActiveReports.Label label1;
		private DataDynamics.ActiveReports.TextBox KeyInfo;
		private DataDynamics.ActiveReports.Label label2;
		private DataDynamics.ActiveReports.TextBox Rationale;
		private DataDynamics.ActiveReports.GroupFooter groupFooter3;

	}
}
