var fs = require("fs"),
    files = [];
console.log("\nStarting the process\n");

if (process.argv.length < 4)
    throw "Invalid call, must contain the root folder to search"
/*Usage compile.less.js parseFolder compiler */
fs.readdir(process.argv[2], function (err, files) {
    if (err) throw err;
    files.forEach(function (file) {
        if (file.substring(file.length - 5) === ".less" && file.substring(0, 1) !== "_")
            compileFile(process.argv[3], process.argv[2], file)
    });
});



function compileFile(compiler, rootPath, file) {
    function fixPath(rootPath, file) {
        if (rootPath.substring(rootPath.length - 1) === "\\")
            return rootPath + file;
        else
            return rootPath + "\\" + file;
    }
    function translateToCss(path) {
        return path.replace(".less", ".css");
    }

    var path = fixPath(rootPath, file);
    var css = translateToCss(path);


    execProcess(compiler, path)
}

function execProcess(compiler, file) {
    var util = require('util')
    var exec = require('child_process').exec;
    var child;

    var command = compiler + ' "' + file + '"';
    console.log(command);
    child = exec(command, function (error, stdout, stderr) {
        if(stdout)
            util.print('stdout: ' + stdout);
        if( stderr)
            util.print('stderr: ' + stderr);
        if (error !== null) {
            console.log('exec error: ' + error);
        }
    });


}