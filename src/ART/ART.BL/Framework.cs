﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Data;

namespace ART
{
	public class Framework
	{
		public static void EntityPopulate(Object entity, DataRow dr)
		{
			if (entity == null)
				return;
			Type t = entity.GetType();
			PropertyInfo pi;
			for (int i = 0; i < dr.Table.Columns.Count; i++)
			{
				pi = t.GetProperty(dr.Table.Columns[i].ColumnName);
				if (pi == null) //field doesn't exist in class.
					continue;
				else if (!IsDBNull(dr[i]) && dr[i].ToString().Length > 0) //field exists and has value
					pi.SetValue(entity, System.Convert.ChangeType(dr[i], getUnderlyingType(pi.PropertyType)), null);
			}
		}

		public static bool IsDBNull(object obj)
		{
			if (obj == DBNull.Value)
				return true;
			else
				return false;
		}

		/// <summary>
		/// If type is nullable, returns underlying type.  If not, returns passed type.
		/// </summary>
		/// <param name="T"></param>
		/// <returns></returns>
		private static Type getUnderlyingType(Type T)
		{
			if (T.IsGenericType && T.GetGenericTypeDefinition() == typeof(Nullable<>))
				return Nullable.GetUnderlyingType(T);
			else
				return T;
		}
	}
}