﻿using System;
using System.Collections.Generic;
using System.Data;
using EntityFramework.MSSQL;

namespace ART
{
	public class ReportSource : BaseBL
	{
		public ReportSource()
			: base()
		{ }

		/// <summary>
		/// Returns a list of datatables that you can feed to the report tool
		/// </summary>
		/// <param name="reportId"></param>
		/// <returns></returns>
		public DataTable Criteria1All(int reportId) //Species, Factor, LocationMethod
		{
			DataTable dt = getCriteria(reportId, 1); //Hard coded Criteria 1
			return dt;
		}

		/// <summary>
		/// Returns a list of datatables that you can feed to the report tool
		/// </summary>
		/// <param name="reportId"></param>
		/// <returns></returns>
		public IList<DataTable> Criteria1(int reportId) //Species, Factor, LocationMethod
		{
			DataTable dt = this.Criteria1All(reportId);

			//Hide subsequently repeated fields.  Uses event on report that hides group when value is missing.
			dt.Columns["Species"].ReadOnly = false;
			string species = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["Species"].ToString() == species)
					dr["Species"] = "";
				else
					species = dr["Species"].ToString();
			}

			dt.Columns["FactorType"].ReadOnly = false;
			string factorType = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["Species"].ToString().Length > 0) //Keep factor type if Species changes (probably not an issue in production, but...)
				{ }
				else if (dr["FactorType"].ToString() == factorType)
					dr["FactorType"] = "";
				else
					factorType = dr["FactorType"].ToString();
			}

			//Separate on Factor
			IList<DataTable> dts = new List<DataTable>();
			DataTable dtTemp = dt.Clone();

			string factorId = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["FactorId"].ToString() == factorId)
				{
					dtTemp.Rows.Add(dr.ItemArray);
				}
				else if (dtTemp.Rows.Count == 0)
				{
					dtTemp.Rows.Add(dr.ItemArray);
					factorId = dr["FactorId"].ToString();
				}
				else
				{
					//Add datatable to collection, reset factorId
					dts.Add(dtTemp.Copy());
					dtTemp.Clear(); //TODO: will this cause byref problems?
					factorId = dr["FactorId"].ToString();
					dtTemp.Rows.Add(dr.ItemArray);
				}
			}
			dts.Add(dtTemp); //Add last record
			return dts;
		}

		public DataTable Criteria2All(int reportId) //Factor, Species
		{
			DataTable dt = getCriteria(reportId, 2); //Hard coded Criteria 2
			return dt;
		}

		public IList<DataTable> Criteria2(int reportId) //Factor, Species
		{
			DataTable dt = Criteria2All(reportId);

			//Hide subsequently repeated fields.  Uses event on report that hides group when value is missing.
			dt.Columns["FactorType"].ReadOnly = false;
			string factorType = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["FactorType"].ToString() == factorType)
					dr["FactorType"] = "";
				else
					factorType = dr["FactorType"].ToString();
			}

			//Separate on Species
			IList<DataTable> dts = new List<DataTable>();
			DataTable dtTemp = dt.Clone();

			string species = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["Species"].ToString() == species)
				{
					dtTemp.Rows.Add(dr.ItemArray);
				}
				else if (dtTemp.Rows.Count == 0)
				{
					dtTemp.Rows.Add(dr.ItemArray);
					species = dr["Species"].ToString();
				}
				else
				{
					//Add datatable to collection, reset factorId
					dts.Add(dtTemp.Copy());
					dtTemp.Clear(); //TODO: will this cause byref problems?
					species = dr["Species"].ToString();
					dtTemp.Rows.Add(dr.ItemArray);
				}
			}
			dts.Add(dtTemp); //Add last record
			return dts;
		}

		public IList<DataTable> Criteria3(int reportId)
		{
			return criteria3And4(reportId, 3);
		}

		public IList<DataTable> Criteria31(int reportId)
		{
			return criteria3And4(reportId, 5);
		}

		public IList<DataTable> Criteria32(int reportId)
		{
			return criteria3And4(reportId, 6);
		}

		public DataTable Criteria31All(int reportId)
		{
			return criteria3And4All(reportId, 5);
		}

		public DataTable Criteria32All(int reportId)
		{
			return criteria3And4All(reportId, 6);
		}

		public IList<DataTable> Criteria4(int reportId) //Factor, LocationMethod
		{
			return criteria3And4(reportId, 4);
		}

		public DataTable Criteria4All(int reportId)
		{
			return criteria3And4All(reportId, 4);
		}

		private DataTable criteria3And4All(int reportId, int factorId)
		{
			DataTable dt = getCriteria(reportId, factorId); //FactorId should only be (5 or 6 for 3) or 4
			return dt;
		}

		private IList<DataTable> criteria3And4(int reportId, int factorId)
		{
			DataTable dt = criteria3And4All(reportId, factorId);

			//Hide subsequently repeated fields.  Uses event on report that hides group when value is missing.
			dt.Columns["FactorType"].ReadOnly = false;
			string factorType = "";
			foreach (DataRow dr in dt.Rows)
			{
			    if (dr["FactorType"].ToString() == factorType)
			        dr["FactorType"] = "";
			    else
			        factorType = dr["FactorType"].ToString();
			}

			//Separate on LocationMethod
			IList<DataTable> dts = new List<DataTable>();
			DataTable dtTemp = dt.Clone();

			string locationMethod = "";
			foreach (DataRow dr in dt.Rows)
			{
				if (dr["LocationMethod"].ToString() == locationMethod)
				{
					dtTemp.Rows.Add(dr.ItemArray);
				}
				else if (dtTemp.Rows.Count == 0)
				{
					dtTemp.Rows.Add(dr.ItemArray);
					locationMethod = dr["LocationMethod"].ToString();
				}
				else
				{
					//Add datatable to collection, reset factorId
					dts.Add(dtTemp.Copy());
					dtTemp.Clear(); //TODO: will this cause byref problems?
					locationMethod = dr["LocationMethod"].ToString();
					dtTemp.Rows.Add(dr.ItemArray);
				}
			}
			dts.Add(dtTemp); //Add last record
			return dts;
		}

		private DataTable getCriteria(int reportId, int criteriaId)
		{
			string str = @"Select Distinct case when @1 = 2 then Replace(ft.Prefix,'1.','2.') else ft.prefix end + ' - ' + ft.Description as FactorType, 
isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,  
f.Id as FactorId, fs.Number as ScoreNum, ' ' + fs.Description as ScoreDescription,
cast(f.KeyInfo as varchar(max)) as KeyInfo, cast(f.Rationale as varchar(max)) as Rationale, 'test' as Reference, ft.SortOrder";
			if ( criteriaId == 1 || criteriaId == 2) str += ", Upper(s.CommonName) as Species"; //Don't include species in crit 3 or 4
			str+= @" FROM Report rpt 
inner join Recommendation rec on rec.ReportId = rpt.Id
inner join Species s on s.Id = rec.SpeciesId 
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
Inner join FactorRecommendationDetail frd on frd.RecommendationId = rec.Id
Inner join Factor f on f.Id = frd.FactorId
Inner join FactorScore fs on fs.Id = f.FactorScoreId
inner join FactorType ft on ft.Id = f.FactorTypeId
Where rec.ReportId = @0 ";
			if (criteriaId == 1)
				str += "and ft.CriteriaId = @1 and rec.RecommendationTypeId = 4 "; //4 = "yes" = not bycatch
			else if (criteriaId == 2)
				str += "and ft.CriteriaId in (1,2) and rec.RecommendationTypeId = 5 "; //5 = "no" = bycatch
			else
				str += "and ft.CriteriaId = @1 ";

			switch(criteriaId)
			{
				case 1:
					str += "ORDER BY upper(s.CommonName), ft.SortOrder, f.Id"; //, isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method"; 
					break;
				case 2:
					str += "ORDER BY upper(s.CommonName), ft.SortOrder";
					break;
				case 3:
				case 4:
					str += "ORDER BY ft.SortOrder, isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method";
					break;
			}

				return DB.GetDataTable(new SQLBuilder(str, reportId.ToString(), criteriaId.ToString()));
		}

		public DataTable OverallRec(int reportId)
		{
			return DB.GetDataTable(new SQLBuilder(@"select rec.Id as RecommendationId, s.CommonName + ', ' + ltrim(rtrim(l.Description + ' ' + bow.Description)) + ', ' + m.Method as Recommendation,
s.CommonName, ltrim(rtrim(l.Description + ' ' + bow.Description)) as Region, m.Method, 
Convert(Decimal(5,2),rsd1.Score) as Score1, c1.Color as Color1, Convert(Decimal(5,2),rsd2.Score) as Score2, c2.Color as Color2,
Convert(Decimal(5,2),rsd3.Score) as Score3, c3.Color as Color3, Convert(Decimal(5,2),rsd4.Score) as Score4, c4.Color as Color4,
rec.Score as Score, c.Color as Color, choose(rec.ColorId, 'Best Choice', 'Good Alternative', 'Avoid') as Rec,
case when rsd1.ColorId = 3 then 1 else 0 end + case when rsd2.ColorId = 3 then 1 else 0 end + 
case when rsd3.ColorId = 3 then 1 else 0 end + case when rsd4.ColorId = 3 then 1 else 0 end + 
case when rsd31.ColorId = 3 then 1 else 0 end + case when rsd32.ColorId = 3 then 1 else 0 end as RedCount,
Case when (
case when rsd1.ColorId = 6 then 1 else 0 end + case when rsd2.ColorId = 6 then 1 else 0 end + 
case when rsd3.ColorId = 6 then 1 else 0 end + case when rsd4.ColorId = 6 then 1 else 0 end + 
case when rsd31.ColorId = 6 then 1 else 0 end + case when rsd32.ColorId = 6 then 1 else 0 end) > 0 then 'Yes' else 'No' end as Criticals
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join Color c on c.Id = rec.ColorId
	left join RecommendationScoreDetail rsd1 on rsd1.RecommendationId = rec.Id and rsd1.criteriaId = 1 left join Color c1 on c1.Id = rsd1.ColorId
	left join RecommendationScoreDetail rsd2 on rsd2.RecommendationId = rec.Id and rsd2.criteriaId = 2 left join Color c2 on c2.Id = rsd2.ColorId
	left join RecommendationScoreDetail rsd3 on rsd3.RecommendationId = rec.Id and rsd3.criteriaId = 3 left join Color c3 on c3.Id = rsd3.ColorId
	left join RecommendationScoreDetail rsd4 on rsd4.RecommendationId = rec.Id and rsd4.criteriaId = 4 left join Color c4 on c4.Id = rsd4.ColorId
	left join RecommendationScoreDetail rsd31 on rsd31.RecommendationId = rec.Id and rsd31.criteriaId = 5 left join Color c31 on c31.Id = rsd31.ColorId
	left join RecommendationScoreDetail rsd32 on rsd32.RecommendationId = rec.Id and rsd32.criteriaId = 6 left join Color c32 on c32.Id = rsd32.ColorId
WHERE rec.RecommendationTypeId = 4", reportId.ToString())); //4 = "yes" = not bycatch ",
		}

		public DataTable References(int reportId)
		{
			return DB.GetDataTable(new SQLBuilder("Select Id, Authors + '. ' + Year + '. ' + Bibliography as Reference from Reference where ReportId = @0 order by Authors, Year DESC, Bibliography", reportId.ToString()));
		}

		public Dictionary<string, Object> FullReport(int reportId)
		{
			ARTService service = new ARTService();
			Dictionary<string, Object> dataSources = new Dictionary<string, Object>();

			dataSources.Add("TextBlockExecSummary", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.ExecSummary));
			dataSources.Add("TextBlockScope", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.Scope));
			dataSources.Add("TextBlockSpeciesOverview", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.SpeciesOverview));
			dataSources.Add("TextBlockProductionStatistics", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.ProductionStatistics));
			dataSources.Add("TextBlockImportance", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.ImportanceToMarket));
			dataSources.Add("TextBlockMarketNames", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.CommonMarketNames));

			dataSources.Add("OverallRec", OverallRec(reportId));

			dataSources.Add("Criteria1Grid", service.Criteria1ReportData(reportId));
			dataSources.Add("Criteria1Synthesis", service.SynthesisGet(reportId, 1));
			dataSources.Add("Criteria1", Criteria1All(reportId));

			dataSources.Add("Criteria2Grid", service.Criteria2ReportData(reportId));
			dataSources.Add("Criteria2Synthesis", service.SynthesisGet(reportId, 2));
			dataSources.Add("Criteria2", Criteria2All(reportId));

			dataSources.Add("Criteria3Grid", service.Criteria3ReportData(reportId));
			dataSources.Add("Criteria31Grid", service.Criteria31ReportData(reportId));
			dataSources.Add("Criteria32Grid", service.Criteria32ReportData(reportId));
			dataSources.Add("Criteria3Synthesis", service.SynthesisGet(reportId, 3));
			dataSources.Add("Criteria31Synthesis", service.SynthesisGet(reportId, 5));
			dataSources.Add("Criteria32Synthesis", service.SynthesisGet(reportId, 6));
			dataSources.Add("Criteria31", Criteria31All(reportId));
			dataSources.Add("Criteria32", Criteria32All(reportId));

			dataSources.Add("Criteria4Grid", service.Criteria4ReportData(reportId));
			dataSources.Add("Criteria4Synthesis", service.SynthesisGet(reportId, 4));
			dataSources.Add("Criteria4", Criteria4All(reportId));

			dataSources.Add("TextBlockAck", service.TextBlockGetDataTable(reportId, ARTService.TextBlock.Acknowledgements));
			dataSources.Add("References", References(reportId));

			dataSources.Add("AppendixAGrid", service.AppendixAReportData(reportId));


			List<int> reportAppendixIds = DB.GetList<int>(new SQLBuilder("select Id from ReportAppendix where ReportId = @0 order by Id", reportId.ToString()));
			
			for (int i = 0; i < reportAppendixIds.Count; i++)
			{
				//Add Appendix1 to AppendixN (currently capped at 7)
				dataSources.Add("Appendix" + (i+1).ToString(), service.AppendixGetDataTable(reportAppendixIds[i]));
			}

			return dataSources;
		}
	}
}