﻿using System;
using System.Collections.Generic;
using System.Data;
using EntityFramework.MSSQL;

namespace ART
{
	public class PublicBL:BaseBL
	{
		public PublicBL()
			: base()
		{ }

		public IList<RecSummaryEntity> GetRecSummaries(string filter)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, Species, Scientific, Location, Method from RecSummary where IsSWAT = 1 and (species like @0 or scientific like @0 or location like @0 or Method like @0 or species + '(' + scientific + ')' like @0) order by Species, Location, Method", "%" + filter + "%"));

			IList<RecSummaryEntity> recs = new List<RecSummaryEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RecSummaryEntity rs = new RecSummaryEntity();
				Framework.EntityPopulate(rs, dr);
				recs.Add(rs);
			}
			return recs;
		}

		public RecSummaryEntity GetRecSummary(int recSummaryId)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, Species, Scientific, Location, Method from RecSummary where Id = @0", recSummaryId.ToString()));
			if (dt.Rows.Count == 0) return null;
			RecSummaryEntity rs = new RecSummaryEntity();
			Framework.EntityPopulate(rs, dt.Rows[0]);
			return rs;
		}

		public int FeedbackSubmit(int recSummaryId, string firstName, string lastName, string affiliation, string title, string email, string phone, string comment)
		{
			return DB.InsertCmd(new SQLBuilder("Insert ReportFeedback(DateSubmitted, RecSummaryId, FirstName, LastName, Affiliation, Title, Email, Phone, Comment) values (@0,@1,@2,@3,@4,@5,@6,@7,@8)", DateTime.Now.ToString(), recSummaryId.ToString(), firstName, lastName, affiliation, title, email, phone, comment), true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="reportId">required</param>
		/// <param name="factorId">pass a 0 to get all feedbacks across the report.</param>
		/// <returns></returns>
		public IList<FeedbackEntity> FeedbackGet(int reportId)
		{
			string str = @"Select Distinct rf.Id, DateSubmitted, rf.RecSummaryId, FirstName, LastName, Affiliation, Title, Email, Phone, Cast(Comment as varchar(8000)) as Comment
from ReportFeedback rf inner join recSummaryDetail rsd on rsd.RecSummaryId = rf.RecSummaryId
inner join Recommendation r on r.SpeciesId = rsd.SpeciesId and isnull(r.LocationId,0) = isnull(rsd.LocationId,0)
and isnull(r.BodyOfWaterId,0) = isnull(rsd.BodyOfWaterId,0) and r.MethodId = rsd.MethodId
Where r.ReportId = @0 order by DateSubmitted";
			DataTable dt = DB.GetDataTable(new SQLBuilder(str, reportId.ToString()));
            IList<FeedbackEntity> results = new List<FeedbackEntity>();
            foreach (DataRow dr in dt.Rows) 
            {
                results.Add(PopulateFeedbackDomain(dr));
            }
            return results;
		}

        private FeedbackEntity PopulateFeedbackDomain(DataRow dr)
        {
            FeedbackEntity feedback = new FeedbackEntity();
            feedback.Id = Convert.ToInt32(dr["Id"]);
            feedback.Affiliation = dr["Affiliation"].ToString();
            feedback.Comment = dr["Comment"].ToString();
            feedback.DateSubmitted = Convert.ToDateTime(dr["DateSubmitted"]);
            feedback.Email = dr["Email"].ToString();
            feedback.FirstName = dr["FirstName"].ToString();
            feedback.LastName = dr["LastName"].ToString();
            feedback.Phone = dr["Phone"].ToString();
            feedback.RecSummaryId = Convert.ToInt32(dr["RecSummaryId"]);
            feedback.Title = dr["Title"].ToString();
            return feedback;
        }

        public FeedbackEntity LoadFeedback(int id)
        {
            string str = "Select Id, DateSubmitted, RecSummaryId, FirstName, LastName, Affiliation, Title, Email, Phone, Comment from ReportFeedback " +
                "where Id = @0";
            DataTable dt = DB.GetDataTable(new SQLBuilder(str, id.ToString()));
            return (dt.Rows.Count != 0) ? PopulateFeedbackDomain(dt.Rows[0]) : null;
        }

		public void SubmitFeedbackAttachment(int feedbackId, string path)
		{
			DB.InsertCmd(new SQLBuilder("Insert Attachment(TableId, PrimaryKey, BlobKey) values (1, @0,@1)", feedbackId.ToString(), path));	
		}

		public List<string> GetFeedbackAttachments(int feedbackId)
		{
			return DB.GetList<string>(new SQLBuilder("Select BlobKey from Attachment where TableId = 1 and PrimaryKey = @0", feedbackId.ToString()));
		}

		public void DeleteFeedbackAttachment(int feedbackId, string path)
		{
			DB.DeleteCmd(new SQLBuilder("Delete from Attachment where TableId = 1 and PrimaryKey = @0 and blobKey = @1", feedbackId.ToString(), path));
		}
	}
}