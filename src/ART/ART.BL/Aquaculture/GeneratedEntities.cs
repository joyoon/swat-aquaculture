using System;
using EntityFramework.MSSQL;

namespace ART
{
	public partial class AqC2DL:BaseDL<AqC2Entity>
	{
		public AqC2DL():base()
		{
			m_TableName="AqC2";
			m_PK="Id";
		}
	}

	public partial class AqC2Entity:BaseEntity
	{
		public AqC2Entity ()
		{}

		/// <summary>
		///
		/// </summary>
		public int Id
		{
			get {return m_id;}
			set
			{
				if (m_id == value) return;
				logChange("Id", value.ToString(), nz(m_id,"").ToString());
				m_id=value;
			}
		}
		private int m_id;

		/// <summary>
		///
		/// </summary>
		public int RecommendationId
		{
			get {return m_recommendationid;}
			set
			{
				if (m_recommendationid == value) return;
				logChange("RecommendationId", value.ToString(), nz(m_recommendationid,"").ToString());
				m_recommendationid=value;
			}
		}
		private int m_recommendationid;

		/// <summary>
		///
		/// </summary>
		public Boolean ExternalFeed
		{
			get {return m_externalfeed;}
			set
			{
				logChange("ExternalFeed", value.ToString(), nz(m_externalfeed,"").ToString());
				m_externalfeed=value;
			}
		}
		private Boolean m_externalfeed;

		/// <summary>
		///
		/// </summary>
		public string RapidScore
		{
			get {return m_rapidscore;}
			set
			{
				if (m_rapidscore == value || (m_rapidscore==null && value=="")) return;
				logChange("RapidScore", value.ToString(), nz(m_rapidscore,"").ToString());
				m_rapidscore=value;
			}
		}
		private string m_rapidscore="";

		/// <summary>
		///
		/// </summary>
		public Decimal ProteinContentFeed
		{
			get {return m_proteincontentfeed;}
			set
			{
				if (m_proteincontentfeed == value) return;
				logChange("ProteinContentFeed", value.ToString(), nz(m_proteincontentfeed,"").ToString());
				m_proteincontentfeed=value;
			}
		}
		private Decimal m_proteincontentfeed;

		/// <summary>
		///
		/// </summary>
		public Decimal eFCR
		{
			get {return m_efcr;}
			set
			{
				if (m_efcr == value) return;
				logChange("eFCR", value.ToString(), nz(m_efcr,"").ToString());
				m_efcr=value;
			}
		}
		private Decimal m_efcr;

		/// <summary>
		///
		/// </summary>
		public int FertilizerNInput
		{
			get {return m_fertilizerninput;}
			set
			{
				if (m_fertilizerninput == value) return;
				logChange("FertilizerNInput", value.ToString(), nz(m_fertilizerninput,"").ToString());
				m_fertilizerninput=value;
			}
		}
		private int m_fertilizerninput;

		/// <summary>
		///
		/// </summary>
		public Decimal ProteinContentFish
		{
			get {return m_proteincontentfish;}
			set
			{
				if (m_proteincontentfish == value) return;
				logChange("ProteinContentFish", value.ToString(), nz(m_proteincontentfish,"").ToString());
				m_proteincontentfish=value;
			}
		}
		private Decimal m_proteincontentfish;

		

		/// <summary>
		///
		/// </summary>
		public int ProductionSystem
		{
			get {return m_productionsystem;}
			set
			{
				if (m_productionsystem == value) return;
				logChange("ProductionSystem", value.ToString(), nz(m_productionsystem,"").ToString());
				m_productionsystem=value;
			}
		}
		private int m_productionsystem;

		/// <summary>
		///
		/// </summary>
		public Decimal BasicDischargeScore
		{
			get {return m_basicdischargescore;}
			set
			{
				if (m_basicdischargescore == value) return;
				logChange("BasicDischargeScore", value.ToString(), nz(m_basicdischargescore,"").ToString());
				m_basicdischargescore=value;
			}
		}
		private Decimal m_basicdischargescore;

		/// <summary>
		///
		/// </summary>
		public Decimal BasicDischargeAdjustment
		{
			get {return m_basicdischargeadjustment;}
			set
			{
				if (m_basicdischargeadjustment == value) return;
				logChange("BasicDischargeAdjustment", value.ToString(), nz(m_basicdischargeadjustment,"").ToString());
				m_basicdischargeadjustment=value;
			}
		}
		private Decimal m_basicdischargeadjustment;

		
		/// <summary>
		///
		/// </summary>
		public Decimal Q22a1
		{
			get {return m_q22a1;}
			set
			{
				if (m_q22a1 == value) return;
				logChange("Q22a1", value.ToString(), nz(m_q22a1,"").ToString());
				m_q22a1=value;
			}
		}
		private Decimal m_q22a1;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22a2
		{
			get {return m_q22a2;}
			set
			{
				if (m_q22a2 == value) return;
				logChange("Q22a2", value.ToString(), nz(m_q22a2,"").ToString());
				m_q22a2=value;
			}
		}
		private Decimal m_q22a2;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22a3
		{
			get {return m_q22a3;}
			set
			{
				if (m_q22a3 == value) return;
				logChange("Q22a3", value.ToString(), nz(m_q22a3,"").ToString());
				m_q22a3=value;
			}
		}
		private Decimal m_q22a3;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22a4
		{
			get {return m_q22a4;}
			set
			{
				if (m_q22a4 == value) return;
				logChange("Q22a4", value.ToString(), nz(m_q22a4,"").ToString());
				m_q22a4=value;
			}
		}
		private Decimal m_q22a4;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22a5
		{
			get {return m_q22a5;}
			set
			{
				if (m_q22a5 == value) return;
				logChange("Q22a5", value.ToString(), nz(m_q22a5,"").ToString());
				m_q22a5=value;
			}
		}
		private Decimal m_q22a5;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22b1
		{
			get {return m_q22b1;}
			set
			{
				if (m_q22b1 == value) return;
				logChange("Q22b1", value.ToString(), nz(m_q22b1,"").ToString());
				m_q22b1=value;
			}
		}
		private Decimal m_q22b1;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22b2
		{
			get {return m_q22b2;}
			set
			{
				if (m_q22b2 == value) return;
				logChange("Q22b2", value.ToString(), nz(m_q22b2,"").ToString());
				m_q22b2=value;
			}
		}
		private Decimal m_q22b2;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22b3
		{
			get {return m_q22b3;}
			set
			{
				if (m_q22b3 == value) return;
				logChange("Q22b3", value.ToString(), nz(m_q22b3,"").ToString());
				m_q22b3=value;
			}
		}
		private Decimal m_q22b3;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22b4
		{
			get {return m_q22b4;}
			set
			{
				if (m_q22b4 == value) return;
				logChange("Q22b4", value.ToString(), nz(m_q22b4,"").ToString());
				m_q22b4=value;
			}
		}
		private Decimal m_q22b4;

		/// <summary>
		///
		/// </summary>
		public Decimal Q22b5
		{
			get {return m_q22b5;}
			set
			{
				if (m_q22b5 == value) return;
				logChange("Q22b5", value.ToString(), nz(m_q22b5,"").ToString());
				m_q22b5=value;
			}
		}
		private Decimal m_q22b5;

		

		/// <summary>
		///
		/// </summary>
		public int C2Score
		{
			get {return m_c2score;}
			set
			{
				if (m_c2score == value) return;
				logChange("C2Score", value.ToString(), nz(m_c2score,"").ToString());
				m_c2score=value;
			}
		}
		private int m_c2score;

	}

	}

