﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityFramework.MSSQL;
using System.Data;

namespace ART
{
    public partial class AqC2Entity
    {
        public static AqC2Entity Load(int recommendationId)
        {
            EntityList<AqC2Entity> entities = new AqC2DL().Get(new SQLBuilder("Select * from AqC2 where RecommendationId = @0", recommendationId.ToString()));
			if (entities.Count == 0)
			{
				AqC2Entity entity = new AqC2Entity();
				//entity.RecommendationId = recommendationId;
				return entity;
			}
			entities[0].Recalculate();
			return entities[0];
        }

        public void Commit()
        {
            Recalculate();
            new AqC2DL().Commit(this);
        }

        public void Recalculate()
        {
			//Create grid and store overall score
			List<int[]> grid = new List<int[]>(); //list order is 2.1 score (0 to 10), int[] index is 10 - floor(mgmt score) (0 - 10)
			grid.Add(new int[] {10,6,5,3,2,2,1,1,0,0,0 }); //0
			grid.Add(new int[] {10,7,6,4,3,3,2,2,1,0,0 });
			grid.Add(new int[] {10,7,6,5,4,4,3,3,2,1,0 });
			grid.Add(new int[] {10,8,7,5,4,4,4,4,3,2,1 });
			grid.Add(new int[] {10,8,7,6,5,5,4,4,4,3,2 });
			grid.Add(new int[] {10,8,7,6,6,5,5,5,4,4,3 });
			grid.Add(new int[] {10,9,8,7,6,6,5,5,5,4,3 });
			grid.Add(new int[] {10,9,8,7,7,6,6,5,5,4,4 });
			grid.Add(new int[] {10,9,9,8,8,7,7,6,6,5,5 });
			grid.Add(new int[] {10,10,9,9,9,8,8,7,7,7,6 });
			grid.Add(new int[] {10,10,10,10,10,10,10,10,10,10,10 }); //10

			int[] row = grid[WasteDischargeScore];
			C2Score = row[Convert.ToInt32(10 - Math.Floor(F22Score))];
		}

		#region rowsources
		public IList<RowsourceEntity> EffluentCriterionRowsource()
		{
			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();

			List<string> values = new List<string>() { "10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0", "C" };
			foreach (string s in values)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//rowsource.Key = s;
				rowsource.Value = s;
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public DataTable F22Rowsource()
		{
			//Kent, I made this a datatable because the rowsource object has int, string, and I need decimal, string.  
			//Let me know if you want me to do something different.
			DataTable dt = new DataTable();
			dt.Columns.Add("Key");
			dt.Columns.Add("Value");

			DataRow dr = dt.NewRow();
			dr["Key"] = 1.0M;
			dr["Value"] = "Yes";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Key"] = 0.75M;
			dr["Value"] = "Mostly";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Key"] = 0.5M;
			dr["Value"] = "Moderately";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Key"] = 0.25M;
			dr["Value"] = "Partly";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Key"] = 0M;
			dr["Value"] = "No";
			dt.Rows.Add(dr);

			return dt;
		}

			
		#endregion

		#region Calculated Fields

		public decimal NitrogenContentFactor { get { return 0.16M; } }

		public decimal NitrogenPerTonProduced { get { return (ProteinContentFeed * NitrogenContentFactor * eFCR * 10) + FertilizerNInput; } }

		public decimal NitrogenPerTonHarvested { get { return ProteinContentFish * NitrogenContentFactor * 10; } }

		public decimal F21AScore { get { return NitrogenPerTonProduced - NitrogenPerTonHarvested; } }

		public decimal F21BScore { get { return BasicDischargeScore + BasicDischargeAdjustment; } }

		public decimal F22AScore { get { return Q22a1 + Q22a2 + Q22a3 + Q22a4 + Q22a5; } }

		public decimal F22BScore { get { return Q22b1 + Q22b2 + Q22b3 + Q22b4 + Q22b5; } }

		public decimal F22Score { get { return F22AScore * F22BScore / 2.5M; } }

		public decimal WasteDischargedPerTon { get { return F21AScore * F21BScore; } }

		/// <summary>
		/// Aka 2.1 score
		/// </summary>
		public int WasteDischargeScore { get { return 10 - Convert.ToInt32(Math.Ceiling(WasteDischargedPerTon / 10)); } }


		#endregion

	}
}
