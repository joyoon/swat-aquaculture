﻿using System;
using System.Collections.Generic;
using System.Web;
using EntityFramework.MSSQL;
using System.Data;
using System.Text;


namespace ART
{
	public class ARTService : BaseBL
	{
		public ARTService()
			: base()
		{ }

		#region Login
		/// <summary>
		/// 
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		/// <returns>Null if username/pwd combo fails</returns>
		public UserEntity Login(string userName, string password)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, DisplayName, IsPresentStaff as IsSFWStaff from [User] where UserName = @0 and Password = @1", userName, password));
			return (dt.Rows.Count != 0) ? PopulateUserEntityDomain(dt.Rows[0]) : null;

		}

		public UserEntity LoadUser(int id)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, DisplayName, IsPresentStaff as IsSFWStaff from [User] where Id = @0", id.ToString()));
			return (dt.Rows.Count != 0) ? PopulateUserEntityDomain(dt.Rows[0]) : null;
		}

		private UserEntity PopulateUserEntityDomain(DataRow dr)
		{
			if (dr == null) return null;
			UserEntity ue = new UserEntity();
			ue.Id = (int)dr["Id"];
			ue.DisplayName = dr["DisplayName"].ToString();

			if (IsDBNull(dr["IsSFWStaff"]))
			{
				ue.IsSFWStaff = false;
			}
			else
			{
				ue.IsSFWStaff = (bool)dr["IsSFWStaff"];
			}
			return ue;
		}

		private ReportEntity PopulateReportEntityDomain(DataRow dr, UserEntity user)
		{
			ReportEntity re = new ReportEntity();


			re.Id = (int)dr["ReportId"];
			re.Title = dr["Title"].ToString();
			if (dr["LastUpdated"].ToString() != "") re.LastUpdated = dr["LastUpdated"].ToString();
			re.ReportStatus = dr["ReportStatus"].ToString();

			if (user == null) return re; //Public doesn't need stuff below

			if (user.IsSFWStaff) //global setting, not report specific
			{
				re.Permissions.Add(ReportEntity.Permission.ExportReport);
				re.Permissions.Add(ReportEntity.Permission.Comment);
				re.Permissions.Add(ReportEntity.Permission.Answer);
				re.Permissions.Add(ReportEntity.Permission.UpdateStatus);
			}
			else if (dr["IsReviewer"].ToString() == "1") //report specific setting
			{
				re.Permissions.Add(ReportEntity.Permission.ExportReport);
				re.Permissions.Add(ReportEntity.Permission.CompleteReview);
			}
			else if (dr["IsAnalyst"].ToString() == "1") //report specific setting
			{
				re.Permissions.Add(ReportEntity.Permission.Comment);
				re.Permissions.Add(ReportEntity.Permission.Answer);
                re.Permissions.Add(ReportEntity.Permission.ExportReport); //TODO: remove after tab is back, but let them download report for now
			}
			else
			{ }

			return re;
		}

		public ReportEntity LoadReport(int id)
		{
			SQLBuilder sb = new SQLBuilder(@"Select r.Id as ReportId, r.Title as Title, r.LastUpdated, rs.Description as ReportStatus
FROM Report r inner join ReportStatus rs on r.ReportStatusId = rs.Id WHERE r.Id=" + id);
			DataTable dt = DB.GetDataTable(sb);
			return (dt.Rows.Count != 0) ? PopulateReportEntityDomain(dt.Rows[0], null) : null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="user">pass null for public user</param>
		/// <returns>List of reports user has access to, their status, and a series of booleans that corresponds to tasks that this user can perform against each report</returns>
		public IList<ReportEntity> ReportList(UserEntity user, string searchString)
		{
			//TODO implement reportGroupId
			//TODO implement Last Updated?
			/**/
			// uAuthor.DisplayName as AssignedTo,
			SQLBuilder sb;
			sb = new SQLBuilder(@"Select * from (Select Distinct r.Id as ReportId, r.Title as Title, r.ReportStatusId, rs.Description as ReportStatus, convert(varchar(16), r.LastUpdated, 121) + ' (PST)' as LastUpdated,
case when r.Id in (Select ReportId from ReportReview where ReviewerUserId = @0 and ReviewReceived is null) then 1 else 0 end as IsReviewer,
case when r.Id in (Select ReportId from ReportAuthor where AuthorUserId = @0) then 1 else 0 end as IsAnalyst
FROM Report r left join ReportStatus rs on r.ReportStatusId = rs.Id  WHERE r.ReportStatusId not in (2) and r.Title like @1) x
WHERE ((IsReviewer = 1 and x.ReportStatusId = 4)  or IsAnalyst = 1 or @2 = 1) ORDER BY Title", user.Id.ToString(), searchString + "%", user.IsSFWStaff ? "1" : "0");
			//status not in (2) = not published.
			List<ReportEntity> entities = new List<ReportEntity>();

			foreach (DataRow dr in DB.GetDataTable(sb).Rows)
			{
				ReportEntity re = PopulateReportEntityDomain(dr, user);
				entities.Add(re);
			}
			return entities;
		}

		public void setLastUpdated(int reportId)
		{
			DateTime x = GmtToPacific(DateTime.UtcNow); //Adjust GMT to PST
			DB.UpdateCmd(new SQLBuilder("Update Report set LastUpdated=@0 where Id = @1", x.ToString("MM/dd/yy hh:mm"), reportId.ToString()));
		}

		private static DateTime GmtToPacific(DateTime dateTime)
		{
			return TimeZoneInfo.ConvertTimeFromUtc(dateTime,
				TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));
		}

		#endregion

		#region Add/Edit Recs

		public IList<RecEntity> RecsByReport(ReportEntity report)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder(@"select rec.Id as RecommendationId, s.CommonName, s.Genus + ' ' + s.Species as ScientificName, 
l.Description as Location, bow.Description as BodyOfWater, m.Method, rt.Description as RecommendationType, rt.Id as RecommendationTypeId,
rec.SpeciesId, rec.LocationId, rec.BodyOfWaterId, rec.MethodId
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	inner join RecommendationType rt on rt.Id = rec.RecommendationTypeId
ORDER BY s.CommonName, l.Description, bow.Description, m.Method", report.Id.ToString()));

			List<RecEntity> recs = new List<RecEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RecEntity rec = new RecEntity();
				//TODO: Talk to Bryan about how this method isn't working
				//Framework.EntityPopulate(rec, dr);
				rec.BodyOfWater = dr["BodyOfWater"].ToString();
				rec.CommonName = dr["CommonName"].ToString();
				rec.Location = dr["Location"].ToString();
				rec.Method = dr["Method"].ToString();
				rec.RecommendationId = Convert.ToInt32(dr["RecommendationId"]);
				rec.RecommendationType = dr["RecommendationType"].ToString();
				rec.ScientificName = dr["ScientificName"].ToString();
				//TODO: Have Bryan review my addition of the Ids fields - it would be better to have objects (RowsourceEntity?) instead of separate ID and Name properties
				rec.RecommendationTypeId = Convert.ToInt32(dr["RecommendationTypeId"]);
				rec.SpeciesId = Convert.ToInt32(dr["SpeciesId"]);
				rec.LocationId = Convert.ToInt32(dr["LocationId"]);
				rec.BodyOfWaterId = Convert.ToInt32(dr["BodyOfWaterId"]);
				rec.MethodId = Convert.ToInt32(dr["MethodId"]);

				recs.Add(rec);
			}
			return recs;
		}

		public IList<RowsourceEntity> SpeciesAutocomplete(string filter)
		{
			//s.Genus + ' ' + s.Species as ScientificName
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select s.Id as [Key], s.CommonName as Value, s.Genus as Genus, s.Species as Species from Species s where (s.Commonname like @0 OR s.Genus + ' ' + s.Species like @0) order by s.CommonName", "%" + filter.ToString() + "%"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//TODO: Figure out why the Framework.EntityPopulate methods isn't working--temporarily replacing it with direct calls to populate rowsource
				//Framework.EntityPopulate(rowsource, dr); 
				rowsource.Key = Convert.ToInt32(dr["Key"]);
				rowsource.Value = dr["Value"].ToString() + " (" + dr["Genus"] + " " + dr["Species"] + ")";
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public IList<RowsourceEntity> LocationAutocomplete(string filter)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select l.Id as [Key], l.Description as Value from Location l where l.Description like @0 order by l.Description", "%" + filter.ToString() + "%"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//TODO: Figure out why the Framework.EntityPopulate methods isn't working--temporarily replacing it with direct calls to populate rowsource
				//Framework.EntityPopulate(rowsource, dr);
				rowsource.Key = Convert.ToInt32(dr["Key"]);
				rowsource.Value = dr["Value"].ToString();
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public IList<RowsourceEntity> BodyOfWaterAutocomplete(string filter)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select bow.Id as [Key], bow.Description as Value from BodyOfWater bow where bow.Description like @0 order by bow.Description", "%" + filter.ToString() + "%"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//TODO: Figure out why the Framework.EntityPopulate methods isn't working--temporarily replacing it with direct calls to populate rowsource
				//Framework.EntityPopulate(rowsource, dr);
				rowsource.Key = Convert.ToInt32(dr["Key"]);
				rowsource.Value = dr["Value"].ToString();
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public IList<RowsourceEntity> MethodAutocomplete(string filter)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select m.Id as [Key], m.Method as Value from Method m where m.Method like @0 order by m.Method", "%" + filter.ToString() + "%"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//TODO: Figure out why the Framework.EntityPopulate methods isn't working--temporarily replacing it with direct calls to populate rowsource
				//Framework.EntityPopulate(rowsource, dr);
				rowsource.Key = Convert.ToInt32(dr["Key"]);
				rowsource.Value = dr["Value"].ToString();
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public IList<RowsourceEntity> RecommendationType()
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select r.Id as [Key], r.Description as Value from RecommendationType r order by r.Description"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				//TODO: Find out why this isn't working
				//Framework.EntityPopulate(rowsource, dr);
				rowsource.Key = Convert.ToInt32(dr["Key"]);
				rowsource.Value = dr["Value"].ToString();
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public int RecommendationGet(int reportId, int speciesId, int locationId, int bodyOfWaterId, int methodId)
		{
			return DB.GetIntNZ(new SQLBuilder("select Id from recommendation where ReportId=@0 and SpeciesId=@1 and isnull(LocationId,0)=@2 and isnull(BodyOfWaterId,0)=@3 and MethodId=@4", reportId.ToString(), speciesId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString()));
		}


		/// <summary>
		/// Adds a recommendation to the DB
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="speciesId"></param>
		/// <param name="locationId">0 is allowed if BOW is provided</param>
		/// <param name="bodyOfWaterId">0 is allowed if Location is provided</param>
		/// <param name="methodId"></param>
		/// <param name="recommendationTypeId"></param>
		public int RecommendationCreate(int reportId, int speciesId, int locationId, int bodyOfWaterId, int methodId, int recommendationTypeId)
		{
			if (reportId == 0) throw new Exception("Must provide a ReportId");
			if (speciesId == 0) throw new Exception("Must provide a Species");
			if (locationId == 0 && bodyOfWaterId == 0) throw new Exception("Must provide at least one Location or BodyOfWater");
			if (methodId == 0) throw new Exception("Must provide a Method");
			if (recommendationTypeId == 0) throw new Exception("Must provide a RecommendationTypeId");

			setLastUpdated(reportId);
			//Exists statement means it won't create a duplicate
			int recId = DB.GetIntNZ(new SQLBuilder("select Id from Recommendation where ReportId=@0 and SpeciesId=@1 and isnull(LocationId,0)=@2 and isnull(BodyOfWaterId,0)=@3 and MethodId=@4",
reportId.ToString(), speciesId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString()));
			if (recId > 0) return 0; //Rec already exists

			recId = DB.InsertCmd(new SQLBuilder(@"Insert Recommendation (ReportId, SpeciesId, LocationId, BodyOfWaterId, MethodId, RecommendationTypeId, NGOId) values(@0,@1,@2,@3,@4,@5,1)", reportId.ToString(), speciesId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString(), recommendationTypeId.ToString()), true);

			//List of Crit 3 and 4
			List<int> factorTypeIds = DB.GetList<int>(new SQLBuilder("select Id from FactorType where CriteriaId in (2,4,5,6)")); //5&6 are 3.1 and 3.2, resp.  

			foreach (int factorTypeId in factorTypeIds)
			{
				int fId = factorIdGet(reportId, factorTypeId, locationId, bodyOfWaterId, methodId);
				if (fId > 0) FactorAssociate(reportId, recId, fId, false);
			}

			return recId;  //Success
		}

		public bool RecommendationCreateCombination(int reportId, int[] speciesIds, int[] locationIds, int[] bodyOfWaterIds, int[] methodIds, int recommendationTypeId)
		{
			//Return error if neither Location or Body Of Water is returned
			if (reportId == 0) throw new Exception("Must provide a ReportId");
			if (speciesIds.GetUpperBound(0) == -1) throw new Exception("Must provide at least one Species");
			if (locationIds.GetUpperBound(0) == -1 && bodyOfWaterIds.GetUpperBound(0) == -1) throw new Exception("Must provide at least one Location or BodyOfWater");
			if (methodIds.GetUpperBound(0) == -1) throw new Exception("Must provide at least one Method");
			if (recommendationTypeId == 0) throw new Exception("Must provide a RecommendationTypeId");

			//Purposely not checking db for valid Ids b/c I don't want to sacrifice performance.

			//Ignore any Recs already created for this report

			for (int s = 0; s <= speciesIds.GetUpperBound(0); s++) //foreach species
			{
				for (int m = 0; m <= methodIds.GetUpperBound(0); m++) //foreach method
				{
					if (locationIds.GetUpperBound(0) >= 0 && bodyOfWaterIds.GetUpperBound(0) >= 0) //Both Location and Body of Water provided
					{
						for (int l = 0; l <= locationIds.GetUpperBound(0); l++)
						{
							for (int b = 0; b <= bodyOfWaterIds.GetUpperBound(0); b++)
							{
								RecommendationCreate(reportId, speciesIds[s], locationIds[l], bodyOfWaterIds[b], methodIds[m], recommendationTypeId);
							}
						}
					}
					else if (locationIds.GetUpperBound(0) >= 0) //Just Location Provided
					{
						for (int l = 0; l <= locationIds.GetUpperBound(0); l++)
						{
							RecommendationCreate(reportId, speciesIds[s], locationIds[l], 0, methodIds[m], recommendationTypeId);
						}
					}
					else //Just Body of Water provided
					{
						for (int b = 0; b <= bodyOfWaterIds.GetUpperBound(0); b++)
						{
							RecommendationCreate(reportId, speciesIds[s], 0, bodyOfWaterIds[b], methodIds[m], recommendationTypeId);
						}
					}
				}
			}
			setLastUpdated(reportId);
			return true; //Success
		}

		public int RecommendationUpdate(int reportId, int recommendationId, int speciesId, int locationId, int bodyOfWaterId, int methodId, int recommendationTypeId)
		{
			DB.UpdateCmd(new SQLBuilder(@"Update Recommendation set SpeciesId=@0, LocationId=@1, BodyOfWaterId=@2, MethodId=@3, RecommendationTypeId=@4 where Id = @5",
speciesId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString(), recommendationTypeId.ToString(), recommendationId.ToString()));
			setLastUpdated(reportId);
			return recommendationId; //success
		}

        public int RecommendationUpdateType(int reportId, int recommendationId,  int recommendationTypeId)
        {
            DB.UpdateCmd(new SQLBuilder(@"Update Recommendation set RecommendationTypeId=@0 where Id = @1", recommendationTypeId.ToString(), recommendationId.ToString()));
			setLastUpdated(reportId);
			return recommendationId; //success
        }

		public bool RecommendationDelete(int reportId, int recommendationId)
		{
			DB.DeleteCmd(new SQLBuilder("Delete from Recommendation where Id = @0", recommendationId.ToString()));
			setLastUpdated(reportId);
			return true; //success
		}

		#endregion

		#region Synthesis
		public string SynthesisGet(int reportId, int criteriaId)
		{
			//Hack: SquareOne is storing 5 as 31 and 6 as 32
			if (criteriaId == 5) criteriaId = 31;
			if (criteriaId == 6) criteriaId = 32;
			return DB.GetString(new SQLBuilder("Select Synthesis from ReportSynthesis where ReportId=@0 and CriteriaId=@1", reportId.ToString(), criteriaId.ToString()));
		}

		public bool SynthesisSet(int reportId, int criteriaId, string synthesis)
		{
			synthesis = synthesis.Trim();
			if (synthesis.StartsWith("<p>")) synthesis = synthesis.Substring(3, synthesis.Length - 3);
			if (synthesis.EndsWith("<p>")) synthesis = synthesis.Substring(0, synthesis.Length - 3);
			synthesis = synthesis.Trim();

			//Create the record if it doesn't already exist
			DB.InsertCmd(new SQLBuilder("Insert ReportSynthesis (ReportId, CriteriaId) select @0,@1 Where not exists (Select Id from ReportSynthesis where ReportId = @0 and CriteriaId = @1)", reportId.ToString(), criteriaId.ToString()));
			//Update Synthesis field
			DB.UpdateCmd(new SQLBuilder("Update ReportSynthesis set Synthesis = @2 where ReportId=@0 and CriteriaId=@1", reportId.ToString(), criteriaId.ToString(), synthesis));
			setLastUpdated(reportId);
			return true;
		}
		#endregion

		#region Factors and Scoring

		public IList<RowsourceEntity> FactorType()
		{
			//excluding 3.1 and 3.2.
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id as [Key], Prefix + ' : ' + isnull(Description,'') as Value from FactorType Where criteriaId <> 3 order by case when criteriaId = 4 then 10 else criteriaId end, SortOrder"));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				Framework.EntityPopulate(rowsource, dr);
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		public FactorTypeEntity FactorTypeGet(int factorTypeId)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder(@"Select Id, Description, Prefix, CriteriaId, SortOrder from FactorType where Id = @0", factorTypeId.ToString()));

			FactorTypeEntity factorType = new FactorTypeEntity();
			Framework.EntityPopulate(factorType, dt.Rows[0]);
			return factorType;
		}

		public IList<RowsourceEntity> FactorScore(int factorTypeId)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id as [Key], isnull(Description,'') as Value from FactorScore where FactorTypeId = @0 order by number", factorTypeId.ToString()));

			List<RowsourceEntity> rowsources = new List<RowsourceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				RowsourceEntity rowsource = new RowsourceEntity();
				Framework.EntityPopulate(rowsource, dr);
				rowsources.Add(rowsource);
			}
			return rowsources;
		}

		/// <summary>
		/// Returns a single factor
		/// </summary>
		/// <param name="factorId"></param>
		/// <returns></returns>
		public FactorEntity FactorGet(int factorId)
		{
			DataTable dt = DB.GetDataTable(new SQLBuilder(@"Select f.Id, f.FactorTypeId, fs.Number as FactorScore, fs.Description as FactorScoreDescription, f.Label, f.Title, f.KeyInfo, f.Rationale from Factor f left join FactorScore fs on fs.Id = f.FactorScoreId 
where f.Id = @0 order by f.Label", factorId.ToString()));

			if (dt.Rows.Count == 0) return null;

			FactorEntity factor = new FactorEntity();
			Framework.EntityPopulate(factor, dt.Rows[0]);
			return factor;

		}

		/// <summary>
		/// returns all factors of the given FactorTypeId.
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="factorTypeId">Optional.  Pass a 0 to get all Factors for all types for the given reportId</param>
		/// <param name="filter">Optional.  Filters on FactorScoreDescription</param>
		/// <returns></returns>
		public IList<FactorEntity> FactorGet(int reportId, int factorTypeId, string filter)
		{
			DataTable dt;
			string sql = @"Select f.Id, f.FactorTypeId, fs.Number as FactorScore, fs.Description as FactorScoreDescription, f.Label, f.Title, f.KeyInfo, f.Rationale from Factor f left join FactorScore fs on fs.Id = f.FactorScoreId
where f.ReportId = @0";
			if (factorTypeId > 0) sql += " and f.FactorTypeId = @1";
			if (filter.Length > 0) sql += " and (f.Title like @2 OR f.KeyInfo like @2)";
			sql += " order by f.Label";
			dt = DB.GetDataTable(new SQLBuilder(sql, reportId.ToString(), factorTypeId.ToString(), "%" + filter + "%"));

			List<FactorEntity> factors = new List<FactorEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				FactorEntity factor = new FactorEntity();
				Framework.EntityPopulate(factor, dr);
				factors.Add(factor);
			}
			return factors;
		}

		private int factorIdGet(int reportId, int factorTypeId, int locationId, int bodyOfWaterId, int methodId)
		{
			//used to find existing factors (Crit 3 and 4) for after-the-fact adding of recs to a report
			string sql = @"Select f.Id from Factor f inner join factorRecommendationDetail frd on frd.FactorId = f.Id inner join Recommendation r on r.Id = frd.RecommendationId
where f.ReportId = @0 and f.FactorTypeId = @1";
			if (locationId > 0) sql += " and r.LocationId = @2";
			if (bodyOfWaterId > 0) sql += " and r.BodyOfWaterId = @3";
			sql += " and r.MethodId = @4";

			return DB.GetIntNZ(new SQLBuilder(sql, reportId.ToString(), factorTypeId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString()));
		}

		public FactorEntity FactorAdd(int reportId, int factorTypeId, int factorScoreId, string title, string keyInfo, string rationale)
		{
			//Get Label (if there are 2 Factors for a factor type with 'abc' as prefix, this will return "abc-03")
			string label = DB.GetString(new SQLBuilder("Select Prefix + '-' + right('0' + convert(varchar(2),isnull(x.suffix,0) + 1),2) from FactorType ft cross join (Select count(*) as Suffix from Factor where ReportId = @0 and FactorTypeId=@1) x where ft.Id = @1", reportId.ToString(), factorTypeId.ToString()));

			int factorId = DB.InsertCmd(new SQLBuilder("Insert Factor(ReportId,FactorTypeId,FactorScoreId,Label,Title,KeyInfo,Rationale) values (@0,@1,@2,@3,@4,@5,@6)", reportId.ToString(), factorTypeId.ToString(), factorScoreId.ToString(), label, title, keyInfo, rationale), true);

			recScoreDetailUpdate(reportId, factorId, false);

			setLastUpdated(reportId);
			return FactorGet(factorId);
		}

		public FactorEntity FactorUpdate(int reportId, int factorId, int factorTypeId, int factorScoreId, string title, string keyInfo, string rationale)
		{
			//trim off leading and trailing <p> tags
			keyInfo = keyInfo.Trim();
			if (keyInfo.StartsWith("<p>")) keyInfo = keyInfo.Substring(3,keyInfo.Length - 3);
			if (keyInfo.EndsWith("<p>")) keyInfo = keyInfo.Substring(0, keyInfo.Length - 3);
			keyInfo = keyInfo.Trim();

			rationale = rationale.Trim();
			if (rationale.StartsWith("<p>")) rationale = rationale.Substring(3, rationale.Length - 3);
			if (rationale.EndsWith("<p>")) rationale = rationale.Substring(0, rationale.Length - 3);
			rationale = rationale.Trim();


			//Update factor
			DB.UpdateCmd(new SQLBuilder("Update Factor set FactorTypeId=@0, FactorScoreId=@1,Title=@2,KeyInfo=@3,Rationale=@4 where Id=@5", factorTypeId.ToString(), factorScoreId.ToString(), title, keyInfo, rationale, factorId.ToString()));

			recScoreDetailUpdate(reportId, factorId, (factorScoreId == 0)); //If score is 0, then score is being updated to blank.
			setLastUpdated(reportId);
			return FactorGet(factorId);
		}

		public void FactorDelete(int reportId, int factorId, int factorTypeId)
		{
			DB.DeleteCmd(new SQLBuilder("Delete from Factor where Id = @0", factorId.ToString()));
			DB.DeleteCmd(new SQLBuilder("Delete from FactorRecommendationDetail where FactorId = @0", factorId.ToString()));
			DB.DeleteCmd(new SQLBuilder("Delete from FactorComment where FactorId = @0", factorId.ToString()));
			recScoreDetailUpdate(reportId, factorId, true);
			setLastUpdated(reportId);
			//TODO delete attachments?
		}

		/// <summary>
		/// Used for Factors 1 and 2
		/// </summary>
		/// <param name="recommendationId"></param>
		/// <param name="factorTypeId"></param>
		/// <param name="factorId"></param>
		public void FactorAssociate(int reportId, int recommendationId, int factorId, bool applyToAll)
		{
			if (applyToAll) //should only fire for Crit 1.  Delete/Insert below looks at all recs with the same report/species as the given factorId
			{
				//Delete any old factors associated with this rec from the same factorTYpeId
				DB.DeleteCmd(new SQLBuilder("delete from FactorRecommendationDetail where RecommendationId in (Select Id from Recommendation where ReportId in (select ReportId from Recommendation where Id = @0) and SpeciesId in (select SpeciesId from Recommendation where Id = @0))  and factorId in (select Id from factor where factorTypeId in (select factorTypeId from factor where Id = @1))", recommendationId.ToString(), factorId.ToString()));

				//Create new association
				DB.InsertCmd(new SQLBuilder("Insert FactorRecommendationDetail(FactorId, CriteriaId, RecommendationId) select Distinct f.Id, ft.CriteriaId, r.Id from Recommendation r cross join Factor f inner join factorType ft on ft.Id = f.FactorTypeId where f.Id = @0 and r.ReportId in (select reportId from recommendation where Id = @1) and r.SpeciesId in (select SpeciesId from recommendation where Id = @1)", factorId.ToString(), recommendationId.ToString()));

				//change any scores if necessary
				recScoreDetailUpdate(reportId, factorId, false);
			}
			else
			{
				//Delete any old factors associated with this rec from the same factorTYpeId
				DB.DeleteCmd(new SQLBuilder("delete from FactorRecommendationDetail where RecommendationId = @0 and factorId in (select Id from factor where factorTypeId in (select factorTypeId from factor where Id = @1))", recommendationId.ToString(), factorId.ToString()));

				//Create new association
				DB.InsertCmd(new SQLBuilder("Insert FactorRecommendationDetail(FactorId, CriteriaId, RecommendationId) select Distinct f.Id, ft.CriteriaId, @1 from Factor f inner join factorTYpe ft on ft.Id = f.FactorTypeId where f.Id = @0", factorId.ToString(), recommendationId.ToString()));

				//change any scores if necessary
				recScoreDetailUpdate(reportId, factorId, false);
			}
		}

		/// <summary>
		/// Used for Factors 3 and 4
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="locationId">pass a 0 if not applicable</param>
		/// <param name="bodyOfWaterId">pass a 0 if not applicable</param>
		/// <param name="methodId"></param>
		/// <param name="factorTypeId"></param>
		public void FactorAssociate(int reportId, int locationId, int bodyOfWaterId, int methodId, int factorId, int factorTypeId)
		{
			//Delete any old factors associated with these recs from the same factorTypeId
			DB.DeleteCmd(new SQLBuilder("delete from FactorRecommendationDetail where RecommendationId in (select Id from Recommendation where ReportId = @0 and isnull(LocationId,0) = @1 and isnull(BodyOfWaterId,0) = @2 and MethodId = @3) and factorId in (select Id from factor where factorTypeId = @4)", reportId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString(), factorTypeId.ToString()));

			//Create new associations
			DB.InsertCmd(new SQLBuilder("Insert FactorRecommendationDetail(FactorId, CriteriaId, RecommendationId) select Distinct @5, ft.CriteriaId, r.Id from Recommendation r cross join (Factor f inner join factorType ft on ft.Id = f.FactorTypeId) where ft.Id = @4 and r.ReportId = @0 and isnull(r.LocationId,0) = @1 and isnull(r.BodyOfWaterId,0) = @2 and r.MethodId = @3", reportId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString(), factorTypeId.ToString(), factorId.ToString()));

			//Get a list of all factors for this Location/BOW/Method
			List<int> factors = DB.GetList<int>(new SQLBuilder("select FactorId from FactorRecommendationDetail frd inner join recommendation r on r.Id = frd.RecommendationId where ReportId = @0 and isnull(LocationId,0) = @1 and isnull(BodyOfWaterId,0) = @2 and MethodId = @3 and frd.factorId in (select Id from factor where factorTypeId = @4)", reportId.ToString(), locationId.ToString(), bodyOfWaterId.ToString(), methodId.ToString(), factorTypeId.ToString()));
			foreach (int factor in factors)
			{
				//change any scores if necessary
				recScoreDetailUpdate(reportId, factor, false);
			}
			setLastUpdated(reportId);
		}

		public void RecalcAllReports()
		{
			List<int> reportIds = DB.GetList<int>(new SQLBuilder("Select Id from Report"));
			foreach (int reportId in reportIds)
			{
				RecalcSingleReport(reportId);
			}
		}

		public void RecalcSingleReport(int reportId)
		{
			//used to fix something I screwed up.  runs recScoreDetailUpdate on all factors for given reports.
			List<int> xs = DB.GetList<int>(new SQLBuilder("select Id from Factor where reportId in (@0)", reportId.ToString()));
			foreach (int x in xs)
			{
				recScoreDetailUpdate(reportId, x, false);
			}
		}

		private void recScoreDetailUpdate(int reportId, int factorId, bool isDelete)
		{
			int criteriaId = DB.GetInt(new SQLBuilder("select criteriaId from FactorType where Id in (select factorTypeId from factor where Id = @0)", factorId.ToString()));

			//Now see if any RecommendationScoreDetail (criteria specific) need to be scored as a result
			if (isDelete)
			{
				//Delete all Criteria level scores related to this factor, b/c this factor has been zeroed out.  Was setting to 0, but 0 is a valid lookup value.
				DB.UpdateCmd(new SQLBuilder(@"Delete from RecommendationScoreDetail where criteriaId = @1
			and recommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0) ", factorId.ToString(), criteriaId.ToString()));
				return;
			}

			//Populate any missing RecScoreDetail records
			DB.InsertCmd(new SQLBuilder("Insert RecommendationScoreDetail (RecommendationId, CriteriaId, score, colorId) select RecommendationId, frd.CriteriaId, 0, 0 from FactorRecommendationDetail frd where FactorId = @0 and not exists (select Id from RecommendationScoreDetail where CriteriaId = frd.CriteriaId and RecommendationId = frd.RecommendationId)", factorId.ToString()));
			if (criteriaId == 1) //Deal with Factor2 as well
			{
				DB.InsertCmd(new SQLBuilder("Insert RecommendationScoreDetail (RecommendationId, CriteriaId, score, colorId) select RecommendationId, 2, 0, 0 from FactorRecommendationDetail frd where FactorId = @0 and not exists (select Id from RecommendationScoreDetail where CriteriaId = 2 and RecommendationId = frd.RecommendationId)", factorId.ToString()));
			}
			if (criteriaId == 5 || criteriaId == 6) //for 3.1 and 3.2, add a RecScoreDetail instance for 3
			{
				DB.InsertCmd(new SQLBuilder("Insert RecommendationScoreDetail (RecommendationId, CriteriaId, score, colorId) select RecommendationId, 3, 0, 0 from FactorRecommendationDetail frd where FactorId = @0 and not exists (select Id from RecommendationScoreDetail where CriteriaId = 3 and RecommendationId = frd.RecommendationId)", factorId.ToString()));
			}
			if (criteriaId == 1)
			{
				//Geometric mean(1.2,1.3).  If 1.3 is critical score = 0
				DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set Score = case when subCrit.Number = 0 then 0 else AvgScore end, ColorId = c.Id
			from RecommendationScoreDetail rsd inner join 
				(select exp(avg(log(case when fs.Number <1 then 100 else fs.Number end))) as AvgScore, frd.RecommendationId, frd.CriteriaId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and f.FactorTypeId in (2,3)
					Where frd.RecommendationId in 
						(select RecommendationId from 
							(Select recommendationId, count(*) as num from factor f inner join factorRecommendationDetail frd on frd.FactorId = f.Id where frd.criteriaId = @1 group by RecommendationId) x
						where x.Num = 3)
					and frd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)
					group by frd.RecommendationId, frd.CriteriaId)
				sub on rsd.RecommendationId = sub.RecommendationId and rsd.CriteriaId = sub.CriteriaId

				inner join (select fs.Number, frd.RecommendationId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and f.FactorTypeId in (3))
				subCrit on rsd.RecommendationId = subCrit.RecommendationId

				cross join Color c WHERE rsd.CriteriaId = 1 and case when subCrit.Number = 0 then 0 else AvgScore end between c.MinScore and c.MaxScore", factorId.ToString(), criteriaId.ToString()));

				//TODO deal with Critical on 2.3
			}

			//Update Criteria 2 if a change is made to Criteria 1 or 2
			if (criteriaId == 1 || criteriaId == 2)
			{
				//GeoMean(1.2,1.3) x 2.4.  Sub2 gets me 2.4, Sub gets avg.  If 1.3 is critical then score = 0
				DataTable dt = DB.GetDataTable(new SQLBuilder(@"Select r.RecommendationTypeId, r.Id as RecommendationId, r.LocationId, r.MethodId, r.BodyOfWaterId, 
rsd.Id, rsd.Score, rsd.ColorId, fs.Number as Score2
From RecommendationScoreDetail rsd inner join recommendation r on r.Id = rsd.RecommendationId and rsd.CriteriaId = 1
left join (FactorRecommendationDetail frd inner join Factor F on f.Id = frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId) on frd.RecommendationId = r.Id and frd.CriteriaId = 2
WHERE rsd.CriteriaId in (1,2) and r.ReportId = @0", reportId.ToString()));
// and rsd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)", factorId.ToString()));
				foreach (DataRow dr in dt.Rows)
				{
					if ((int)dr["RecommendationTypeId"] != 4) continue;
					//ok, dr is your 4 (target).  Now spin through to find all 5s in the same fishery.

					decimal score = 5; //default to 5 if no other matches

					foreach (DataRow dr2 in dt.Rows)
					{
						if (dr["RecommendationId"].ToString() == dr2["RecommendationId"].ToString()) continue;
						if (dr["MethodId"].ToString() == dr2["MethodId"].ToString() && dr["LocationId"].ToString() == dr2["LocationId"].ToString() && dr["BodyOFWaterId"].ToString() == dr2["BodyOfWaterId"].ToString())
						{
							if (score <= Convert.ToDecimal(dr2["Score"])) continue;
							score = Convert.ToDecimal(dr2["Score"]);

						}
					}
					//Get GeoMean (score 1 x discard rate) and save it.
					if (dr["Score2"].ToString().Length == 0) continue;
					score *= (decimal)dr["Score2"];
					DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set score = @0, colorId = c.Id
from RecommendationScoreDetail cross join Color c WHERE @0 between c.MinScore and c.MaxScore
and RecommendationId = @1 and CriteriaId = 2", score.ToString(), dr["RecommendationId"].ToString()));
				}
			}

			if (criteriaId == 4)
			{
				//                DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set Score = AvgScore, ColorId = c.Id
				//			from RecommendationScoreDetail rsd inner join 
				//				(select avg(fs.Number) as AvgScore, frd.RecommendationId, frd.CriteriaId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = @1
				//					Where frd.RecommendationId in 
				//						(select RecommendationId from 
				//							(Select recommendationId, count(*) as num from factor f inner join factorRecommendationDetail frd on frd.FactorId = f.Id where frd.criteriaId = @1 group by RecommendationId) x
				//						where x.Num in (select count(*) from FactorType where CriteriaId = @1))
				//					and frd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)
				//					group by frd.RecommendationId, frd.CriteriaId)
				//				sub on rsd.RecommendationId = sub.RecommendationId and rsd.CriteriaId = sub.CriteriaId
				//				cross join Color c WHERE AvgScore between c.MinScore and c.MaxScore", factorId.ToString(), criteriaId.ToString()));

				//geometric mean of (4.1 + 4.2) and 4.3.
				DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set Score = Power((fs41.Number + fs42.Number) * fs43.Number, 0.5), ColorId = c.Id
From RecommendationScoreDetail rsd 
inner join (Select frd.RecommendationId, Number From FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = 4 and f.FactorTypeId = 7) fs41 on fs41.RecommendationId = rsd.Recommendationid
inner join (Select frd.RecommendationId, Number From FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = 4 and f.FactorTypeId = 8) fs42 on fs42.RecommendationId = rsd.Recommendationid
inner join (Select frd.RecommendationId, Number From FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = 4 and f.FactorTypeId = 9) fs43 on fs43.RecommendationId = rsd.Recommendationid
inner join FactorRecommendationDetail frd on frd.recommendationId = rsd.RecommendationId 
Cross Join color c 
where frd.factorId = @0 and rsd.CriteriaId = 4
and Power((fs41.Number + fs42.Number) * fs43.Number, 0.5) between c.MinScore and c.MaxScore", factorId.ToString()));
			}


			if (criteriaId == 5) //3.1.  Have to get as Datatable b/c complex rules for scoring.  Inner joins mean only those with all scores are shown.
			{
				DataTable dt = DB.GetDataTable(new SQLBuilder(@"create table #temp (recommendationId int, number int, sortorder int)
insert #temp (recommendationId, number, sortorder)
select frd.RecommendationId, fs.Number, ft.SortOrder from FactorRecommendationDetail frd inner join Factor f on f.Id = frd.FactorId left join FactorScore fs on fs.Id = f.FactorScoreId
	inner join FactorType ft on ft.id = f.FactorTypeId and ft.CriteriaId = 5
	 and frd.recommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)

Select rsd.Id, rsd.Score, rsd.ColorId, fs1.Number as Critical, fs2.Number as Score1, fs3.Number as Score2, fs4.Number as Score3,
fs5.Number as Score4, fs6.Number as Score5, fs7.Number as Score6, fs8.Number as Score7
From RecommendationScoreDetail rsd
inner join #temp fs1 on rsd.RecommendationId = fs1.RecommendationId and fs1.SortOrder = 1
inner join #temp fs2 on rsd.RecommendationId = fs2.RecommendationId and fs2.SortOrder = 2
inner join #temp fs3 on rsd.RecommendationId = fs3.RecommendationId and fs3.SortOrder = 3 --3.1.2
inner join #temp fs4 on rsd.RecommendationId = fs4.RecommendationId and fs4.SortOrder = 4
inner join #temp fs5 on rsd.RecommendationId = fs5.RecommendationId and fs5.SortOrder = 5
inner join #temp fs6 on rsd.RecommendationId = fs6.RecommendationId and fs6.SortOrder = 6
inner join #temp fs7 on rsd.RecommendationId = fs7.RecommendationId and fs7.SortOrder = 7
inner join #temp fs8 on rsd.RecommendationId = fs8.RecommendationId and fs8.SortOrder = 8
WHERE rsd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0) and rsd.CriteriaId = 5
Drop Table #temp", factorId.ToString()));

				foreach (DataRow dr in dt.Rows)
				{
					//calculate score (Score 2 (3.1.2 has an n/a value of -1)
					int score;
					int colorId;
					if (Convert.ToInt32(dr["Critical"]) == 1)
					{
						score = 0;
						colorId = 6; //critical
					}
					else if (Convert.ToInt32(dr["Score1"]) > 2 && (Convert.ToInt32(dr["Score2"]) > 2 || Convert.ToInt32(dr["Score2"]) == -1) && Convert.ToInt32(dr["Score3"]) > 2 && Convert.ToInt32(dr["Score4"]) > 2 && Convert.ToInt32(dr["Score5"]) > 2 && Convert.ToInt32(dr["Score6"]) > 2 && Convert.ToInt32(dr["Score7"]) > 2)
					{
						score = 5; //Meets or exceeds the standard of “highly effective” management for all seven subfactors
						colorId = 1;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 2 && (Convert.ToInt32(dr["Score2"]) > 2 || Convert.ToInt32(dr["Score2"]) == -1) && Convert.ToInt32(dr["Score3"]) > 1 && Convert.ToInt32(dr["Score4"]) > 1 && Convert.ToInt32(dr["Score5"]) > 1 && Convert.ToInt32(dr["Score6"]) > 1 && Convert.ToInt32(dr["Score7"]) > 1)
					{
/*Meets or exceeds all the standard for “moderately effective “ management for 
all seven subfactors
and
Meets or exceeds the standard of “highly effective” management for, at a 
minimum, “management strategy and implementation” and “recovery of stocks of concern”*/
						score = 4;
						colorId = 1;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 1 && (Convert.ToInt32(dr["Score2"]) > 1 || Convert.ToInt32(dr["Score2"]) == -1) && Convert.ToInt32(dr["Score3"]) > 1 && Convert.ToInt32(dr["Score4"]) > 1 && Convert.ToInt32(dr["Score5"]) > 1 && Convert.ToInt32(dr["Score6"]) > 1 && Convert.ToInt32(dr["Score7"]) > 1)
					{
						//Meets or exceeds all the standards for “moderately effective” management for all seven subfactors
						score = 3;
						colorId = 2;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 1 && (Convert.ToInt32(dr["Score2"]) > 1 || Convert.ToInt32(dr["Score2"]) == -1))
					{
						score = 2;
						colorId = 3;
					}
					else
					{
						score = 1;
						colorId = 3;
					}
					//update rsd if value has changed.
					if (Convert.ToInt32(dr["Score"]) != score)
					{
						DB.UpdateCmd(new SQLBuilder("Update RecommendationScoreDetail set Score = @0, colorId = @1 where Id = @2", score.ToString(), colorId.ToString(), dr["Id"].ToString()));
					}
				}


				//                DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set Score = case when subCrit.Number = 1 then 0 else AvgScore end, ColorId = c.Id
				//			from RecommendationScoreDetail rsd inner join 
				//				(select Avg(fs.Number) as AvgScore, frd.RecommendationId, frd.CriteriaId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = @1
				//					Where f.FactorTypeId NOT in (select Id from FactorType where SortOrder = 1)
				//					and frd.RecommendationId in 
				//						(select RecommendationId from 
				//							(Select recommendationId, count(*) as num from factor f inner join factorRecommendationDetail frd on frd.FactorId = f.Id where frd.criteriaId = @1 group by RecommendationId) x
				//						where x.Num in (8))
				//					and frd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)
				//					group by frd.RecommendationId, frd.CriteriaId)
				//				sub on rsd.RecommendationId = sub.RecommendationId and rsd.CriteriaId = sub.CriteriaId
				//
				//				inner join (select fs.Number, frd.RecommendationId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and f.FactorTypeId in (10))
				//				subCrit on rsd.RecommendationId = subCrit.RecommendationId
				//
				//				cross join Color c WHERE case when subCrit.Number = 1 then 0 else AvgScore end between c.MinScore and c.MaxScore", factorId.ToString(), criteriaId.ToString()));
			}

			if (criteriaId == 6) //3.2
			{
				DataTable dt = DB.GetDataTable(new SQLBuilder(@"create table #temp (recommendationId int, number int, sortorder int)
insert #temp (recommendationId, number, sortorder)
select frd.RecommendationId, fs.Number, ft.SortOrder from FactorRecommendationDetail frd inner join Factor f on f.Id = frd.FactorId left join FactorScore fs on fs.Id = f.FactorScoreId
	inner join FactorType ft on ft.id = f.FactorTypeId and ft.CriteriaId = 6
	 and frd.recommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)

Select rsd.Id, isnull(rsd.Score,-1) as Score, rsd.ColorId, fs1.Number as AllRetained, isnull(fs2.Number,0) as Critical, isnull(fs3.Number,-1) as Score1, isnull(fs4.Number,-1) as Score2,
isnull(fs5.Number,-1) as Score3, isnull(fs6.Number,-1) as Score4
From RecommendationScoreDetail rsd
inner join #Temp fs1 on rsd.RecommendationId = fs1.RecommendationId and fs1.SortOrder = 1
left join #Temp fs2 on rsd.RecommendationId = fs2.RecommendationId and fs2.SortOrder = 2
left join #Temp fs3 on rsd.RecommendationId = fs3.RecommendationId and fs3.SortOrder = 3
left join #Temp fs4 on rsd.RecommendationId = fs4.RecommendationId and fs4.SortOrder = 4
left join #Temp fs5 on rsd.RecommendationId = fs5.RecommendationId and fs5.SortOrder = 5
left join #Temp fs6 on rsd.RecommendationId = fs6.RecommendationId and fs6.SortOrder = 6
WHERE rsd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0) and rsd.CriteriaId = 6
Drop table #Temp", factorId.ToString()));

				foreach (DataRow dr in dt.Rows)
				{
					//calculate score
					int score;
					int colorId;
					if (Convert.ToInt32(dr["Critical"]) == 1)
					{
						score = 0;
						colorId = 6; //critical
					}
					if (Convert.ToInt32(dr["AllRetained"]) == 1)
					{
						score = -1;
						colorId = 1;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 2 && Convert.ToInt32(dr["Score2"]) > 2 && Convert.ToInt32(dr["Score3"]) > 2 && Convert.ToInt32(dr["Score4"]) > 2)
					{
						score = 5;
						colorId = 1;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 2 && Convert.ToInt32(dr["Score2"]) > 1 && Convert.ToInt32(dr["Score3"]) > 1 && Convert.ToInt32(dr["Score4"]) > 1)
					{
						score = 4;
						colorId = 1;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 1 && Convert.ToInt32(dr["Score2"]) > 1 && Convert.ToInt32(dr["Score3"]) > 1 && Convert.ToInt32(dr["Score4"]) > 1)
					{
						score = 3;
						colorId = 2;
					}
					else if (Convert.ToInt32(dr["Score1"]) > 1)
					{
						score = 2;
						colorId = 3;
					}
					else
					{
						score = 1;
						colorId = 3;
					}

					//update rsd if value has changed.
					if (dr["Score"] != DBNull.Value && Convert.ToInt32(dr["Score"]) != score)
					{
						DB.UpdateCmd(new SQLBuilder("Update RecommendationScoreDetail set Score = case when @0 = -1 then null else @0 end, ColorId = @1 where Id = @2", score.ToString(), colorId.ToString(), dr["Id"].ToString()));
					}
				}

				if (criteriaId == 5 || criteriaId == 6)
				{
					//Update the 3 score.
					try //added this b/c it fails if there are nulls or 0s 
					{
						DataTable dtCrit3 = DB.GetDataTable(new SQLBuilder(@"select rsd.RecommendationId, exp(avg(log(score))) as Score, min(score) as MinScore from recommendationScoreDetail rsd inner join factorrecommendationDetail frd on frd.RecommendationId =rsd.RecommendationId and frd.CriteriaId in (5,6) and rsd.CriteriaId in (5,6)
where frd.FactorId = @0 group by rsd.RecommendationId having count(*)=2", factorId.ToString()));

						foreach (DataRow dr in dtCrit3.Rows)
						{
							decimal score = Convert.ToDecimal(dr["Score"]);
							decimal minScore = Convert.ToDecimal(dr["MinScore"]);
							int recId = Convert.ToInt32(dr["RecommendationId"]);
							int color = 3; //red
							if (score > 2.2M && minScore >= 2) color = 2; //yellow
							if (score > 3.2M) color = 1; //Green
							if (minScore == 0) color = 6; //Critical

							DB.UpdateCmd(new SQLBuilder("update RecommendationScoreDetail set Score = @0, ColorId = @1 where recommendationId = @2 and CriteriaId = 3", score.ToString(), color.ToString(), recId.ToString()));
						}
					}
					catch (Exception ex)
					{ }
				}



				//                DB.UpdateCmd(new SQLBuilder(@"Update RecommendationScoreDetail set Score = case when subCrit.Number = 1 then 0 else AvgScore end, ColorId = c.Id
				//			from RecommendationScoreDetail rsd inner join 
				//				(select Avg(fs.Number) as AvgScore, frd.RecommendationId, frd.CriteriaId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and frd.CriteriaId = @1
				//					Where f.FactorTypeId NOT in (select Id from FactorType where SortOrder in (1,2))
				//					and frd.RecommendationId in 
				//						(select RecommendationId from 
				//							(Select recommendationId, count(*) as num from factor f inner join factorRecommendationDetail frd on frd.FactorId = f.Id where frd.criteriaId = @1 group by RecommendationId) x
				//						where x.Num in (6))
				//					and frd.RecommendationId in (select RecommendationId from FactorRecommendationDetail where FactorId = @0)
				//					group by frd.RecommendationId, frd.CriteriaId)
				//				sub on rsd.RecommendationId = sub.RecommendationId and rsd.CriteriaId = sub.CriteriaId
				//
				//				inner join (select fs.Number, frd.RecommendationId from FactorRecommendationDetail frd inner join Factor f on f.Id=frd.FactorId inner join FactorScore fs on fs.Id = f.FactorScoreId and f.FactorTypeId in (19))
				//				subCrit on rsd.RecommendationId = subCrit.RecommendationId
				//
				//				cross join Color c WHERE case when subCrit.Number = 1 then 0 else AvgScore end between c.MinScore and c.MaxScore", factorId.ToString(), criteriaId.ToString()));
			}

			//Now update Recommendation score if applicable (note that this updates all recs.  It's fast, so it shouldn't matter)
			DB.UpdateCmd(new SQLBuilder(@"Update Recommendation set score = x.Score
from Recommendation r inner join 
(select RecommendationId, exp(avg(log(cast(Score as real)))) as Score
	from (select * from RecommendationScoreDetail where score>0 and CriteriaId between 1 and 4) x group by recommendationId having count(*) = 4) x 
	on x.RecommendationId = r.Id
where r.RecommendationTypeId = 4"));

			//Color is a little more complex, there are rules, so calculate that here.
			DataTable dtFinal = DB.GetDataTable(new SQLBuilder(@"select r.Id as RecommendationId, isnull(r.Score,0) as Score, 
rsd1.ColorId as Color1, rsd2.ColorId as Color2, rsd3.ColorId as Color3, rsd4.ColorId as Color4,
rsd5.Score as Score5, rsd6.Score as Score6
from recommendation r 
inner join RecommendationScoreDetail rsd1 on rsd1.RecommendationId = r.Id and rsd1.CriteriaId = 1
inner join RecommendationScoreDetail rsd2 on rsd2.RecommendationId = r.Id and rsd2.CriteriaId = 2
inner join RecommendationScoreDetail rsd3 on rsd3.RecommendationId = r.Id and rsd3.CriteriaId = 3
inner join RecommendationScoreDetail rsd4 on rsd4.RecommendationId = r.Id and rsd4.CriteriaId = 4
inner join RecommendationScoreDetail rsd5 on rsd5.RecommendationId = r.Id and rsd5.CriteriaId = 5
inner join RecommendationScoreDetail rsd6 on rsd6.RecommendationId = r.Id and rsd6.CriteriaId = 6
where reportId = @0 and recommendationTypeId = 4", reportId.ToString()));

			foreach (DataRow dr in dtFinal.Rows)
			{
				decimal score = (decimal)dr["Score"];
				int color1 = (int)dr["Color1"];
				int color2 = (int)dr["Color2"];
				int color3 = (int)dr["Color3"];
				int color4 = (int)dr["Color4"];

				decimal Score5 = (decimal)nz(dr["Score5"],5M); //null = all species retained, so fake it above 1.
				decimal Score6 = (decimal)nz(dr["Score6"],5M);

				int criticalCount = 0;
				if (color1 == 6) criticalCount++;
				if (color2 == 6) criticalCount++;
				if (color3 == 6) criticalCount++;
				if (color4 == 6) criticalCount++;

				int redCount = 0;
				if (color1 == 3) redCount++;
				if (color2 == 3) redCount++;
				if (color3 == 3) redCount++;
				if (color4 == 3) redCount++;


				if (score >= 3.2M && redCount == 0 && criticalCount == 0) //Green: Final Score between 3.2 and 5, and no Red Criteria, and no Critical scores
					DB.UpdateCmd(new SQLBuilder("Update Recommendation set ColorId = 1 where Id = @0", dr["RecommendationId"].ToString()));
				else if (score >= 2.2M && Score5 > 1 && Score6 > 1 && redCount < 2 && criticalCount < 1) //Yellow: Final score between 2.2 and 3.199, and neither Criterion 3.1 nor Criterion 3.2 is scored Poor (1) or below and no more than one Red Criterion, and no Critical scores
					DB.UpdateCmd(new SQLBuilder("Update Recommendation set ColorId = 2 where Id = @0", dr["RecommendationId"].ToString()));
				else //Red: Final Score between 0 and 2.199, or either Criterion 3.1 or Criterion 3.2 is scored Very High Concern (1) or below, or two or more Red Criteria, or one or more Critical scores. 
					DB.UpdateCmd(new SQLBuilder("Update Recommendation set ColorId = 3 where Id = @0", dr["RecommendationId"].ToString()));
			}

			setLastUpdated(reportId);
		}

		public int FactorCommentAdd(int reportId, UserEntity user, int factorId, string comment, int parentFactorCommentId, string path)
		{
			return DB.InsertCmd(new SQLBuilder("insert FactorComment(FactorId, Comment, UserId, DCreated, ParentFactorCommentId, BlobKey) values(@0,@1,@2,getDate(),@3,@4)", factorId.ToString(), comment, user.Id.ToString(), parentFactorCommentId.ToString(), path), true);
			setLastUpdated(reportId);
		}

		public void FactorCommentUpdate(int reportId, int factorCommentId, string comment, string path)
		{
			DB.UpdateCmd(new SQLBuilder("Update FactorComment set Comment=@0, BlobKey = @1 where Id = @2 ", comment, path, factorCommentId.ToString()));
			setLastUpdated(reportId);
		}

		public void FactorCommentUpdateStatus(int reportId, int factorCommentId, bool isReviewed)
		{
			DB.UpdateCmd(new SQLBuilder("Update FactorComment set Reviewed=@1 where Id = @0", factorCommentId.ToString(), Convert.ToInt64(isReviewed).ToString()));
			setLastUpdated(reportId);
		}

		public void FactorCommentDelete(int reportId, int factorCommentId)
		{
			DB.DeleteCmd(new SQLBuilder("Delete from FactorComment where Id = @0", factorCommentId.ToString()));
			setLastUpdated(reportId);
		}

		public IList<FactorCommentEntity> FactorCommentGet(int factorId)
		{
			//Get dictionary of UserEntities
			DataTable dt2 = DB.GetDataTable(new SQLBuilder("select * from [User] where Id in (Select UserId from FactorComment where FactorId = @0)", factorId.ToString()));
			Dictionary<int, UserEntity> users = new Dictionary<int, UserEntity>();
			foreach (DataRow dr in dt2.Rows)
			{
				UserEntity user = new UserEntity();
				Framework.EntityPopulate(user, dr);
				users.Add(user.Id, user);
			}

			DataTable dt = DB.GetDataTable(new SQLBuilder("select Id as FactorCommentId, FactorId, Comment, UserId, DCreated as DateCreated, ParentFactorCommentId, Reviewed, BlobKey as Path FROM FactorComment where FactorId = @0 order by case when ParentFactorCommentId = 0 then Id else ParentFactorCommentId end, DCreated", factorId.ToString()));
			List<FactorCommentEntity> factorComments = new List<FactorCommentEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				FactorCommentEntity factorComment = new FactorCommentEntity();
				Framework.EntityPopulate(factorComment, dr);
				factorComment.FactorCommentId = Convert.ToInt32(dr["FactorCommentId"]);
				factorComment.User = users[Convert.ToInt32(dr["UserId"])];
				factorComments.Add(factorComment);
			}
			return factorComments;
		}

		//private void SubmitFactorCommentAttachment(int factorCommentId, string path)
		//{
		//    DB.InsertCmd(new SQLBuilder("Insert Attachment(TableId, PrimaryKey, BlobKey) values (2, @0,@1)", factorCommentId.ToString(), path));
		//}

		//private List<string> GetFactorCommentAttachments(int factorCommentId)
		//{
		//    return DB.GetList<string>(new SQLBuilder("Select BlobKey from Attachment where TableId = 2 and PrimaryKey = @0", factorCommentId.ToString()));
		//}

		//private void DeleteFactorCommentAttachment(int factorCommentId, string path)
		//{
		//    DB.DeleteCmd(new SQLBuilder("Delete from Attachment where TableId = 2 and PrimaryKey = @0 and blobKey = @1", factorCommentId.ToString(), path));
		//}

		#endregion

		#region References
		/// <summary>
		/// 
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="filter"></param>
		/// <param name="recordsPerPage">eg: 10, 25</param>
		/// <param name="page">1 to x</param>
		/// <returns></returns>
		public IList<ReferenceEntity> ReferenceList(int reportId, string filter)
		{
			/*DataTable dt = DB.GetDataTable(new SQLBuilder(@"WITH result_set AS (SELECT ROW_NUMBER() OVER (ORDER BY ShortName) AS [row_number], 
Id as ReferenceId, ShortName, Bibliography from Reference where ReportId = @0 and (@1='%%' or (ReferenceKey like @1 or ShortName like @1 or Bibliography like @1))) 
SELECT  * FROM result_set WHERE [row_number] BETWEEN @2 AND @3", reportId.ToString(), "%" + filter + "%",startRecord.ToString(), endRecord.ToString()));*/

			DataTable dt = DB.GetDataTable(new SQLBuilder(@"Select Id as ReferenceId, ShortName, Bibliography, Path, IsURL, Year, Authors from Reference where ReportId = @0 and (@1='%%' or (Authors like @1) or (Bibliography like @1)) Order by ShortName", reportId.ToString(), "%" + filter.Trim() + "%"));

			List<ReferenceEntity> References = new List<ReferenceEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				ReferenceEntity Reference = new ReferenceEntity();
				Framework.EntityPopulate(Reference, dr);
				// SQ1 nm: using old entity-less dev dB, must end-run Framework.EntityPopulate(). to restore:
				//  - uncomment first commented line 
				//  - remove lines below to last commented line "//-----------------"
				//Reference.Bibliography = dr.Field<string>("Bibliography");
				//Reference.ShortName = dr.Field<string>("ShortName");
				//Reference.ReferenceId = dr.Field<int>("ReferenceId");
				//-------------------------
				Reference.isUrl = dr.Field<bool>("isUrl");
				References.Add(Reference);
			}
			return References;

		}

		public int GetReferencePageCount(int reportId, int recordsPerPage)
		{
			int records = DB.GetIntNZ(new SQLBuilder("Select count(*) from Reference where ReportId = @0", reportId.ToString()));
			return Convert.ToInt32(Math.Ceiling((double)records / (double)recordsPerPage));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="shortName"></param>
		/// <param name="bibliography"></param>
		/// <returns>ReferenceId.  -1 means ShortName already exists in this report</returns>
		public int ReferenceAdd(int reportId, string shortName, string bibliography, string authors, string year, string path, bool isURL)
		{
			//trap for dupe shortname
			int count = DB.GetIntNZ(new SQLBuilder("Select count(*) from Reference where ReportId = @0 and ShortName =@1", reportId.ToString(), shortName));
			if (count > 0) return -1;

			setLastUpdated(reportId);

			return DB.InsertCmd(new SQLBuilder("Insert Reference(ReportId, ShortName, Bibliography, authors, year, path, IsURL) values (@0,@1,@2,@3,@4,@5,@6)", reportId.ToString(), shortName, bibliography, authors, year, path, isURL ? "1" : "0"), true);
		}

		public bool ReferenceUpdate(int reportId, int referenceId, string shortName, string bibliography, string authors, string year, string path, bool isURL)
		{
			//trap for dupe shortname (excepting ReferenceId)
			int count = DB.GetIntNZ(new SQLBuilder("Select count(*) from Reference where ReportId = @0 and ShortName = @1 and Id <> @2", reportId.ToString(), shortName, referenceId.ToString()));
			if (count > 0) return false;

			string oldShortName = DB.GetString(new SQLBuilder("Select shortName from Reference where Id = @0", referenceId.ToString()));

			DB.UpdateCmd(new SQLBuilder("Update Reference set ShortName =@0, Bibliography=@1, authors=@2, year=@3, path = @4, IsURL = @5 where Id = @6", shortName, bibliography, authors, year, path, isURL ? "1" : "0", referenceId.ToString()));

			if (oldShortName != shortName)
			{
				oldShortName = "{" + oldShortName + "}";
				shortName = "{" + shortName + "}";

				//do a find and replace throughout the app
				DB.UpdateCmd(new SQLBuilder("Update Factor set KeyInfo = Replace(KeyInfo, @0, @1) from Factor where ReportId = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Factor set Rationale = Replace(Rationale, @0, @1) from Factor where ReportId = @2", oldShortName, shortName, reportId.ToString()));

				DB.UpdateCmd(new SQLBuilder("Update Report set Scope = Replace(Scope, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set Intro = Replace(Intro, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set Conclusion = Replace(Conclusion, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set ExecSummary = Replace(ExecSummary, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set SpeciesOverview = Replace(SpeciesOverview, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set SupplementalInfo = Replace(SupplementalInfo, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set Acknowledgements = Replace(Acknowledgements, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set ReviewSchedule = Replace(ReviewSchedule, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set ProductionStatistics = Replace(ProductionStatistics, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set ImportanceToMarket = Replace(ImportanceToMarket, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set CommonMarketNames = Replace(CommonMarketNames, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set ProductForms = Replace(ProductForms, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set TextSummary = Replace(TextSummary, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
				DB.UpdateCmd(new SQLBuilder("Update Report set EcoCertification = Replace(EcoCertification, @0, @1) From Report where Id = @2", oldShortName, shortName, reportId.ToString()));
			}

			//DB.UpdateCmd(new SQLBuilder("Update Reference set ShortName =@0, Bibliography=@1 where Id = @2", shortName, bibliography, referenceId.ToString()));
			// SQ1 dev - for staging uncomment line above, delete this and line below
			setLastUpdated(reportId);
			return true;
		}

		public void ReferenceDelete(int reportId, int referenceId)
		{
			DB.DeleteCmd(new SQLBuilder("Delete from Reference where Id = @0", referenceId.ToString()));
			setLastUpdated(reportId);
			//This will leave orphaned references in text, but that's ok
		}

		//SQ1 nm
		public ReferenceEntity ReferenceGet(int reportid, int referenceId)
		{
			ReferenceEntity Reference = new ReferenceEntity();
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select * from Reference where ReportId = @0 AND Id = @1", reportid.ToString(), referenceId.ToString()));
			if (dt.Rows.Count > 0)
			{
				Framework.EntityPopulate(Reference, dt.Rows[0]);
				Reference.ReferenceId = dt.Rows[0].Field<int>("Id");
				Reference.isUrl = dt.Rows[0].Field<bool>("isUrl");
			}
			return Reference;
		}

        public ReferenceEntity GetReferenceByShortName(int reportid, string shortName)
        {
            ReferenceEntity Reference = new ReferenceEntity();
            DataTable dt = DB.GetDataTable(new SQLBuilder("Select * from Reference where ReportId = @0 AND ShortName = @1", reportid.ToString(), shortName.ToString()));
            if (dt.Rows.Count > 0)
            {
                Framework.EntityPopulate(Reference, dt.Rows[0]);
                Reference.ReferenceId = dt.Rows[0].Field<int>("Id");
                Reference.isUrl = dt.Rows[0].Field<bool>("isUrl");
            }
            return Reference;
        }


		/// <summary>
		/// Replaces coded references with the full reference (eg: {123} -> Banks 2012)
		/// </summary>
		/// <param name="input"></param>
		/// <param name="referenceList">call ReferenceList(), and then pass the resulting list in every time ReferenceReplace is used.</param>
		/// <returns></returns>
		public string ReferenceReplace(string input, IList<ReferenceEntity> referenceList)
		{
			StringBuilder sb = new StringBuilder(input);

			foreach (ReferenceEntity re in referenceList)
			{
				sb.Replace("{" + re.ReferenceId + "}", re.ShortName);
			}
			return sb.ToString();
		}


		#endregion

        #region Images
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="filter"></param>
        /// <param name="recordsPerPage">eg: 10, 25</param>
        /// <param name="page">1 to x</param>
        /// <returns></returns>
        public IList<ImageEntity> ImageList(int reportId, string filter)
        {
			string sql = "Select Id, ReportId, Name, Description, Path from [Image] where reportId = @0";
			if (filter.Length>0) sql += " and (name like @1 OR description like @1)";
			sql += " ORDER BY Id DESC";
			DataTable dt = DB.GetDataTable(new SQLBuilder(sql, reportId.ToString(), "%" + filter + "%"));

            IList<ImageEntity> images = new List<ImageEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				ImageEntity entity = new ImageEntity();
				entity.ImageId = Convert.ToInt32(dr["Id"]);
				entity.ReportId = Convert.ToInt32(dr["ReportId"]);
				entity.Name = dr["Name"].ToString();
				entity.Description = dr["Description"].ToString();
				entity.Path = dr["Path"].ToString();
				images.Add(entity);
			}
			return images;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="shortName"></param>
        /// <param name="bibliography"></param>
        /// <returns>ReferenceId.  -1 means ShortName already exists in this report</returns>
        public int ImageAdd(int reportId, string name, string description, string path) 
        {
			if (DB.GetInt(new SQLBuilder("select Id from [Image] where name = @0 and reportId = @1", name, reportId.ToString())) > 0) return -1;
			return DB.InsertCmd(new SQLBuilder("Insert Image(ReportId, Name, Description, Path) values (@0,@1,@2,@3)", reportId.ToString(), name, description, path), true);
        }

		//ReportId is ignored here.
        public void ImageUpdate(int reportId, int id, string name, string description, string path)
        {
			DB.UpdateCmd(new SQLBuilder("Update [Image] set Name = @0, Description = @1, Path = @2 where Id = @3", name, description, path, id.ToString()));
        }

        public void ImageDelete(int reportId, int id)
        {
			DB.DeleteCmd(new SQLBuilder("Delete from [Image] where Id = @0", id.ToString()));
			setLastUpdated(reportId);
        }

        public ImageEntity ImageGet(int reportId, int id)
        {
			ImageEntity entity = new ImageEntity();
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, ReportId, Name, Description, Path from [Image] where Id = @0", id.ToString()));
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                entity.ImageId = Convert.ToInt32(dr["Id"]);
                entity.ReportId = Convert.ToInt32(dr["ReportId"]);
                entity.Name = dr["Name"].ToString();
                entity.Description = dr["Description"].ToString();
                entity.Path = dr["Path"].ToString();
            }
			return entity;
        }



        #endregion

        public FactorColumnEntity FactorColumnEntityPopulate(FactorColumnEntity entity, DataRow dr, int number)
		{
			if (entity == null) entity = new FactorColumnEntity();
			string num = number.ToString();
			entity.FactorId = Convert.ToInt32(dr["Factor" + num + "Id"]);
			entity.FactorLabel = dr["Factor" + num + "Label"].ToString();
			entity.FactorTitle = dr["Factor" + num + "Title"].ToString();
			entity.FactorKeyInfo = dr["Factor" + num + "KeyInfo"].ToString();
			entity.FactorScore = dr["Factor" + num + "Score"].ToString();
			entity.FactorScoreDescription = dr["Factor" + num + "ScoreDescription"].ToString();
			entity.FactorCommentCount = Convert.ToInt32(dr["Factor" + num + "CommentCount"]);
            try
            {
                entity.FactorDetailedRationale = dr["Factor" + num + "Rationale"].ToString();
            }
            catch(Exception){

            }
            return entity;
		}

		#region Criteria 1

		public IList<Criteria1Entity> StockHealthOverview(int reportId)
		{
			return getCriteria1Data(reportId, false);
		}

		public IList<Criteria1Entity> Criteria1Overview(int reportId)
		{
			return getCriteria1Data(reportId, true);
		}

		public DataTable Criteria1ReportData(int reportId)
		{
			DataTable dt = criteria1DataTable(reportId, true);

			////Find and replace references on Keyinfo and Rationale
			//IList<ReferenceEntity> refs = ReferenceList(reportId, "");
			//foreach (DataRow dr in dt.Rows)
			//{
			//    dr["Factor1KeyInfo"] = ReferenceReplace(dr["Factor1KeyInfo"].ToString(), refs);
			//    dr["Factor2KeyInfo"] = ReferenceReplace(dr["Factor2KeyInfo"].ToString(), refs);
			//    dr["Factor3KeyInfo"] = ReferenceReplace(dr["Factor3KeyInfo"].ToString(), refs);
			//}

			return dt;
		}

		private IList<Criteria1Entity> getCriteria1Data(int reportId, bool showRecsOnly)
		{
			//Species, region, method, inherent resilience, stock status, fishing mortality, score
			DataTable dt = criteria1DataTable(reportId, showRecsOnly);

			List<Criteria1Entity> criteria1s = new List<Criteria1Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria1Entity criteria1 = new Criteria1Entity();
				Framework.EntityPopulate(criteria1, dr);
                criteria1.Factor1 = FactorColumnEntityPopulate(criteria1.Factor1, dr, 1);
                criteria1.Factor2 = FactorColumnEntityPopulate(criteria1.Factor2, dr, 2);
                criteria1.Factor3 = FactorColumnEntityPopulate(criteria1.Factor3, dr, 3);
				criteria1s.Add(criteria1);
			}
			return criteria1s;
		}

		private DataTable criteria1DataTable(int reportId, bool showRecsOnly)
		{
			StringBuilder sb = new StringBuilder(@"select rec.Id as RecId, Upper(s.CommonName) as CommonName, s.Genus + ' ' + s.Species as ScientificName,
l.Description as Location, bow.Description as BodyOfWater, m.Method,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, cast(f1.Rationale as varchar(max)) as Factor1Rationale, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, cast(f2.Rationale as varchar(max)) as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, cast(f3.Rationale as varchar(max)) as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
rsd.Score, c.Color
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 1
	left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 1 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 1 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 1 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id");
			if (showRecsOnly) sb.Append(" WHERE rec.RecommendationTypeId = 4"); // 4 = Yes.
			sb.Append(" ORDER BY s.CommonName, l.Description, bow.Description, m.Method");
			return DB.GetDataTable(new SQLBuilder(sb.ToString(), reportId.ToString()));
		}

		#endregion

		#region Criteria 2

		public IList<Criteria2Entity> Criteria2Overview(int reportId)
		{
			DataTable dt = criteria2DataTable(reportId);

			List<Criteria2Entity> criteria2s = new List<Criteria2Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria2Entity criteria2 = new Criteria2Entity();
				Framework.EntityPopulate(criteria2, dr);
                criteria2.Factor1 = FactorColumnEntityPopulate(criteria2.Factor1, dr, 1);
                criteria2.Factor2 = FactorColumnEntityPopulate(criteria2.Factor2, dr, 2);
                criteria2.Factor3 = FactorColumnEntityPopulate(criteria2.Factor3, dr, 3);
				criteria2s.Add(criteria2);
			}
			return criteria2s;
		}

		public DataTable Criteria2ReportData(int reportId)
		{
			return criteria2DataTable(reportId);
		}

		/*l.Description as Location, bow.Description as BodyOfWater, m.Method,
isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0) as Factor3CommentCount,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, f4.KeyInfo as Factor4KeyInfo, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0) as Factor4CommentCount,
		  */

		public DataTable xcriteria2DataTable(int reportId)
		{
			//grab lowest scored Bycatch for that Region/Method
			string sql = @"Select rec.Id as RecId, Upper(s.CommonName) as CommonName, s.Genus + ' ' + s.Species as ScientificName,
l.Description as Location, bow.Description as BodyOfWater, m.Method,
CommonName + ', ' + isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, f1.Rationale as Factor1Rationale, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, f2.Rationale as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, f3.Rationale as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0) as Factor3CommentCount,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, f4.KeyInfo as Factor4KeyInfo, f4.Rationale as Factor4Rationale, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0) as Factor4CommentCount,
rsd1.score as HeaderSubScore, rsd2.Score as HeaderScore, fs4.Number as HeaderDiscardRate, fs4.Description as HeaderDiscardRateDescription, rsd1.Score as SubScore, c.Color as HeaderScoreColor, case when qryMin.MethodId is null then 0 else 1 end as IsLowest
from Recommendation rec
inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0 
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
left join RecommendationScoreDetail rsd1 on rec.Id = rsd1.RecommendationId and rsd1.CriteriaId = 1
left join RecommendationScoreDetail rsd2 on rec.Id = rsd2.RecommendationId and rsd2.CriteriaId = 2
LEFT JOIN (
		Select rec2.SpeciesId, Rec2.LocationId, rec2.BodyOfWaterId, rec2.MethodId, Min(isnull(rsd.Score,9.0)) as Score 
		from RecommendationScoreDetail rsd inner join Recommendation rec on rec.Id = rsd.RecommendationId 
		inner join Recommendation rec2 on rec.locationId = rec2.locationId and rec.BodyOfWaterId = rec2.BodyOfWaterId and rec.MethodId = rec2.MethodId
		and rec.speciesId <> rec2.speciesId	where rsd.CriteriaId = 2 and rec.ReportId = @0
		Group by Rec2.LocationId, rec2.BodyOfWaterId, rec2.MethodId, rec2.SpeciesId) qryMin 
	on isnull(qryMin.LocationId,0) = isnull(l.Id,0) and isnull(qryMin.bodyOfWaterId,0) = isnull(bow.Id,0) and qryMin.MethodId = m.Id and qryMin.SpeciesId = s.Id and qryMin.Score = rsd2.Score
left join Color c on c.Id = rsd2.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 1 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 1 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 1 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.id = f4.FactorTypeId and ft4.CriteriaId = 2 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id ";
			sql += "ORDER BY l.Description, bow.Description, m.Method, rsd2.score, s.CommonName"; //bb switched common, score to score, common.

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		public DataTable criteria2DataTable(int reportId)
		{
			//grab lowest scored Bycatch for that Region/Method
			//Factor4 = Discard Rate
			string sql = @"Select rec.Id as RecId, 
Upper(s.CommonName) as CommonName, s.Genus + ' ' + s.Species as ScientificName,
Upper(s4.CommonName) as TargetCommonName, s4.Genus + ' ' + s4.Species as TargetScientificName,
l.Description as Location, bow.Description as BodyOfWater, m.Method,
s4.CommonName + ': ' + isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, cast(f1.Rationale as varchar(max)) as Factor1Rationale, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, cast(f2.Rationale as varchar(max)) as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, cast(f3.Rationale as varchar(max)) as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, f4.KeyInfo as Factor4KeyInfo, cast(f4.Rationale as varchar(max)) as Factor4Rationale, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor4CommentCount,
qryMin.score as HeaderSubScore, rsd2.Score as HeaderScore, fs4.Number as HeaderDiscardRate, fs4.Description as HeaderDiscardRateDescription, rsd1.Score as SubScore, c.Color as HeaderScoreColor, case when qryMin.MethodId is null then 0 else 1 end as IsLowest
FROM Recommendation rec4
inner join Recommendation rec on isnull(rec4.LocationId,0) = isnull(rec.LocationId,0) and isnull(rec4.BodyOfWaterId,0) = isnull(rec.BodyOfWaterId,0) and isnull(rec4.MethodId,0) = isnull(rec.MethodId,0) and rec4.ReportId = @0 and rec4.RecommendationTypeId = 4
inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0 
inner join Species s4 on s4.Id = rec4.SpeciesId
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
left join RecommendationScoreDetail rsd1 on rec.Id = rsd1.RecommendationId and rsd1.CriteriaId = 1
left join RecommendationScoreDetail rsd2 on rec4.Id = rsd2.RecommendationId and rsd2.CriteriaId = 2
LEFT JOIN (Select Rec4.Id as RecommendationId, Rec4.LocationId, rec4.BodyOfWaterId, rec4.MethodId, Min(isnull(rsd.Score,5.0)) as Score 
		from Recommendation rec4 
		left join Recommendation rec5  on isnull(rec4.LocationId,0) = isnull(rec5.LocationId,0) and isnull(rec4.BodyOfWaterId,0) = isnull(rec5.BodyOfWaterId,0) and isnull(rec4.MethodId,0) = isnull(rec5.MethodId,0)
		left join RecommendationScoreDetail rsd on rec5.Id = rsd.RecommendationId and rsd.CriteriaId = 1
		and rec4.Id <> rec5.Id and rec5.ReportId = @0
		where rec4.RecommendationTypeId = 4 and rec4.ReportId = @0 Group by rec4.Id, Rec4.LocationId, rec4.BodyOfWaterId, rec4.MethodId) qryMin 
	on qryMin.RecommendationId = rec4.Id
left join Color c on c.Id = rsd2.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 1 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 1 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 1 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.Id = f4.FactorTypeId and ft4.CriteriaId = 2 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id ";
			sql += "ORDER BY s4.CommonName, l.Description, bow.Description, m.Method, rsd1.score, s.CommonName"; //bb switched common, score to score, common.

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}

		public DataTable AppendixAReportData(int reportId)
		{
			return criteria2DataTableOLD(reportId, false);
		}

		private DataTable criteria2DataTableOLD(int reportId, bool onlyShowLimitingFactor)
		{
			//grab lowest scored Bycatch for that Region/Method
			//Factor4 = Discard Rate
			string sql = @"Select rec.Id as RecId, Upper(s.CommonName) as CommonName, s.Genus + ' ' + s.Species as ScientificName,
l.Description as Location, bow.Description as BodyOfWater, m.Method,
isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
f1.Id as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, f1.Rationale as Factor1Rationale, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0) as Factor1CommentCount,
f2.Id as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, f2.Rationale as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0) as Factor2CommentCount,
f3.Id as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, f3.Rationale as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0) as Factor3CommentCount,
f4.Id as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, f4.KeyInfo as Factor4KeyInfo, f4.Rationale as Factor4Rationale, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0) as Factor4CommentCount,
rsd.Score, c.Color, case when qryMin.MethodId is null then 0 else 1 end as IsLowest
from Recommendation rec
inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0 
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 2 
";
			//if (onlyShowLimitingFactor) 
			//	sql +=" INNER JOIN ";
			//else
			sql += " LEFT JOIN ";

			sql += @"(
		Select Rec.LocationId, rec.BodyOfWaterId, rec.MethodId, Min(isnull(rsd.Score,9.0)) as Score from RecommendationScoreDetail rsd inner join Recommendation rec on rec.Id = rsd.RecommendationId 
		where rec.ReportId = @0 Group by Rec.LocationId, rec.BodyOfWaterId, rec.MethodId) qryMin 
	on qryMin.LocationId = l.Id and qryMin.bodyOfWaterId = bow.Id and qryMin.MethodId = m.Id and qryMin.Score = rsd.Score
left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 1 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 1 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 1 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.id = f4.FactorTypeId and ft4.CriteriaId = 2 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id ";
			//if (onlyShowLimitingFactor)
			sql += "ORDER BY l.Description, bow.Description, m.Method, rsd.score, s.CommonName"; //bb switched common, score to score, common.

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		public IList<Criteria21Entity> Criteria21Overview(int reportId)
		{
			DataTable dt = criteria21DataTable(reportId, true);

			List<Criteria21Entity> criteria2s = new List<Criteria21Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria21Entity criteria21 = new Criteria21Entity();
				Framework.EntityPopulate(criteria21, dr);
                criteria21.Factor1 = FactorColumnEntityPopulate(criteria21.Factor1, dr, 1);
                criteria21.Factor2 = FactorColumnEntityPopulate(criteria21.Factor2, dr, 2);
                criteria21.Factor3 = FactorColumnEntityPopulate(criteria21.Factor3, dr, 3);

				criteria2s.Add(criteria21);
			}
			return criteria2s;
		}

		public DataTable Criteria21ReportData(int reportId)
		{
			return criteria21DataTable(reportId, false);
		}

		private DataTable criteria21DataTable(int reportId, bool onlyShowLimitingFactor)
		{
			//grab lowest scored Bycatch for that Region/Method
			//Factor4 = Discard Rate
			string sql = @"Select rec.Id as RecId, Upper(s.CommonName) as CommonName, s.Genus + ' ' + s.Species as ScientificName,
l.Description as Location, bow.Description as BodyOfWater, m.Method,
isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
isnull(f1.Id,0) as Factor1Id, isnull(f1.Label,'') as Factor1Label, f1.Title as Factor1Title, f1.KeyInfo as Factor1KeyInfo, f1.Rationale as Factor1Rationale, isnull(fs1.Number,0) as Factor1Score, isnull(fs1.Description,'') as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, isnull(f2.Label,'') as Factor2Label, f2.Title as Factor2Title, f2.KeyInfo as Factor2KeyInfo, f2.Rationale as Factor2Rationale, isnull(fs2.Number,0) as Factor2Score, isnull(fs2.Description,'') as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, isnull(f3.Label,'') as Factor3Label, f3.Title as Factor3Title, f3.KeyInfo as Factor3KeyInfo, f3.Rationale as Factor3Rationale, isnull(fs3.Number,0) as Factor3Score, isnull(fs3.Description,'') as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
rsd1.Score, c.Color
from Recommendation rec
inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0 
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
left join RecommendationScoreDetail rsd1 on rec.Id = rsd1.RecommendationId and rsd1.CriteriaId = 1
left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 2 
left join Color c on c.Id = rsd1.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 1 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 1 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 1 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id 
ORDER BY l.Description, bow.Description, m.Method, rsd.score, s.CommonName";

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		public IList<Criteria24Entity> Criteria24Overview(int reportId)
		{
			DataTable dt = criteria24DataTable(reportId, true);

			List<Criteria24Entity> criteria2s = new List<Criteria24Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria24Entity criteria24 = new Criteria24Entity();
				Framework.EntityPopulate(criteria24, dr);
                criteria24.Factor4 = FactorColumnEntityPopulate(criteria24.Factor4, dr, 4);

				criteria2s.Add(criteria24);
			}
			return criteria2s;
		}

		public DataTable Criteria24ReportData(int reportId)
		{
			return criteria24DataTable(reportId, false);
		}

		private DataTable criteria24DataTable(int reportId, bool onlyShowLimitingFactor)
		{
			//grab lowest scored Bycatch for that Region/Method
			//Factor4 = Discard Rate
			string sql = @"Select Distinct rec.Id as RecId, l.Description as Location, bow.Description as BodyOfWater, m.Method,l.Id as LocationId, bow.Id as BodyOfWaterId, m.Id as MethodId,
isnull(l.Description,'') + ' ' + isnull(bow.Description,'') + ', ' + m.Method as LocationMethod,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, f4.KeyInfo as Factor4KeyInfo, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0) as Factor4CommentCount,
rsd.Score, c.Color
from Recommendation rec
inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0 
left join Location l on l.Id = rec.Locationid
left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
left join Method m on m.Id = rec.MethodId
left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 2 
left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.id = f4.FactorTypeId and ft4.CriteriaId = 2 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id
ORDER BY l.Description, bow.Description, m.Method, rsd.score";

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		#endregion

		#region Criteria 3

		public IList<Criteria3Entity> Criteria3Overview(int reportId)
		{
			DataTable dt = criteria3DataTable(reportId);

			List<Criteria3Entity> criteria3s = new List<Criteria3Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria3Entity criteria3 = new Criteria3Entity();
				Framework.EntityPopulate(criteria3, dr);
				criteria3s.Add(criteria3);
			}
			return criteria3s;
		}

		public DataTable Criteria3ReportData(int reportId)
		{
			return criteria3DataTable(reportId);
		}

		private DataTable criteria3DataTable(int reportId)
		{
			string sql = (@"select DISTINCT 
@0 as ReportId, l.Description as Location, bow.Description as BodyOfWater, m.Method, l.Id as LocationId, bow.Id as BodyOfWaterId, m.Id as MethodId,
rsd1.Score as Score1, c1.Color as Color1, Choose(rsd1.score+1,'Critical','Poor','Needs Improvement','Moderate','Well managed','Excellent') as Score1Description,
case when rsd2.Score is null then 'All Species Retained' else cast(rsd2.Score as varchar(20)) end as Score2, c2.Color as Color2, Choose(rsd2.score+1,'Critical','Poor','Needs Improvement','Moderate','Well managed','Excellent') as Score2Description,
rsd.Score, c.Color
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 3
	left join Color c on c.Id = rsd.ColorId
left join (RecommendationScoreDetail rsd1 left join Color c1 on c1.Id = rsd1.ColorId) on rsd1.RecommendationId = rec.Id and rsd1.CriteriaId = 5
left join (RecommendationScoreDetail rsd2 left join Color c2 on c2.Id = rsd2.ColorId) on rsd2.RecommendationId = rec.Id and rsd2.CriteriaId = 6
ORDER BY l.Description, bow.Description, m.Method");

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		public IList<Criteria31Entity> Criteria31Overview(int reportId)
		{
			DataTable dt = criteria31DataTable(reportId);

			List<Criteria31Entity> criteria31s = new List<Criteria31Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria31Entity criteria31 = new Criteria31Entity();
				Framework.EntityPopulate(criteria31, dr);
                criteria31.Factor1 = FactorColumnEntityPopulate(criteria31.Factor1, dr, 1);
                criteria31.Factor2 = FactorColumnEntityPopulate(criteria31.Factor2, dr, 2);
                criteria31.Factor3 = FactorColumnEntityPopulate(criteria31.Factor3, dr, 3);
                criteria31.Factor4 = FactorColumnEntityPopulate(criteria31.Factor4, dr, 4);
                criteria31.Factor5 = FactorColumnEntityPopulate(criteria31.Factor5, dr, 5);
                criteria31.Factor6 = FactorColumnEntityPopulate(criteria31.Factor6, dr, 6);
                criteria31.Factor7 = FactorColumnEntityPopulate(criteria31.Factor7, dr, 7);
                criteria31.Factor8 = FactorColumnEntityPopulate(criteria31.Factor8, dr, 8);
				criteria31s.Add(criteria31);
			}
			return criteria31s;
		}

		public DataTable Criteria31ReportData(int reportId)
		{
			return criteria31DataTable(reportId);
		}

		private DataTable criteria31DataTable(int reportId)
		{
			//remove distinct
			string sql = (@"select Distinct
l.Description as Location, bow.Description as BodyOfWater, m.Method, l.Id as LocationId, bow.Id as BodyOfWaterId, m.Id as MethodId,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, Cast(f1.KeyInfo as varchar(max)) as Factor1KeyInfo, cast(f1.Rationale as varchar(max)) as Factor1Rationale,    '3.1.0' as Factor1Score, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, Cast(f2.KeyInfo as varchar(max)) as Factor2KeyInfo, cast(f2.Rationale as varchar(max)) as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, Cast(f3.KeyInfo as varchar(max)) as Factor3KeyInfo, cast(f3.Rationale as varchar(max)) as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, Cast(f4.KeyInfo as varchar(max)) as Factor4KeyInfo, cast(f4.Rationale as varchar(max)) as Factor4Rationale, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor4CommentCount,
isnull(f5.Id,0) as Factor5Id, f5.Label as Factor5Label, f5.Title as Factor5Title, Cast(f5.KeyInfo as varchar(max)) as Factor5KeyInfo, cast(f5.Rationale as varchar(max)) as Factor5Rationale, fs5.Number as Factor5Score, fs5.Description as Factor5ScoreDescription, (select count(*) from FactorComment where FactorId = f5.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor5CommentCount,
isnull(f6.Id,0) as Factor6Id, f6.Label as Factor6Label, f6.Title as Factor6Title, Cast(f6.KeyInfo as varchar(max)) as Factor6KeyInfo, cast(f6.Rationale as varchar(max)) as Factor6Rationale, fs6.Number as Factor6Score, fs6.Description as Factor6ScoreDescription, (select count(*) from FactorComment where FactorId = f6.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor6CommentCount,
isnull(f7.Id,0) as Factor7Id, f7.Label as Factor7Label, f7.Title as Factor7Title, Cast(f7.KeyInfo as varchar(max)) as Factor7KeyInfo, cast(f7.Rationale as varchar(max)) as Factor7Rationale, fs7.Number as Factor7Score, fs7.Description as Factor7ScoreDescription, (select count(*) from FactorComment where FactorId = f7.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor7CommentCount,
isnull(f8.Id,0) as Factor8Id, f8.Label as Factor8Label, f8.Title as Factor8Title, Cast(f8.KeyInfo as varchar(max)) as Factor8KeyInfo, cast(f8.Rationale as varchar(max)) as Factor8Rationale, fs8.Number as Factor8Score, fs8.Description as Factor8ScoreDescription, (select count(*) from FactorComment where FactorId = f8.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor8CommentCount,
rsd.Score, c.Color
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 5
	left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 5 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 5 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 5 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.id = f4.FactorTypeId and ft4.CriteriaId = 5 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd5 
	inner join Factor f5 on f5.Id = frd5.FactorId
	left join FactorScore fs5 on fs5.Id = f5.FactorScoreId
	inner join FactorType ft5 on ft5.id = f5.FactorTypeId and ft5.CriteriaId = 5 and ft5.SortOrder = 5
	) on frd5.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd6 
	inner join Factor f6 on f6.Id = frd6.FactorId
	left join FactorScore fs6 on fs6.Id = f6.FactorScoreId
	inner join FactorType ft6 on ft6.id = f6.FactorTypeId and ft6.CriteriaId = 5 and ft6.SortOrder = 6
	) on frd6.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd7 
	inner join Factor f7 on f7.Id = frd7.FactorId
	left join FactorScore fs7 on fs7.Id = f7.FactorScoreId
	inner join FactorType ft7 on ft7.id = f7.FactorTypeId and ft7.CriteriaId = 5 and ft7.SortOrder = 7
	) on frd7.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd8 
	inner join Factor f8 on f8.Id = frd8.FactorId
	left join FactorScore fs8 on fs8.Id = f8.FactorScoreId
	inner join FactorType ft8 on ft8.id = f8.FactorTypeId and ft8.CriteriaId = 5 and ft8.SortOrder = 8
	) on frd8.recommendationId = rec.Id
ORDER BY l.Description, bow.Description, m.Method");

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}


		public IList<Criteria32Entity> Criteria32Overview(int reportId)
		{
			DataTable dt = criteria32DataTable(reportId);

			List<Criteria32Entity> criteria32s = new List<Criteria32Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria32Entity criteria32 = new Criteria32Entity();
				Framework.EntityPopulate(criteria32, dr);
                criteria32.Factor1 = FactorColumnEntityPopulate(criteria32.Factor1, dr, 1);
                criteria32.Factor2 = FactorColumnEntityPopulate(criteria32.Factor2, dr, 2);
                criteria32.Factor3 = FactorColumnEntityPopulate(criteria32.Factor3, dr, 3);
                criteria32.Factor4 = FactorColumnEntityPopulate(criteria32.Factor4, dr, 4);
                criteria32.Factor5 = FactorColumnEntityPopulate(criteria32.Factor5, dr, 5);
                criteria32.Factor6 = FactorColumnEntityPopulate(criteria32.Factor6, dr, 6);
				criteria32s.Add(criteria32);
			}
			return criteria32s;
		}

		public DataTable Criteria32ReportData(int reportId)
		{
			return criteria32DataTable(reportId);
		}

		private DataTable criteria32DataTable(int reportId)
		{
			//Removed distinct 
			string sql = (@"select DISTINCT
l.Description as Location, bow.Description as BodyOfWater, m.Method, l.Id as LocationId, bow.Id as BodyOfWaterId, m.Id as MethodId,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, '' as Factor1KeyInfo,                               cast(f1.Rationale as varchar(max)) as Factor1Rationale,    '3.2.0' as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, '' as Factor2KeyInfo,                               cast(f2.Rationale as varchar(max)) as Factor2Rationale,    '3.2.0' as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, Cast(f3.KeyInfo as varchar(max)) as Factor3KeyInfo, cast(f3.Rationale as varchar(max)) as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
isnull(f4.Id,0) as Factor4Id, f4.Label as Factor4Label, f4.Title as Factor4Title, Cast(f4.KeyInfo as varchar(max)) as Factor4KeyInfo, cast(f4.Rationale as varchar(max)) as Factor4Rationale, fs4.Number as Factor4Score, fs4.Description as Factor4ScoreDescription, (select count(*) from FactorComment where FactorId = f4.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor4CommentCount,
isnull(f5.Id,0) as Factor5Id, f5.Label as Factor5Label, f5.Title as Factor5Title, Cast(f5.KeyInfo as varchar(max)) as Factor5KeyInfo, cast(f5.Rationale as varchar(max)) as Factor5Rationale, fs5.Number as Factor5Score, fs5.Description as Factor5ScoreDescription, (select count(*) from FactorComment where FactorId = f5.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor5CommentCount,
isnull(f6.Id,0) as Factor6Id, f6.Label as Factor6Label, f6.Title as Factor6Title, Cast(f6.KeyInfo as varchar(max)) as Factor6KeyInfo, cast(f6.Rationale as varchar(max)) as Factor6Rationale, fs6.Number as Factor6Score, fs6.Description as Factor6ScoreDescription, (select count(*) from FactorComment where FactorId = f6.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor6CommentCount,
rsd.Score, c.Color
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 6
	left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 6 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 6 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 6 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd4 
	inner join Factor f4 on f4.Id = frd4.FactorId
	left join FactorScore fs4 on fs4.Id = f4.FactorScoreId
	inner join FactorType ft4 on ft4.id = f4.FactorTypeId and ft4.CriteriaId = 6 and ft4.SortOrder = 4
	) on frd4.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd5 
	inner join Factor f5 on f5.Id = frd5.FactorId
	left join FactorScore fs5 on fs5.Id = f5.FactorScoreId
	inner join FactorType ft5 on ft5.id = f5.FactorTypeId and ft5.CriteriaId = 6 and ft5.SortOrder = 5
	) on frd5.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd6 
	inner join Factor f6 on f6.Id = frd6.FactorId
	left join FactorScore fs6 on fs6.Id = f6.FactorScoreId
	inner join FactorType ft6 on ft6.id = f6.FactorTypeId and ft6.CriteriaId = 6 and ft6.SortOrder = 6
	) on frd6.recommendationId = rec.Id
ORDER BY l.Description, bow.Description, m.Method");

			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}

		#endregion

		#region Criteria 4

		public IList<Criteria4Entity> Criteria4Overview(int reportId)
		{
			DataTable dt = criteria4DataTable(reportId);

			List<Criteria4Entity> criteria4s = new List<Criteria4Entity>();
			foreach (DataRow dr in dt.Rows)
			{
				Criteria4Entity criteria4 = new Criteria4Entity();
                criteria4.Factor1 = FactorColumnEntityPopulate(criteria4.Factor1, dr, 1);
				criteria4.Factor2 =FactorColumnEntityPopulate(criteria4.Factor2, dr, 2);
				criteria4.Factor3 =FactorColumnEntityPopulate(criteria4.Factor3, dr, 3);
				Framework.EntityPopulate(criteria4, dr);
				criteria4s.Add(criteria4);
			}
			return criteria4s;
		}

		public DataTable Criteria4ReportData(int reportId)
		{
			return criteria4DataTable(reportId);
		}

		private DataTable criteria4DataTable(int reportId)
		{
			//removed distinct 
			string sql = (@"select DISTINCT
l.Description as Location, bow.Description as BodyOfWater, m.Method, l.Id as LocationId, bow.Id as BodyOfWaterId, m.Id as MethodId,
isnull(f1.Id,0) as Factor1Id, f1.Label as Factor1Label, f1.Title as Factor1Title, Cast(f1.KeyInfo as varchar(max)) as Factor1KeyInfo, cast(f1.Rationale as varchar(max)) as Factor1Rationale, fs1.Number as Factor1Score, fs1.Description as Factor1ScoreDescription, (select count(*) from FactorComment where FactorId = f1.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor1CommentCount,
isnull(f2.Id,0) as Factor2Id, f2.Label as Factor2Label, f2.Title as Factor2Title, Cast(f2.KeyInfo as varchar(max)) as Factor2KeyInfo, cast(f2.Rationale as varchar(max)) as Factor2Rationale, fs2.Number as Factor2Score, fs2.Description as Factor2ScoreDescription, (select count(*) from FactorComment where FactorId = f2.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor2CommentCount,
isnull(f3.Id,0) as Factor3Id, f3.Label as Factor3Label, f3.Title as Factor3Title, Cast(f3.KeyInfo as varchar(max)) as Factor3KeyInfo, cast(f3.Rationale as varchar(max)) as Factor3Rationale, fs3.Number as Factor3Score, fs3.Description as Factor3ScoreDescription, (select count(*) from FactorComment where FactorId = f3.Id and ParentFactorCommentId = 0 and isnull(Reviewed,0)=0) as Factor3CommentCount,
rsd.Score, c.Color
from Recommendation rec
	inner join Species s on s.Id = rec.SpeciesId and rec.ReportId = @0
	left join Location l on l.Id = rec.Locationid
	left join BodyOfWater bow on bow.Id = rec.BodyOfwaterId
	left join Method m on m.Id = rec.MethodId
	left join RecommendationScoreDetail rsd on rec.Id = rsd.RecommendationId and rsd.CriteriaId = 4
	left join Color c on c.Id = rsd.ColorId
left join
	(FactorRecommendationDetail frd1 
	inner join Factor f1 on f1.Id = frd1.FactorId
	left join FactorScore fs1 on fs1.Id = f1.FactorScoreId
	inner join FactorType ft1 on ft1.id = f1.FactorTypeId and ft1.CriteriaId = 4 and ft1.SortOrder = 1
	) on frd1.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd2 
	inner join Factor f2 on f2.Id = frd2.FactorId
	left join FactorScore fs2 on fs2.Id = f2.FactorScoreId
	inner join FactorType ft2 on ft2.id = f2.FactorTypeId and ft2.CriteriaId = 4 and ft2.SortOrder = 2
	) on frd2.recommendationId = rec.Id
left join
	(FactorRecommendationDetail frd3 
	inner join Factor f3 on f3.Id = frd3.FactorId
	left join FactorScore fs3 on fs3.Id = f3.FactorScoreId
	inner join FactorType ft3 on ft3.id = f3.FactorTypeId and ft3.CriteriaId = 4 and ft3.SortOrder = 3
	) on frd3.recommendationId = rec.Id
ORDER BY l.Description, bow.Description, m.Method");
			return DB.GetDataTable(new SQLBuilder(sql, reportId.ToString()));
		}

		#endregion

		#region Text blocks
		public enum TextBlock
		{
			//note: these titles need to correspond to Report table field names
			ExecSummary,
			Scope,
			SpeciesOverview,
			SupplementalInfo,
			Acknowledgements,
			ReviewSchedule,
            TextSummary,
            EcoCertification,

			ProductionStatistics,
			ImportanceToMarket,
			CommonMarketNames,
			ProductForms
		}

		public void TextBlockSet(int reportId, TextBlock block, string text)
		{
			text = text.Trim();
			if (text.StartsWith("<p>")) text = text.Substring(3, text.Length - 3);
			if (text.EndsWith("<p>")) text = text.Substring(0, text.Length - 3);
			text = text.Trim();

			DB.UpdateCmd(new SQLBuilder("Update Report set [" + block.ToString() + "] = @0 where Id = @1", text, reportId.ToString()));
			setLastUpdated(reportId);
		}

		public string TextBlockGet(int reportId, TextBlock block)
		{
			return DB.GetString(new SQLBuilder("Select [" + block.ToString() + "] From Report where Id = @0", reportId.ToString()));
		}

		public DataTable TextBlockGetDataTable(int reportId, TextBlock block)
		{
			//Translate the enum to a display-able title
			string blockTitle = block.ToString();
			switch (block)
			{
				case TextBlock.ExecSummary:
					blockTitle = "EXECUTIVE SUMMARY";
					break;
				case TextBlock.SpeciesOverview:
					blockTitle = "SPECIES OVERVIEW";
					break;
				case TextBlock.SupplementalInfo:
					blockTitle = "SUPPLEMENTAL INFO";
					break;
				case TextBlock.ReviewSchedule:
					blockTitle = "REVIEW SCHEDULE";
					break;
				case TextBlock.Scope:
					blockTitle = "SCOPE";
					break;
				case TextBlock.ProductionStatistics:
					blockTitle = "PRODUCTION STATISTICS";
					break;
				case TextBlock.ImportanceToMarket:
					blockTitle = "IMPORTANCE TO THE US/NORTH AMERICAN MARKET";
					break;
				case TextBlock.CommonMarketNames:
					blockTitle = "COMMON AND MARKET NAMES";
					break;
				case TextBlock.ProductForms:
					blockTitle = "PRIMARY PRODUCT FORMS";
					break;


			}
			return DB.GetDataTable(new SQLBuilder("Select '" + blockTitle + "' as Title, [" + block.ToString() + "] as Body From Report where Id = @0", reportId.ToString()));
		}

		/// <summary>
		/// Add an appendix.  UI dev is responsible for not double adding a letter.
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="letter">Exactly 1 digit (eg: A, B)</param>
		/// <param name="title"></param>
		/// <param name="text"></param>
		// local dev dB had no ReportAppendix table, we created one but did not add to rdbm so as not to step on (FK Constraints, whatever MBay may have in there...)
		public int AppendixAdd(int reportId, string title, string text)
		{
			//DB.InsertCmd(new SQLBuilder("Insert ReportAppendix(ReportId, Letter, Title, Appendix) values(@0,@1,@2,@3)", reportId.ToString(), letter, title, text));
			// SQ1 nm: using old dev db, adding missing tables and fields, end-run entitry framework.  to restore:
			//  - uncomment top commented line, remove all other lines
			setLastUpdated(reportId);
			return DB.InsertCmd(new SQLBuilder("Insert ReportAppendix(ReportId, Title, Appendix) values(@0,@1,@2)", reportId.ToString(), title, text), true);
		}

		public void AppendixRemove(int reportId, int appendixId)
		{
			//DB.DeleteCmd(new SQLBuilder("Delete from ReportAppendix where ReportId = @0 and Letter = @1", reportId.ToString(), letter));
			// SQ1 nm: using old dev db, adding missing tables and fields, end-run entitry framework.  to restore:
			//  - uncomment top commented line, remove all other lines
			DB.DeleteCmd(new SQLBuilder("Delete from ReportAppendix where Id = @0", appendixId.ToString()));
			setLastUpdated(reportId);
		}

		/// <summary>
		/// Given a reportId and letter, overwrite title and text (both must be passed, blanks will be persisted to db as blank)
		/// </summary>
		/// <param name="reportId"></param>
		/// <param name="letter"></param>
		/// <param name="title"></param>
		/// <param name="text"></param>
		public void AppendixUpdate(int reportId, int appendixId, string title, string text)
		{
			DB.UpdateCmd(new SQLBuilder("Update ReportAppendix set Title=@0, Appendix=@1 where Id=@2", title, text, appendixId.ToString()));
			setLastUpdated(reportId);
		}

		public IList<AppendixEntity> AppendixesGet(int reportId)
		{
			//DataTable dt = DB.GetDataTable(new SQLBuilder("Select Letter, Title, Appendix from ReportAppendix where ReportId = @0 order by Letter", reportId.ToString()));
			// SQ1 nm: using old dev db, adding missing tables and fields, end-run entity framework.  to restore:
			//  - uncomment top commented line
			//  - uncomment "//Framework.EntityPopulate(appendix, dr);"
			//  - remove next 3 lines (appendix.Letter -- appendix.Appendix).
			DataTable dt = DB.GetDataTable(new SQLBuilder("Select Id, Title, Appendix from ReportAppendix where ReportId = @0 order by Id", reportId.ToString()));

			List<AppendixEntity> appendices = new List<AppendixEntity>();
			foreach (DataRow dr in dt.Rows)
			{
				AppendixEntity appendix = new AppendixEntity();
				//Framework.EntityPopulate(appendix, dr);
				appendix.Id = dr.Field<int>("Id");
				appendix.Title = dr.Field<string>("Title");
				appendix.Appendix = dr.Field<string>("Appendix");
				appendices.Add(appendix);
			}
			return appendices;
		}

		public DataTable AppendixGetDataTable(int appendixId)
		{
			return DB.GetDataTable(new SQLBuilder("Select Id, Title, Appendix from ReportAppendix where Id = @0", appendixId.ToString()));
		}

		#endregion

		#region Report Status


		public enum ReportStatus
		{
			InProgress = 1,
			InternalReview1 = 3,
			ExternalPeerReview = 4,
			InternalReview2 = 5,
			Finalization = 6,
			Complete = 7
		}

		public List<string> UpdateReportStatus(int reportId, int status)
		{
			DB.UpdateCmd(new SQLBuilder("Update Report set ReportStatusId = @0 where Id = @1", status.ToString(), reportId.ToString()));

			List<string> emails = new List<string>();
			switch (status)
			{
				case (int)ReportStatus.InternalReview1:
				case (int)ReportStatus.ExternalPeerReview:
				case (int)ReportStatus.InternalReview2:
					emails.Add("sfwresearch@mbayaq.org");
					emails.Add("ehudson@mbayaq.org");
					break;
			}
			setLastUpdated(reportId);
			return emails;
		}

		public void ReviewerReviewed(int reportId, int userId)
		{
			DB.UpdateCmd(new SQLBuilder("update ReportReview set ReviewReceived = getdate() where ReportId = @0 and ReviewerUserId = @1", reportId.ToString(), userId.ToString()));
			setLastUpdated(reportId);
		}

		public void ReviewerUnReviewed(int reportId, int userId) //put in for testing purposes
		{
			DB.UpdateCmd(new SQLBuilder("update ReportReview set ReviewReceived = null where ReportId = @0 and ReviewerUserId = @1", reportId.ToString(), userId.ToString()));
			setLastUpdated(reportId);
		}

		#endregion

		#region Help

		public string GetHelp(string key)
		{
			return DB.GetString(new SQLBuilder("Select Content from Help where [Key] = @0", key));
		}

		#endregion
	}
}