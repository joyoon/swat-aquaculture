﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ART
{
	/// <summary>
	/// Generic Key/Value (int,string) entity for dropdown rowsources
	/// </summary>
	public class RowsourceEntity
	{
		public RowsourceEntity()
		{ }

		public RowsourceEntity(int key, string value)
		{
			Key = key;
			Value = value;
		}

		public int Key { get; set; }
		public string Value { get; set; }
	}

	public class UserEntity
	{
		public int Id { get; internal set; }
		public string DisplayName { get; set; }
		public bool IsSFWStaff { get; set; }
	}

	public class RecSummaryEntity
	{
		public int Id { get; set; }
		public string Species { get; set; }
		public string Location { get; set; }
		public string Method { get; set; }
		public string Scientific { get; set; }
	}

    public class FeedbackEntity
    {
        public int Id { get; set; }
        public DateTime DateSubmitted { get; set; }
        public int RecSummaryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Affiliation { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public IList<string> Documents { get; set; }
    }

	public class ReportEntity
	{
		public ReportEntity()
		{
			Permissions = new List<Permission>();
		}
        public ReportEntity(int id) : this()
        {
            this.Id = id;
        }

		public enum Permission
		{
			ExportReport,
			Comment,
			Answer,
			CompleteReview,
			UpdateStatus
		}

		public int Id { get; internal set; }
		public string Title { get; set; }
		public string ReportStatus { get; set; }
		public string LastUpdated { get; set; }
		public string AssignedTo { get; set; } //This goes away.

		public bool HasAccess(Permission permission)
		{
			return (Permissions.Contains(permission));
		}

		//set internally
		internal IList<Permission> Permissions { get; set; }
	}

	public class RecEntity
	{
		public int RecommendationId {get; set;}
		public string CommonName {get; set;}
		public string ScientificName {get; set;}
		public string Location {get; set;}
		public string BodyOfWater {get; set;}
		public string Method {get; set;}
		public string RecommendationType {get; set;}
        public int RecommendationTypeId { get; set; }
        public int SpeciesId { get; set; }
        public int LocationId { get; set; }
        public int BodyOfWaterId { get; set; }
        public int MethodId { get; set; }
	}

	public class FactorEntity
	{
		public int Id { get; set; }
		public int FactorTypeId { get; set; }
		public string FactorScore { get; set; }
		public string FactorScoreDescription { get; set; }
		public string Label { get; set; }
		public string Title { get; set; }
		public string KeyInfo { get; set; }
		public string Rationale { get; set; }
	}

	public class FactorTypeEntity
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public string Prefix { get; set; }
		public int CriteriaId { get; set; }
		public string SortOrder { get; set; }
	}

	public class ReferenceEntity 
	{
        public int ReportId { get; set; }
        public int ReferenceId { get; set; }
        public string ShortName { get; set; }
        //public string Title { get; set; }
        public string Bibliography { get; set; }
        public string Authors { get; set; }
        public string Year { get; set; }
        public string Path { get; set; }
        public bool isUrl { get; set; }
	}

    public class ImageEntity
    {
        public int ImageId { get; set; }
        public int ReportId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
    }

	public class FactorCommentEntity
	{
		public int FactorCommentId { get; set; }
		public int FactorId { get; set; }
		public int UserId { get; set; }
		public UserEntity User { get; set; }
		public DateTime DateCreated { get; set;  }
		public string Comment { get; set; }
		public int ParentFactorCommentId { get; set; }
		public bool Reviewed { get; set; }
		public string Path { get; set; }
	}

	public class FactorColumnEntity
	{
		public int FactorId { get; set; }
		public string FactorLabel { get; set; }
		public string FactorTitle { get; set; }
		public string FactorKeyInfo { get; set; }
		public string FactorScore { get; set; }
		public string FactorScoreDescription { get; set; }
		public int FactorCommentCount { get; set; }
        public string FactorDetailedRationale { get; set; }
	}

    public class EntityEqualityComparer : IEqualityComparer<FactorColumnEntity>
    {
        public bool Equals(FactorColumnEntity f1, FactorColumnEntity f2)
        {
            return f1.FactorId == f2.FactorId;
        }

        public int GetHashCode(FactorColumnEntity f)
        {
            return f.FactorId.GetHashCode();
        }
    }

    public class ReferenceComparer : IEqualityComparer<ReferenceEntity>
    {
        public bool Equals(ReferenceEntity r1, ReferenceEntity r2)
        {
            return r1.ReferenceId == r2.ReferenceId;
        }

        public int GetHashCode(ReferenceEntity r)
        {
            return r.ReferenceId.GetHashCode();
        }
    }

    public class ReviewFactorColumnEntity
    {
        public ReviewFactorColumnEntity()
        {
            this.RelevantRows = new List<string>();
        }
        public IList<string> RelevantRows { get; set; }
        public FactorColumnEntity FactorColumnEntity { get; set; }
    }

    public class GroupedFactorColumnEntity
    {
        public GroupedFactorColumnEntity()
        {
            Factor1Entities = new List<ReviewFactorColumnEntity>();
            Factor2Entities = new List<ReviewFactorColumnEntity>();
            Factor3Entities = new List<ReviewFactorColumnEntity>();
            Factor4Entities = new List<ReviewFactorColumnEntity>();
            Factor5Entities = new List<ReviewFactorColumnEntity>();
            Factor6Entities = new List<ReviewFactorColumnEntity>();
            Factor7Entities = new List<ReviewFactorColumnEntity>();
            Factor8Entities = new List<ReviewFactorColumnEntity>();
        }
        public string CommonName { get; set; }
        public IList<ReviewFactorColumnEntity> Factor1Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor2Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor3Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor4Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor5Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor6Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor7Entities { get; set; }
        public IList<ReviewFactorColumnEntity> Factor8Entities { get; set; }
    }

    public class CriteriaEntityBase
    {
        public FactorColumnEntity Factor1 { get; set; }
        public FactorColumnEntity Factor2 { get; set; }
        public FactorColumnEntity Factor3 { get; set; }
        public FactorColumnEntity Factor4 { get; set; }
        public FactorColumnEntity Factor5 { get; set; }
        public FactorColumnEntity Factor6 { get; set; }
        public FactorColumnEntity Factor7 { get; set; }
        public FactorColumnEntity Factor8 { get; set; }
        public string Location { get; set; }
        public string BodyOfWater { get; set; }
        public string Method { get; set; }
    }

	public class Criteria1Entity : CriteriaEntityBase
	{
		public int RecId {get; set;}
		public string CommonName {get; set;}
		public string ScientificName {get; set;}
		public string Score { get; set; }
		public string Color { get; set; }

		//TODO get rid of properties below
	}

	public class Criteria2Entity : CriteriaEntityBase
	{
		public int RecId { get; set; }
		public string TargetCommonName { get; set; }
		public string TargetScientificName { get; set; }
		public decimal HeaderSubScore { get; set; }
		public decimal HeaderDiscardRate { get; set; }
		public string HeaderDiscardRateDescription { get; set; }
		public decimal HeaderScore { get; set; }
		public string HeaderScoreColor { get; set; }
		public Decimal DiscardRate { get; set; }
		public string SubScore { get; set; } 

		public string CommonName { get; set; }
		public string ScientificName { get; set; }

		public bool IsLowest { get; set; } //Is this needed anymore?

	}

	public class Criteria21Entity : CriteriaEntityBase
	{
		public int RecId { get; set; }
		public string CommonName { get; set; }
		public string ScientificName { get; set; }
		public string Score { get; set; }
		public string Color { get; set; }
		public bool IsLowest { get; set; }

		//TODO get rid of properties below
		public int Factor1Id {get; set;}
		public string Factor1Label {get; set;}
		public string Factor1Title { get; set; }
		public string Factor1KeyInfo { get; set; }
		public string Factor1Score {get; set;}
		public string Factor1ScoreDescription {get; set;}
		public int Factor1CommentCount {get; set;}
		
		public int Factor2Id {get; set;}
		public string Factor2Label {get; set;}
		public string Factor2Title { get; set; }
		public string Factor2KeyInfo { get; set; }
		public string Factor2Score {get; set;}
		public string Factor2ScoreDescription {get; set;}
		public int Factor2CommentCount {get; set;}
		
		public int Factor3Id {get; set;}
		public string Factor3Label {get; set;}
		public string Factor3Title { get; set; }
		public string Factor3KeyInfo { get; set; }
		public string Factor3Score {get; set;}
		public string Factor3ScoreDescription {get; set;}
		public int Factor3CommentCount {get; set;}
	}

	public class Criteria24Entity : CriteriaEntityBase
	{
		public int LocationId { get; set; }
		public int BodyOfWaterId { get; set; }
		public int MethodId { get; set; }
		public string Score { get; set; }
		public string Color { get; set; }
		//public bool IsLowest { get; set; }
	}

	public class Criteria3Entity
	{
		public string Location { get; set; }
		public string BodyOfWater { get; set; }
		public string Method { get; set; }
		public string Score { get; set; }
		public string Color { get; set; }

		public int LocationId { get; set; }
		public int BodyOfWaterId { get; set; }
		public int MethodId { get; set; }

		public string Score1 { get; set; }
		public string Color1 { get; set; }
		public string Score1Description { get; set; }
		
		public string Score2 { get; set; }
		public string Color2 { get; set; }
		public string Score2Description { get; set; }
		
	}

	public class Criteria31Entity : CriteriaEntityBase
	{
		public string Score { get; set; }
		public string Color { get; set; }

		public int LocationId { get; set; }
		public int BodyOfWaterId { get; set; }
		public int MethodId { get; set; }

		//TODO get rid of properties below
		public int Factor1Id { get; set; }
		public string Factor1Label { get; set; }
		public string Factor1Title { get; set; }
		public string Factor1KeyInfo { get; set; }
		public string Factor1Score { get; set; }
		public string Factor1ScoreDescription { get; set; }
		public int Factor1CommentCount {get; set;}

		public int Factor2Id { get; set; }
		public string Factor2Label { get; set; }
		public string Factor2Title { get; set; }
		public string Factor2KeyInfo { get; set; }
		public string Factor2Score { get; set; }
		public string Factor2ScoreDescription { get; set; }
		public int Factor2CommentCount {get; set;}

		public int Factor3Id { get; set; }
		public string Factor3Label { get; set; }
		public string Factor3Title { get; set; }
		public string Factor3KeyInfo { get; set; }
		public string Factor3Score { get; set; }
		public string Factor3ScoreDescription { get; set; }
		public int Factor3CommentCount { get; set; }

		public int Factor4Id { get; set; }
		public string Factor4Label { get; set; }
		public string Factor4Title { get; set; }
		public string Factor4KeyInfo { get; set; }
		public string Factor4Score { get; set; }
		public string Factor4ScoreDescription { get; set; }
		public int Factor4CommentCount { get; set; }

		public int Factor5Id { get; set; }
		public string Factor5Label { get; set; }
		public string Factor5Title { get; set; }
		public string Factor5KeyInfo { get; set; }
		public string Factor5Score { get; set; }
		public string Factor5ScoreDescription { get; set; }
		public int Factor5CommentCount { get; set; }

		public int Factor6Id { get; set; }
		public string Factor6Label { get; set; }
		public string Factor6Title { get; set; }
		public string Factor6KeyInfo { get; set; }
		public string Factor6Score { get; set; }
		public string Factor6ScoreDescription { get; set; }
		public int Factor6CommentCount { get; set; }

		public int Factor7Id { get; set; }
		public string Factor7Label { get; set; }
		public string Factor7Title { get; set; }
		public string Factor7KeyInfo { get; set; }
		public string Factor7Score { get; set; }
		public string Factor7ScoreDescription { get; set; }
		public int Factor7CommentCount { get; set; }

		public int Factor8Id { get; set; }
		public string Factor8Label { get; set; }
		public string Factor8Title { get; set; }
		public string Factor8KeyInfo { get; set; }
		public string Factor8Score { get; set; }
		public string Factor8ScoreDescription { get; set; }
		public int Factor8CommentCount { get; set; }
	}

	public class Criteria32Entity : CriteriaEntityBase
	{
		public string Score { get; set; }
		public string Color { get; set; }

		public int LocationId { get; set; }
		public int BodyOfWaterId { get; set; }
		public int MethodId { get; set; }

		//TODO get rid of properties below

		public int Factor1Id { get; set; }
		public string Factor1Label { get; set; }
		public string Factor1Title { get; set; }
		//public string Factor1KeyInfo { get; set; }
		public string Factor1Score { get; set; }
		public string Factor1ScoreDescription { get; set; }
		public int Factor1CommentCount { get; set; }

		public int Factor2Id { get; set; }
		public string Factor2Label { get; set; }
		public string Factor2Title { get; set; }
		//public string Factor2KeyInfo { get; set; }
		public string Factor2Score { get; set; }
		public string Factor2ScoreDescription { get; set; }
		public int Factor2CommentCount { get; set; }

		public int Factor3Id { get; set; }
		public string Factor3Label { get; set; }
		public string Factor3Title { get; set; }
		public string Factor3KeyInfo { get; set; }
		public string Factor3Score { get; set; }
		public string Factor3ScoreDescription { get; set; }
		public int Factor3CommentCount { get; set; }

		public int Factor4Id { get; set; }
		public string Factor4Label { get; set; }
		public string Factor4Title { get; set; }
		public string Factor4KeyInfo { get; set; }
		public string Factor4Score { get; set; }
		public string Factor4ScoreDescription { get; set; }
		public int Factor4CommentCount { get; set; }

		public int Factor5Id { get; set; }
		public string Factor5Label { get; set; }
		public string Factor5Title { get; set; }
		public string Factor5KeyInfo { get; set; }
		public string Factor5Score { get; set; }
		public string Factor5ScoreDescription { get; set; }
		public int Factor5CommentCount { get; set; }

		public int Factor6Id { get; set; }
		public string Factor6Label { get; set; }
		public string Factor6Title { get; set; }
		public string Factor6KeyInfo { get; set; }
		public string Factor6Score { get; set; }
		public string Factor6ScoreDescription { get; set; }
		public int Factor6CommentCount { get; set; }
	}

	public class Criteria4Entity : CriteriaEntityBase
	{
		public string Score { get; set; }
		public string Color { get; set; }

		public int LocationId { get; set; }
		public int BodyOfWaterId { get; set; }
		public int MethodId { get; set; }

		//TODO get rid of properties below

		public int Factor1Id { get; set; }
		public string Factor1Label { get; set; }
		public string Factor1Title { get; set; }
		public string Factor1KeyInfo { get; set; }
		public string Factor1Score { get; set; }
		public string Factor1ScoreDescription { get; set; }
		public int Factor1CommentCount {get; set;}

		public int Factor2Id { get; set; }
		public string Factor2Label { get; set; }
		public string Factor2Title { get; set; }
		public string Factor2KeyInfo { get; set; }
		public string Factor2Score { get; set; }
		public string Factor2ScoreDescription { get; set; }
		public int Factor2CommentCount {get; set;}

		public int Factor3Id { get; set; }
		public string Factor3Label { get; set; }
		public string Factor3Title { get; set; }
		public string Factor3KeyInfo { get; set; }
		public string Factor3Score { get; set; }
		public string Factor3ScoreDescription { get; set; }
		public int Factor3CommentCount {get; set;}
	}

	public class AppendixEntity
	{
		public int Id {get; set;}
		public string Title {get; set;}
		public string Appendix { get; set; }
	}
}